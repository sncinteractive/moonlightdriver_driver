<?php
	require_once("./cURL.php");

	class SGISManager {
		var $consumer_key = "bbfa355704bc4a50975a";
		var $consumer_secret = "1b87c010301a4d2b82a3";
		var	$access_token = "";
		var $access_timeout = 0;

		var $openApiURL = "https://sgisapi.kostat.go.kr";
		var $getAccessTokenURL = "/OpenAPI3/auth/authentication.json";
		var $getAreaURL = "/OpenAPI3/boundary/hadmarea.geojson";
		var $getStageURL = "/OpenAPI3/addr/stage.json";
		var $transformURL = "/OpenAPI3/transformation/transcoord.json";

		function SGISManager() {

		}

		function checkTimeout() {
            $now = time() * 1000;

            if($this->access_timeout < $now)
                return false;

            return true;
        }

		function getAccessToken() {
			if(!empty($this->access_token) && checkTimeout())
				return $this->access_token;

    	    $cURL = new cURL();

			$url = $this->openApiURL . $this->getAccessTokenURL;
			$params = "consumer_key=" . $this->consumer_key . "&consumer_secret=" . $this->consumer_secret;

	        $result = $cURL->get($url . "?" . $params);
			$result_json = json_decode($result);
			$this->access_token = $result_json->result->accessToken;
			$this->access_timeout = $result_json->result->accessTimeout;
			
			return $this->access_token;
		}

		function getArea($_adm_cd, $_low_search) {
			$cURL = new cURL();

            $url = $this->openApiURL . $this->getAreaURL;
            $params = "accessToken=" . $this->access_token . "&year=2013&adm_cd=" . $_adm_cd;
            $result = $cURL->get($url ."?" . $params);

            return $result;
		}

		function getStage($_cd, $_pg_yn) {
			$cURL = new cURL();

            $url = $this->openApiURL . $this->getStageURL;

            $params = "accessToken=" . $this->access_token . "&cd=" . $_cd . "&pg_yn=" . $_pg_yn;
            $result = $cURL->get($url ."?" . $params);

            return $result;
		}
		
		function transformCoordinates($_src, $_dst, $_x, $_y) {
			$cURL = new cURL();
			
            $url = $this->openApiURL . $this->transformURL;
            $params = "accessToken=" . $this->access_token . "&src=" . $_src . "&dst=" . $_dst . "&posX=" . $_x . "&posY=" . $_y;
            $result = $cURL->get($url ."?" . $params);

            return $result;
		}
	}

?>
