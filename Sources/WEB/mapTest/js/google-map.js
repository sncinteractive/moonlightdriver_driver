var geoType = 0;
var max_call = {'gu' : -1, 'dong' : -1};
var min_call = {'gu' : -1, 'dong' : -1};
var coordinatesData = {"gu_data" : [], "dong_data": []};
var callHistoryData = {"gu_data" : [],  "dong_data": []};
var gmap = new google.maps.Map(document.getElementById('gmap'), {
	disableDefaultUI: true,
	keyboardShortcuts: false,
	draggable: false,
	disableDoubleClickZoom: true,
	scrollwheel: false,
	streetViewControl: false
});

function getFillColor(_amount) {
	var max = max_call.gu;
	var min = min_call.gu;
	
	if(geoType == 1) {
		max = max_call.dong;
		min = min_call.dong;
	}
	
	var step = parseInt((max - min) / 5);
	
	if(_amount <= min) {
		return "#00B0FF";
	} else if(_amount > min && _amount <= (min + step)) {
		return "#14E715";
	} else if(_amount > (min + step) && _amount <= (min + (step * 2))) {
		return "#FFEB3B";
	} else if(_amount > (min + (step * 2)) && _amount <= (max - step)) {
		return "#FF8F00";
	} else if(_amount > (max - step)) {
		return "#E51C23";
	}
}

var style = new ol.style.Style({
	fill: new ol.style.Fill({
		color: 'rgba(255, 255, 255, 9.0)'
	}),
	stroke: new ol.style.Stroke({
		color: '#319FD3',
		width: 1
	}),
	text: new ol.style.Text({
		font: '12px Calibri,sans-serif',
		fill: new ol.style.Fill({
			color: '#000'
		}),
		stroke: new ol.style.Stroke({
			color: '#fff',
			width: 3
		})
	})
});

var styles = [style];

var view = new ol.View({
	// make sure the view doesn't go beyond the 22 zoom levels of Google Maps
	maxZoom: 16,
	minZoom: 7
});

view.on('change:center', function() {
	var center = ol.proj.transform(view.getCenter(), 'EPSG:3857', 'EPSG:4326');
	gmap.setCenter(new google.maps.LatLng(center[1], center[0]));
});

view.on('change:resolution', function() {
	var zoom = view.getZoom();
	gmap.setZoom(zoom);
	
	if(zoom >= 13 && geoType == 0) {
		geoType = 1;
		changeLayerSource();
		showMinMax();
	} else if(zoom < 13 && geoType == 1) {
		geoType = 0;
		changeLayerSource();
		showMinMax();
	}
});

var markerFeature = new ol.Feature({
	geometry: new ol.geom.Point(ol.proj.transform([126.8320201, 37.65835990000001], 'EPSG:4326', 'EPSG:3857')),
	name: 'My Position'
});

var markerStyle = new ol.style.Style({
	image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
		anchor: [0.5, 46],
		anchorXUnits: 'fraction',
		anchorYUnits: 'pixels',
		opacity: 0.75,
		src: './img/icon.png'
	}))
});

markerFeature.setStyle(markerStyle);

var markerSource = new ol.source.Vector({
	features: [markerFeature]
});

var markerLayer = new ol.layer.Vector({
	source: markerSource
});

var vectorLayer = new ol.layer.Vector({
	source: new ol.source.Vector({
		features: (new ol.format.GeoJSON()).readFeatures(coordinatesData.gu_data, {'dataProjection': 'EPSG:4326', 'featureProjection' : 'EPSG:3857'})
	}),
	style: function(feature, resolution) {
		var amount = 0;
		var name = feature.get('name');
		
		if(geoType == 0) {
			if(callHistoryData.gu_data[name])
				amount = Math.floor(callHistoryData.gu_data[name]);
		} else if(geoType == 1) {
			if(callHistoryData.dong_data[name])
				amount = Math.floor(callHistoryData.dong_data[name]);
			
			name = name + '(' + comma(amount) + ')';
		}
		
		var fillColor = getFillColor(amount);
		
		style.getText().setText(resolution < 500 ? name : '');
		style.getFill().setColor(fillColor);
		
		return styles;
	}
});

var olMapDiv = document.getElementById('olmap');
var map = new ol.Map({
	layers: [vectorLayer, markerLayer],
	interactions: ol.interaction.defaults({
		altShiftDragRotate: false,
		dragPan: false,
		rotate: false
	}).extend([new ol.interaction.DragPan({kinetic: null})]),
	target: olMapDiv,
	view: view
});

function changeLayerSource() {
	var data = coordinatesData.gu_data;
	if(geoType == 1)
		data = coordinatesData.dong_data;
	
	vectorLayer.setSource(new ol.source.Vector({
		features: (new ol.format.GeoJSON()).readFeatures(data, {'dataProjection': 'EPSG:4326', 'featureProjection' : 'EPSG:3857'})
	}));
}

if (navigator.geolocation) {
	// GeoLocation을 이용해서 접속 위치를 얻어옵니다
	navigator.geolocation.getCurrentPosition(function(position) {
		var lat = position.coords.latitude; // 위도
		var lng = position.coords.longitude; // 경도
//		console.log('lat : ' + lat + ', lng : ' + lng);
		
		markerFeature.setGeometry(new ol.geom.Point(ol.proj.transform([lng, lat], 'EPSG:4326', 'EPSG:3857')));
	});
}

view.setCenter(ol.proj.transform([126.9926764185501, 37.52917408980922], 'EPSG:4326', 'EPSG:3857'));
view.setZoom(11);

olMapDiv.parentNode.removeChild(olMapDiv);
gmap.controls[google.maps.ControlPosition.TOP_LEFT].push(olMapDiv);

$('#selectType').on('change', function() {
//	view.setCenter(ol.proj.transform([126.9926764185501, 37.52917408980922], 'EPSG:4326', 'EPSG:3857'));
//	view.setZoom(11);

	var selected_type = $('#selectType').val();
	getCallHistoryData(selected_type);
});

$(document).ready(function() {
	getCoordinatesData($('#selectType').val());
});

function getCoordinatesData(_selected_type) {
	var url = "/test/a/getData.php";
	var params = {'type' : 'coordinates_data'};
	getAjaxData(url, params, function(_res) {
		coordinatesData = _res;
		getCallHistoryData(_selected_type);
	});
}

function getCallHistoryData(_selected_type) {
	var url = "/test/a/getData.php";
	var params = {'type' : 'call_history', 'select_type' : _selected_type};
	getAjaxData(url, params, function(_res) {
		callHistoryData = _res;
		getMinMaxCall(callHistoryData);
		changeLayerSource();
	});
}

function getAjaxData(_url, _params, _callback) {
	$.ajax({
		type: "POST",
		url: _url,
		data: _params,
		dataType: 'json',
		success: function(response) {
			_callback(response);
		},
		error: function() {
			alert('통신 상태를 확인해주세요.');
		}
	});
}

function getMinMaxCall(_data) {
	min_call.gu = -1;
	min_call.dong = -1;
	max_call.gu = -1;
	max_call.dong = -1;
	
	$.each(_data.gu_data, function(_key, _val) {
		if(max_call.gu == -1) {
			max_call.gu = _val;
		} else if(max_call.gu < _val) {
			max_call.gu = _val;
		}
		
		if(min_call.gu == -1) {
			min_call.gu = _val;
		} else if(min_call.gu > _val) {
			min_call.gu = _val;
		}
	});
	
	$.each(_data.dong_data, function(_key, _val) {
		if(max_call.dong == -1) {
			max_call.dong = _val;
		} else if(max_call.dong < _val) {
			max_call.dong = _val;
		}
		
		if(min_call.dong == -1) {
			min_call.dong = _val;
		} else if(min_call.dong > _val) {
			min_call.dong = _val;
		}
	});
	
	showMinMax();
}

function showMinMax() {
	if(geoType == 0) {
		$('#txtMin').html(comma(Math.floor(min_call.gu)));
		$('#txtMax').html(comma(Math.floor(max_call.gu)));
	} else if(geoType == 1) {
		$('#txtMin').html(comma(Math.floor(min_call.dong)));
		$('#txtMax').html(comma(Math.floor(max_call.dong)));
	}
}

//콤마찍기
function comma(_value) {
	_value = String(_value);
	return _value.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
}

//콤마풀기
function uncomma(_value) {
	_value = String(_value);
	return _value.replace(/[^\d]+/g, '');
}
