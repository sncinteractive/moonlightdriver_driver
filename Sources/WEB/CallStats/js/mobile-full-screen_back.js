var geoDataSido = JSON.parse(sidoGeoData);
var geoDataDong = JSON.parse(dongGeoData);
var geoType = 0;

function getFillColor(_amount) {
	if(_amount <= 400) {
		return "#00B0FF";
	} else if(_amount > 400 && _amount <= 800) {
		return "#14E715";
	} else if(_amount > 800 && _amount <= 1200) {
		return "#FFEB3B";
	} else if(_amount > 1200 && _amount <= 1600) {
		return "#FF8F00";
	} else if(_amount > 1600) {
		return "#E51C23";
	}
}

var vectorLayer = new ol.layer.Vector({
	source: new ol.source.Vector({
		features: (new ol.format.GeoJSON()).readFeatures(geoDataSido, {'dataProjection': 'EPSG:5179', 'featureProjection' : 'EPSG:3857'})
	}),
	style: function(feature, resolution) {
		var amount = Math.floor((Math.random() * 2000) + 0);
		var fillColor = getFillColor(amount);
		var adm_name = feature.get('adm_nm');
		
		if(geoType == 1) {
			adm_name = adm_name + '(' + amount + ')';
		}
		
		style.getText().setText(resolution < 500 ? adm_name : '');
		style.getFill().setColor(fillColor);
		
		return styles;
	}
});

var style = new ol.style.Style({
	fill: new ol.style.Fill({
		color: 'rgba(255, 255, 255, 9.0)'
	}),
	stroke: new ol.style.Stroke({
		color: '#319FD3',
		width: 1
	}),
	text: new ol.style.Text({
		font: '12px Calibri,sans-serif',
		fill: new ol.style.Fill({
			color: '#000'
		}),
		stroke: new ol.style.Stroke({
			color: '#fff',
			width: 3
		})
	})
});

var styles = [style];

var view = new ol.View({
	projection: new ol.proj.Projection({
		code: 'EPSG:3857',
		units: 'm'
	}),
	center: ol.proj.transform([953427, 1950827], "EPSG:5179", "EPSG:3857"),
	zoom: 11
});

var markerFeature = new ol.Feature({
	geometry: new ol.geom.Point(ol.proj.transform([126.8320201, 37.65835990000001], 'EPSG:4326', 'EPSG:3857')),
	name: 'My Position'
});

var markerStyle = new ol.style.Style({
	image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
		anchor: [0.5, 46],
		anchorXUnits: 'fraction',
		anchorYUnits: 'pixels',
		opacity: 0.75,
		src: './img/icon.png'
	}))
});

markerFeature.setStyle(markerStyle);

var markerSource = new ol.source.Vector({
	features: [markerFeature]
});

var markerLayer = new ol.layer.Vector({
	source: markerSource
});

var attribution = new ol.control.Attribution({
	collapsible: false
});

var map = new ol.Map({
	layers: [
		new ol.layer.Tile({
			source: new ol.source.OSM()
		}),
		vectorLayer,
		markerLayer
	],
	controls: ol.control.defaults({ attribution: false }).extend([attribution]),
	target: 'map',
	view: view
});

function checkSize() {
  var small = map.getSize()[0] < 600;
  attribution.setCollapsible(small);
  attribution.setCollapsed(small);
}

window.addEventListener('resize', checkSize);
checkSize();

map.getView().on('propertychange', function(e) {
	switch (e.key) {
		case 'resolution':
			var resolution = this.getResolution();
			if(resolution < 38)
				geoType = 1;
			else
				geoType = 0;
			
			changeLayerSource();
			
		break;
	}
});

function changeLayerSource() {
	var geoData = geoDataSido;
	
	if(geoType == 1)
		geoData = geoDataDong;
	
	vectorLayer.setSource(new ol.source.Vector({
		features: (new ol.format.GeoJSON()).readFeatures(geoData, {'dataProjection': 'EPSG:5179', 'featureProjection' : 'EPSG:3857'})
	}));
}

if (navigator.geolocation) {
	// GeoLocation을 이용해서 접속 위치를 얻어옵니다
	navigator.geolocation.getCurrentPosition(function(position) {
		var lat = position.coords.latitude; // 위도
		var lng = position.coords.longitude; // 경도
		console.log('lat : ' + lat + ', lng : ' + lng);
		
		markerFeature.setGeometry(new ol.geom.Point(ol.proj.transform([lng, lat], 'EPSG:4326', 'EPSG:3857')));
	});
}

/*
var geolocation = new ol.Geolocation({
	projection: view.getProjection(),
	tracking: true
});

geolocation.once('change:position', function() {
	view.setCenter(geolocation.getPosition());
	view.setResolution(2.388657133911758);
});
*/
// Use FastClick to eliminate the 300ms delay between a physical tap
// and the firing of a click event on mobile browsers.
// See http://updates.html5rocks.com/2013/12/300ms-tap-delay-gone-away
// for more information.
document.addEventListener('DOMContentLoaded', function() {
	FastClick.attach(document.body);
});
