<?php
	header("Content-Type:text/html; charset=utf-8");
	require_once("../db.php");
	
	$busnews = $db->busnews;
	$shuttleInfo = $db->shuttleinfos;
	$shuttlePoints = $db->shuttlepoints;
	
	$shuttleInfo->drop();
	$shuttlePoints->drop();
	
	$cur = $busnews->find();
	foreach($cur as $row) {
// 		echo "mShuttleColorList.put(\"" . $row['_id'] . "\", \"#" . rgbToHex(rand(0, 255), rand(0, 255), rand(0, 255)) . "\");<br/>";
		$shuttle_document = array(
			"_id" => $row['_id'],
			"name" => $row['name'],
			"category" => $row['category'],
			"desc" => $row['desc'],
			"phone" => isset($row["phone"]) ? $row["phone"] : "",
			"firstTime" => isset($row["firstTime"]) ? $row["firstTime"] : "",
			"lastTime" => isset($row["lastTime"]) ? $row["lastTime"] : "",
			"term" => isset($row["term"]) ? $row["term"] : "",
			"totalTime" => isset($row["totalTime"]) ? $row["totalTime"] : ""
		);
		
		if(!empty($row["comment"])) {
			$shuttle_document["comment"] = array();
			foreach ($row["comment"] as $comment) {
				$commentsArray = array();
				$commentsArray['_id'] = $comment['_id'];
				$commentsArray['message'] = $comment['message'];
				$commentsArray['nickName'] = $comment['nickname'];
				$commentsArray['createdAt'] = $comment['createdAt'];
			
				$shuttle_document["comment"][] = $commentsArray;
			}
		}

		$shuttleInfo->save($shuttle_document);
		$shuttle_id = $shuttle_document['_id'];
		
		$time_per_miter = 0;
		if(!empty($row["totalTime"])) {
			$total_dist = getTotalDistance($row['lists']);
// 			$total_dist = round(($total_dist / 1000), 2);	// m단위를 km로 변환
// 			echo "" . $shuttle_id . " : " . $total_dist . "m / " . ($row["totalTime"] * 60) . "sec => " . (($row["totalTime"] * 60) / $total_dist) . "sec/m<br/>";
			$time_per_miter = (($row["totalTime"] * 60) / $total_dist);
		}
		
		$prev_first_time = $row["firstTime"] . ":00";
		$prev_last_time = $row["lastTime"] . ":00";
		$prev_lat = null;
		$prev_lng = null;
		
		foreach($row['lists'] as $lists) {
			$increase_time = 0;
			if($prev_lat != null) {
				$dist = get_distance($prev_lat, $prev_lng, $lists['lat'], $lists['lng']);
				$increase_time = $dist * $time_per_miter;
			}
			
			$first_time = calculateTime($prev_first_time, $increase_time);
			$last_time = calculateTime($prev_last_time, $increase_time);
			$arsId = (str_replace(".", "", sprintf("%2.10f", $lists["lat"])) . str_replace(".", "", sprintf("%3.10f", $lists["lng"])));
			$shuttle_points_doc = array(
				"busId" => $shuttle_id . "",
				"index" => $lists['index'],
				"text" => $lists['text'],
				"arsId" => $arsId,
				"firstTime" => substr($first_time, 0, 5),
				"lastTime" => substr($last_time, 0, 5),
				"locations" => array("type" => "Point", "coordinates" => array((double)$lists['lng'], (double)$lists['lat'])),
				"createdAt" => $lists['createdAt']
			);
			
			$shuttlePoints->save($shuttle_points_doc);

			$prev_first_time = $first_time;
			$prev_last_time = $last_time;
			$prev_lat = $lists['lat'];
			$prev_lng = $lists['lng'];
		}
	}
	
	function rgbToHex($_r, $_g, $_b) {
		return toHex($_r) . toHex($_g) . toHex($_b);
	}

	function toHex($_n) {
		$str = "0123456789ABCDEF";
		$min = min(array($_n, 255));
		$_n = max(array(0, $min));
		
		return substr($str, (($_n - $_n % 16) / 16), 1) . substr($str, ($_n % 16), 1);
	}
	
	function getTotalDistance($_lists) {
		$tot_dist = 0;
		$prev_lat = null;
		$prev_lng = null;
		foreach($_lists as $r) {
			if($prev_lat != null) {
				$tot_dist += get_distance($prev_lat, $prev_lng, $r["lat"], $r["lng"]);
// 				echo "dist[(" . $prev_lat . ", " . $prev_lng . ") to (" . $r["lat"] . ", " . $r["lng"] . ")] : " . $tot_dist . "<br/>";
			}
			
			$prev_lat = $r["lat"];
			$prev_lng = $r["lng"];
		}
		
		return $tot_dist;
	}
	
	function calculateTime($_orgTime, $_incrTime) {
		$orgTimeArray = split(":", $_orgTime);
		$org_hours = intval($orgTimeArray[0]);
		$org_minutes = intval($orgTimeArray[1]);
		$org_seconds = intval($orgTimeArray[2]) + $_incrTime;
		
		if($org_seconds >= 60) {
			$org_seconds = $org_seconds % 60;
			$org_minutes = $org_minutes + 1;
		}
		
		if($org_minutes >= 60) {
			$org_minutes = $org_minutes % 60;
			$org_hours = $org_hours + 1;
		}
		
		if($org_hours >= 24) {
			$org_hours = $org_hours % 24;
		}
		
		if($org_minutes < 10) {
			$org_minutes = '0' . $org_minutes;
		}
		
		if($org_hours < 10) {
			$org_hours = '0' . $org_hours;
		}
		
		if($org_seconds < 10) {
			$org_seconds = '0' . $org_seconds;
		}
		
		return $org_hours . ":" . $org_minutes . ":" . $org_seconds;
	}
	
	function get_distance($lat1, $lon1, $lat2, $lon2) {
		/* WGS84 stuff */
		$a = 6378137;
		$b = 6356752.3142;
		$f = 1/298.257223563;
		/* end of WGS84 stuff */
	
		$L = deg2rad($lon2-$lon1);
		$U1 = atan((1-$f) * tan(deg2rad($lat1)));
		$U2 = atan((1-$f) * tan(deg2rad($lat2)));
		$sinU1 = sin($U1);
		$cosU1 = cos($U1);
		$sinU2 = sin($U2);
		$cosU2 = cos($U2);
	
		$lambda = $L;
		$lambdaP = 2*pi();
		$iterLimit = 20;
		while ((abs($lambda-$lambdaP) > pow(10, -12)) && ($iterLimit-- > 0)) {
			$sinLambda = sin($lambda);
			$cosLambda = cos($lambda);
			$sinSigma = sqrt(($cosU2*$sinLambda) * ($cosU2*$sinLambda) + ($cosU1*$sinU2-$sinU1*$cosU2*$cosLambda) * ($cosU1*$sinU2-$sinU1*$cosU2*$cosLambda));
	
			if ($sinSigma == 0) {
				return 0;
			}
	
			$cosSigma   = $sinU1*$sinU2 + $cosU1*$cosU2*$cosLambda;
			$sigma      = atan2($sinSigma, $cosSigma);
			$sinAlpha   = $cosU1 * $cosU2 * $sinLambda / $sinSigma;
			$cosSqAlpha = 1 - $sinAlpha*$sinAlpha;
			$cos2SigmaM = $cosSigma - 2*$sinU1*$sinU2/$cosSqAlpha;
	
			if (is_nan($cos2SigmaM)) {
				$cos2SigmaM = 0;
			}
	
			$C = $f/16*$cosSqAlpha*(4+$f*(4-3*$cosSqAlpha));
			$lambdaP = $lambda;
			$lambda = $L + (1-$C) * $f * $sinAlpha *($sigma + $C*$sinSigma*($cos2SigmaM+$C*$cosSigma*(-1+2*$cos2SigmaM*$cos2SigmaM)));
		}
	
		if ($iterLimit == 0) {
			// formula failed to converge
			return NaN;
		}
	
		$uSq = $cosSqAlpha * ($a*$a - $b*$b) / ($b*$b);
		$A = 1 + $uSq/16384*(4096+$uSq*(-768+$uSq*(320-175*$uSq)));
		$B = $uSq/1024 * (256+$uSq*(-128+$uSq*(74-47*$uSq)));
		$deltaSigma = $B*$sinSigma*($cos2SigmaM+$B/4*($cosSigma*(-1+2*$cos2SigmaM*$cos2SigmaM)- $B/6*$cos2SigmaM*(-3+4*$sinSigma*$sinSigma)*(-3+4*$cos2SigmaM*$cos2SigmaM)));
	
// 		return round($b*$A*($sigma-$deltaSigma) / 1000);
		return round($b*$A*($sigma-$deltaSigma));	// m단위로 표시
	
	
		/* sphere way */
		$distance = rad2deg(acos(sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($lon1 - $lon2))));
	
		$distance *= 111.18957696; // Convert to km
	
		return $distance;
	}
	
// 	$shuttlePoints->ensureIndex(array('locations.coordinates' => '2dsphere'));
/*
	$shuttlePoints->ensureIndex(array('locations.coordinates' => '2dsphere'));
	$query = array(
		"geoNear" => "shuttlepoints",
		"distanceField" => "dist.calculated",
		"includeLocs" => "dist.location",
		"near" => array("type" => "Point", "coordinates" => [126.7754522, 37.7709692]),
		"spherical" => true,
		"maxDistance" => 5000
	);
	$result = $db->command($query);
	echo "<pre>";
	print_r(count($result["results"]));
	print_r($result);
*/
?>
