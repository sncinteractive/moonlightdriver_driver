<?php
	header("Content-Type:text/html; charset=utf-8");
	require_once("./SGISManager.php");
	require_once("../../db.php");
	
	function changeDongName($_dongName) {
		$replaced_name = "";
			
		if($_dongName == "중계2·3동") {
			$replaced_name = "중계동";
		} else if($_dongName == "상도1동") {
			$replaced_name = $_dongName;
		} else if($_dongName == "금호1가동") {
			$replaced_name = "금호동1가";
		} else if($_dongName == "금호2·3가동") {
			$replaced_name = "금호동2가";
		} else if($_dongName == "금호4가동") {
			$replaced_name = "금호동4가";
		} else if($_dongName == "성수1가1동" || $_dongName == "성수1가2동") {
			$replaced_name = "성수동1가";
		} else if($_dongName == "성수2가1동" || $_dongName == "성수2가3동") {
			$replaced_name = "성수동2가";
		} else if($_dongName == "여의동") {
			$replaced_name = "여의도동";
		} else if($_dongName == "용산2가동") {
			$replaced_name = "용산동2가";
		} else if($_dongName == "종로1·2·3·4가동") {
			$replaced_name = "종로동1·2·3·4가";
		} else if($_dongName == "종로5·6가동") {
			$replaced_name = "종로동5·6가";
		} else {
			$replaced_name = preg_replace("/[0-9]동/s", "동", $_dongName);
		}
		
		return $replaced_name;
	}
	
	$collection = $db->coordinates;

	$coord = $collection->find()->sort(array('si_name' => 1, 'gu_name' => 1, 'dong_name' => 1));
	foreach($coord as $row) {
		if(!empty($row['dong_name']))
			echo $row['si_name'] . ' '. $row['gu_name'] . ' '. $row['dong_name'] . "</br>";
	}
	exit;

	$collection->drop();
	
	$SGISManager = new SGISManager();
	$access_token = $SGISManager->getAccessToken();

// 구별 좌표 조회
	$stage = $SGISManager->getArea(11, 0);
	$stage_json = json_decode($stage);

	foreach($stage_json->features as $feature) {
		$gucode = $feature->properties->adm_cd;
		$siguName = split(' ', $feature->properties->adm_nm);

		$doc = array(
			"si_name" => $siguName[0],
			"gu_name" => $siguName[1],
			"dong_name" => '',
			"areaType" => "gu",
			"type" => "Feature",
			"geometry" => array(
				"type" => "Polygon",
				"coordinates" => $feature->geometry->coordinates
			),
			"properties" => array(
				"name" => $siguName[1]
			)
		);
		
		$collection->insert($doc);
		
// 동별 좌표 조회
		$ret = $SGISManager->getArea($gucode, 0);
		$ret_json = json_decode($ret);
		
		foreach($ret_json->features as $feature) {
			$dongName = split(' ', $feature->properties->adm_nm);
			$changedName = changeDongName($dongName[2]);

			$doc = array(
				"si_name" => $dongName[0],
				"gu_name" => $dongName[1],
				"dong_name" => $changedName,
				"areaType" => "dong",
				"type" => "Feature",
				"geometry" => array(
					"type" => "Polygon",
					"coordinates" => $feature->geometry->coordinates
				),
				"properties" => array(
					"name" => $changedName
				)
			);
			
			$collection->insert($doc);
		}
	}

echo $collection->count();
exit;

?>
