<?php
	header("Content-Type:text/html; charset=utf-8");
	
	require_once ("./reader.php");
	require_once ("./db.php");
	
	function dir_list($dir)
	{
		if ($dir[strlen($dir)-1] != '/') 
			$dir .= '/';
		
		if (!is_dir($dir)) 
			return array();
		
		$dir_handle  = opendir($dir);
		$dir_objects = array();
		
		while ($object = readdir($dir_handle)) {
			if (!in_array($object, array('.','..'))) {
				$dir_objects[] = $object;
			}
		}
		
		sort($dir_objects);
		
		return $dir_objects;
	}
	
	$collection = $db->callhistories;

// 	$coord = $collection->find(array("date" => "201505"))->sort(array('si_name' => 1, 'gu_name' => 1, 'dong_name' => 1));
// 	foreach($coord as $row) {
// 		echo $row['si_name'] . ' '. $row['gu_name'] . ' '. $row['dong_name'] . ' '. $row['call_count'] . "\n";
// 	}
// 	exit;

	$collection->drop();
	
	$path = "/moonlight/WEB/admin/test/xlsData/";
	$file_list = dir_list($path);
	foreach($file_list as $file) {
		$data = new Spreadsheet_Excel_Reader();
		$data->setOutputEncoding('UTF-8');
		$data->read($path . $file);
		error_reporting(E_ALL ^ E_NOTICE);

// 	echo "<pre>";
// 	print_r($data->sheets[0]['cells']);
// 	exit;

		$i = 0;
		foreach($data->sheets[0]['cells'] as $rows) {
			if($i > 0) {
				$doc = array("date" => $rows[1], "si_name" => $rows[2], "gu_name" => $rows[3], "dong_name" => $rows[4], "call_count" => intval($rows[5]));
				$collection->insert($doc);
			}
			
			$i++;
		}
		
		echo $file . ' : ' . $collection->count() . "\n";
	}
?>