<?php
	header("Content-Type: text/html; charset=UTF-8");
	session_start();
	session_destroy();
	session_unset();
	
	echo "<script>alert('로그아웃 되었습니다.')</script>";
	header("Location: /admin/");
	
	die();
?>