<?php
	header("Content-Type: text/html; charset=UTF-8");
	session_start();
	
	include_once($_SERVER["CONTEXT_DOCUMENT_ROOT"] . "/db.php");
	include_once($_SERVER["CONTEXT_DOCUMENT_ROOT"] . "/functions.php");
	
	$id = isset($_POST["id"]) ? $_POST["id"] : "";
	$pw = isset($_POST["pw"]) ? $_POST["pw"] : "";
	
	if(!empty($id) && !empty($pw)) {
		$admin = $db->admin;
		
		$pw = md5($pw);
		
		if(!login($admin, $id, $pw)) {
			echo "<script>";
			echo "alert('아이디 또는 비밀번호가 틀립니다.');";
			echo "location.href = '/admin';";
			echo "</script>";
			exit;
		}
	}
	
	header("Location: http://115.68.104.30/admin");
?>
