var AJAX_URL = "/admin/a/ajax.php";

$( window ).resize(function() {
	$('.widget .widget-body.no-padding').css('height', $(window).height() - 210);
});

function Pagination(_url, totalPages, nowPage, limit) {
	var url = "#/" + _url;
	$(".pagination").empty();
	$(".pagination").html("<li class='active'><a href='" + url + "'>PREV</a>");
	var currentPage = lowerLimit = upperLimit = Math.min(nowPage, totalPages);
	
	for (var b = 1; b < limit && b < totalPages;) {
		if (lowerLimit > 1 ) { 
			lowerLimit--; 
			b++; 
		}
		
		if (b < limit && upperLimit < totalPages) { 
			upperLimit++; 
			b++; 
		}
	}
	
	for (var i = lowerLimit; i <= upperLimit; i++) {
		if (i == currentPage) {
			$(".pagination").append("<li class='active'><a href='" + url + "'>" + i + "</a></li>");
		} else {
			$(".pagination").append("<li><a href='" + url + "'>" + i + "</a></li>");
		}
	}

	$(".pagination").append("<li class='active'><a href='" + url + "'>NEXT</a></li>");
	$(".pagination > li > a").click(function(){
		var id = $(this).parents(".tab-pane").attr("id");
		var page = $(this).text();
		
		if(page == "PREV") {
			if(lowerLimit-1 < 1) {
				page = 1;
			}
			
			page = lowerLimit-1;
		} else if(page == "NEXT") {
			page = upperLimit+1;
		}
	});
}

function sendAjaxPost(_params, _callback) {
	$.post("/admin/a/ajax.php", _params, function(_ret_data) {
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			_callback(ret_data.data);
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.message);
			location.href = "/admin";
		} else {
			alert(ret_data.message);
		}
	}, "text");
}

function getDateTimeString(_date) {
	var dateTimeString = "";
	var date = new Date(_date);
	
	var fullYear = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	var hours = date.getHours();
	hours = hours < 10 ? '0' + hours : hours;
	var minutes = date.getMinutes();
	minutes = minutes < 10 ? '0' + minutes : minutes;
	var seconds = date.getSeconds();
	seconds = seconds < 10 ? '0' + seconds : seconds;
	
	dateTimeString = fullYear + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds;
	
	return dateTimeString;
}
