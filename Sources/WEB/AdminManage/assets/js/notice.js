$(document).ready(function() {
	setTimeout(function() {
		$('.widget .widget-body.no-padding').css('height', $(window).height() - 210);
		$("tbody > tr").click(function(){
			var notice_id = $(this).attr("data-id");
			var params = {};

			params.type = "get_notice_info";
			params.notice_id = notice_id;
			
			sendAjaxPost(params, function(_ret_data) {
				showNoticePopup(_ret_data);
				
				$('#btnEdit').on('click', function() {
					editNotice(_ret_data.noticeId);
				});
			});
			
//			$.post("/admin/a/ajax.php", params, function(_data) {
//				var ret_data = JSON.parse(_data);
//				
//				if(ret_data.result == "OK") {
//					showNoticePopup(ret_data.data);
//					
//					$('#btnEdit').on('click', function() {
//						editNotice(ret_data.data.noticeId);
//					});
//				} else if(ret_data.result == "NOT_LOGIN") {
//					location.href = "/admin";
//				} else {
//					alert(ret_data.message);
//				}
//			}, "text");
		});
	}, 1);
});

function showNoticePopup(_data) {
	$("#first_modal_title").text("공지사항 상세보기");
	$("#first_modal_content").html(getNoticeContentsHtml());
	$("#first_modal_footer").html(getNoticeFooterHtml());
	
	$("#title").text(_data.title);
	$("#contents").text(_data.contents);
	$("#image").val(_data.image);
	$("#link").val(_data.link);
	$("#start_date").val(_data.startDate);
	$("#end_date").val(_data.endDate);
	
	$("#first_modal").modal('show');
}

function getNoticeContentsHtml() {
	var html = "";
	
	html += "<div class='row'>";
	html += "<form class='form-horizontal'>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-2 control-label' for='title'>제목</label>";
	html += "<div class='col-sm-9'><textarea class='form-control' id='title' rows='3'/></div>";
	html += "</div>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-2 control-label' for='contents'>내용</label>";
	html += "<div class='col-sm-9'><textarea class='form-control' id='contents' rows='20'/></div>";
	html += "</div>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-2 control-label' for='image'>이미지</label>";
	html += "<div class='col-sm-9'><input type='text' class='form-control' id='image'></div>";
	html += "</div>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-2 control-label' for='link'>링크</label>";
	html += "<div class='col-sm-9'><input type='text' class='form-control' id='link'></div>";
	html += "</div>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-2 control-label' for='start_date'>시작일자</label>";
	html += "<div class='col-sm-9'>";
	html += "<input type='text' class='form-control' id='start_date'>";
	html += "<p class='help-block'>e.g. 2015-12-31 12:50:30</p>";
	html += "</div>";
	html += "</div>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-2 control-label' for='end_date'>종료일자</label>";
	html += "<div class='col-sm-9'>";
	html += "<input type='text' class='form-control' id='end_date'>";
	html += "<p class='help-block'>e.g. 2016-01-01 09:00:00</p>";
	html += "</div>";
	html += "</div>";
	html += "</form>";
	html += "</div>";
	
	return html;
}

function getNoticeFooterHtml() {
	var html = "";
	
	html += "<button type='button' id='btnEdit' class='btn btn-danger' data-dismiss='modal'>수정</button>";
	html += "<button type='button' class='btn btn-default' data-dismiss='modal'>닫기</button>";
	
	return html;
}

function editNotice(_notice_id) {
	var params = {};
	params.type = "edit_notice";
	params.notice_id = _notice_id;
	params.title = $("#title").text();
	params.contents = $("#contents").text();
	params.image = $("#image").val();
	params.link = $("#link").val();
	params.start_date = $("#start_date").val();
	params.end_date = $("#end_date").val();
	
	console.log(params);
	alert("준비중입니다.");
	return;
	
	sendAjaxPost(params, function(_ret_data) {
		alert("수정되었습니다.");
	});
	
//	$.post("/admin/a/ajax.php", params, function(_data) {
//		var ret_data = JSON.parse(_data);
//		console.log(ret_data);
//		return;
//		
//		if(ret_data.result == "OK") {
//			alert("수정되었습니다.");
//		} else if(ret_data.result == "NOT_LOGIN") {
//			location.href = "/admin";
//		} else {
//			alert(ret_data.message);
//		}
//	}, "text");
}
