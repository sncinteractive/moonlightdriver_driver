<?php
	header("Content-Type: text/html; charset=UTF-8");
	session_start();
	
	include_once($_SERVER["CONTEXT_DOCUMENT_ROOT"] . "/db.php");
	include_once($_SERVER["CONTEXT_DOCUMENT_ROOT"] . "/functions.php");
	
	$retResult = array("result" => "OK", "msg" => "SUCCESS", "data" => array());
	
	if(!checkLogin()) {
		$retResult["result"] = "NOT_LOGIN";
		$retResult["msg"] = "로그인 후 사용가능합니다.";
		
		echo json_encode($retResult);
		exit;
	}
	
	$type = $_POST["type"];
	
	if($type == "get_question_info") {
		$permissions = array("question" => array("inquiry"));
		if(!checkPermission($permissions)) {
			$retResult["result"] = "PERMISSION_DENIED";
			$retResult["msg"] = "접근권한이 없습니다.";
			
			echo json_encode($retResult);
			exit;
		}
		
		$questionType = $_POST["question_type"];
		
		if($questionType == "question") {
			$questionId = $_POST["data_id"];
			$question = $db->questions;
			$driver = $db->drivers;
			
			$ret = $question->findOne(array('_id' => new MongoId($questionId)));
			
			$driver_info = $driver->findOne(array('_id' => new MongoId($ret["driverId"])));
			
			$retResult["data"] = array(
				"questionId" => $ret["_id"] . "",
				"seq" => $ret["seq"],
				"driverId" => $ret["driverId"],
				"name" => isset($driver_info["name"]) ? $driver_info["name"] : "",
				"phone" => $ret["phone"],
				"title" => $ret["title"],
				"contents" => $ret["contents"],
				"status" => $ret["status"],
				"updatedDate" => date("Y-m-d H:i:s", ($ret["updatedDate"] / 1000)),
				"createdDate" => date("Y-m-d H:i:s", ($ret["createdDate"] / 1000)),
				"locations" => $ret["locations"]
			);
		} else if($questionType == "answer") {
			$questionAnswerId = $_POST["data_id"];
			$questionanswer = $db->questionanswers;
			
			$ret = $questionanswer->findOne(array('_id' => new MongoId($questionAnswerId)));
			$retResult["data"] = array(
				"questionAnswerId" => $ret["_id"] . "",
				"questionId" => $ret["questionId"],
				"seq" => $ret["seq"],
				"answer" => $ret["answer"],
				"createdDate" => date("Y-m-d H:i:s", ($ret["createdDate"] / 1000))
			);
		} else {
			$retResult["result"] = "ERROR";
			$retResult["msg"] = "정상적인 호출이 아닙니다.";
			
			echo json_encode($retResult);
			exit;
		}
	} else if($type == "save_question_answer") {
		$questionId = $_POST["question_id"];
		$answer = $_POST["answer"];
		
		$questionanswer = $db->questionanswers;
		
		$answer_count = $questionanswer->count(array('questionId' => $questionId));
		$nextSeq = $answer_count + 1;
		
		$newData = array(
			"questionId" => $questionId,
			"seq" => $nextSeq,
			"answer" => $answer,
			"createdDate" => (time() * 1000)
		);
		
		$ret = $questionanswer->insert($newData);
		
		if(isset($ret["err"]) && !empty($ret["err"])) {
			$retResult["result"] = "ERROR";
			$retResult["msg"] = $ret["err"];
		} else if((isset($ret["errmsg"]) && !empty($ret["errmsg"]))) {
			$retResult["result"] = "ERROR";
			$retResult["msg"] = $ret["errmsg"];
		} else {
			$question = $db->questions;
			$updateData = array('$inc' => array('answerCount' => 1));
			$ret = $question->update(array('_id' => new MongoId($questionId)), $updateData);
		}
	} else if($type == "get_notice_info") {
		$noticeId = $_POST["notice_id"];
		
		$notice = $db->notices;
		$ret = $notice->findOne(array('_id' => new MongoId($noticeId)));
		$retResult["data"] = array(
			"noticeId" => $ret["_id"] . "",
			"seq" => $ret["seq"],
			"title" => $ret["title"],
			"contents" => $ret["contents"],
			"link" => $ret["link"],
			"startDate" => date("Y-m-d H:i:s", ($ret["startDate"] / 1000)),
			"endDate" => date("Y-m-d H:i:s", ($ret["endDate"] / 1000)),
			"updatedDate" => date("Y-m-d H:i:s", ($ret["updatedDate"] / 1000)),
			"createdDate" => date("Y-m-d H:i:s", ($ret["createdDate"] / 1000))
		);
	} else if($type == "edit_notice") {
		$noticeId = $_POST["notice_id"];
		$title = $_POST["title"];
		$contents = $_POST["contents"];
		$image = $_POST["image"];
		$link = $_POST["link"];
		$start_date = strtotime($_POST["start_date"]);
		$end_date = strtotime($_POST["end_date"]);
		
		$notice = $db->notices;
		$updateData = array('$set' => array('title' => $title, 'contents' => $contents, 'link' => $link, 'image' => $image, 'startDate' => $start_date, 'endDate' => $end_date, 'updatedDate' => time()));
		$ret = $question->update(array('_id' => new MongoId($noticeId)), $updateData);
		
		if(isset($ret["err"]) && !empty($ret["err"])) {
			$retResult["result"] = "ERROR";
			$retResult["msg"] = $ret["err"];
		} else if((isset($ret["errmsg"]) && !empty($ret["errmsg"]))) {
			$retResult["result"] = "ERROR";
			$retResult["msg"] = $ret["errmsg"];
		}
	} else if($type == "get_near_station") {
		$lat = floatval($_POST["lat"]);
		$lng = floatval($_POST["lng"]);
		$range = floatval($_POST["range"]);
		
		$taxifinds = $db->taxifinds;
		$shuttlepoints = $db->shuttlepoints;
		$shuttleinfos = $db->shuttleinfos;
		$taxicarpools = $db->taxicarpools;
		$busstations = $db->busstations;
		$subwaytimes = $db->subwaytimes;
		$subwaystations = $db->subwaystations;
		
		$shuttleSearchRange = 5000;
		
		$retShuttleMarkerPoints = getShuttleStationList($shuttlepoints, $lat, $lng, $range);
		$retShuttleLinePoints = getShuttleLineList($shuttlepoints, $lat, $lng, $range);
		
		$busIds = array();
		foreach ($retShuttleLinePoints as $row) {
			if(in_array($row["_id"]["busId"], $busIds)) {
				continue;
			}
		
			$busIds[] = new MongoId($row["_id"]["busId"]);
		}
		
		$retShuttles = getShuttleInfoList($shuttleinfos, $busIds);
		
		$retCarpoolRegistrantList = getCarpoolRegistrantList($taxicarpools, $taxifinds, $lat, $lng, $range);
		
		$retBusStationList = getBusStationList($busstations, $lat, $lng, $range);
		
		$retSubwayStationList = getSubwayStationList($subwaystations, $subwaytimes, $lat, $lng, $range);
		
		$retResult["data"] = array(
			"shuttleMarkerList" => $retShuttleMarkerPoints,
			"shuttleLineList" => $retShuttleLinePoints,
			"shuttleInfoList" => $retShuttles,
			"carpoolRegistrantList" => $retCarpoolRegistrantList,
			"subwayStationList" => $retSubwayStationList,
			"busStationList" => $retBusStationList
		);
	} else if($type == "get_bus_route_list_by_station") {
		$arsId = $_POST["ars_id"];
		$busRouteArea = $_POST["bus_route_area"];
		
		$busstations = $db->busstations;
		$busroutes = $db->busroutes;
		
		$retResult["data"] = getBusRouteListByStation($busroutes, $busstations, $arsId, $busRouteArea);
	} else if($type == "get_shuttle_route_list_by_station") {
		$arsId = $_POST["ars_id"];
		
		$shuttlepoints = $db->shuttlepoints;
		$shuttleinfos = $db->shuttleinfos;
		
		$retResult["data"] = getShuttleRouteListByStation($shuttlepoints, $shuttleinfos, $arsId);
	} else {
		$retResult = array("result" => "ERROR", "msg" => "정상적인 호출이 아닙니다.");
	}
	
	echo json_encode($retResult, JSON_UNESCAPED_UNICODE);
	exit;
?>
