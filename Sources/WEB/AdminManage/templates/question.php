<?php
	header("Content-Type: text/html; charset=UTF-8");
	session_start();
	
	include_once($_SERVER["CONTEXT_DOCUMENT_ROOT"] . "/db.php");
	include_once($_SERVER["CONTEXT_DOCUMENT_ROOT"] . "/functions.php");
	
	if(!checkLogin()) {
		header("Location: /admin/pages/login.php");
		exit;
	}
	
	$permissions = array("question" => array("inquiry"));
	if(!checkPermission($permissions)) {
		echo "<script>";
		echo "alert('접근권한이 없습니다.');";
		echo "location.href = '/admin';";
		echo "</script>";
		exit;
	}
	
	$page = $_POST["page"];
	if(empty($page)) {
		$page = 1;
	}

	$question = $db->questions;
	$questionanswer = $db->questionanswers;
	$driver = $db->drivers;
	
	$total = $question->count();
	$pageitem = 20;
	$endpage = (int)($total / $pageitem);

	if(($total % $pageitem) != 0) {
		$endpage++;
	}

	$start = ($page * $pageitem) - $pageitem;
	$end = $start + $pageitem;

	if($start == (int)($total / $pageitem)) {
		$end = ($start + $total) % $pageitem;
	}

	$question_list = $question->find()->skip($start)->limit($pageitem)->sort(array('seq' => -1));
	$driverIds = array();
	$questionIds = array();
	foreach ($question_list as $row) {
		$driverId = new MongoId($row["driverId"]);
		if(!in_array($driverId, $driverIds)) {
			$driverIds[] = $driverId;
		}
		
		$questionIds[] = $row["_id"] . "";
	}
		
	$driver_list = $driver->find(array('_id' => array('$in' => $driverIds)));
	$driver_info_list = array();
	foreach ($driver_list as $row) {
		$driverId = $row["_id"] . "";
		$driver_info_list[$driverId] = $row;
	}
	
	$question_answer_list = $questionanswer->find(array('questionId' => array('$in' => $questionIds)))->sort(array("_id" => 1));
	$question_answer_info_list = array();
	foreach ($question_answer_list as $row) {
		$questionId = $row["questionId"];
		
		if(!isset($question_answer_info_list[$questionId])) {
			$question_answer_info_list[$questionId] = array();
		}
		
		$question_answer_info_list[$questionId][] = $row;
	}
	
	$key = array("seq", "title", "contents", "name", "phone", "answerCount", "createdDate");
	$keyCount = count($key);
?>

<script src="./assets/js/main.js"></script>

<div class="row">
	<div class="col-lg-12">
		<rd-widget>
			<rd-widget-header icon="fa-tasks" title="문의사항 목록"></rd-widget-header>
			<rd-widget-body classes="medium no-padding">
				<div class="table-responsive">
					<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>#</th>
								<th>제목</th>
								<th>내용</th>
								<th>등록자 이름</th>
								<th>등록자 전화번호</th>
								<th>답변수</th>
								<th>등록 일자</th>
								<th>위치확인</th>
							</tr>
						</thead>
						<tbody>
							<?php
								foreach ($question_list as $document) {
									$question_id = $document["_id"] . "";
									echo "<tr class='question_row' data-id='question_" . $question_id . "'>";
									
									for($i = 0; $i < $keyCount; $i++) {										
										if($key[$i] == "name") {
											$document[$key[$i]] = $driver_info_list[$document["driverId"]]["name"];
										} else if($key[$i] == "createdDate") {
											$document[$key[$i]] = date("Y-m-d H:i:s", ($document[$key[$i]] / 1000));
										}

										echo "<td style='vertical-align: middle;'>" .  $document[$key[$i]]. "</td>";
									}
									
									echo "<td><button type='button' class='btn btn-primary btn-sm show_map'><i class='fa fa-map-marker'></i></button></td>";
									
									echo "</tr>";
									
									foreach ($question_answer_info_list[$question_id] as $answer_doc) {
										$answer_id = $answer_doc["_id"] . "";
										echo "<tr class='answer_row' data-id='answer_" . $answer_id . "'>";
										echo "<td><span class='fa fa-hand-o-right answer_no'> " . $answer_doc["seq"] . " </span></td>";
										echo "<td colspan='5'>" .  $answer_doc["answer"]. "</td>";
										echo "<td>" .  date("Y-m-d H:i:s", ($answer_doc["createdDate"] / 1000)). "</td>";
										echo "<td></td>";
										echo "</tr>";
									}
								}
							?>
						</tbody>
					</table>
        		</div>
			</rd-widget-body>
		<rd-widget>
		<ul class="pagination"></ul>
		<script>
			Pagination("question", <? echo $endpage; ?>, <? echo $page; ?>, 10);
		</script>
	</div>
</div>

<?
    include_once("first_modal.html");
    include_once("second_modal.html");
?>

<script src="./assets/js/question.js"></script>
<link rel="stylesheet" href="./assets/css/call_list.css">
