<?php
	header("Content-Type: text/html; charset=UTF-8");
	session_start();
	
	include_once($_SERVER["CONTEXT_DOCUMENT_ROOT"] . "/functions.php");
	
	if(!checkLogin()) {
		header("Location: " . $_SERVER["CONTEXT_DOCUMENT_ROOT"] . "/pages/login.php");
		exit;
	}
?>

<script type="text/javascript" src="/admin/assets/js/main.js"></script>

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">Short Cut</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-3 col-md-6">
							<div class="panel panel-primary">
								<div class="panel-heading">
									<div class="row">
										<div class="col-xs-3"><i class="fa fa-user fa-5x"></i></div>
										<div class="col-xs-9 text-right">
											<div class="huge">콜 관리</div>
										</div>
									</div>
								</div>
								<a href="#/call_list">
									<div class="panel-footer">
										<span class="pull-left">View Details</span>
										<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
										<div class="clearfix"></div>
									</div>
								</a>
							</div>
						</div>
						<div class="col-lg-3 col-md-6">
							<div class="panel panel-green">
								<div class="panel-heading">
									<div class="row">
										<div class="col-xs-3"><i class="fa fa-group fa-5x"></i></div>
										<div class="col-xs-9 text-right">
											<div class="huge">문의사항</div>
										</div>
									</div>
								</div>
								<a href="#/question">
									<div class="panel-footer">
										<span class="pull-left">View Details</span>
										<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
										<div class="clearfix"></div>
									</div>
								</a>
							</div>
						</div>
						<div class="col-lg-3 col-md-6">
							<div class="panel panel-red">
								<div class="panel-heading">
									<div class="row">
										<div class="col-xs-3"><i class="fa fa-wrench fa-5x"></i></div>
										<div class="col-xs-9 text-right">
											<div class="huge">공지사항</div>
										</div>
									</div>
								</div>
								<a href="#/notice">
									<div class="panel-footer">
										<span class="pull-left">View Details</span>
										<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
										<div class="clearfix"></div>
									</div>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
