<?php
	function checkLogin() {
		return isset($_SESSION["user"]);
	}
	
	function checkPermission($_check_permissions) {
		if(!checkLogin()) {
			return false;
		}
				
		$user_permissions = $_SESSION["user"]["permissions"];
		foreach ($_check_permissions as $key => $_permissions) {
			foreach ($_permissions as $_perm) {
				if($user_permissions[$key][$_perm] != 1) {
					return false;
				}
			}
			
		}
		
		return true;
	}
	
	function login($_admin, $_id, $_pw) {
		$admin_data = $_admin->findOne(array('id' => $_id, 'pw' => $_pw));
		
		if(!isset($admin_data)) {
			return false;
		}
		
		$_SESSION["user"] = array(
			"id" => $admin_data["id"],
			"level" => $admin_data["level"],
			"permissions" => $admin_data["permissions"],
		);
		
		return true;
	}
	
	function getNextSequence($_collection, $_seq_name) {
		$retVal = $_collection->findAndModify(
			array("_id" => $_seq_name),
			array('$inc' => array('seq' => 1)),
			null,
			array("new" => true)
		);
	
		return $retVal["seq"];
	}

	function getShuttleStationList($_shuttlepoints, $_lat, $_lng, $_range) {
		$retShuttleMarkerPoints = array();
		
		$shuttleMarkerPoints = $_shuttlepoints->aggregate(
			array(
				'$geoNear' => array(
					'near' => array(
						'type' => 'Point',
						'coordinates' => array($_lng, $_lat)
					),
					'distanceField' => 'dist.distance',
					"maxDistance" => $_range,
					"spherical" => true,
					"limit" => 100000
				)
			),
			array(
				'$match' => array(
					'text' => array(
						'$ne' => '__point'
					)
				)
			),
			array(
				'$group' => array(
					'_id' => array(
						'arsId' => '$arsId',
						'text' => '$text',
						'locations' => '$locations',
						'exceptOutback' => '$exceptOutback'
					),
					'minDistance' => array(
						'$min' => '$dist.distance'
					),
					'shuttle_times' => array(
						'$push' => array(
							'firstTime' => '$firstTime',
							'lastTime' => '$lastTime'
						)
					)
				)
			),
			array(
				'$sort' => array(
					'minDistance' => 1
				)
			)
		);
	
		foreach ($shuttleMarkerPoints["result"] as $row) {
			$firstTime = "";
			$lastTime = "";
		
			foreach ($row["shuttle_times"] as $shuttle_times_row) {
				$firstTime .= $shuttle_times_row["firstTime"] . ",";
				$lastTime .= $shuttle_times_row["lastTime"] . ",";
			}
		
			$firstTime = substr($firstTime, 0, -1);
			$lastTime = substr($lastTime, 0, -1);
		
			$retShuttleMarkerPoints[] = array(
				"arsId" => $row["_id"]["arsId"],
				"text" => $row["_id"]["text"],
				"location" => array("lat" => $row["_id"]["locations"]["coordinates"][1], "lng" => $row["_id"]["locations"]["coordinates"][0]),
				"exceptOutback" => $row["_id"]["exceptOutback"],
				"distance" => $row["minDistance"],
				"firstTime" => $firstTime,
				"lastTime" => $lastTime,
				"stationType" => "shuttle"
			);
		}
		
		return $retShuttleMarkerPoints;
	}
	
	function getShuttleLineList($_shuttlepoints, $_lat, $_lng, $_range) {
		$shuttleLinePoints = $_shuttlepoints->aggregate(
			array(
				'$geoNear' => array(
					'near' => array(
						'type' => 'Point',
							'coordinates' => array($_lng, $_lat)
						),
						'distanceField' => 'distance',
						"maxDistance" => $_range,
						"spherical" => true,
						"limit" => 100000
					)
			),
			array(
				'$group' => array(
					'_id' => array(
						'busId' => '$busId'
					),
					'shuttle_point' => array(
						'$push' => array(
							'busId' => '$busId',
							'index' => '$index',
							'locations' => '$locations',
							'text' => '$text'
						)
					)
				)
			)
		);
		
		return $shuttleLinePoints["result"];
	}
	
	function getShuttleInfoList($_shuttleinfos, $_busIds) {		
		$retShuttles = array();
		
		$shuttles_cursor = $_shuttleinfos->find(array('_id' => array('$in' => $_busIds)));
		
		foreach ($shuttles_cursor as $row) {
			$busId = $row["_id"] . "";
			$retShuttles[] = array(
				"busId" => $busId,
				"name" => $row["name"]
			);
		}
		
		return $retShuttles;
	}
	
	function getCarpoolRegistrantList($_taxicarpools, $_taxifind, $_lat, $_lng, $_range) {
		$taxiCarpoolRegistrant = $_taxicarpools->aggregate(
			array(
				'$geoNear' => array(
					'near' => array(
						'type' => 'Point',
						'coordinates' => array($_lng, $_lat)
					),
					'distanceField' => 'distance',
					"maxDistance" => $_range,
					"spherical" => true,
					"limit" => 100000
				)
			),
			array(
				'$match' => array(
					'time' => array(
						'$gte' => time() * 1000
					)
				)
			)
		);
		
		$retCarpoolRegistrantList = array();
		$registrantList = array();
		
		foreach ($taxiCarpoolRegistrant["result"] as $row) {
			$taxiId = $row["_id"] . "";
			$retCarpoolRegistrantList[] = array(
				"taxiId" => $taxiId,
				"driverId" => $row["driverId"],
				"registrant" => $row["registrant"],
				"distance" => $row["distance"],
				"stationType" => "carpool"
			);
			
			$registrantList[] = $row["driverId"];
		}
		
		$taxifind_cursor = $_taxifind->find(array('driverId' => array('$in' => $registrantList)));
		
		$retDriverPoint = array();
		foreach ($taxifind_cursor as $row) {
			$retDriverPoint[$row["driverId"]] = $row["location"];
		}
		
		foreach ($retCarpoolRegistrantList as &$row) {
			if(isset($retDriverPoint[$row["driverId"]])) {
				$row["location"] = array("lat" => $retDriverPoint[$row["driverId"]]["coordinates"][1], "lng" => $retDriverPoint[$row["driverId"]]["coordinates"][0]);
			}
		}
		
		return $retCarpoolRegistrantList;
	}
	
	function getBusStationList($_busstations, $_lat, $_lng, $_range) {
		$retBusStationList = array();
		
		$busStationList = $_busstations->aggregate(
			array(
				'$geoNear' => array(
					'near' => array(
						'type' => 'Point',
						'coordinates' => array($_lng, $_lat)
					),
					'distanceField' => 'dist.distance',
					"maxDistance" => $_range,
					"spherical" => true,
					"limit" => 100000
				)
			),
			array(
				'$match' => array(
					'inSeoul' => 0,
					'arsId' => array(
						'$ne' => null
					)
				)
			),
			array(
				'$group' => array(
					'_id' => array(
						'stationName' => '$stationName',
		                'busRouteArea' => '$busRouteArea',
		                'arsId' => '$arsId',
		                'gpsX' => '$gpsX',
		                'gpsY' => '$gpsY'
					),
					'minDistance' => array(
						'$min' => '$dist.distance'
					),
					'bus_times' => array(
						'$push' => array(
							'startTime' => '$startTime',
							'endTime' => '$endTime'
						)
					)
				)
			),
			array(
				'$sort' => array(
					'minDistance' => 1
				)
			)
		);
		
		foreach ($busStationList["result"] as $row) {
			$startTime = "";
			$endTime = "";
		
			foreach ($row["bus_times"] as $bus_times_row) {
				$startTime .= $bus_times_row["startTime"] . ",";
				$endTime .= $bus_times_row["endTime"] . ",";
			}
		
			$startTime = substr($startTime, 0, -1);
			$endTime = substr($endTime, 0, -1);
		
			$retBusStationList[] = array(
				"arsId" => $row["_id"]["arsId"],
				"stationName" => $row["_id"]["stationName"],
				"busRouteArea" => $row["_id"]["busRouteArea"],
				"location" => array("lat" => $row["_id"]["gpsY"], "lng" => $row["_id"]["gpsX"]),
				"distance" => $row["minDistance"],
				"startTime" => $startTime,
				"endTime" => $endTime,
				"stationType" => "bus"
			);
		}
		
		return $retBusStationList;
	}
	
	function getSubwayStationList($_subwaystations, $_subwaytimes, $_lat, $_lng, $_range) {
		$retSubwayStationList = array();
		
		$stationCodes = array();
		$subwayStationList = $_subwaystations->aggregate(
			array(
				'$geoNear' => array(
					'near' => array(
						'type' => 'Point',
						'coordinates' => array($_lng, $_lat)
					),
					'distanceField' => 'distance',
					"maxDistance" => $_range,
					"spherical" => true,
					"limit" => 100000
				)
			)
		);
		
		foreach ($subwayStationList["result"] as $row) {
			$retSubwayStationList[$row["stationCode"]] = array(
				"stationCode" => $row["stationCode"],
				"stationName" => $row["stationName"],
				"location" => array("lat" => $row["gpsY"], "lng" => $row["gpsX"]),
				"stationCodeDaum" => $row["stationCodeDaum"],
				"distance" => $row["distance"],
				"stationType" => "subway"
			);
			
			$stationCodes[] = strval($row["stationCode"]);
		}
		
		$dayOfWeek = date("N") < 6 ? 1 : (date("N") == 6 ? 2 : 3);
				
		$subwayTimeList = getSubwayTimeList($_subwaytimes, $stationCodes);
		
		if($subwayTimeList != null) {
			foreach ($subwayTimeList["result"] as $row) {
				$firstTime = "";
				$lastTime = "";
			
				foreach ($row["subwayTime"] as $subway_times_row) {
					$firstTime .= $subway_times_row["firstTime"] . ",";
					$lastTime .= $subway_times_row["lastTime"] . ",";
				}
			
				$firstTime = substr($firstTime, 0, -1);
				$lastTime = substr($lastTime, 0, -1);
				
				$stationCode = $row["_id"]["stationCode"];
				
				$retSubwayStationList[$stationCode]["firstTime"] = $firstTime;
				$retSubwayStationList[$stationCode]["lastTime"] = $lastTime;
			}
		}
				
		return $retSubwayStationList;
	}
	
	function getSubwayTimeList($_subwaytimes, $_stationCodes) {
		$dayOfWeek = date("N") < 6 ? 1 : (date("N") == 6 ? 2 : 3);
		
		$subwayTimeList = $_subwaytimes->aggregate(
			array(
				'$match' => array(
					'stationCode' => array('$in' => $_stationCodes),
					'weekTag' => strval($dayOfWeek)
				)
			),
			array(
				'$group' => array(
					'_id' => array(
						'stationCode' => '$stationCode'
					),
					'subwayTime' => array(
						'$push' => array(
							'firstTime' => '$firstTime',
							'lastTime' => '$lastTime'
						)
					)
				)
			)
		);
		
		return $subwayTimeList["result"];
	}
	
	function getBusRouteListByStation($_busroutes, $_busstations, $_arsId, $_busRouteArea) {
		$retBusRouteList = array();
		
		$busStationList = $_busstations->aggregate(
			array(
				'$match' => array(
					'arsId' => $_arsId,
					'busRouteArea' => $_busRouteArea
				)
			),
			array(
				'$group' => array(
					'_id' => array(
						'busRouteId' => '$busRouteId',
						'direction' => '$direction',
						'stationSequence' => '$stationSequence'
					),
				)
			)
		);
		
		$busRoutes = array();
		
		foreach ($busStationList["result"] as $row) {
			$retBusRouteList[] = $row["_id"];
			$busRoutes[] = $row["_id"]["busRouteId"];
		}
		
		$busRouteListCursor = $_busroutes->find(array('busRouteId' => array('$in' => $busRoutes), 'busRouteArea' => $_busRouteArea), array('busRouteId' => true, 'busRouteName' => true, 'busRouteType' => true, 'busRouteTypeName' => true));
		
		$busRouteList = array();
		foreach ($busRouteListCursor as $row) {
			$busRouteList[$row["busRouteId"]] = $row;
		}
		
		foreach ($retBusRouteList as &$row) {
			$nextSeq = intval($row['stationSequence']) + 1;
			$nextStation = $_busstations->findOne(array('busRouteId' => $row['busRouteId'], 'busRouteArea' => $_busRouteArea, 'stationSequence' => $nextSeq), array('stationName' => true));
			
			$row["nextStation"] = "";
			if(isset($nextStation) && isset($nextStation["stationName"])) {
				$row["nextStation"] = $nextStation["stationName"];
			}
			
			if(isset($busRouteList[$row['busRouteId']])) {
				$row["busRouteName"] = $busRouteList[$row['busRouteId']]["busRouteName"];
				$row["busRouteType"] = $busRouteList[$row['busRouteId']]["busRouteType"];
				$row["busRouteTypeName"] = $busRouteList[$row['busRouteId']]["busRouteTypeName"];
			}
		}

		return $retBusRouteList;
	}
	
	function getShuttleRouteListByStation($_shuttlepoints, $_shuttleinfos, $_arsId) {
		$retShuttleRouteList = array();
		$shuttlePointListCusor = $_shuttlepoints->find(array('arsId' => $_arsId), array('busId' => true));
		
		$busIds = array();
		foreach ($shuttlePointListCusor as $row) {
			$busIds[] = new MongoId($row["busId"]);
		}
		
		$shuttleInfoListCursor = $_shuttleinfos->find(array('_id' => array('$in' => $busIds)));
		foreach ($shuttleInfoListCursor as $row) {
			$retShuttleRouteList[] = array(
				"name" => $row["name"],
				"desc" => $row["desc"],
				"category" => $row["category"],
				"firstTime" => $row["firstTime"],
				"lastTime" => $row["lastTime"],
				"term" => $row["term"],
				"totalTime" => $row["totalTime"]
			);
		}
		
		return $retShuttleRouteList;
	}
	
	function getCircleCoordinatesLatLng($_lat, $_lng, $_radius, $_points) {
		$d2r = M_PI / 180;
		$r2d = 180 / M_PI;
		$earthsradius = 3959;
		$start = 0;
		$end = $_points + 1;
		$radius = $_radius / 1.61;
		$coordinates = array();
	
		$rLat = ($radius / $earthsradius) * $r2d;
		$rLng = $rLat / cos($_lat * $d2r);
		for($i = $start; $i < $end; $i++) {
			$theta = M_PI * ($i / ($_points / 2));
			$resultLng = $_lng + ($rLng * cos($theta));
			$resultLat = $_lat + ($rLat * sin($theta));
			$coordinates[] = array("lat" => $resultLat, "lng" => $resultLng);
		}
	
		return $coordinates;
	}
?>