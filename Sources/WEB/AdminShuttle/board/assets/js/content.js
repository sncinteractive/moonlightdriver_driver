$( document ).ready(function() {
	var array = ["성별/나이/경력","수입배분(희망)","활동 범위","출발지","업무시간","찾는 대상","보유/희망 차종","연락처","특이사항"];

	var data = "&n=" + $(".n").attr('id');

	$.ajax(
	{
        type:"POST",
        url : "./ajax.php",
        data : "query=content_show" + data,
        async: false,
        success : function(response) {
            data = response;

			var temp = response.split('|');

			$("#date").append(temp[1]);
			$("#content_title").append(temp[2]);
			$("#nick").append(temp[4]);

			if(temp[3] == 0)
				$("#category").addClass("want")
			else
				$("#category").addClass("volunteer");

			for(i=4;i<array.length+4;i++)
			{
				var temp2 = '<p class="need2"><span class="lb">' + array[i-4] + " : " + '</span>' + temp[i] + '</p>';
				$("#writes2").append(temp2);
			}
        },
        error: function() {
            alert('통신 상태를 확인해주세요.');
		}
	});
	$.ajax(
	{
        type:"POST",
        url : "./ajax.php",
        data : "query=comment_show" + "&n=" + $(".n").attr('id'),
        async: false,
        success : function(response) {
            data = response;

			var temp = response.split('*');
			for(i=1;i<temp.length;i++)
			{
				var temp2 = temp[i].split('|');
				var temp3 = '<div id="bottom_comment_content"><div id="comment_title"><span id="nick">' + temp2[1] + '</span><span id="date">' + temp2[0] + '</span></div><div id="comment_content"><p>' + temp2[2] + '</p></div></div>';
				$("#comments").append(temp3);
			}
			var count = temp.length-1;
			$("#count").append(temp.length-1);
			var counts = '( ' + count + ' )';
			$("#counts").append(counts);
        },
        error: function() {
            alert('통신 상태를 확인해주세요.');
		}
	});
});
$("#btn_submit").click(function() {

	var data = "&n=" + $(".n").attr('id') + "&nick=" + $("#text_nick").val() + "&comment=" + $("#text_comment").val();

	$.ajax(
	{
        type:"POST",
        url : "./ajax.php",
        data : "query=comment_insert" + data,
        async: false,
        success : function(response) {
            data = response;
            alert(response);
            location.reload();
        },
        error: function() {
            alert('통신 상태를 확인해주세요.');
		}
	});
});
$(".left").click(function() {
	window.location.href = "./list.php";
});