<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, user-scalable=no">
		<meta name="viewport" content="initial-scale=1, maximum-scale=1">
		<link href="./assets/css/style.css" rel="stylesheet">
		<script src="./assets/js/jquery-1.11.1.min.js"></script>
	</head>
	<body>
		<div id="container">
			<div class="header">
				<div class="left"></div>
				<span class="menu">글쓰기</span>
				<div class="right"></div>
			</div>
			<div id="content">
				<div id="title">
					<input id="txt_title" name="title" type="text" placeholder="제목">
				</div>
				<div id="check">
					<div id="span_look">
						<div id="chk_look" class="checked"></div>
					<p id="lb_ggonzi">꽁지 구해요</p>
					</div>
					<div id="span_volunteer">
						<div id="chk_volunteer" class="unchecked"></div>
					<p id="lb_ggonzi">꽁지 지원할게요</p>
					</div>
				</div>
				<div id="writes">
					<p class="need"><span class="lb">이름(닉네임) : </span><input name="nickname" class="fill" type="text"></p>
					<p class="need"><span class="lb">성별/나이/경력 : </span><input name="gender" class="fill" type="text"></p>
					<p class="need"><span class="lb">수입배분(희망) : </span><input name="divide" class="fill" type="text"></p>
					<p class="need"><span class="lb">활동 범위 : </span><input name="range" class="fill" type="text"></p>
					<p class="need"><span class="lb">출발지 : </span><input name="start" class="fill" type="text"></p>
					<p class="need"><span class="lb">업무시간 : </span><input name="time" class="fill" type="text"></p>
					<p class="need"><span class="lb">찾는 대상 : </span><input name="target" class="fill" type="text"></p>
					<p class="need"><span class="lb">보유/희망 차종 : </span><input name="hope" class="fill" type="text"></p>
					<p class="need"><span class="lb">연락처 : </span><input name="contact" class="fill" type="text"></p>
					<p class="need"><span class="lb">특이사항 : </span><input name="remark" class="fill" type="text"></p>
				</div>
			</div>
			<div id="submit">
				<div id="button">
				   <input id="submit_button" type="image" value="submit" src="./assets/img/write_btn_write.png" alt="" />
				   <span class="inline_text">등록하기</span>
				</div>
			</div>
		</div>
		<script src="./assets/js/write.js"></script>
		<script type="text/javascript">
		  WebFontConfig = {
		    custom: {
		        families: ['Nanum Gothic'],
		        urls: ['http://fonts.googleapis.com/earlyaccess/nanumgothic.css']
		    }
		  };
		  (function() {
		    var wf = document.createElement('script');
		    wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
		      '://ajax.googleapis.com/ajax/libs/webfont/1.4.10/webfont.js';
		    wf.type = 'text/javascript';
		    wf.async = 'true';
		    var s = document.getElementsByTagName('script')[0];
		    s.parentNode.insertBefore(wf, s);
		  })();
 		</script>

	</body>
</html>