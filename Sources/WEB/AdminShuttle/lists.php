<?php
	header('Access-Control-Allow-Origin: *');
	include_once "login.php";
	include_once 'db.php';

	session_start();
	
	$id = $_POST["id"];
	$type = $_POST["type"];
	$func = $_POST["func"];
	$level = 2;

	if(!login($_SESSION["id"],$_SESSION["pw"])) {
		exit;
	}
	
	if($_SESSION["level"] > $level) {
		echo "<script>alert('권한 없음');</script>";
		exit;
	}
	
	if(empty($func)) {
		$func = "normal";
	}
	
	$collection = $db->$type;
	$cursor = $collection->find(array("_id" =>  new MongoId($id)));

	$subkeyname = array("ID","이름","위도","경도","생성 날짜","오지기준제외","추가","수정","삭제");
	$subkey = array("_id","text","lat","lng","createdAt","exceptOutback","add","modify","delete");
?>

<script src="/js/lists.js"></script>
<div id="sub_<?=$id?>" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
	    	<div class="modal-header">
	    		<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
	    		<h4 class="modal-title" id="myLargeModalLabel">lists</h4>
	    	</div>
	    	<table id="<?=$id?>" class="table table-condensed lists">
		    	<thead>
			    	<tr>
				    	<?php
							foreach($subkeyname as $v) {
								if($v == "오지기준제외") {
									echo "<td>" . $v . "<div><button class='btn btn-default sub_check'>적용</button></div></td>";
								} else {
									echo "<td>" . $v . "</td>";
								}
							}
						?>
			    	</tr>
		    	</thead>
		    	<tbody>
				<?
					foreach($cursor as $document) {
				    	while(current($document["lists"])) {
				?>
					<tr>
				<?
					    	for($j=0;$j<count($subkey);$j++) {
								if($document["lists"][key($document["lists"])]["text"] == "__point" && $func == "hell") {
									continue;
								}
								
								echo "<td>";

								if($subkey[$j] == "createdAt") {
									$document["lists"][key($document["lists"])][$subkey[$j]] = date( "Y-m-d H:i:s", $document["lists"][key($document["lists"])][$subkey[$j]]->sec);
									echo $document["lists"][key($document["lists"])][$subkey[$j]];
								} else if($subkey[$j] == "delete") {
									$id = $document["lists"][key($document["lists"])]["_id"];
									echo '<button type="button" id=' . "'" . $document["_id"] . "," .  $id . "'" . 'class="btn btn-default sub_delete">삭제</button>';
								} else if($subkey[$j] == "modify") {
									$id = $document["lists"][key($document["lists"])]["_id"];
									echo '<button type="button" id=' . "'modify," . $document["_id"] . "," .  $id . "'" . 'class="btn btn-default sub_modify">수정</button>';
								} else if($subkey[$j] == "add") {
									$id = $document["lists"][key($document["lists"])]["_id"];
									echo '<button type="button" id=' . "'add," . $document["_id"] . "," .  $id . "'" . 'class="btn btn-default sub_add">추가</button>';
								} else if($subkey[$j] == "exceptOutback") {
									$id = $document["lists"][key($document["lists"])]["_id"];
									$checkValue = "";
								
									if(!empty($document["lists"][key($document["lists"])][$subkey[$j]]) && $document["lists"][key($document["lists"])][$subkey[$j]] == 1) {
										$checkValue = "checked";
									}
									echo "<input type='checkbox' id='except," . $document["_id"] . "," .  $id . "' " . $checkValue . "/>";
								} else {
									echo $document["lists"][key($document["lists"])][$subkey[$j]];
								}

								echo "</td>";
							}
							next($document["lists"]);
						}
					}
				?>
			    	</tr>
		    	</tbody>
	    	</table>
		</div>
	</div>
</div>
