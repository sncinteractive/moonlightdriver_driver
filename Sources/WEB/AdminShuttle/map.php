<!DOCTYPE html>
<html>
	<?
		session_start();
		$type = $_GET["type"];
		$func = $_GET["func"];
		$id = $_GET["id"];
		$hl = $_GET["hl"];
		$category = $_GET["category"];
		$level = 2;
		$n2 = 0;

		include_once "login.php";

		if($_SESSION["level"] > $level)
		{
			echo "<script>alert('권한 없음');</script>";
			exit;
		}
		if(!login($_SESSION["id"],$_SESSION["pw"]))
		{
			echo "<script>alert('세션 없음');</script>";
			exit;
		}

		if(empty($id) && empty($category))
		{
			echo "<script>alert('파라메터값이 비었습니다.')</script>";
			exit;
		}
		if($func == "edit" && isset($category))
		{
			echo "<script>alert('카테고리로 묶은상태에선 수정 할 수 없습니다.')</script>";
			exit;
		}
		if(empty($func))
			$func = "insert";
		if(isset($hl))
			$hl = split(",",$hl);

		include 'db.php';
		$collection = $db->$type;

		if(isset($category))
			$cursor = $collection->find(array('category' => $category));
		else
			$cursor = $collection->find(array('_id' => new MongoId($id)));

		foreach ($cursor as $document)
		{
			$n = 0;
			while(current($document["lists"]))
			{
				$temp[$n2] .= "new google.maps.LatLng(" . $document["lists"][key($document["lists"])][lat] . "," . $document["lists"][key($document["lists"])][lng] . "),";
				$n++;
				next($document["lists"]);
			}
			$n2++;
		}
		$colorArray = array("#FF0000","#FF7F00","#FFFF00","#00FF00","#0000FF","#4B0082","#8B00FF","#FF66CC","#000000","#d72f28","#2295a7","#6bf79d","#ddf029","#f4c5f0","#5cf775","#ea1997","#d6d939","#e9807c","#250f23","#5b1d88","#aae58e","#fe7715","#04d2d9","#b23cb9","#23fc54","#54561c","#4f5976","#7eebd3","#898af0","#3ff474","#556d63");

		if($func == "edit")
		{
	    	$temp2 = trim($temp[0] ,",");
	    	$temp2 = split(",",$temp2);
	    	$n=0;
			for($i=0;$i<count($temp2);$i+=2)
			{
				$temp3[$n] = $temp2[$i] . "," . $temp2[$i+1];
				$n++;
			}
		}
	?>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>좌표 MAP</title>
    <style>
    html, body, #map-canvas {
        height: 100%;
        margin: 0px;
        padding: 0px;
        cursor: default;
    }
    </style>
	<script src="./dist/js/jquery-1.11.1.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>
    <script>


		var map = null;
		var marker = null;
		var func = '<?=$func?>';

		var infowindow = new google.maps.InfoWindow(
		{
			size: new google.maps.Size(150,50)
		});

		function createMarker(latlng, name, html) {
		    var contentString = html;
		    var marker = new google.maps.Marker({
		        position: latlng,
		        map: map,
		        zIndex: Math.round(latlng.lat()*-100000)<<5
		        });
		    google.maps.event.addListener(marker, 'click', function() {
		        infowindow.setContent(contentString);
		        infowindow.open(map,marker);
		        });
		    google.maps.event.trigger(marker, 'click');
		    return marker;
		}

		function initialize() {
		  var mapOptions;
			if(localStorage.mapLat!=null && localStorage.mapLng!=null && localStorage.mapZoom!=null){
				mapOptions = {
				draggableCursor:'crosshair',
		        center: new google.maps.LatLng(localStorage.mapLat,localStorage.mapLng),
		        zoom: parseInt(localStorage.mapZoom),
		        scaleControl: true,
		        mapTypeControl: true,
				mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
				navigationControl: true,
				mapTypeId: google.maps.MapTypeId.ROADMAP
				};
		    }
		    else{
		        mapOptions = {
					<? echo "center: new google.maps.LatLng(37.56641923090, 126.9778741551),"; ?>
				draggableCursor:'crosshair',
		        zoom: 11,
		        scaleControl: true,
		        mapTypeControl: true,
				mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
				navigationControl: true,
				mapTypeId: google.maps.MapTypeId.ROADMAP
				};
		    };

		  map = new google.maps.Map(document.getElementById('map-canvas'),
		      mapOptions);

		mapCentre = map.getCenter();

		localStorage.mapLat = mapCentre.lat();
		localStorage.mapLng = mapCentre.lng();
		localStorage.mapZoom = map.getZoom();


		google.maps.event.addListener(map,"center_changed", function() {
		    mapCentre = map.getCenter();

		    localStorage.mapLat = mapCentre.lat();
		    localStorage.mapLng = mapCentre.lng();
		    localStorage.mapZoom = map.getZoom();
		});

		google.maps.event.addListener(map,"zoom_changed", function() {
		    mapCentre = map.getCenter();

		    localStorage.mapLat = mapCentre.lat();
		    localStorage.mapLng = mapCentre.lng();
		    localStorage.mapZoom = map.getZoom();
		});

		google.maps.event.addListener(map, 'click', function() {
		        infowindow.close();
		});

		  google.maps.event.addListener(map, 'click', function(event) {
		         if(func == "insert")
		         {
		         	if (marker) {
			            marker.setMap(null);
			            marker = null;
			         }
					 marker = createMarker(event.latLng, "IDAdd", "<b>ID 좌표 추가</b><br><input id='" + event.latLng + "' class='NameValue' type='text' value='__point'><input id='submitId' type='button' value='확인' autofocus>");
				 }
				 else if ( func == "edit")
				 {
					var closest = -1;
					google.maps.event.addListener(map, 'click', find_closest_marker);
					function rad(x) {return x*Math.PI/180;}
					function find_closest_marker( event ) {
					    var lat = event.latLng.lat();
					    var lng = event.latLng.lng();
					    var R = 6371; // radius of earth in km
					    var distances = [];

					    for( i=0;i<lineCoordinates0.length; i++ ) {
						    var temp = "" + lineCoordinates0[i];
						    temp = temp.replace("(" , "");
							temp = temp.replace(")" , "");
							temp = temp.replace(" " , "");
							temp = temp.split(",");
					        var mlat = temp[0];
					        var mlng = temp[1];


					        var dLat  = rad(mlat - lat);
					        var dLong = rad(mlng - lng);
					        var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
					            Math.cos(rad(lat)) * Math.cos(rad(lat)) * Math.sin(dLong/2) * Math.sin(dLong/2);
					        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
					        var d = R * c;
					        distances[i] = d;
					        if ( closest == -1 || d < distances[closest] ) {
					            closest = i;
					        }
					    }
					    if (marker) {
				            marker.setMap(null);
				            marker = null;
				         }
					    marker = createMarker(event.latLng, "IDEdit", "<b>ID 좌표 수정</b><br><input id='" + closest + "' class='NameValue' type='text' value=''><input id='submitId' type='button' value='확인' autofocus>");
					}
				 }
		  });


		  var lineSymbol = {
		    path: 'M 0,-1 0,1',
		    strokeOpacity: 1,
		    scale: 5
		  };
		  <?
		    for($i=0;$i<$n2;$i++)
		    {
			 	echo "var lineCoordinates" . $i . "= [";

			  	if(isset($temp[$i]))
					echo trim($temp[$i], ",");
				else
					echo "new google.maps.LatLng(0,0)";
				echo "];";

				echo "var line" . $i . " = new google.maps.Polyline({
				    path: lineCoordinates" . $i . ",
				    strokeColor: '" . $colorArray[$i] . "',
				    strokeOpacity: 0,
				    icons: [{
				      icon: lineSymbol,
				      offset: '0',
				      repeat: '10px'
				    }],
				    map: map
				});";
			}
			    if(isset($hl))
		        	echo "var myLatlng = new google.maps.LatLng(" . $hl[0] . "," . $hl[1] . ");";
		        else
		        	echo "var myLatlng = new google.maps.LatLng(0,0);";
				?>
				var marker2 = new google.maps.Marker({
					        position: myLatlng,
					        map: map
				});
				<?
		    	if($func == "edit"){
					for($i=0;$i<count($temp3);$i++)
					{
					?>
				        var marker2 = new google.maps.Marker({
					        position: <?=$temp3[$i]?>,
					        map: map
					    });
					<?
					}
				}
		    ?>
		}

		google.maps.event.addDomListener(window, 'load', initialize);

		google.maps.event.addListener(infowindow, 'domready', function() {

			$( '#submitId' ).click(function(){

				var temp = $(".NameValue").attr('id');
				if(func == "insert")
				{
					temp = temp.replace("(" , "");
					temp = temp.replace(")" , "");
					temp = temp.replace(" " , "");
					temp = temp.split(",");
					$.ajax({
					        type:"POST",
					        url : "http://115.68.104.30:8888/busnew/" + "<?=$id?>" + "/record",
					        data : "lat=" + temp[0] + "&lng=" + temp[1] + "&name=" + $(".NameValue").val() + "&busId=" + "<?=$id?>",
					        async: false,
					        success : function(response) {
								location.reload();
					        },
					        error: function() {
					            alert('통신 상태를 확인해주세요.');
					        }
					});
				}
				else if(func == "edit")
				{
					$.ajax({
					        type:"POST",
					        url : "http://115.68.104.30:8888/busnew/" + "<?=$id?>" + "/edittext",
					        data : "text=" + $(".NameValue").val() + "&busId=" + "<?=$id?>" + "&index=" + temp,
					        async: false,
					        success : function(response) {
								location.reload();
					        },
					        error: function() {
					            alert('통신 상태를 확인해주세요.');
					        }
					});
				}
			});
		});
	</script>
  </head>
  <body>
    <div id="map-canvas"></div>
  </body>
</html>