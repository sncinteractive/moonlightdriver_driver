<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <title>달빛기사 :: 관리자 페이지</title>
    <link href="./dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="./css/style.css" rel="stylesheet">
    <link href="./css/signin.css" rel="stylesheet">
    <?
		include_once "login.php";
	    session_start();
	?>
  </head>
  <body>
	  <script src="./dist/js/jquery-1.11.1.min.js"></script>
      <?
		if(isset($_SESSION["id"]) && isset($_SESSION["pw"]))
	    {
		    if(login($_SESSION["id"],$_SESSION["pw"]))
		    {
		    	include_once("manage.php");
		    }
		    else
		    {
			 	include_once("login_page.php");
			 	session_unset();
			}
	    }
	    else if(isset($_POST["id"]) && isset($_POST["pw"]))
	    {
		   	if(login($_POST["id"],md5($_POST["pw"])))
		   	{
			   	echo "<script>alert('로그인 성공');</script>";
			   	$_SESSION["id"] = $_POST["id"];
			   	$_SESSION["pw"] = md5($_POST["pw"]);
		    	include_once("manage.php");
		    }
		    else
		    {
			    echo "<script>alert('로그인 실패');</script>";
			 	include_once("login_page.php");
		    }
	    }
	    else
	    {
		    include_once("login_page.php");
	    }
	  ?>
	  <script src="./dist/js/bootstrap.min.js"></script>
	  <script src="./dist/js/jquery.confirm.min.js"></script>
  </body>
</html>

