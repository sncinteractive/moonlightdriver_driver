<div class="container" style="margin-top:40px">
	<div class="row">
		<div class="col-sm-6 col-md-4 col-md-offset-4">
			<div class="panel panel-default">
				<div class="panel-heading">
					<strong> 관리자 로그인 </strong>
				</div>
				<div class="panel-body">
					<form role="form" action="./" method="POST">
						<div class="row">
							<div class="center-block">
								<img class="profile-img"
									src="https://ssl.gstatic.com/accounts/ui/avatar_2x.png" alt="">
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12 col-md-10  col-md-offset-1 ">
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon">
											<i class="glyphicon glyphicon-user"></i>
										</span>
										<input class="form-control" placeholder="아이디" name="id" type="text" autofocus>
									</div>
								</div>
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon">
											<i class="glyphicon glyphicon-lock"></i>
										</span>
										<input class="form-control" placeholder="비밀번호" name="pw" type="password" value="">
									</div>
								</div>
								<div class="form-group">
									<input type="submit" class="btn btn-lg btn-primary btn-block" value="로그인">
								</div>
							</div>
						</div>
					</form>
				</div>
            </div>
		</div>
	</div>
</div>