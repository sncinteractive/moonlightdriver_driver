<?php
	header("Content-Type:text/html; charset=utf-8");

	try {
		require_once ("../inc/config.php");
		require_once_classes(array("CSession", "CDatabaseManager", "CCallManager"));
	
		$session = new CSession();

		if(!$session->isLogin()) {
			moveToSpecificPage(CONF_URL_LOGIN);
			exit;
		}
		
		$permissions = array("call" => array("inquiry"));
		if(!$session->checkPermission($permissions)) {
			echo "<script>";
			echo "alert('접근권한이 없습니다.');";
			echo "location.href = '" . CONF_URL_ROOT . "';";
			echo "</script>";
			exit;
		}
		
		$call_manager = new CCallManager();
		$database_manager = new CDatabaseManager();

		$database = $database_manager->getDb();
		
		$call_list = $call_manager->getCallList($database->calls);
		$call_list_json = json_encode($call_list, JSON_UNESCAPED_UNICODE);
		
		global $CALL_STATUS;
		$call_status_list = json_encode($CALL_STATUS, JSON_UNESCAPED_UNICODE);
	} catch (Exception $e) {
		echo $e->getMessage();
		mld_error($e->getMessage());
		moveToSpecificPage(CONF_URL_ERROR);
		exit;
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo CONF_SITE_TITLE . " > 콜 관리"; ?></title>
		
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/bootstrap.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/metisMenu.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/sb-admin-2.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/font-awesome.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/dataTables.bootstrap.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/dataTables.responsive.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/bootstrap-toggle.min.css">
		
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/jquery-2.1.3.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/metisMenu.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/sb-admin-2.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/dataTables.bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap-toggle.min.js"></script>
		
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css/main.css?<?php echo time();?>">
		
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js/call.js?<?php echo time();?>"></script>
		
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
		<script type="text/javascript">
			var LOGIN_URL = "<?php echo CONF_URL_LOGIN; ?>";
			var AJAX_URL = "<?php echo CONF_URL_AJAX; ?>";

			var list_table;
			var call_list = <?php echo $call_list_json; ?>;
			var call_status_list = <?php echo $call_status_list; ?>;
			
			$(function() {
				list_table = $('#call_list').DataTable({
					columns: [
						null,
						null,
						null,
						null,
						null,
						null
					],
					initComplete: function () {
						var html = "";
						html += "<label>";
						html += "<button type='button' class='btn btn-primary' id='btnCreateCall'>콜 추가</button>";
						html += "</label>";
						$('#call_list_length').html(html);
						
						$('#btnCreateCall').off("click").on('click', function() {
							showCallDetailPopup(null);
						});
					},
					drawCallback: function() {						
						$('#call_list > tbody').off("click").on('click', 'tr', function (event) {
							var index = $(this).index("#shuttle_list > tbody > tr");
							if($(event.target).is('#call_list > tbody > tr:eq(' + index + ') > td:eq(0),#call_list > tbody > tr:eq(' + index + ') > td:eq(1),#call_list > tbody > tr:eq(' + index + ') > td:eq(2),#call_list > tbody > tr:eq(' + index + ') > td:eq(3),#call_list > tbody > tr:eq(' + index + ') > td:eq(4)')) {
								var call_id = $(this).attr("id");
								
								showCallDetailPopup(call_list[call_id]);
							}
						});
					}
				});
							
				$.each(call_list, function(key, val) {
					var new_node = list_table.row.add([
						val.start.text,
						val.through.text,
						val.end.text,
						val.money,
						val.createdAt,
						val.status_text
					]).node();

					$(new_node).attr("id", val.call_id);
				});

				list_table.draw();
			});
		</script>
	</head>
	<body>
		<div id="wrapper">
			<?php include_once CONF_URL_MENU; ?>
			<div id="page-wrapper">
				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">콜 관리</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">콜 리스트</div>
							<div class="panel-body">
								<div class="dataTable_wrapper">
									<table class="table table-striped table-bordered table-hover" id="call_list">
										<thead>
											<tr>
												<th>출발지</th>
												<th>경유지</th>
												<th>도착지</th>
												<th>요금</th>
												<th>등록 시간</th>
												<th>상태</th>
											</tr>
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade" id="modal_popup" tabindex="-1" role="dialog" aria-labelledby="modal_popup_label" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="modal_popup_label"></h4>
						</div>
						<div class="modal-body" id="modal_popup_content"></div>
						<div class="modal-footer" id="modal_popup_footer"></div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>