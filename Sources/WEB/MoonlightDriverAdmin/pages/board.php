<?php
	header("Content-Type:text/html; charset=utf-8");

	try {
		require_once ("../inc/config.php");
		require_once_classes(array("CSession", "CDatabaseManager", "CBoardManager"));
	
		$session = new CSession();

		if(!$session->isLogin()) {
			moveToSpecificPage(CONF_URL_LOGIN);
			exit;
		}
		
		$permissions = array("board" => array("inquiry"));
		if(!$session->checkPermission($permissions)) {
			echo "<script>";
			echo "alert('접근권한이 없습니다.');";
			echo "location.href = '" . CONF_URL_ROOT . "';";
			echo "</script>";
			exit;
		}
		
		$board_manager = new CBoardManager();
		$database_manager = new CDatabaseManager();

		$database = $database_manager->getDb();
		
		$board_list = $board_manager->getBoardList($database->boards);
		$board_list_json = json_encode($board_list, JSON_UNESCAPED_UNICODE);
	} catch (Exception $e) {
		echo $e->getMessage();
		mld_error($e->getMessage());
		moveToSpecificPage(CONF_URL_ERROR);
		exit;
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo CONF_SITE_TITLE . " > 2인1조 관리"; ?></title>
		
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/bootstrap.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/metisMenu.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/sb-admin-2.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/font-awesome.min.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/dataTables.bootstrap.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/dataTables.responsive.css">
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css_ext/bootstrap-toggle.min.css">
		
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/jquery-2.1.3.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/metisMenu.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/sb-admin-2.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/dataTables.bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js_ext/bootstrap-toggle.min.js"></script>
		
		<link type="text/css" rel="stylesheet" href="<?php echo CONF_PATH_ASSETS; ?>css/main.css?<?php echo time();?>">
		
		<script type="text/javascript" src="<?php echo CONF_PATH_ASSETS; ?>js/board.js?<?php echo time();?>"></script>
		
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
		<script type="text/javascript">
			var LOGIN_URL = "<?php echo CONF_URL_LOGIN; ?>";
			var AJAX_URL = "<?php echo CONF_URL_AJAX; ?>";

			var list_table;
			var board_list = <?php echo $board_list_json; ?>;

			$(function() {
				list_table = $('#board_list').DataTable({
					order: [[ 0, 'desc' ]],
					initComplete: function () {
// 						var html = "";
// 						html += "<label>";
// 						html += "<button type='button' class='btn btn-primary' id='btnCreateBoard'>2인1조 등록</button>";
// 						html += "</label>";
// 						$('#board_list_length').html(html);
						
// 						$('#btnCreateBoard').off("click").on('click', function() {
// 							showBoardDetailPopup(null);
// 						});
					},
					drawCallback: function() {
						$('#board_list > tbody').off("click").on('click', 'tr', function (event) {
							var index = $(this).index("#board_list > tbody > tr");
							
							if($(event.target).is('#board_list > tbody > tr:eq(' + index + ') > td:eq(0),#board_list > tbody > tr:eq(' + index + ') > td:eq(1),#board_list > tbody > tr:eq(' + index + ') > td:eq(2),#board_list > tbody > tr:eq(' + index + ') > td:eq(3),#board_list > tbody > tr:eq(' + index + ') > td:eq(4),#board_list > tbody > tr:eq(' + index + ') > td:eq(5),#board_list > tbody > tr:eq(' + index + ') > td:eq(6),#board_list > tbody > tr:eq(' + index + ') > td:eq(7),#board_list > tbody > tr:eq(' + index + ') > td:eq(8)')) {
								var board_id = $(this).attr("id");
								showBoardDetailPopup(board_list[board_id]);
							}
						});

						$(".btn_delete_board").off("click").on("click", function() {
							var ret = confirm("삭제하시겠습니까?");
							if(ret) {
								var board_id = $(this).parent("td").parent("tr").attr("id");
								console.log(board_id);

								deleteBoard(board_id);
							}
						});
					}
				});

				$.each(board_list, function(key, val) {
					var deleteHtml = "<button type='button' class='btn btn-danger btn-sm btn_delete_board'><i class='fa fa-times'></i></button>";
					
					var new_node = list_table.row.add([
						val.createdDate,
						val.categoryText,
						val.title,
						val.nickName,
// 						val.sex,
// 						val.age,
// 						val.career,
						val.divide,
						val.range,
// 						val.start,
						val.time,
// 						val.target,
// 						val.car,
						val.contact,
// 						val.etc,
						val.commentCount,
						deleteHtml
					]).node();

					$(new_node).attr("id", val.board_id);
				});

				list_table.draw();
			});
		</script>
	</head>
	<body>
		<div id="wrapper">
			<?php include_once CONF_URL_MENU; ?>
			<div id="page-wrapper">
				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">2인1조 관리</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">2인1조 리스트</div>
							<div class="panel-body">
								<div class="dataTable_wrapper">
									<table class="table table-bordered table-hover" id="board_list">
										<thead>
											<tr>
												<th>등록일</th>
												<th>카테고리</th>
												<th>제목</th>
												<th>닉네임</th>
<!-- 												<th>성별</th> -->
<!-- 												<th>나이</th> -->
<!-- 												<th>경력</th> -->
												<th>수입배분</th>
												<th>활동범위</th>
<!-- 												<th>출발지</th> -->
												<th>업무시간</th>
<!-- 												<th>찾는대상</th> -->
<!-- 												<th>보유/희망 차종</th> -->
												<th>연락처</th>
<!-- 												<th>특이사항</th> -->
												<th>댓글</th>
												<th>삭제</th>
											</tr>
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade" id="modal_popup" tabindex="-1" role="dialog" aria-labelledby="modal_popup_label" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="modal_popup_label"></h4>
						</div>
						<div class="modal-body" id="modal_popup_content"></div>
						<div class="modal-footer" id="modal_popup_footer"></div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>