<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
	<ul class="nav navbar-top-links navbar-right">
		<li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#">
				<i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
			</a>
			<ul class="dropdown-menu dropdown-user">
				<li><a href="<?php echo CONF_URL_LOGOUT; ?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
			</ul>
		</li>
	</ul>
	<div class="navbar-header"><a class="navbar-brand" href="<?php echo CONF_URL_ROOT; ?>">달빛기사 관리자</a></div>
	<div class="navbar-default sidebar" role="navigation">
		<div class="sidebar-nav navbar-collapse">
			<ul class="nav" id="side-menu">
				<li>
					<a href="<?php echo CONF_URL_ROOT; ?>"><i class="fa fa-dashboard fa-fw"></i>Home</a>
				</li>
			<?php 
				$permissions = array("account" => array("inquiry"));
				if($session->checkPermission($permissions)) {
			?>
				<li>
					<a href="<?php echo CONF_URL_DRIVER; ?>"><i class="fa fa-dashboard fa-fw"></i>기사 관리 및 푸시 발송</a>
				</li>
			<?php
				}
			?>
				<li>
					<a href="<?php echo CONF_URL_NEAR_STATION; ?>"><i class="fa fa-dashboard fa-fw"></i>기사 위치 및 주변정류장 조회</a>
				</li>
				<li>
					<a href="<?php echo CONF_URL_SHUTTLE; ?>"><i class="fa fa-dashboard fa-fw"></i>셔틀 관리</a>
				</li>
				<li>
					<a href="<?php echo CONF_URL_QUESTION; ?>"><i class="fa fa-dashboard fa-fw"></i>문의사항 관리</a>
				</li>
				<li>
					<a href="<?php echo CONF_URL_BOARD; ?>"><i class="fa fa-dashboard fa-fw"></i>2인1조 관리</a>
				</li>
				<li>
					<a href="<?php echo CONF_URL_TAXI_CARPOOL; ?>"><i class="fa fa-dashboard fa-fw"></i>택시카풀 관리</a>
				</li>
				<li>
					<a href="<?php echo CONF_URL_NOTICE; ?>"><i class="fa fa-dashboard fa-fw"></i>공지사항 관리</a>
				</li>
				<li>
					<a href="<?php echo CONF_URL_CALL; ?>"><i class="fa fa-dashboard fa-fw"></i>콜 관리</a>
				</li>
			<?php 
				$permissions = array("account" => array("inquiry"));
				if($session->checkPermission($permissions)) {
			?>
				<li>
					<a href="<?php echo CONF_URL_ACCOUNT; ?>"><i class="fa fa-dashboard fa-fw"></i>계정 관리</a>
				</li>
			<?php
				}
			?>
			</ul>
		</div>
	</div>
</nav>