function PopupLayer(_opt) {
	this.id = _opt.id ? _opt.id : "popup_layer_" + new Date().getTime();
	this.has_arrow = _opt.has_arrow == true ? true : false;
}

PopupLayer.prototype.createPopup = function() {
	var popup_html = "";
	popup_html += "<div class='popup_layer right' id='" + this.id + "' aria-labelledby='" + this.id + "_label' aria-hidden='true'>";
	if(this.has_arrow == true) {
		popup_html += "<div class='arrow'></div>";
	}
	popup_html += "<div class='modal-content'>";
	popup_html += "<div class='modal-header'>";
	popup_html += "<button type='button' class='close' id='" + this.id + "_close' data-dismiss='modal' aria-hidden='true'>&times;</button>";
	popup_html += "<h5 class='modal-title' id='" + this.id + "_label'></h5>";
	popup_html += "</div>";
	popup_html += "<div class='modal-body' id='" + this.id + "_content'></div>";
	popup_html += "<div class='modal-footer' id='" + this.id + "_footer'></div>";
	popup_html += "</div>";
	popup_html += "</div>";
	
	$("body").append(popup_html);
	
	var self = this;
	$("#" + this.id + "_close").off("click").on("click", function() {
		self.hidePopup();
	});
}

PopupLayer.prototype.initPopup = function() {
	$("#" + this.id + "_label").text("");
	$("#" + this.id + "_content").html("");
	$("#" + this.id + "_footer").html("");
}

PopupLayer.prototype.setStyle = function(_style) {
	$('#' + this.id).css(_style);	
}

PopupLayer.prototype.hidePopup = function() {
	$("#" + this.id).hide();
	
	this.initPopup();
}

PopupLayer.prototype.showPopup = function() {
	$("#" + this.id).show();
}

PopupLayer.prototype.setLabel = function(_label) {
	$("#" + this.id + "_label").text(_label);
}

PopupLayer.prototype.setContent = function(_content) {
	$("#" + this.id + "_content").html(_content);
}

PopupLayer.prototype.setFooter = function(_content) {
	$("#" + this.id + "_footer").html(_content);
}

PopupLayer.prototype.hideFooter = function() {
	$("#" + this.id + "_footer").hide();
}

PopupLayer.prototype.hideHeader = function() {
	$("#" + this.id + "_label").parent("div").hide();
}

PopupLayer.prototype.getContentPadding = function() {
	return parseInt($("#" + this.id + "_content").css("padding").replace("px", ""));
}

PopupLayer.prototype.getHeaderHeight = function() {
	return $("#" + this.id + " > .modal-header").outerHeight(true);
}

PopupLayer.prototype.getFooterHeight = function() {
	return $("#" + this.id + "_footer").outerHeight(true);
}
