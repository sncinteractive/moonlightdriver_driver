function showCallDetailPopup(_call_info) {
	var mode = _call_info ? "edit" : "create";
	
	$("#modal_popup_content").html(getCallDetailContentsHtml());
	
	var footerHtml = "";
	
	if(mode == "edit") {
		$("#start").val(_call_info.start.text);
		$("#through").val(_call_info.through.text);
		$("#end").val(_call_info.end.text);
		$("#price").val(_call_info.money);
		$('input:radio[name=payment_options]:input[value=' + _call_info.payment_option + ']').attr("checked", true);
		$("#etc").val(_call_info.etc);
		$("#customer_phone").val(_call_info.user.realPhone);
		$("#driver_name").val(_call_info.driver.driverId);
		$("#driver_phone").val(_call_info.driver.phone);
		
		$("#modal_popup_label").text("콜 상세보기");

		footerHtml += "<button type='button' id='btnSaveChanges' class='btn btn-primary'>수정</button>";
		
		setCallStatus(_call_info.status);
	} else {
		$("#modal_popup_label").text("콜 추가");
		footerHtml += "<button type='button' id='btnAddCall' class='btn btn-primary'>추가</button>";
		
		setCallStatus();
	}
	
	footerHtml += "<button type='button' class='btn btn-default' data-dismiss='modal'>닫기</button>";
	$("#modal_popup_footer").html(footerHtml);
	
	$("#modal_popup").modal('show');
	
	if(mode == "edit") {
		$('#btnSaveChanges').off("click").on("click", function() {
			editCallInfo(_call_info._id);
		});
	} else {
		$('#btnAddCall').off("click").on("click", function() {
			addCall();
		});
	}
}

function getCallDetailContentsHtml() {
	var html = "";
	
	html += "<div class='row'>";
	html += "<form class='form-horizontal'>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-3 control-label' for='start'>출발지</label>";
	html += "<div class='col-sm-8'><input type='text' class='form-control' id='start'/></div>";
	html += "</div>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-3 control-label' for='through'>경유지</label>";
	html += "<div class='col-sm-8'><input type='text' class='form-control' id='through'/></div>";
	html += "</div>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-3 control-label' for='end'>도착지</label>";
	html += "<div class='col-sm-8'><input type='text' class='form-control' id='end'/></div>";
	html += "</div>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-3 control-label' for='price'>가격</label>";
	html += "<div class='col-sm-8'><input type='text' class='form-control' id='price'/></div>";
	html += "</div>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-3 control-label' for='payment'>결제 방법</label>";
	html += "<div class='col-sm-8'>";
	html += "<div class='radio'>";
	html += "<label>";
	html += "<input type='radio' name='payment_options' id='payment_option_card' value='card'/>카드";
	html += "</label>";
	html += "</div>";
	html += "<div class='radio'>";
	html += "<label>";
	html += "<input type='radio' name='payment_options' id='payment_option_cash' value='cash'/>현금";
	html += "</label>";
	html += "</div>";
	html += "</div>";
	html += "</div>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-3 control-label' for='etc'>기타</label>";
	html += "<div class='col-sm-8'><textarea class='form-control' id='etc' rows='3'></textarea></div>";
	html += "</div>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-3 control-label' for='call_status'>상태</label>";
	html += "<div class='col-sm-8'><select class='form-control' id='call_status'/></div>";
	html += "</div>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-3 control-label' for='customer_phone'>고객 전화</label>";
	html += "<div class='col-sm-8'><input type='text' class='form-control' id='customer_phone' readonly/></div>";
	html += "</div>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-3 control-label' for='driver_name'>기사님 이름</label>";
	html += "<div class='col-sm-8'><input type='text' class='form-control' id='driver_name' readonly/></div>";
	html += "</div>";
	html += "<div class='form-group form-group-md'>";
	html += "<label class='col-sm-3 control-label' for='driver_phone'>기사님 전화</label>";
	html += "<div class='col-sm-8'><input type='text' class='form-control' id='driver_phone' readonly/></div>";
	html += "</div>";
	html += "</form>";
	html += "</div>";
	
	return html;
}

function setCallStatus(_status) {
	var html = "";
	$.each(call_status_list, function(key, val) {
		if(_status != null && _status == key) {
			html += "<option value='" + key + "' selected>" + val.status_text + "</option>";
		} else {
			html += "<option value='" + key + "'>" + val.status_text + "</option>";
		}
	});
	
	$("#call_status").html(html);
}

function addCall() {
	var params = {};
	params.type = "add_call";
	params.start = $("#start").val();
	params.end = $("#end").val();
	params.price = $("#price").val();
	params.payment_option = $(':radio[name="payment_options"]:checked').val();
	params.etc = $("#etc").val();
	params.customer_phone = $("#customer_phone").val();
	params.driver_name = $("#driver_name").val();
	params.driver_phone = $("#driver_phone").val();
		
	$.post(AJAX_URL, params, function(_data) {
		$("#modal_popup").modal('hide');
		
		var ret_data = JSON.parse(_data);
		if(ret_data.result == "OK") {
			call_list[ret_data.data.call_id] = ret_data.data;
			
			var new_node = list_table.row.add([
				ret_data.data.start.text,
				ret_data.data.through.text,
				ret_data.data.end.text,
				ret_data.data.money,
				ret_data.data.createdAt,
				ret_data.data.status_text
			]).draw(false).node();
			
			$(new_node).attr("id", ret_data.data.call_id);
			alert("추가 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = LOGIN_URL;
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function editCallInfo(_call_id) {
	var params = {};
	params.type = "edit_call";
	params.call_id = _call_id;
	params.start = $("#start").val();
	params.end = $("#end").val();
	params.price = $("#price").val();
	params.by_card = $("#by_card").val();
	params.etc = $("#etc").val();
	params.customer_phone = $("#customer_phone").val();
	params.driver_name = $("#driver_name").val();
	params.driver_phone = $("#driver_phone").val();
		
	$.post(AJAX_URL, params, function(_data) {
		$("#modal_popup").modal('hide');
		
		var ret_data = JSON.parse(_data);
		if(ret_data.result == "OK") {
			call_list[_call_id] = ret_data.data;
			
			list_table.row($('#' + _call_id)).data([
				call_list[_call_id].start.text,
				call_list[_call_id].through.text,
				call_list[_call_id].end.text,
				call_list[_call_id].money,
				call_list[_call_id].createdAt,
				call_list[_call_id].status_text
			]).draw(false);
			
			alert("수정 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = LOGIN_URL;
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}
