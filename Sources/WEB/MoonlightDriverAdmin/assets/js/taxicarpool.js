function showTaxiCarpoolDetailPopup(_data) {
	var mode = _data ? "edit" : "create";
	
	if(mode == "edit") {
		$("#modal_popup_label").text("택시카풀 상세보기");
	} else {
		$("#modal_popup_label").text("택시카풀 등록");
	}
	
	$("#modal_popup_content").html(getTaxiCarpoolContentsHtml(mode));
	
	var footerHtml = "";
	
	if(mode == "edit") {
		$("#isCatch").val(_data.isCatch == false ? 1 : 2);
		$("#startAddress").val(_data.start.text);
		$("#endAddress").val(_data.end.text);
		$("#money").val(_data.money);
		$("#driverCount").val(_data.drivers.max);
		$("#time").val(_data.time);
		
		if(_data.isPush == true) {
			$("#isPush").prop("checked", "checked");	
		} else {
			$("#isPush").prop("checked", "");
		}
		
		
		footerHtml += "<button type='button' id='btnEdit' class='btn btn-primary'>수정</button>";
	} else {
		footerHtml += "<button type='button' id='btnAdd' class='btn btn-primary'>등록</button>";
	}
	
	footerHtml += "<button type='button' class='btn btn-default' data-dismiss='modal'>닫기</button>";
	$("#modal_popup_footer").html(footerHtml);
	
	$("#modal_popup").modal('show');
	
	if(mode == "edit") {
		$("#btnEdit").off("click").on("click", function() {
			editTaxiCarpool(_data);
		});
	} else {
		$("#btnAdd").off("click").on("click", function() {
			addTaxiCarpool();
		});
	}
}

function getTaxiCarpoolContentsHtml(_mode) {
	var html = "";
	
	html += "<div class='row'>";
	html += "	<form class='form-horizontal'>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='isCatch'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>구분</label>";
	html += "			<div class='col-sm-9'>";
	html += "				<select class='form-control' id='isCatch'>";
	html += "					<option value=''>선택...</option>";
	html += "					<option value='1'>픽업요청</option>";
	html += "					<option value='2'>픽업가능</option>";
	html += "				</select>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='startAddress'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>출발지</label>";
	html += "			<div class='col-sm-9'><input type='text' class='form-control' id='startAddress'/></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='endAddress'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>도착지</label>";
	html += "			<div class='col-sm-9'><input type='text' class='form-control' id='endAddress'/></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='money'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>요금</label>";
	html += "			<div class='col-sm-9'><input type='text' class='form-control' id='money' value='3000'/></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='driverCount'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>모집인원</label>";
	html += "			<div class='col-sm-9'>";
	html += "				<select class='form-control' id='driverCount'>";
	html += "					<option value='2'>2</option>";
	html += "					<option value='3'>3</option>";
	html += "					<option value='4'>4</option>";
	html += "				</select>";
	html += "			</div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='time'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>출발예정</label>";
	html += "			<div class='col-sm-9'><input type='text' class='form-control' id='time'/></div>";
	html += "		</div>";
	html += "		<div class='form-group form-group-md'>";
	html += "			<label class='col-sm-3 control-label' for='isPush'><span style='font-size: 18px; vertical-align: middle; font-weight: bold; color: red;'>* </span>주변기사 푸시</label>";
	html += "			<div class='col-sm-9'><input type='checkbox' class='form-control' id='isPush'/></div>";
	html += "		</div>";
	html += "		<div class='form-group'>";
	html += "			<label>";
	html += "				<input type='checkbox' class='minimal'>Minimal skin checkbox";
	html += "			</label>";
	html += "		</div>";
	html += "	</form>";
	html += "</div>";
	
	return html;
}

function editTaxiCarpool(_taxicarpool_info) {
	var params = {};
	params.type = "edit_taxi_carpool";
	params.taxicarpool_id = _taxicarpool_info.taxicarpool_id;
	params.category = $.trim($("#category").val());
	params.title = $.trim($("#title").val());
	params.nickName = $.trim($("#nickName").val());
	params.sex = $.trim($("#sex").val());
	params.age = $.trim($("#age").val());
	params.career = $.trim($("#career").val());
	params.divide = $.trim($("#divide").val());
	params.range = $.trim($("#range").val());
	params.start = $.trim($("#start").val());
	params.time = $.trim($("#time").val());
	params.target = $.trim($("#target").val());
	params.car = $.trim($("#car").val());
	params.contact = $.trim($("#contact").val());
	params.etc = $.trim($("#etc").val());
	
	if(params.category.length <= 0) {
		alert("카테고리를 선택해주세요.");
		return;
	}
	
	if(params.title.length <= 0) {
		alert("제목을 입력해주세요.");
		return;
	}
	
	if(params.nickName.length <= 0) {
		alert("닉네임을 입력해주세요.");
		return;
	}
	
	if(params.sex.length <= 0) {
		alert("성별을 입력해주세요.");
		return;
	}
	
	if(params.age.length <= 0) {
		alert("나이을 입력해주세요.");
		return;
	}
	
	if(params.career.length <= 0) {
		alert("경력을 입력해주세요.");
		return;
	}
	
	if(params.divide.length <= 0) {
		alert("수입배분을 입력해주세요.");
		return;
	}
	
	if(params.range.length <= 0) {
		alert("활동범위를 입력해주세요.");
		return;
	}
	
	if(params.start.length <= 0) {
		alert("출발지를 입력해주세요.");
		return;
	}
	
	if(params.time.length <= 0) {
		alert("업무시간을 입력해주세요.");
		return;
	}
	
	if(params.target.length <= 0) {
		alert("찾는대상을 입력해주세요.");
		return;
	}
	
	if(params.car.length <= 0) {
		alert("보유/희망 차종을 입력해주세요.");
		return;
	}
	
	if(params.contact.length <= 0) {
		alert("연락처를 입력해주세요.");
		return;
	}
	
	$.post(AJAX_URL, params, function(_ret_data) {
		$("#modal_popup").modal('hide');
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			var deleteHtml = "<button type='button' class='btn btn-danger btn-sm btn_delete_board'><i class='fa fa-times'></i></button>";

			board_list[_taxicarpool_info.board_id].category = ret_data.data.category;
			board_list[_taxicarpool_info.board_id].title = ret_data.data.title;
			board_list[_taxicarpool_info.board_id].nickName = ret_data.data.nickName;
			board_list[_taxicarpool_info.board_id].sex = ret_data.data.sex;
			board_list[_taxicarpool_info.board_id].age = ret_data.data.age;
			board_list[_taxicarpool_info.board_id].career = ret_data.data.career;
			board_list[_taxicarpool_info.board_id].divide = ret_data.data.divide;
			board_list[_taxicarpool_info.board_id].range = ret_data.data.range;
			board_list[_taxicarpool_info.board_id].start = ret_data.data.start;
			board_list[_taxicarpool_info.board_id].time = ret_data.data.time;
			board_list[_taxicarpool_info.board_id].target = ret_data.data.target;
			board_list[_taxicarpool_info.board_id].car = ret_data.data.car;
			board_list[_taxicarpool_info.board_id].contact = ret_data.data.contact;
			board_list[_taxicarpool_info.board_id].etc = ret_data.data.etc;
			board_list[_taxicarpool_info.board_id].createdDate = ret_data.data.createdDate;
			
			list_table.row($('#' + _taxicarpool_info.board_id)).data([
				board_list[_taxicarpool_info.board_id].createdDate,
				board_list[_taxicarpool_info.board_id].category,
				board_list[_taxicarpool_info.board_id].title,
				board_list[_taxicarpool_info.board_id].nickName,
//				board_list[_taxicarpool_info.board_id].sex,
//				board_list[_taxicarpool_info.board_id].age,
//				board_list[_taxicarpool_info.board_id].career,
				board_list[_taxicarpool_info.board_id].divide,
				board_list[_taxicarpool_info.board_id].range,
//				board_list[_taxicarpool_info.board_id].start,
				board_list[_taxicarpool_info.board_id].time,
//				board_list[_taxicarpool_info.board_id].target,
//				board_list[_taxicarpool_info.board_id].car,
				board_list[_taxicarpool_info.board_id].contact,
//				board_list[_taxicarpool_info.board_id].etc,
				board_list[_taxicarpool_info.board_id].commentCount,
				deleteHtml
			]).draw(false);
			
			alert("수정 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function addTaxiCarpool() {
	var params = {};
	params.type = "add_board";
	params.category = $.trim($("#category").val());
	params.title = $.trim($("#title").val());
	params.nickName = $.trim($("#nickName").val());
	params.sex = $.trim($("#sex").val());
	params.age = $.trim($("#age").val());
	params.career = $.trim($("#career").val());
	params.divide = $.trim($("#divide").val());
	params.range = $.trim($("#range").val());
	params.start = $.trim($("#start").val());
	params.time = $.trim($("#time").val());
	params.target = $.trim($("#target").val());
	params.car = $.trim($("#car").val());
	params.contact = $.trim($("#contact").val());
	params.etc = $.trim($("#etc").val());
	
	if(params.category.length <= 0) {
		alert("카테고리를 선택해주세요.");
		return;
	}
	
	if(params.title.length <= 0) {
		alert("제목을 입력해주세요.");
		return;
	}
	
	if(params.nickName.length <= 0) {
		alert("닉네임을 입력해주세요.");
		return;
	}
	
	if(params.sex.length <= 0) {
		alert("성별을 입력해주세요.");
		return;
	}
	
	if(params.age.length <= 0) {
		alert("나이을 입력해주세요.");
		return;
	}
	
	if(params.career.length <= 0) {
		alert("경력을 입력해주세요.");
		return;
	}
	
	if(params.divide.length <= 0) {
		alert("수입배분을 입력해주세요.");
		return;
	}
	
	if(params.range.length <= 0) {
		alert("활동범위를 입력해주세요.");
		return;
	}
	
	if(params.start.length <= 0) {
		alert("출발지를 입력해주세요.");
		return;
	}
	
	if(params.time.length <= 0) {
		alert("업무시간을 입력해주세요.");
		return;
	}
	
	if(params.target.length <= 0) {
		alert("찾는대상을 입력해주세요.");
		return;
	}
	
	if(params.car.length <= 0) {
		alert("보유/희망 차종을 입력해주세요.");
		return;
	}
	
	if(params.contact.length <= 0) {
		alert("연락처를 입력해주세요.");
		return;
	}
	
	$.post(AJAX_URL, params, function(_ret_data) {
		$("#modal_popup").modal('hide');
		
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			var board_id = ret_data.data.taxicarpool_id;
			board_list[board_id] = ret_data.data;
			
			var deleteHtml = "<button type='button' class='btn btn-danger btn-sm btn_delete_board'><i class='fa fa-times'></i></button>";

			var new_node = list_table.row.add([
				board_list[board_id].createdDate,
				board_list[board_id].category,
				board_list[board_id].title,
				board_list[board_id].nickName,
//				board_list[board_id].sex,
//				board_list[board_id].age,
//				board_list[board_id].career,
				board_list[board_id].divide,
				board_list[board_id].range,
//				board_list[board_id].start,
				board_list[board_id].time,
//				board_list[board_id].target,
//				board_list[board_id].car,
				board_list[board_id].contact,
				board_list[board_id].etc,
				board_list[board_id].commentCount,
				deleteHtml
			]).draw(false).node();
			
			$(new_node).attr("id", board_id);
			
			alert("등록 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function deleteTaxiCarpool(_taxicarpool_id) {
	var params = {};
	params.type = "delete_taxi_carpool";
	params.taxicarpool_id = _taxicarpool_id;
	
	$.post(AJAX_URL, params, function(_ret_data) {
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			list_table.row($('#' + _taxicarpool_id)).remove().draw(false);
			alert("삭제 되었습니다.");
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}
