function sendAjaxPost(_params, _callback) {
	$.post("/ajax/ajax.php", _params, function(_ret_data) {
		var ret_data = JSON.parse(_ret_data);
		if(ret_data.result == "OK") {
			_callback(ret_data.data);
		} else if(ret_data.result == "NOT_LOGIN" || ret_data.result == "PERMISSION_DENIED") {
			alert(ret_data.msg);
			location.href = "/";
		} else {
			alert(ret_data.msg);
		}
	}, "text");
}

function getDateTimeString(_date) {
	var dateTimeString = "";
	var date = new Date(_date);
	
	var fullYear = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	var hours = date.getHours();
	hours = hours < 10 ? '0' + hours : hours;
	var minutes = date.getMinutes();
	minutes = minutes < 10 ? '0' + minutes : minutes;
	var seconds = date.getSeconds();
	seconds = seconds < 10 ? '0' + seconds : seconds;
	
	dateTimeString = fullYear + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds;
	
	return dateTimeString;
}
