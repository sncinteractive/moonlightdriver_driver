<?php
	/**
	 * URL
	 */
	define('CONF_URL_INDEX',		CONF_URL_ROOT . "index.php");
	define('CONF_URL_ERROR',		CONF_URL_ROOT . "error.php");
	define('CONF_URL_AJAX',			CONF_URL_ROOT . "ajax/ajax.php");
	
	define('CONF_URL_MENU',			CONF_PATH_ROOT . "menu.php");
	
	define('CONF_URL_LOGIN_ROOT',	CONF_URL_ROOT . "login/");
	define('CONF_URL_LOGIN',		CONF_URL_LOGIN_ROOT . "login.php");
	define('CONF_URL_LOGOUT',		CONF_URL_LOGIN_ROOT . "logout.php");
	define('CONF_URL_LOGIN_ACTION',	CONF_URL_LOGIN_ROOT . "login_action.php");
	
	define('CONF_URL_PAGE_ROOT',	CONF_URL_ROOT . "pages/");
	define('CONF_URL_NEAR_STATION',	CONF_URL_PAGE_ROOT . "near_station.php");
	define('CONF_URL_SHUTTLE',		CONF_URL_PAGE_ROOT . "shuttle.php");
	define('CONF_URL_CALL',			CONF_URL_PAGE_ROOT . "call.php");
	define('CONF_URL_BOARD',		CONF_URL_PAGE_ROOT . "board.php");
	define('CONF_URL_TAXI_CARPOOL',	CONF_URL_PAGE_ROOT . "taxicarpool.php");
	define('CONF_URL_QUESTION',		CONF_URL_PAGE_ROOT . "question.php");
	define('CONF_URL_NOTICE',		CONF_URL_PAGE_ROOT . "notice.php");
	define('CONF_URL_ACCOUNT',		CONF_URL_PAGE_ROOT . "account.php");
	define('CONF_URL_DRIVER',		CONF_URL_PAGE_ROOT . "driver.php");
	
	define('MAX_PUS_MESSAGE_LENGTH',	4000);
	define('DEFAULT_PAGE_ITEM_COUNT',	10);
	define('DEFAULT_NAVI_PAGE',	10);
	define('DEFAULT_TIME_PER_METER', 0.093115342964555);
	
	define("CONF_SITE_TITLE", "달빛기사 :: 관리자 페이지");
	
	define("GOOGLE_API_KEY", "AIzaSyDgKpZDI_xkyqEQtE-F6hWpdnkLOPXofRA");
	define("GOOGLE_SERVER_KEY", "AIzaSyDb5DTLNl-uMS820Bozj5S1AVohg1g_uHY");
	
	$SHUTTLE_CATEGORY = array();
	$SHUTTLE_CATEGORY[1] = array("index" => 1, "category_name" => "김포/사우동");
	$SHUTTLE_CATEGORY[2] = array("index" => 2, "category_name" => "고양/파주");
	$SHUTTLE_CATEGORY[3] = array("index" => 3, "category_name" => "의정부/양주");
	$SHUTTLE_CATEGORY[4] = array("index" => 4, "category_name" => "구리/남양주");
	$SHUTTLE_CATEGORY[5] = array("index" => 5, "category_name" => "인천/부천");
	$SHUTTLE_CATEGORY[6] = array("index" => 6, "category_name" => "안양/안산");
	$SHUTTLE_CATEGORY[7] = array("index" => 7, "category_name" => "수원/성남");
	$SHUTTLE_CATEGORY[8] = array("index" => 8, "category_name" => "강동/하남");
	$SHUTTLE_CATEGORY[9] = array("index" => 9, "category_name" => "합정");
	$SHUTTLE_CATEGORY[10] = array("index" => 10, "category_name" => "연신내/수유");
	$SHUTTLE_CATEGORY[11] = array("index" => 11, "category_name" => "노원/태릉");
	$SHUTTLE_CATEGORY[12] = array("index" => 12, "category_name" => "구로/화곡");
	$SHUTTLE_CATEGORY[13] = array("index" => 13, "category_name" => "교보");
	$SHUTTLE_CATEGORY[14] = array("index" => 14, "category_name" => "대구/칠곡");
	$SHUTTLE_CATEGORY[15] = array("index" => 15, "category_name" => "대구/두류");
	$SHUTTLE_CATEGORY[16] = array("index" => 16, "category_name" => "대구/다사-서재-다사");
	$SHUTTLE_CATEGORY[17] = array("index" => 17, "category_name" => "대구/대곡");
	$SHUTTLE_CATEGORY[18] = array("index" => 18, "category_name" => "대구/사수동-세천-사수동");
	$SHUTTLE_CATEGORY[19] = array("index" => 19, "category_name" => "경산-범어-경산");
	$SHUTTLE_CATEGORY[20] = array("index" => 20, "category_name" => "경산-대구범물-경산");
	$SHUTTLE_CATEGORY[21] = array("index" => 21, "category_name" => "경산/사동");
	$SHUTTLE_CATEGORY[22] = array("index" => 22, "category_name" => "대구/성서");
	$SHUTTLE_CATEGORY[23] = array("index" => 23, "category_name" => "대구/범어");
	$SHUTTLE_CATEGORY[24] = array("index" => 24, "category_name" => "대구/반야월");
	$SHUTTLE_CATEGORY[25] = array("index" => 25, "category_name" => "대구/송정");
	$SHUTTLE_CATEGORY[26] = array("index" => 26, "category_name" => "대구/화원");
	$SHUTTLE_CATEGORY[27] = array("index" => 27, "category_name" => "대구/죽전");
	$SHUTTLE_CATEGORY[28] = array("index" => 28, "category_name" => "경산/다산");
	$SHUTTLE_CATEGORY[29] = array("index" => 29, "category_name" => "북대구/서변");
	$SHUTTLE_CATEGORY[30] = array("index" => 30, "category_name" => "북대구/지묘");
	$SHUTTLE_CATEGORY[31] = array("index" => 31, "category_name" => "대구/가창");
	$SHUTTLE_CATEGORY[32] = array("index" => 32, "category_name" => "경산/진량");
	$SHUTTLE_CATEGORY[33] = array("index" => 33, "category_name" => "경산/대구대");
	$SHUTTLE_CATEGORY[34] = array("index" => 34, "category_name" => "부산");
	
	$CALL_STATUS = array();
	$CALL_STATUS["wait"] 		= array("status" => "wait", "status_text" => "배차대기");
	$CALL_STATUS["dispatched"]	= array("status" => "dispatched", "status_text" => "배차완료");
	$CALL_STATUS["started"]		= array("status" => "started", "status_text" => "출발");
	$CALL_STATUS["finished"]	= array("status" => "finished", "status_text" => "도착");
	$CALL_STATUS["done"]		= array("status" => "done", "status_text" => "운행종료");
	$CALL_STATUS["canceled"]	= array("status" => "canceled", "status_text" => "취소");
	$CALL_STATUS["error"]		= array("status" => "error", "status_text" => "오류");
	
	$ROLE = array(
		1 => "admin",
		2 => "manager",
		3 => "counsel_manager"
	);
	
	$ROLE_LEVEL = array(
		"account" => array("inquiry" => 1, "create" => 1, "modify" => 1, "delete" => 1),
		"shuttle" => array("inquiry" => 2, "create" => 1, "modify" => 1, "delete" => 1), 
		"driver" => array("inquiry" => 2, "create" => 2, "modify" => 2, "delete" => 2),
		"question" => array("inquiry" => 3, "create" => 3, "modify" => 3, "delete" => 3),
		"notice" => array("inquiry" => 3, "create" => 3, "modify" => 3, "delete" => 3),
		"call" => array("inquiry" => 3, "create" => 3, "modify" => 3, "delete" => 3),
		"board" => array("inquiry" => 3, "create" => 3, "modify" => 3, "delete" => 3),
		"taxicarpool" => array("inquiry" => 3, "create" => 3, "modify" => 3, "delete" => 3)
	);
?>