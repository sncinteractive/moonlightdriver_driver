<?php
	session_start();

	/**
	 * DATABASE
	 */
	define("DEF_MONGO_IP",		"mongodb://115.68.13.216");
	define("DEF_MONGO_PORT",	"");
	define("DEF_MONGO_USER",	"kjccjk");
	define("DEF_MONGO_PASS",	"c9a61056608b20e4c667e5af4b5d503060471fbd");
	define("DEF_MONGO_DB",		"dev_moonlight");
	
	/**
	 * PATH
	 */
// 	define('CONF_SERVER_HOST',		"http://" . $_SERVER['HTTP_HOST']);
	
	define('CONF_URL_ROOT',			"/");
	define('CONF_PATH_ASSETS', 		CONF_URL_ROOT . "assets/");
	
	define("CONF_PATH_ROOT",		$_SERVER["DOCUMENT_ROOT"] . "/");
	define("CONF_PATH_CLASS",		CONF_PATH_ROOT . "inc/classes/");
	define('CONF_PATH_DEBUG_FILE',	CONF_PATH_ROOT . "logs/");
	define('CONF_PATH_DATA',		CONF_PATH_ROOT . "data/");
	define('CONF_PATH_UPLOAD',		CONF_PATH_ROOT . "upload/");
	
	/**
	 * DEBUG
	 */
	define('CONF_DEBUG',			true);
	define('CONF_DEBUG_INFO',		true);
	define('CONF_DEBUG_ERROR',		true);
	define('CONF_DEBUG_DISPLAY',	false);
	define('CONF_DEBUG_SAVE',		true);
	define('CONF_DEBUG_SAVE_FILE_ERROR',	'mld_debug_mesg_error.log');
	define('CONF_DEBUG_SAVE_FILE_ETC',		'mld_debug_mesg_etc.log');
	
	/**
	 * DEFINE
	 */
	include_once('config_master.php');
	
	/**
	 * GLOBAL FUNCTIONS
	 */
	include_once("global_function.php");
?>