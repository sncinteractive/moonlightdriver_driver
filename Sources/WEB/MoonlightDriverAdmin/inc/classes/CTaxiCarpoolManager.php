<?php 
	class CTaxiCarpoolManager {
		function CTaxiCarpoolManager() {}
		
		function getTaxiCarpoolList($_taxi_carpools) {
			$taxi_carpool_list = $_taxi_carpools->find()->sort(array('_id' => -1));
		
			$ret_taxi_carpool_list = array();
			foreach ($taxi_carpool_list as $row) {
				$taxicarpool_id = strval($row["_id"]);
		
				$row["taxicarpool_id"] = $taxicarpool_id;
				$row["driverId"] = $row["driverId"];
				$row["registrant"] = $row["registrant"];
				$row["isValid"] = $row["isValid"];
				$row["isCatch"] = $row["isCatch"];
				$row["isCatchText"] = $row["isCatch"] ? "픽업가능" : "픽업요청";
				$row["isPush"] = $row["isPush"];
				$row["money"] = $row["money"];
				$row["host"] = $row["host"];
				$row["time"] = date("Y-m-d H:i:s", ($row["time"] / 1000));
				$row["divideN"] = $row["divideN"];
				$row["drivers"] = $row["drivers"];
				$row["end"] = $row["end"];
				$row["start"] = $row["start"];
				
				$validCount = 0;
				foreach ($row["drivers"]["list"] as $key => $val) {
					if($val["isAccept"] == true && $val["status"] == 'accept') {
						$validCount++;
					}
				}
				
				$row["validCount"] = $validCount;
		
				$ret_taxi_carpool_list[$taxicarpool_id] = $row;
			}
				
			return $ret_taxi_carpool_list;
		}
		
		function getTaxiCarpoolInfo($_taxi_carpools, $_taxicarpool_id) {
			$ret_taxi_carpool_info = $_taxi_carpools->findOne(array('_id' => new MongoId($_taxicarpool_id)));
		
			if(isset($ret_taxi_carpool_info)) {
				$taxicarpool_id = strval($ret_taxi_carpool_info["_id"]);
		
				$ret_taxi_carpool_info["taxicarpool_id"] = $taxicarpool_id;
				$ret_taxi_carpool_info["time"] = date("Y-m-d H:i:s", ($ret_taxi_carpool_info["time"] / 1000));
			}
		
			return $ret_taxi_carpool_info;
		}
		
		function addTaxiCarpool($_taxi_carpools, $_driverId, $_nickName, $_title, $_category, $_sex, $_age, $_career, $_divide, $_range, $_start, $_time, $_target, $_car, $_contact, $_etc) {
			$newData = array(
				'driverId' => $_driverId,
				'nickName' => $_nickName,
				'title' => $_title,
				'category' => $_category,
				'sex' => $_sex,
				'age' => $_age,
				'career' => $_career,
				'divide' => $_divide,
				'range' => $_range,
				'start' => $_start,
				'time' => $_time,
				'target' => $_target,
				'car' => $_car,
				'contact' => $_contact,
				'etc' => $_etc,
				'comment' => array(),
				'createdDate' => time() * 1000
			);
				
			$ret = $_taxi_carpools->insert($newData);
		
			if(isset($newData["_id"])) {
				global $ROLE;
		
				$newData["taxicarpool_id"] = strval($newData["_id"]);
				$ret_taxi_carpool_info["commentCount"] = count($ret_taxi_carpool_info["comment"]);
				$newData["createdDate"] = date("Y-m-d H:i:s", ($newData["createdDate"] / 1000));
			}
		
			return $newData;
		}
		
		function updateTaxiCarpool($_taxi_carpools, $_taxicarpool_id, $_registrant, $_isValid, $_isCatch, $_isPush, $_money, $_host, $_time, $_divideN, $_drivers, $_end, $_start) {
			$updateData = array(
				'registrant' => $_registrant,
				'isValid' => $_isValid,
				'isCatch' => $_isCatch,
				'isPush' => $_isPush,
				'money' => $_money,
				'host' => $_host,
				'time' => $_time,
				'divideN' => $_divideN,
				'drivers' => $_drivers,
				'end' => $_end,
				'start' => $_start
			);
				
			$ret = $_taxi_carpools->update(array('_id' => new MongoId($_taxicarpool_id)), array('$set' => $updateData));
		
			return $this->getTaxiCarpoolInfo($_taxi_carpools, $_taxicarpool_id);
		}
		
		function deleteTaxiCarpool($_taxi_carpools, $_taxicarpool_id) {
			$_taxi_carpools->remove(array("_id" => new MongoId($_taxicarpool_id)));
		}
	}
?>