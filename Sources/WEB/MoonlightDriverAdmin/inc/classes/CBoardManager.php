<?php 
	class CBoardManager {
		function CBoardManager() {}
		
		function getBoardList($_boards) {
			$board_list = $_boards->find()->sort(array('createdDate' => -1));
				
			$ret_board_list = array();
			foreach ($board_list as $row) {
				$board_id = strval($row["_id"]);
		
				$row["board_id"] = $board_id;
				$row["driverId"] = $row["driverId"];
				$row["nickName"] = $row["nickName"];
				$row["title"] = $row["title"];
				$row["category"] = $row["category"];
				$row["categoryText"] = $row["category"] == "find" ? "꽁지구함" : "꽁지지원";
				$row["sex"] = $row["sex"];
				$row["age"] = $row["age"];
				$row["career"] = $row["career"];
				$row["divide"] = $row["divide"];
				$row["range"] = $row["range"];
				$row["start"] = $row["start"];
				$row["time"] = $row["time"];
				$row["target"] = $row["target"];
				$row["car"] = $row["car"];
				$row["contact"] = $row["contact"];
				$row["etc"] = $row["etc"];
				$row["comment"] = $row["comment"];
				$row["commentCount"] = count($row["comment"]);
				$row["createdDate"] = date("Y-m-d H:i:s", ($row["createdDate"] / 1000));
		
				$ret_board_list[$board_id] = $row;
			}
			
			return $ret_board_list;
		}
		
		function getBoardInfo($_boards, $_board_id) {
			$ret_board_info = $_boards->findOne(array('_id' => new MongoId($_board_id)));
				
			if(isset($ret_board_info)) {
				$board_id = strval($ret_board_info["_id"]);
				
				$ret_board_info["board_id"] = $board_id;
				$ret_board_info["commentCount"] = count($ret_board_info["comment"]);
				$ret_board_info["createdDate"] = date("Y-m-d H:i:s", ($ret_board_info["createdDate"] / 1000));
			}
				
			return $ret_board_info;
		}
		
		function addBoard($_boards, $_driverId, $_nickName, $_title, $_category, $_sex, $_age, $_career, $_divide, $_range, $_start, $_time, $_target, $_car, $_contact, $_etc) {
			$newData = array(
				'driverId' => $_driverId,
				'nickName' => $_nickName,
				'title' => $_title,
				'category' => $_category,
				'sex' => $_sex,
				'age' => $_age,
				'career' => $_career,
				'divide' => $_divide,
				'range' => $_range,
				'start' => $_start,
				'time' => $_time,
				'target' => $_target,
				'car' => $_car,
				'contact' => $_contact,
				'etc' => $_etc,
				'comment' => array(),
				'createdDate' => time() * 1000
			);
			
			$ret = $_boards->insert($newData);
				
			if(isset($newData["_id"])) {
				$newData["board_id"] = strval($newData["_id"]);
				$ret_board_info["commentCount"] = count($ret_board_info["comment"]);
				$newData["createdDate"] = date("Y-m-d H:i:s", ($newData["createdDate"] / 1000));
			}
				
			return $newData;
		}
		
		function updateBoard($_boards, $_board_id, $_nickName, $_title, $_category, $_sex, $_age, $_career, $_divide, $_range, $_start, $_time, $_target, $_car, $_contact, $_etc) {
			$updateData = array(
				'nickName' => $_nickName,
				'title' => $_title,
				'category' => $_category,
				'sex' => $_sex,
				'age' => $_age,
				'career' => $_career,
				'divide' => $_divide,
				'range' => $_range,
				'start' => $_start,
				'time' => $_time,
				'target' => $_target,
				'car' => $_car,
				'contact' => $_contact,
				'etc' => $_etc
			);
			
			$ret = $_boards->update(array('_id' => new MongoId($_board_id)), array('$set' => $updateData));
				
			return $this->getBoardInfo($_boards, $_board_id);
		}
		
		function addComment($_boards, $_board_id, $_comment) {
			$boardInfo = $this->getBoardInfo($_boards, $_board_id);
			if(isset($boardInfo)) {
				return false;
			}
			
			$boardInfo['comment'][] = $_comment;
			
			$updateData = array(
				'comment' => $boardInfo['comment']
			);
				
			$ret = $_boards->update(array('_id' => new MongoId($_board_id)), array('$set' => $updateData));
		
			return $boardInfo;
		}
		
		function deleteBoard($_boards, $_board_id) {
			$_boards->remove(array("_id" => new MongoId($_board_id)));
		}
	}
?>