<?php
	class CShuttleManager {		
		function CShuttleManager() {}
		
		function addNewShuttleInfo($_shuttleinfos, $_name, $_category, $_desc, $_lineColor, $_routeType, $_phone, $_firstTime, $_lastTime, $_saturdayFirstTime, $_saturdayLastTime, $_sundayFirstTime, $_sundayLastTime, 
									$_runFirstTime, $_runLastTime, $_saturdayRunFirstTime, $_saturdayRunLastTime, $_sundayRunFirstTime, $_sundayRunLastTime, $_holidayDate, $_term, $_totalTime, $_ynEnabled, 
									$_pointCount) {
			$newData = array(
				"name" => $_name,
				"category" => $_category,
				"desc" => $_desc,
				"routeType" => $_routeType,
				"phone" => $_phone,
				"firstTime" => convertShuttleTime($_firstTime),
				"lastTime" => convertShuttleTime($_lastTime),
				"saturdayFirstTime" => convertShuttleTime($_saturdayFirstTime),
				"saturdayLastTime" => convertShuttleTime($_saturdayLastTime),
				"sundayFirstTime" => convertShuttleTime($_sundayFirstTime),
				"sundayLastTime" => convertShuttleTime($_sundayLastTime),
				"runFirstTime" => convertShuttleTime($_runFirstTime),
				"runLastTime" => convertShuttleTime($_runLastTime),
				"saturdayRunFirstTime" => convertShuttleTime($_saturdayRunFirstTime),
				"saturdayRunLastTime" => convertShuttleTime($_saturdayRunLastTime),
				"sundayRunFirstTime" => convertShuttleTime($_sundayRunFirstTime),
				"sundayRunLastTime" => convertShuttleTime($_sundayRunLastTime),
				"holidayDate" => $_holidayDate,
				"term" => intval($_term),
				"totalTime" => intval($_totalTime),
				"ynEnabled" => $_ynEnabled,
				"pointCount" => intval($_pointCount),
				"lineColor" => $_lineColor
			);
			
			$ret = $_shuttleinfos->insert($newData);
			
			return $newData;
		}
		
		function addNewShuttleInfoPoints($_shuttlepoints, $_shuttleId, $_firstTime, $_lastTime, $_saturdayFirstTime, $_saturdayLastTime, $_sundayFirstTime, $_sundayLastTime, $_totalTime, $_new_points_list, $_ynEnabled) {
			$timePerMeter = getCalculatedTimesPerMeter($_new_points_list, $_totalTime);
			
			$prev_first_time = $_firstTime . ":00";
			$prev_last_time = $_lastTime . ":00";
			$prev_saturday_first_time = "";
			$prev_saturday_last_time = "";
			$prev_sunday_first_time = "";
			$prev_sunday_last_time = "";
			
			if(isset($_saturdayFirstTime) && !empty($_saturdayFirstTime)) {
				$prev_saturday_first_time = $_saturdayFirstTime . ":00";
			}
			
			if(isset($_saturdayLastTime) && !empty($_saturdayLastTime)) {
				$prev_saturday_last_time = $_saturdayLastTime . ":00";
			}
			
			if(isset($_sundayFirstTime) && !empty($_sundayFirstTime)) {
				$prev_sunday_first_time = $_sundayFirstTime . ":00";
			}
			
			if(isset($_sundayLastTime) && !empty($_sundayLastTime)) {
				$prev_sunday_last_time = $_sundayLastTime . ":00";
			}
			
			$prev_lat = null;
			$prev_lng = null;
			$accumulate_dist = 0.0;
			
			foreach($_new_points_list as $lists) {
				$lat = $lists["locations"]["coordinates"][1];
				$lng = $lists["locations"]["coordinates"][0];
				
				$increase_time = 0;
				if($prev_lat != null) {
					$dist = getDistance($prev_lat, $prev_lng, $lat, $lng);
					$accumulate_dist += $dist;
					$increase_time = doubleval($accumulate_dist * $timePerMeter);
				}
				
				$first_time = date("H:i:s", strtotime(date("Ymd") . " " . $prev_first_time) + $increase_time);
				$last_time = date("H:i:s", strtotime(date("Ymd") . " " . $prev_last_time) + $increase_time);
				
				$arsId = (str_replace(".", "", sprintf("%2.10f", $lat)) . str_replace(".", "", sprintf("%3.10f", $lng)));
				
				$shuttle_points_doc = array(
					"busId" => $_shuttleId,
					"index" => intval($lists['index']),
					"text" => $lists['text'],
					"arsId" => $arsId,
					"firstTime" => convertShuttleTime(substr($first_time, 0, 5)),
					"lastTime" => convertShuttleTime(substr($last_time, 0, 5)),
					"saturdayFirstTime" => "",
					"saturdayLastTime" => "",
					"sundayFirstTime" => "",
					"sundayLastTime" => "",
					"exceptOutback" => intval($lists['exceptOutback']),
					"isTransfer" => intval($lists['isTransfer']),
					"transferName" => $lists['transferName'],
					"locations" => array("type" => "Point", "coordinates" => array((double)$lng, (double)$lat)),
					"ynEnabled" => $_ynEnabled,
					"createdAt" => new MongoDate()
				);
				
				if(isset($prev_saturday_first_time) && !empty($prev_saturday_first_time)) {
					$saturday_first_time = date("H:i:s", strtotime(date("Ymd") . " " . $prev_saturday_first_time) + $increase_time);
					$shuttle_points_doc["saturdayFirstTime"] = convertShuttleTime(substr($saturday_first_time, 0, 5));
				}
				
				if(isset($prev_saturday_last_time) && !empty($prev_saturday_last_time)) {
					$saturday_last_time = date("H:i:s", strtotime(date("Ymd") . " " . $prev_saturday_last_time) + $increase_time);
					$shuttle_points_doc["saturdayLastTime"] = convertShuttleTime(substr($saturday_last_time, 0, 5));
				}
				
				if(isset($prev_sunday_first_time) && !empty($prev_sunday_first_time)) {
					$sunday_first_time = date("H:i:s", strtotime(date("Ymd") . " " . $prev_sunday_first_time) + $increase_time);
					$shuttle_points_doc["sundayFirstTime"] = convertShuttleTime(substr($sunday_first_time, 0, 5));
				}
				
				if(isset($prev_sunday_last_time) && !empty($prev_sunday_last_time)) {
					$sunday_last_time = date("H:i:s", strtotime(date("Ymd") . " " . $prev_sunday_last_time) + $increase_time);
					$shuttle_points_doc["sundayLastTime"] = convertShuttleTime(substr($sunday_last_time, 0, 5));
				}
				
				$_shuttlepoints->insert($shuttle_points_doc);
	
				$prev_lat = $lat;
				$prev_lng = $lng;
			}
		}
		
		function updateShuttlePointTime($_shuttleinfos, $_shuttlepoints, $_shuttleId, $_ynEnabled) {
			$shuttle_info = $_shuttleinfos->findOne(array("_id" => new MongoId($_shuttleId)));
			if(!isset($shuttle_info)) {
				return;
			}
			
			$shuttle_points_cursor = $_shuttlepoints->find(array("busId" => $_shuttleId))->sort(array("index" => 1));
			$shuttle_points = array();
			foreach ($shuttle_points_cursor as $row) {
				$shuttle_points[] = $row;
			}
			
			$timePerMeter = getCalculatedTimesPerMeter($shuttle_points, $shuttle_info["totalTime"]);
			
			$prev_first_time = convertShuttleTime($shuttle_info["firstTime"]) . ":00";
			$prev_last_time = convertShuttleTime($shuttle_info["lastTime"]) . ":00";
			$prev_saturday_first_time = convertShuttleTime($shuttle_info["saturdayFirstTime"]);
			$prev_saturday_last_time = convertShuttleTime($shuttle_info["saturdayLastTime"]);
			$prev_sunday_first_time = convertShuttleTime($shuttle_info["sundayFirstTime"]);
			$prev_sunday_last_time = convertShuttleTime($shuttle_info["sundayLastTime"]);
			
			if(!empty($prev_saturday_first_time)) {
				$prev_saturday_first_time = $prev_saturday_first_time . ":00";
			}
			
			if(!empty($prev_saturday_last_time)) {
				$prev_saturday_last_time = $prev_saturday_last_time . ":00";
			}
			
			if(!empty($prev_sunday_first_time)) {
				$prev_sunday_first_time = $prev_sunday_first_time . ":00";
			}
			
			if(!empty($prev_sunday_last_time)) {
				$prev_sunday_last_time = $prev_sunday_last_time . ":00";
			}
				
			$prev_lat = null;
			$prev_lng = null;
			$accumulate_dist = 0.0;
				
			foreach($shuttle_points as $lists) {
				$lat = $lists["locations"]["coordinates"][1];
				$lng = $lists["locations"]["coordinates"][0];
		
				$increase_time = 0;
				if($prev_lat != null) {
					$dist = getDistance($prev_lat, $prev_lng, $lat, $lng);
					$accumulate_dist += $dist;
					$increase_time = doubleval($accumulate_dist * $timePerMeter);
				}
		
				$first_time = date("H:i:s", strtotime(date("Ymd") . " " . $prev_first_time) + $increase_time);
				$last_time = date("H:i:s", strtotime(date("Ymd") . " " . $prev_last_time) + $increase_time);
				
				if(isset($prev_saturday_first_time) && !empty($prev_saturday_first_time)) {
					$saturday_first_time = date("H:i:s", strtotime(date("Ymd") . " " . $prev_saturday_first_time) + $increase_time);
					$lists["saturdayFirstTime"] = convertShuttleTime(substr($saturday_first_time, 0, 5));
				}
				
				if(isset($prev_saturday_last_time) && !empty($prev_saturday_last_time)) {
					$saturday_last_time = date("H:i:s", strtotime(date("Ymd") . " " . $prev_saturday_last_time) + $increase_time);
					$lists["saturdayLastTime"] = convertShuttleTime(substr($saturday_last_time, 0, 5));
				}
				
				if(isset($sunday_first_time) && !empty($sunday_first_time)) {
					$sunday_first_time = date("H:i:s", strtotime(date("Ymd") . " " . $prev_sunday_first_time) + $increase_time);
					$lists["sundayFirstTime"] = convertShuttleTime(substr($sunday_first_time, 0, 5));
				}
				
				if(isset($sunday_last_time) && !empty($sunday_last_time)) {
					$sunday_last_time = date("H:i:s", strtotime(date("Ymd") . " " . $prev_sunday_last_time) + $increase_time);
					$lists["sundayLastTime"] = convertShuttleTime(substr($sunday_last_time, 0, 5));
				}
								
				$lists["firstTime"] = convertShuttleTime(substr($first_time, 0, 5));
				$lists["lastTime"] = convertShuttleTime(substr($last_time, 0, 5));
				$lists["ynEnabled"] = $_ynEnabled;
				
				$_shuttlepoints->save($lists);
		
				$prev_lat = $lat;
				$prev_lng = $lng;
			}
		}
		
		function updateShuttleInfo($_shuttleinfos, $_shuttleId, $_name, $_category, $_desc, $_lineColor, $_routeType, $_phone, $_firstTime, $_lastTime, $_saturdayFirstTime, $_saturdayLastTime, $_sundayFirstTime, 
									$_sundayLastTime, $_runFirstTime, $_runLastTime, $_saturdayRunFirstTime, $_saturdayRunLastTime, $_sundayRunFirstTime, $_sundayRunLastTime, $_holidayDate, $_term, 
									$_totalTime, $_ynEnabled, $_pointCount) {
			$updateData = array(
				"name" => $_name,
				"category" => $_category,
				"desc" => $_desc,
				"routeType" => $_routeType,
				"phone" => $_phone,
				"firstTime" => convertShuttleTime($_firstTime),
				"lastTime" => convertShuttleTime($_lastTime),
				"saturdayFirstTime" => convertShuttleTime($_saturdayFirstTime),
				"saturdayLastTime" => convertShuttleTime($_saturdayLastTime),
				"sundayFirstTime" => convertShuttleTime($_sundayFirstTime),
				"sundayLastTime" => convertShuttleTime($_sundayLastTime),
				"runFirstTime" => convertShuttleTime($_runFirstTime),
				"runLastTime" => convertShuttleTime($_runLastTime),
				"saturdayRunFirstTime" => convertShuttleTime($_saturdayRunFirstTime),
				"saturdayRunLastTime" => convertShuttleTime($_saturdayRunLastTime),
				"sundayRunFirstTime" => convertShuttleTime($_sundayRunFirstTime),
				"sundayRunLastTime" => convertShuttleTime($_sundayRunLastTime),
				"holidayDate" => $_holidayDate,
				"term" => intval($_term),
				"totalTime" => intval($_totalTime),
				"ynEnabled" => $_ynEnabled,
				"pointCount" => intval($_pointCount),
				"lineColor" => $_lineColor
			);
			
			$_shuttleinfos->update(array('_id' => new MongoId($_shuttleId)), array('$set' => $updateData));
				
			return $updateData;
		}
		
		function deleteShuttleInfoPoints($_shuttlpoints, $_shuttleId) {
			$_shuttlpoints->remove(array("busId" => $_shuttleId), array("justOne" => false));
		}
		
		function updateShuttleInfoStatus($_shuttleinfos, $_shuttleId, $_ynEnabled) {
			$_shuttleinfos->update(array('_id' => new MongoId($_shuttleId)), array('$set' => array("ynEnabled" => $_ynEnabled)));
		}
		
		function updateShuttlePointsStatus($_shuttlpoints, $_shuttleId, $_ynEnabled) {
			$_shuttlpoints->update(array('busId' => $_shuttleId), array('$set' => array("ynEnabled" => $_ynEnabled)), array("multiple" => true));
		}
		
		function updateShuttlePointsCount($_shuttleinfos, $_shuttleId, $_pointCount) {
			$_shuttleinfos->update(array('_id' => new MongoId($_shuttleId)), array('$set' => array("pointCount" => $_pointCount)));
		}
		
		function getShuttleInfoList($_shuttleinfos) {
			global $SHUTTLE_CATEGORY;
			
			$shuttle_list = $_shuttleinfos->find()->sort(array('name' => 1, 'category' => 1));
			
			$ret_shuttle_list = array();
			foreach ($shuttle_list as $row) {
				$shuttle_id = strval($row["_id"]);
				
				$row["shuttle_id"] = $shuttle_id;
				$row["category_name"] = $SHUTTLE_CATEGORY[$row["category"]]["category_name"];
				$row["firstTime"] = convertShuttleTime($row["firstTime"]);
				$row["lastTime"] = convertShuttleTime($row["lastTime"]);
				$row["saturdayFirstTime"] = convertShuttleTime($row["saturdayFirstTime"]);
				$row["saturdayLastTime"] = convertShuttleTime($row["saturdayLastTime"]);
				$row["sundayFirstTime"] = convertShuttleTime($row["sundayFirstTime"]);
				$row["sundayLastTime"] = convertShuttleTime($row["sundayLastTime"]);
				$row["runFirstTime"] = convertShuttleTime($row["runFirstTime"]);
				$row["runLastTime"] = convertShuttleTime($row["runLastTime"]);
				$row["saturdayRunFirstTime"] = convertShuttleTime($row["saturdayRunFirstTime"]);
				$row["saturdayRunLastTime"] = convertShuttleTime($row["saturdayRunLastTime"]);
				$row["sundayRunFirstTime"] = convertShuttleTime($row["sundayRunFirstTime"]);
				$row["sundayRunLastTime"] = convertShuttleTime($row["sundayRunLastTime"]);
				
				$ret_shuttle_list[$shuttle_id] = $row;
			}
			
			return $ret_shuttle_list;
		}
		
		function getShuttleRouteInfo($_shuttleinfos, $_shuttle_id) {
			$ret_shuttle_info = $_shuttleinfos->findOne(array('_id' => new MongoId($_shuttle_id)));
			
			$shuttle_id = strval($ret_shuttle_info["_id"]);
			
			global $SHUTTLE_CATEGORY;
			
			$ret_shuttle_info["shuttle_id"] = $shuttle_id;
			$ret_shuttle_info["category_name"] = $SHUTTLE_CATEGORY[$ret_shuttle_info["category"]]["category_name"];
			$ret_shuttle_info["firstTime"] = convertShuttleTime($ret_shuttle_info["firstTime"]);
			$ret_shuttle_info["lastTime"] = convertShuttleTime($ret_shuttle_info["lastTime"]);
			$ret_shuttle_info["saturdayFirstTime"] = convertShuttleTime($ret_shuttle_info["saturdayFirstTime"]);
			$ret_shuttle_info["saturdayLastTime"] = convertShuttleTime($ret_shuttle_info["saturdayLastTime"]);
			$ret_shuttle_info["sundayFirstTime"] = convertShuttleTime($ret_shuttle_info["sundayFirstTime"]);
			$ret_shuttle_info["sundayLastTime"] = convertShuttleTime($ret_shuttle_info["sundayLastTime"]);
			$ret_shuttle_info["runFirstTime"] = convertShuttleTime($ret_shuttle_info["runFirstTime"]);
			$ret_shuttle_info["runLastTime"] = convertShuttleTime($ret_shuttle_info["runLastTime"]);
			$ret_shuttle_info["saturdayRunFirstTime"] = convertShuttleTime($ret_shuttle_info["saturdayRunFirstTime"]);
			$ret_shuttle_info["saturdayRunLastTime"] = convertShuttleTime($ret_shuttle_info["saturdayRunLastTime"]);
			$ret_shuttle_info["sundayRunFirstTime"] = convertShuttleTime($ret_shuttle_info["sundayRunFirstTime"]);
			$ret_shuttle_info["sundayRunLastTime"] = convertShuttleTime($ret_shuttle_info["sundayRunLastTime"]);
			
			return $ret_shuttle_info;
		}
		
		function getShuttlePointsList($_shuttlepoints, $_shuttle_id) {
			$shuttle_points_list = $_shuttlepoints->find(array("busId" => $_shuttle_id))->sort(array('index' => 1));
				
			$ret_shuttle_points_list = array();
			foreach ($shuttle_points_list as $row) {
				$shuttle_points_id = strval($row["_id"]);
			
				$row["shuttle_points_id"] = $shuttle_points_id;
				$row["firstTime"] = convertShuttleTime($row["firstTime"]);
				$row["lastTime"] = convertShuttleTime($row["lastTime"]);
				$row["saturdayFirstTime"] = convertShuttleTime($row["saturdayFirstTime"]);
				$row["saturdayLastTime"] = convertShuttleTime($row["saturdayLastTime"]);
				$row["sundayFirstTime"] = convertShuttleTime($row["sundayFirstTime"]);
				$row["sundayLastTime"] = convertShuttleTime($row["sundayLastTime"]);
				
				$ret_shuttle_points_list[] = $row;
			}
				
			return $ret_shuttle_points_list;
		}
		
		function getShuttleStationList($_shuttlepoints, $_lat, $_lng, $_range) {
			$retShuttleMarkerPoints = array();
		
			$shuttleMarkerPoints = $_shuttlepoints->aggregate(
				array(
					'$geoNear' => array(
						'near' => array(
							'type' => 'Point',
							'coordinates' => array($_lng, $_lat)
						),
						'distanceField' => 'dist.distance',
						"maxDistance" => $_range,
						"spherical" => true,
						"limit" => 100000
					)
				),
				array(
					'$match' => array(
						'text' => array(
							'$ne' => '__point'
						),
						'ynEnabled' => 'Y'
					)
				),
				array(
					'$group' => array(
						'_id' => array(
							'busId' => '$busId',
							'arsId' => '$arsId',
							'text' => '$text',
							'locations' => '$locations',
							'exceptOutback' => '$exceptOutback'
						),
						'minDistance' => array(
							'$min' => '$dist.distance'
						),
						'firstTime' => array(
							'$min' => '$firstTime'
						),
						'lastTime' => array(
							'$max' => '$lastTime'
						),
						'saturdayFirstTime' => array(
							'$min' => '$saturdayFirstTime'
						),
						'saturdayLastTime' => array(
							'$max' => '$saturdayLastTime'
						),
						'sundayFirstTime' => array(
							'$min' => '$sundayFirstTime'
						),
						'sundayLastTime' => array(
							'$max' => '$sundayLastTime'
						)
					)
				),
				array(
					'$sort' => array(
						'minDistance' => 1
					)
				)
			);
		
			foreach ($shuttleMarkerPoints["result"] as $row) {
				$retShuttleMarkerPoints[] = array(
					"busId" => $row["_id"]["busId"],
					"arsId" => $row["_id"]["arsId"],
					"text" => $row["_id"]["text"],
					"location" => array("lat" => $row["_id"]["locations"]["coordinates"][1], "lng" => $row["_id"]["locations"]["coordinates"][0]),
					"exceptOutback" => $row["_id"]["exceptOutback"],
					"distance" => $row["minDistance"],
					"firstTime" => $row["firstTime"],
					"lastTime" => $row["lastTime"],
					"saturdayFirstTime" => $row["saturdayFirstTime"],
					"saturdayLastTime" => $row["saturdayLastTime"],
					"sundayFirstTime" => $row["sundayFirstTime"],
					"sundayLastTime" => $row["sundayLastTime"],
					"stationType" => "shuttle"
				);
			}
		
			return $retShuttleMarkerPoints;
		}
		
		function getShuttleLineList($_shuttlepoints, $_lat, $_lng, $_range) {
			$shuttleLinePoints = $_shuttlepoints->aggregate(
				array(
					'$geoNear' => array(
						'near' => array(
							'type' => 'Point',
							'coordinates' => array($_lng, $_lat)
						),
						'distanceField' => 'distance',
						"maxDistance" => $_range,
						"spherical" => true,
						"limit" => 100000
					)
				),
				array(
					'$match' => array(
						'ynEnabled' => "Y"
					)	
				),
				array(
					'$group' => array(
						'_id' => array(
							'busId' => '$busId'
						),
						'shuttle_point' => array(
							'$push' => array(
								'busId' => '$busId',
								'index' => '$index',
								'locations' => '$locations',
								'text' => '$text'
							)
						)
					)
				)
			);
		
			return $shuttleLinePoints["result"];
		}
		
		function getShuttleInfoListByIds($_shuttleinfos, $_busIds) {
			$retShuttles = array();
		
			$shuttles_cursor = $_shuttleinfos->find(array('_id' => array('$in' => $_busIds)));
		
			foreach ($shuttles_cursor as $row) {
				$busId = strval($row["_id"]);
				$retShuttles[] = array(
					"busId" => $busId,
					"name" => $row["name"],
					"firstTime" => $row["firstTime"],
					"lastTime" => $row["lastTime"],
					"saturdayFirstTime" => $row["saturdayFirstTime"],
					"saturdayLastTime" => $row["saturdayLastTime"],
					"sundayFirstTime" => $row["sundayFirstTime"],
					"sundayLastTime" => $row["sundayLastTime"],
					"runFirstTime" => $row["runFirstTime"],
					"runLastTime" => $row["runLastTime"],
					"saturdayRunFirstTime" => $row["saturdayRunFirstTime"],
					"saturdayRunLastTime" => $row["saturdayRunLastTime"],
					"sundayRunFirstTime" => $row["sundayRunFirstTime"],
					"sundayRunLastTime" => $row["sundayRunLastTime"],
					"holidayDate" => $row["holidayDate"]
				);
			}
		
			return $retShuttles;
		}
		
		function getCarpoolRegistrantList($_taxicarpools, $_taxifind, $_lat, $_lng, $_range) {
			$taxiCarpoolRegistrant = $_taxicarpools->aggregate(
				array(
					'$geoNear' => array(
						'near' => array(
							'type' => 'Point',
							'coordinates' => array($_lng, $_lat)
						),
						'distanceField' => 'distance',
						"maxDistance" => $_range,
						"spherical" => true,
						"limit" => 100000
					)
				),
				array(
					'$match' => array(
						'time' => array(
							'$gte' => time() * 1000
						)
					)
				)
			);
		
			$retCarpoolRegistrantList = array();
			$registrantList = array();
		
			foreach ($taxiCarpoolRegistrant["result"] as $row) {
				$taxiId = $row["_id"] . "";
				$retCarpoolRegistrantList[] = array(
					"taxiId" => $taxiId,
					"driverId" => $row["driverId"],
					"registrant" => $row["registrant"],
					"distance" => $row["distance"],
					"stationType" => "carpool"
				);
					
				$registrantList[] = $row["driverId"];
			}
		
			$taxifind_cursor = $_taxifind->find(array('driverId' => array('$in' => $registrantList)));
		
			$retDriverPoint = array();
			foreach ($taxifind_cursor as $row) {
				$retDriverPoint[$row["driverId"]] = $row["location"];
			}
		
			foreach ($retCarpoolRegistrantList as &$row) {
				if(isset($retDriverPoint[$row["driverId"]])) {
					$row["location"] = array("lat" => $retDriverPoint[$row["driverId"]]["coordinates"][1], "lng" => $retDriverPoint[$row["driverId"]]["coordinates"][0]);
				}
			}
		
			return $retCarpoolRegistrantList;
		}
		
		function getBusStationList($_busstations, $_lat, $_lng, $_range) {
			$retBusStationList = array();
		
			$busStationList = $_busstations->aggregate(
				array(
					'$geoNear' => array(
						'near' => array(
							'type' => 'Point',
							'coordinates' => array($_lng, $_lat)
						),
						'distanceField' => 'dist.distance',
						"maxDistance" => $_range,
						"spherical" => true,
						"limit" => 100000
					)
				),
				array(
					'$match' => array(
						'arsId' => array(
							'$ne' => ''
						)
					)
				),
				array(
					'$group' => array(
						'_id' => array(
							'stationId' => '$stationId',
							'stationName' => '$stationName',
							'arsId' => '$arsId',
							'gpsX' => '$gpsX',
							'gpsY' => '$gpsY'
						),
						'minDistance' => array(
							'$min' => '$dist.distance'
						),
						'startTime' => array(
							'$min' => '$startTime'
						),
						'endTime' => array(
							'$max' => '$endTime'
						)
					)
				),
				array(
					'$sort' => array(
						'minDistance' => 1
					)
				)
			);
		
			foreach ($busStationList["result"] as $row) {
				$retBusStationList[] = array(
					"stationId" => $row["_id"]["stationId"],
					"arsId" => $row["_id"]["arsId"],
					"stationName" => $row["_id"]["stationName"],
					"location" => array("lat" => $row["_id"]["gpsY"], "lng" => $row["_id"]["gpsX"]),
					"distance" => $row["minDistance"],
					"startTime" => $row["startTime"],
					"endTime" => $row["endTime"],
					"stationType" => "bus"
				);
			}
		
			return $retBusStationList;
		}
		
		function getSubwayStationList($_subwaystations, $_subwaytimes, $_lat, $_lng, $_range) {
			$retSubwayStationList = array();
		
			$stationCodes = array();
			$subwayStationList = $_subwaystations->aggregate(
				array(
					'$geoNear' => array(
						'near' => array(
							'type' => 'Point',
							'coordinates' => array($_lng, $_lat)
						),
						'distanceField' => 'distance',
						"maxDistance" => $_range,
						"spherical" => true,
						"limit" => 100000
					)
				)
			);
		
			foreach ($subwayStationList["result"] as $row) {
				$retSubwayStationList[$row["stationCode"]] = array(
					"stationCode" => $row["stationCode"],
					"stationName" => $row["stationName"],
					"location" => array("lat" => $row["gpsY"], "lng" => $row["gpsX"]),
					"stationCodeDaum" => $row["stationCodeDaum"],
					"distance" => $row["distance"],
					"stationType" => "subway"
				);
					
				$stationCodes[] = strval($row["stationCode"]);
			}
		
			$dayOfWeek = date("N") < 6 ? 1 : (date("N") == 6 ? 2 : 3);
		
			$subwayTimeList = $this->getSubwayTimeList($_subwaytimes, $stationCodes);
			
			if($subwayTimeList != null) {
				foreach ($subwayTimeList as $row) {
					$stationCode = $row["_id"]["stationCode"];
		
					$retSubwayStationList[$stationCode]["firstTime"] = $row["firstTime"];
					$retSubwayStationList[$stationCode]["lastTime"] = $row["lastTime"];
				}
			}
		
			return $retSubwayStationList;
		}
		
		function getSubwayTimeList($_subwaytimes, $_stationCodes) {
			$dayOfWeek = date("N") < 6 ? 1 : (date("N") == 6 ? 2 : 3);
		
			$subwayTimeList = $_subwaytimes->aggregate(
				array(
					'$match' => array(
						'stationCode' => array('$in' => $_stationCodes),
						'weekTag' => strval($dayOfWeek)
					)
				),
				array(
					'$group' => array(
						'_id' => array(
							'stationCode' => '$stationCode'
						),
						'firstTime' => array(
							'$min' => '$firstTime'
						),
						'lastTime' => array(
							'$max' => '$lastTime'
						)
					)
				)
			);
		
			return $subwayTimeList["result"];
		}
		
		function getBusRouteListByStation($_busroutes, $_busroutestations, $_busstations, $_stationId) {
			$retBusRouteList = array();
			
			$busRouteStationCursor = $_busroutestations->find(array('stationId' => intval($_stationId)), array('busRouteId' => true, 'stationSequence' => true, 'direction' => true));
		
			$busRoutes = array();
			foreach ($busRouteStationCursor as $row) {
				$retBusRouteList[] = $row;
				$busRoutes[] = strval($row["busRouteId"]);
			}
		
			$busRouteListCursor = $_busroutes->find(array('busRouteId' => array('$in' => $busRoutes)), array('busRouteId' => true, 'busRouteName' => true, 'busRouteType' => true, 'busRouteTypeName' => true));
		
			$busRouteList = array();
			foreach ($busRouteListCursor as $row) {
				$busRouteList[$row["busRouteId"]] = $row;
			}
		
			foreach ($retBusRouteList as &$row) {
				$nextSeq = intval($row['stationSequence']) + 1;
				$nextStationId = $_busroutestations->findOne(array('busRouteId' => strval($row['busRouteId']), 'stationSequence' => intval($nextSeq)), array('stationId' => true));
								
				$row["nextStation"] = "";
				if(isset($nextStationId) && isset($nextStationId["stationId"])) {
					$nextBusStationInfo = $_busstations->findOne(array('stationId' => intval($nextStationId["stationId"])), array("stationName" => true));
					if(isset($nextBusStationInfo) && isset($nextBusStationInfo["stationName"])) {
						$row["nextStation"] = $nextBusStationInfo["stationName"];
					}
				}
				
				if(isset($busRouteList[$row['busRouteId']])) {
					$row["busRouteName"] = $busRouteList[$row['busRouteId']]["busRouteName"];
					$row["busRouteType"] = $busRouteList[$row['busRouteId']]["busRouteType"];
					$row["busRouteTypeName"] = $busRouteList[$row['busRouteId']]["busRouteTypeName"];
				}
			}
			
			return $retBusRouteList;
		}
		
		function getShuttleRouteListByStation($_shuttlepoints, $_shuttleinfos, $_stationId) {
			$retShuttleRouteList = array();
			$shuttlePointListCusor = $_shuttlepoints->find(array('arsId' => $_stationId), array('busId' => true));
		
			$busIds = array();
			foreach ($shuttlePointListCusor as $row) {
				$busIds[] = new MongoId($row["busId"]);
			}
		
			$shuttleInfoListCursor = $_shuttleinfos->find(array('_id' => array('$in' => $busIds)));
			foreach ($shuttleInfoListCursor as $row) {
				$retShuttleRouteList[] = array(
					"name" => $row["name"],
					"desc" => $row["desc"],
					"category" => $row["category"],
					"firstTime" => $row["firstTime"],
					"lastTime" => $row["lastTime"],
					"saturdayFirstTime" => $row["saturdayFirstTime"],
					"saturdayLastTime" => $row["saturdayLastTime"],
					"sundayFirstTime" => $row["sundayFirstTime"],
					"sundayLastTime" => $row["sundayLastTime"],
					"runFirstTime" => $row["runFirstTime"],
					"runLastTime" => $row["runLastTime"],
					"saturdayRunFirstTime" => $row["saturdayRunFirstTime"],
					"saturdayRunLastTime" => $row["saturdayRunLastTime"],
					"sundayRunFirstTime" => $row["sundayRunFirstTime"],
					"sundayRunLastTime" => $row["sundayRunLastTime"],
					"holidayDate" => $row["holidayDate"],
					"term" => $row["term"],
					"totalTime" => $row["totalTime"]
				);
			}
		
			return $retShuttleRouteList;
		}
		
		function searchDriver($_drivers, $_taxifinds, $_search_text) {
			$retData = null;
			
			$phone_search_text = $_search_text;
			if(!startsWith($_search_text, "+82")) {
				$phone_search_text = "+82" . substr($_search_text, 1);
			}
			
			$retDriver = $_drivers->findOne(array('$or' => array(array('name' => $_search_text), array('phone' => $_search_text), array('phone' => $phone_search_text))));
			if(isset($retDriver) && isset($retDriver['_id'])) {
				$driverId = strval($retDriver['_id']);
					
				$retDriverPosition = $_taxifinds->findOne(array('driverId' => $driverId));
					
				$retData = array(
					"name" => $retDriver["name"],
					"phone" => $retDriver["phone"],
					"location" => $retDriverPosition["location"],
					"date" => date("Y-m-d H:i:s", ($retDriverPosition["date"] / 1000)),
				);				
			}
			
			return $retData;
		}
		
		function deleteShuttle($_shuttleinfos, $_shuttlepoints, $_shuttleId) {
			$_shuttlepoints->remove(array("busId" => $_shuttleId), array("justOne" => false));
			$_shuttleinfos->remove(array('_id' => new MongoId($_shuttleId)), array("justOne" => false));
		}
		
		function getShuttleLineColorList($_shuttleinfos) {
			$shuttleInfoListCursor = $_shuttleinfos->find();
			
			$retShuttleRouteList = array();
			
			foreach ($shuttleInfoListCursor as $row) {
				$busId = strval($row['_id']);
				
				$retShuttleRouteList[$busId] = array(
					"busId" => $busId,
					"name" => $row["name"],
					"lineColor" => $row["lineColor"]
				);
			}
			
			return $retShuttleRouteList;
		}
	}
?>