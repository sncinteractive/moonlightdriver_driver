<?php
	class CSession {
		function CSession() {}
		
		function login($_accounts, $_id, $_pw) {
			$accounts_data = $_accounts->findOne(array('id' => $_id, 'pw' => md5($_pw)));
			
			if(!isset($accounts_data)) {
				return false;
			}
			
			$_SESSION["manager"] = array(
				"id" => $accounts_data["id"],
				"level" => $accounts_data["level"],
				"name" => $accounts_data["name"],
				"phone" => $accounts_data["phone"],
				"email" => $accounts_data["email"]
			);
			
			return true;
		}
		
		function isLogin() {
			return isset($_SESSION["manager"]);
		}
		
		function checkPermission($_check_permissions) {
			if(!$this->isLogin()) {
				return false;
			}
		
			if(!isset($_SESSION["manager"]["level"])) {
				return false;
			}
			
			global $ROLE_LEVEL;
			
			$level = $_SESSION["manager"]["level"];
			foreach ($_check_permissions as $key => $_permissions) {
				foreach ($_permissions as $_perm) {
					if($ROLE_LEVEL[$key][$_perm] < $level) {
						return false;
					}
				}
			}
		
			return true;
		}
		
		function logout() {
			unset($_SESSION['manager']);
			session_destroy();
			session_unset();
		}
		
		function getManagerInfo() {
			if(!$this->isLogin()) {
				return false;
			}
			
			return $_SESSION["manager"];
		}
	}
?>