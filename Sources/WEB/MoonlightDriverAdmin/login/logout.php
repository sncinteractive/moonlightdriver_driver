<?php 
	header("Content-Type:text/html; charset=utf-8");
	
	try {
		require_once ("../inc/config.php");
		require_once_classes(array("CSession"));
	
		$session = new CSession();
	
		$session->logout();
		
		moveToSpecificPage(CONF_URL_ROOT);
		exit;
		
	} catch (Exception $e) {
		mld_error($e->getMessage());
		moveToSpecificPage(CONF_URL_ERROR);
		exit;
	}	
?>
