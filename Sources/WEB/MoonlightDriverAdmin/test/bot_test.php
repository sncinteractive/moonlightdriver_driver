<?php
	header("Content-Type:text/html; charset=utf-8");
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
	
	try {
		//$server_host = "http://218.36.4.85:8888";
		$server_host = "http://115.68.104.30:8888";
		
		$lat = "37.53998786839401";
		$lng = "126.94841707125306";
		$range = "2000";
		
		$shuttle_station_url = $server_host . "/busnew2/station?lat=" . $lat . "&lng=" . $lng . "&range=" . $range;
		$driver_list_url = $server_host . "/taxi/list/registrant?lat=" . $lat . "&lng=" . $lng . "&range=" . $range;
		$bus_station_url = $server_host . "/public/transit/near/station/list?lat=" . $lat . "&lng=" . $lng . "&range=" . $range;
		
		for($i = 0; $i <= 10; $i++) {
			echo "shuttle_station : " . $i . "\n";
			send($shuttle_station_url);
			echo "driver_list : " . $i . "\n";
			send($driver_list_url);
			echo "bus_station : " . $i . "\n";
			send($bus_station_url);
			echo "\n";
		}
	} catch (Exception $e) {
		echo "<pre>";
		print_r($e);
	}
	
	exit;
	
	function send($_url) {
		try {
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $_url);
			curl_setopt($ch, CURLOPT_POST, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			$response = curl_exec($ch);
			curl_close($ch);
			
// 			echo "<pre>";
// 			print_r($response);
		} catch(Exception $e) {
			echo "<pre>";
			print_r($e);
		}
	}
?>
