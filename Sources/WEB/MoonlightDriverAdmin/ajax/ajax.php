<?php
	header("Content-Type:text/html; charset=utf-8");
	
	try {
		require_once ("../inc/config.php");
		require_once_classes(array("CSession", "CDatabaseManager", "CShuttleManager", "CCallManager", "CQuestionManager", "CNoticeManager", "CAccountManager", "CDriverManager", "CBoardManager", "CTaxiCarpoolManager"));
	
		$session = new CSession();
		
		$retResult = array("result" => "OK", "msg" => "SUCCESS", "data" => array());
	
		if(!$session->isLogin()) {
			$retResult["result"] = "NOT_LOGIN";
			$retResult["msg"] = "로그인 후 사용가능합니다.";
				
			echo json_encode($retResult);
			exit;
		}
		
		$database_manager = new CDatabaseManager();
		$database = $database_manager->getDb();
		
		$type = $_POST["type"];
		
		if($type == "add_shuttle") {
			$permissions = array("shuttle" => array("inquiry", "create"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult);
				exit;
			}
			
			$shuttle_manager = new CShuttleManager();
				
			$name = $_POST["name"];
			$desc = $_POST["desc"];
			$category = $_POST["category"];
			$lineColor = $_POST["lineColor"];
			$routeType = $_POST["routeType"];
			$phone = $_POST["phone"];
			$firstTime = isset($_POST["firstTime"]) ? $_POST["firstTime"] : "";
			$lastTime = isset($_POST["lastTime"]) ? $_POST["lastTime"] : "";
			$saturdayFirstTime = isset($_POST["saturdayFirstTime"]) ? $_POST["saturdayFirstTime"] : "";
			$saturdayLastTime = isset($_POST["saturdayLastTime"]) ? $_POST["saturdayLastTime"] : "";
			$sundayFirstTime = isset($_POST["sundayFirstTime"]) ? $_POST["sundayFirstTime"] : "";
			$sundayLastTime = isset($_POST["sundayLastTime"]) ?$_POST["sundayLastTime"] : "";
			$runFirstTime = isset($_POST["runFirstTime"]) ? $_POST["runFirstTime"] : "";
			$runLastTime = isset($_POST["runLastTime"]) ? $_POST["runLastTime"] : "";
			$saturdayRunFirstTime = isset($_POST["saturdayRunFirstTime"]) ? $_POST["saturdayRunFirstTime"] : "";
			$saturdayRunLastTime = isset($_POST["saturdayRunLastTime"]) ? $_POST["saturdayRunLastTime"] : "";
			$sundayRunFirstTime = isset($_POST["sundayRunFirstTime"]) ? $_POST["sundayRunFirstTime"] : "";
			$sundayRunLastTime = isset($_POST["sundayRunLastTime"]) ?$_POST["sundayRunLastTime"] : "";
			$holidayDate = isset($_POST["holidayDate"]) ? $_POST["holidayDate"] : "";
			$term = $_POST["term"];
			$totalTime = $_POST["totalTime"];
			$ynEnabled = $_POST["ynEnabled"];
			$pointCount = $_POST["pointCount"];
			$ynUpdatePointsList = $_POST["ynUpdatePointsList"];
			
			$retResult["data"] = $shuttle_manager->addNewShuttleInfo($database->shuttleinfos, $name, $category, $desc, $lineColor, $routeType, $phone, $firstTime, $lastTime, $saturdayFirstTime, $saturdayLastTime, 
																	$sundayFirstTime, $sundayLastTime, $runFirstTime, $runLastTime, $saturdayRunFirstTime, $saturdayRunLastTime, $sundayRunFirstTime, 
																	$sundayRunLastTime, $holidayDate, $term, $totalTime, $ynEnabled, $pointCount);
			if(isset($retResult["data"]["_id"])) {
				$shuttleId = strval($retResult["data"]["_id"]);
				
				global $SHUTTLE_CATEGORY;
				
				$retResult["data"]["shuttle_id"] = $shuttleId;
				$retResult["data"]["category_name"] = $SHUTTLE_CATEGORY[$retResult["data"]["category"]]["category_name"];
				$retResult["data"]["firstTime"] = convertShuttleTime($retResult["data"]["firstTime"]);
				$retResult["data"]["lastTime"] = convertShuttleTime($retResult["data"]["lastTime"]);
				$retResult["data"]["saturdayFirstTime"] = convertShuttleTime($retResult["data"]["saturdayFirstTime"]);
				$retResult["data"]["saturdayLastTime"] = convertShuttleTime($retResult["data"]["saturdayLastTime"]);
				$retResult["data"]["sundayFirstTime"] = convertShuttleTime($retResult["data"]["sundayFirstTime"]);
				$retResult["data"]["sundayLastTime"] = convertShuttleTime($retResult["data"]["sundayLastTime"]);
				$retResult["data"]["runFirstTime"] = convertShuttleTime($retResult["data"]["runFirstTime"]);
				$retResult["data"]["runLastTime"] = convertShuttleTime($retResult["data"]["runLastTime"]);
				$retResult["data"]["saturdayRunFirstTime"] = convertShuttleTime($retResult["data"]["saturdayRunFirstTime"]);
				$retResult["data"]["saturdayRunLastTime"] = convertShuttleTime($retResult["data"]["saturdayRunLastTime"]);
				$retResult["data"]["sundayRunFirstTime"] = convertShuttleTime($retResult["data"]["sundayRunFirstTime"]);
				$retResult["data"]["sundayRunLastTime"] = convertShuttleTime($retResult["data"]["sundayRunLastTime"]);
			
				if($ynUpdatePointsList == "Y" && $pointCount > 0) {
					$new_points_list = json_decode($_POST["new_points_list"], true);
					$shuttle_manager->addNewShuttleInfoPoints($database->shuttlepoints, $shuttleId, $firstTime, $lastTime, $saturdayFirstTime, $saturdayLastTime, $sundayFirstTime, $sundayLastTime, 
																$totalTime, $new_points_list, $ynEnabled);
				}
			}
		} else if($type == "edit_shuttle") {
			$permissions = array("shuttle" => array("inquiry", "modify"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult);
				exit;
			}
			
			$shuttle_manager = new CShuttleManager();
			
			$shuttleId = $_POST["shuttle_id"];
			$name = $_POST["name"];
			$category = $_POST["category"];
			$lineColor = $_POST["lineColor"];
			$desc = $_POST["desc"];
			$routeType = $_POST["routeType"];
			$phone = $_POST["phone"];
			$firstTime = isset($_POST["firstTime"]) ? $_POST["firstTime"] : "";
			$lastTime = isset($_POST["lastTime"]) ? $_POST["lastTime"] : "";
			$saturdayFirstTime = isset($_POST["saturdayFirstTime"]) ? $_POST["saturdayFirstTime"] : "";
			$saturdayLastTime = isset($_POST["saturdayLastTime"]) ? $_POST["saturdayLastTime"] : "";
			$sundayFirstTime = isset($_POST["sundayFirstTime"]) ? $_POST["sundayFirstTime"] : "";
			$sundayLastTime = isset($_POST["sundayLastTime"]) ?$_POST["sundayLastTime"] : "";
			$runFirstTime = isset($_POST["runFirstTime"]) ? $_POST["runFirstTime"] : "";
			$runLastTime = isset($_POST["runLastTime"]) ? $_POST["runLastTime"] : "";
			$saturdayRunFirstTime = isset($_POST["saturdayRunFirstTime"]) ? $_POST["saturdayRunFirstTime"] : "";
			$saturdayRunLastTime = isset($_POST["saturdayRunLastTime"]) ? $_POST["saturdayRunLastTime"] : "";
			$sundayRunFirstTime = isset($_POST["sundayRunFirstTime"]) ? $_POST["sundayRunFirstTime"] : "";
			$sundayRunLastTime = isset($_POST["sundayRunLastTime"]) ?$_POST["sundayRunLastTime"] : "";
			$holidayDate = isset($_POST["holidayDate"]) ? $_POST["holidayDate"] : "";
			$term = $_POST["term"];
			$totalTime = $_POST["totalTime"];
			$ynEnabled = $_POST["ynEnabled"];
			$pointCount = $_POST["pointCount"];
			$ynUpdatePointsList = $_POST["ynUpdatePointsList"];
			
			$retResult["data"] = $shuttle_manager->updateShuttleInfo($database->shuttleinfos, $shuttleId, $name, $category, $desc, $lineColor, $routeType, $phone, $firstTime, $lastTime, $saturdayFirstTime, 
																	$saturdayLastTime, $sundayFirstTime, $sundayLastTime, $runFirstTime, $runLastTime, $saturdayRunFirstTime, $saturdayRunLastTime, 
																	$sundayRunFirstTime, $sundayRunLastTime, $holidayDate, $term, $totalTime, $ynEnabled, $pointCount);
			
			global $SHUTTLE_CATEGORY;
			
			$retResult["data"]["shuttle_id"] = $shuttleId;
			$retResult["data"]["category_name"] = $SHUTTLE_CATEGORY[$retResult["data"]["category"]]["category_name"];
			$retResult["data"]["firstTime"] = convertShuttleTime($retResult["data"]["firstTime"]);
			$retResult["data"]["lastTime"] = convertShuttleTime($retResult["data"]["lastTime"]);
			$retResult["data"]["saturdayFirstTime"] = convertShuttleTime($retResult["data"]["saturdayFirstTime"]);
			$retResult["data"]["saturdayLastTime"] = convertShuttleTime($retResult["data"]["saturdayLastTime"]);
			$retResult["data"]["sundayFirstTime"] = convertShuttleTime($retResult["data"]["sundayFirstTime"]);
			$retResult["data"]["sundayLastTime"] = convertShuttleTime($retResult["data"]["sundayLastTime"]);
			$retResult["data"]["runFirstTime"] = convertShuttleTime($retResult["data"]["runFirstTime"]);
			$retResult["data"]["runLastTime"] = convertShuttleTime($retResult["data"]["runLastTime"]);
			$retResult["data"]["saturdayRunFirstTime"] = convertShuttleTime($retResult["data"]["saturdayRunFirstTime"]);
			$retResult["data"]["saturdayRunLastTime"] = convertShuttleTime($retResult["data"]["saturdayRunLastTime"]);
			$retResult["data"]["sundayRunFirstTime"] = convertShuttleTime($retResult["data"]["sundayRunFirstTime"]);
			$retResult["data"]["sundayRunLastTime"] = convertShuttleTime($retResult["data"]["sundayRunLastTime"]);
			
			if($ynUpdatePointsList == "Y") {
				$shuttle_manager->deleteShuttleInfoPoints($database->shuttlepoints, $shuttleId);
				
				if($pointCount > 0) {
					$new_points_list = json_decode($_POST["new_points_list"], true);
					$shuttle_manager->addNewShuttleInfoPoints($database->shuttlepoints, $shuttleId, $firstTime, $lastTime, $saturdayFirstTime, $saturdayLastTime, $sundayFirstTime, $sundayLastTime, 
																$totalTime, $new_points_list, $ynEnabled);
				}
			} else {
				$shuttle_manager->updateShuttlePointTime($database->shuttleinfos, $database->shuttlepoints, $shuttleId, $ynEnabled);
			}
		} else if($type == "edit_shuttle_points") {
			$permissions = array("shuttle" => array("inquiry", "modify"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult);
				exit;
			}
			
			$shuttle_manager = new CShuttleManager();
			
			$shuttleId = $_POST["shuttle_id"];
			$pointCount = intval($_POST["pointCount"]);
			$new_points_list = json_decode($_POST["new_points_list"], true);
			
			$shuttle_info = $shuttle_manager->getShuttleRouteInfo($database->shuttleinfos, $shuttleId);
			$shuttle_info["firstTime"] = convertShuttleTime($shuttle_info["firstTime"]);
			$shuttle_info["lastTime"] = convertShuttleTime($shuttle_info["lastTime"]);
			$shuttle_info["saturdayFirstTime"] = convertShuttleTime($shuttle_info["saturdayFirstTime"]);
			$shuttle_info["saturdayLastTime"] = convertShuttleTime($shuttle_info["saturdayLastTime"]);
			$shuttle_info["sundayFirstTime"] = convertShuttleTime($shuttle_info["sundayFirstTime"]);
			$shuttle_info["sundayLastTime"] = convertShuttleTime($shuttle_info["sundayLastTime"]);
			$shuttle_info["runFirstTime"] = convertShuttleTime($shuttle_info["runFirstTime"]);
			$shuttle_info["runLastTime"] = convertShuttleTime($shuttle_info["runLastTime"]);
			$shuttle_info["saturdayRunFirstTime"] = convertShuttleTime($shuttle_info["saturdayRunFirstTime"]);
			$shuttle_info["saturdayRunLastTime"] = convertShuttleTime($shuttle_info["saturdayRunLastTime"]);
			$shuttle_info["sundayRunFirstTime"] = convertShuttleTime($shuttle_info["sundayRunFirstTime"]);
			$shuttle_info["sundayRunLastTime"] = convertShuttleTime($shuttle_info["sundayRunLastTime"]);
			
			$shuttle_manager->deleteShuttleInfoPoints($database->shuttlepoints, $shuttleId);
				
			if($pointCount > 0) {
				$shuttle_manager->addNewShuttleInfoPoints($database->shuttlepoints, $shuttleId, $shuttle_info["firstTime"], $shuttle_info["lastTime"], $shuttle_info["saturdayFirstTime"], 
															$shuttle_info["saturdayLastTime"], $shuttle_info["sundayFirstTime"], $shuttle_info["sundayLastTime"], $shuttle_info["totalTime"], 
															$new_points_list, $shuttle_info["ynEnabled"]);
			}
			
			$shuttle_manager->updateShuttlePointsCount($database->shuttleinfos, $shuttleId, $pointCount);
			
			$retResult["data"] = array("pointCount" => $pointCount);
		} else if($type == "get_shuttle_points") {
			$permissions = array("shuttle" => array("inquiry", "modify"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult);
				exit;
			}
			
			$shuttle_manager = new CShuttleManager();
			
			$shuttleId = $_POST["shuttle_id"];
			$shuttle_points_list = $shuttle_manager->getShuttlePointsList($database->shuttlepoints, $shuttleId);
			
			$retResult["data"] = array("point_list" => $shuttle_points_list);
		} else if($type == "change_shuttle_status") {
			$permissions = array("shuttle" => array("inquiry", "modify"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult);
				exit;
			}
			
			$shuttle_manager = new CShuttleManager();
			
			$shuttleId = $_POST["shuttle_id"];
			$ynEnabled = $_POST["ynEnabled"];
			
			$shuttle_manager->updateShuttleInfoStatus($database->shuttleinfos, $shuttleId, $ynEnabled);
			$shuttle_manager->updateShuttlePointsStatus($database->shuttlepoints, $shuttleId, $ynEnabled);
		} else if($type == "delete_shuttle") {
			$permissions = array("shuttle" => array("inquiry", "delete"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult);
				exit;
			}
			
			$shuttleId = $_POST["shuttle_id"];
			
			$shuttle_manager = new CShuttleManager();
			$shuttle_manager->deleteShuttle($database->shuttleinfos, $database->shuttlepoints, $shuttleId);
		} else if($type == "get_call_detail") {
			$permissions = array("call" => array("inquiry", "modify"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult);
				exit;
			}
			
			$call_manager = new CCallManager();
				
			$call_id = $_POST["call_id"];
				
			$retResult["data"] = $call_manager->getCallInfo($database->calls, $call_id);
		} else if($type == "save_question_answer") {
			$permissions = array("question" => array("inquiry", "modify"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult);
				exit;
			}
			
			$questionId = $_POST["question_id"];
			$answer = $_POST["answer"];
		
			$question_manager = new CQuestionManager();
			
			$question_answer_info = $question_manager->insertNewAnswer($database->questionanswers, $questionId, $answer);
			if(isset($question_answer_info["_id"])) {
				$question_answer_id = strval($question_answer_info["_id"]);
				
				$question_answer_info["question_answer_id"] = $question_answer_id;
				
				$answer_count = $question_manager->updateAnswerCount($database->questions, $database->questionanswers, $questionId);
					
				$retResult["data"] = array(
					"question_answer" => $question_answer_info,
					"question_answer_count" => $answer_count
				);
			}
		} else if($type == "update_question_answer") {
			$permissions = array("question" => array("inquiry", "modify"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult);
				exit;
			}
			
			$questionId = $_POST["question_id"];
			$questionAnswerId = $_POST["question_answer_id"];
			$answer = $_POST["answer"];
		
			$question_manager = new CQuestionManager();
			
			$question_answer_info = $question_manager->updateAnswer($database->questionanswers, $questionAnswerId, $answer);
			if(isset($question_answer_info["_id"])) {
				$question_answer_info["question_answer_id"] = strval($question_answer_info["_id"]);
				
				$answer_count = $question_manager->getQuestionAnswerCount($database->questionanswers, $questionId);
				
				$retResult["data"] = array(
					"question_answer" => $question_answer_info,
					"question_answer_count" => $answer_count
				);
			}
		} else if($type == "delete_question") {
			$permissions = array("question" => array("delete"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult);
				exit;
			}
			
			$questionId = $_POST["question_id"];
		
			$question_manager = new CQuestionManager();
				
			$question_manager->deleteQuestion($database->questions, $database->questionanswers, $questionId);
		} else if($type == "add_notice") {
			$permissions = array("notice" => array("inquiry", "create"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult);
				exit;
			}
			
			$title = $_POST["title"];
			$contents = isset($_POST["contents"]) ? $_POST["contents"] : "";
			$image = isset($_POST["image"]) ? $_POST["image"] : "";
			$link = isset($_POST["link"]) ? $_POST["link"] : "";
			$start_date = $_POST["start_date"];
			$end_date = $_POST["end_date"];
			
			$notice_manager = new CNoticeManager();
			
			$retResult["data"] = $notice_manager->addNotice($database->notices, $title, $contents, $image, $link, $start_date, $end_date);
		} else if($type == "edit_notice") {
			$permissions = array("notice" => array("inquiry", "modify"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult);
				exit;
			}
			
			$noticeId = $_POST["notice_id"];
			$title = $_POST["title"];
			$contents = isset($_POST["contents"]) ? $_POST["contents"] : "";
			$image = isset($_POST["image"]) ? $_POST["image"] : "";
			$link = isset($_POST["link"]) ? $_POST["link"] : "";
			$start_date = $_POST["start_date"];
			$end_date = $_POST["end_date"];
		
			$notice_manager = new CNoticeManager();
			
			$retResult["data"] = $notice_manager->updateNotice($database->notices, $noticeId, $title, $contents, $image, $link, $start_date, $end_date);
		} else if($type == "delete_notice") {
			$permissions = array("notice" => array("inquiry", "delete"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult);
				exit;
			}
				
			$noticeId = $_POST["notice_id"];
		
			$notice_manager = new CNoticeManager();
				
			$retResult["data"] = $notice_manager->deleteNotice($database->notices, $noticeId);
		} else if($type == "shuttle_line_color_list") {
			$shuttle_manager = new CShuttleManager();			
			$retResult["data"] = $shuttle_manager->getShuttleLineColorList($database->shuttleinfos);
		} else if($type == "get_near_station") {
			$lat = floatval($_POST["lat"]);
			$lng = floatval($_POST["lng"]);
			$range = floatval($_POST["range"]);
			
			$shuttle_manager = new CShuttleManager();
			$driver_manager = new CDriverManager();
		
			$retShuttleMarkerPoints = $shuttle_manager->getShuttleStationList($database->shuttlepoints, $lat, $lng, $range);
			$retShuttleLinePoints = $shuttle_manager->getShuttleLineList($database->shuttlepoints, $lat, $lng, $range);
		
			$busIds = array();
			foreach ($retShuttleLinePoints as $row) {
				if(in_array($row["_id"]["busId"], $busIds)) {
					continue;
				}
		
				$busIds[] = new MongoId($row["_id"]["busId"]);
			}
		
			$retShuttles = $shuttle_manager->getShuttleInfoListByIds($database->shuttleinfos, $busIds);
		
			$retCarpoolRegistrantList = $shuttle_manager->getCarpoolRegistrantList($database->taxicarpools, $database->taxifinds, $lat, $lng, $range);
		
			$retBusStationList = $shuttle_manager->getBusStationList($database->busstations, $lat, $lng, $range);
		
			$retSubwayStationList = $shuttle_manager->getSubwayStationList($database->subwaystations, $database->subwaytimes, $lat, $lng, $range);
			
			$retDriverList = $driver_manager->getNearDriverList($database->taxifinds, $lat, $lng, $range);
		
			$retResult["data"] = array(
				"shuttleMarkerList" => $retShuttleMarkerPoints,
				"shuttleLineList" => $retShuttleLinePoints,
				"shuttleInfoList" => $retShuttles,
				"carpoolRegistrantList" => $retCarpoolRegistrantList,
				"subwayStationList" => $retSubwayStationList,
				"busStationList" => $retBusStationList,
				"driverList" => $retDriverList
			);
		} else if($type == "get_near_shuttle_line") {
			$lat = floatval($_POST["lat"]);
			$lng = floatval($_POST["lng"]);
			$range = floatval($_POST["range"]);
			
			$shuttle_manager = new CShuttleManager();
		
			$retShuttleLinePoints = $shuttle_manager->getShuttleLineList($database->shuttlepoints, $lat, $lng, $range);
		
			$retResult["data"] = $retShuttleLinePoints;
		} else if($type == "get_bus_route_list_by_station") {
			$stationId = $_POST["station_id"];
			
			$shuttle_manager = new CShuttleManager();

			$retResult["data"] = $shuttle_manager->getBusRouteListByStation($database->busroutes, $database->busroutestations, $database->busstations, $stationId);
		} else if($type == "get_shuttle_route_list_by_station") {
			$stationId = $_POST["station_id"];
		
			$shuttlepoints = $database->shuttlepoints;
			$shuttleinfos = $database->shuttleinfos;
			
			$shuttle_manager = new CShuttleManager();
		
			$retResult["data"] = $shuttle_manager->getShuttleRouteListByStation($database->shuttlepoints, $database->shuttleinfos, $stationId);
		} else if($type == "edit_call") {
			$callId = $_POST["call_id"];
			$start = $_POST["start"];
			$end = $_POST["end"];
			$price = $_POST["price"];
			$etc = $_POST["etc"];
		} else if($type == "search_driver") {
			$search_text = $_POST["search_text"];
			
			$shuttle_manager = new CShuttleManager();
			$retResult["data"] = $shuttle_manager->searchDriver($database->drivers, $database->taxifinds, $search_text);
		} else if($type == "update_drunken_driving_data") {
			$drunken_driving_data = $_POST["drunken_driving_data"];
			$drunkendriving = $database->drunkendriving;
			
			foreach ($drunken_driving_data as $row) {
				$row["updatedTime"] = time() * 1000;
				$drunkendriving->insert($row);
			}
		} else if($type == "add_account") {
			$permissions = array("account" => array("inquiry", "create"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult);
				exit;
			}
			
			$id = $_POST["id"];
			$pw = $_POST["pw"];
			$name = $_POST["name"];
			$phone = $_POST["phone"];
			$email = isset($_POST["email"]) ? $_POST["email"] : "";
			$level = $_POST["level"];
			$ynPush = $_POST["ynPush"];
			
			$account_manager = new CAccountManager();
			if($account_manager->isDuplicatedId($database->accounts, $id, null)) {
				$retResult["result"] = "DUPLICATE_ID";
				$retResult["msg"] = "이미 사용중인 아이디입니다.";
					
				echo json_encode($retResult);
				exit;
			}
			
			$retResult["data"] = $account_manager->addAccount($database->accounts, $id, $pw, $name, $phone, $email, $level, $ynPush);
		} else if($type == "edit_account") {
			$permissions = array("account" => array("inquiry", "modify"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult);
				exit;
			}
			
			$account_id = $_POST["account_id"];
			$id = $_POST["id"];
			$pw = isset($_POST["pw"]) ? $_POST["pw"] : "";
			$name = $_POST["name"];
			$phone = $_POST["phone"];
			$email = isset($_POST["email"]) ? $_POST["email"] : "";
			$level = $_POST["level"];
			$ynPush = $_POST["ynPush"];
			
			$account_manager = new CAccountManager();
			if($account_manager->isDuplicatedId($database->accounts, $id, $account_id)) {
				$retResult["result"] = "DUPLICATE_ID";
				$retResult["msg"] = "이미 사용중인 아이디입니다.";
					
				echo json_encode($retResult);
				exit;
			}
			
			$retResult["data"] = $account_manager->updateAccount($database->accounts, $account_id, $id, $pw, $name, $phone, $email, $level, $ynPush);
		} else if($type == "delete_account") {
			$permissions = array("account" => array("inquiry", "delete"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult);
				exit;
			}
			
			$account_id = $_POST["account_id"];
			
			$account_manager = new CAccountManager();
			$account_manager->deleteAccount($database->accounts, $account_id);
		} else if($type == "change_account_ynpush") {
			$permissions = array("account" => array("inquiry", "modify"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult);
				exit;
			}
			
			$account_id = $_POST["account_id"];
			$ynPush = $_POST["ynPush"];
			
			$account_manager = new CAccountManager();
			$account_manager->updateAccountYnPush($database->accounts, $account_id, $ynPush);
// 		} else if($type == "add_board") {
// 			$permissions = array("board" => array("inquiry", "create"));
// 			if(!$session->checkPermission($permissions)) {
// 				$retResult["result"] = "PERMISSION_DENIED";
// 				$retResult["msg"] = "접근권한이 없습니다.";
					
// 				echo json_encode($retResult);
// 				exit;
// 			}
			
// 			$driverId = "";
// 			$category = $_POST["category"];
// 			$title = $_POST["title"];
// 			$nickName = $_POST["nickName"];
// 			$sex = $_POST["sex"];
// 			$age = $_POST["age"];
// 			$career = $_POST["career"];
// 			$divide = $_POST["divide"];
// 			$range = $_POST["range"];
// 			$start = $_POST["start"];
// 			$time = $_POST["time"];
// 			$target = $_POST["target"];
// 			$car = $_POST["car"];
// 			$contact = $_POST["contact"];
// 			$etc = $_POST["etc"];
				
// 			$board_manager = new CBoardManager();
				
// 			$retResult["data"] = $board_manager->addBoard($database->boards, $driverId, $nickName, $title, $category, $sex, $age, $career, $divide, $range, $start, $time, $target, $car, $contact, $etc);
		} else if($type == "edit_board") {
			$permissions = array("board" => array("inquiry", "modify"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult);
				exit;
			}
				
			$driverId = "";
			$boardId = $_POST["board_id"];
			$category = $_POST["category"];
			$title = $_POST["title"];
			$nickName = $_POST["nickName"];
			$sex = $_POST["sex"];
			$age = $_POST["age"];
			$career = $_POST["career"];
			$divide = $_POST["divide"];
			$range = $_POST["range"];
			$start = $_POST["start"];
			$time = $_POST["time"];
			$target = $_POST["target"];
			$car = $_POST["car"];
			$contact = $_POST["contact"];
			$etc = $_POST["etc"];
			
			$board_manager = new CBoardManager();
			
			$retResult["data"] = $board_manager->updateBoard($database->boards, $boardId, $nickName, $title, $category, $sex, $age, $career, $divide, $range, $start, $time, $target, $car, $contact, $etc);
		} else if($type == "delete_board") {
			$permissions = array("board" => array("inquiry", "delete"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult);
				exit;
			}
				
			$boardId = $_POST["board_id"];
				
			$board_manager = new CBoardManager();
			$board_manager->deleteBoard($database->boards, $boardId);
		} else if($type == "edit_taxi_carpool") {
			$permissions = array("taxicarpool" => array("inquiry", "modify"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult);
				exit;
			}
		
			$driverId = "";
			$taxicarpoolId = $_POST["taxicarpool_id"];
			$registrant = $_POST["registrant"];
			$isValid = $_POST["isValid"];
			$isCatch = $_POST["isCatch"];
			$isPush = $_POST["isPush"];
			$money = $_POST["money"];
			$host = $_POST["host"];
			$time = $_POST["time"];
			$divideN = $_POST["divideN"];
			$drivers = $_POST["drivers"];
			$end = $_POST["end"];
			$start = $_POST["start"];
				
			$taxicarpool_manager = new CTaxiCarpoolManager();
			$retResult["data"] = $taxicarpool_manager->updateTaxiCarpool($database->taxicarpools, $taxicarpoolId, $registrant, $isValid, $isCatch, $isPush, $money, $host, $time, $divideN, $drivers, $end, $start);
		} else if($type == "delete_taxi_carpool") {
			$permissions = array("taxicarpool" => array("inquiry", "delete"));
			if(!$session->checkPermission($permissions)) {
				$retResult["result"] = "PERMISSION_DENIED";
				$retResult["msg"] = "접근권한이 없습니다.";
					
				echo json_encode($retResult);
				exit;
			}
		
			$taxicarpoolId = $_POST["taxicarpool_id"];
		
			$taxicarpool_manager = new CTaxiCarpoolManager();
			$taxicarpool_manager->deleteTaxiCarpool($database->taxicarpools, $taxicarpoolId);
		} else if($type == "send_push") {
			$push_title = $_POST["push_title"];
			$push_message = $_POST["push_message"];
			$recv_user = isset($_POST["recv_user"]) ? $_POST["recv_user"] : "";
			$recv_user_list = explode(",", $recv_user);
			$recv_user_type = $_POST["recv_user_type"];
			
			$message_length = strlen($push_message);
			if($message_length > MAX_PUS_MESSAGE_LENGTH) {
				$retResult["result"] = "ERROR";
				$retResult["msg"] = "메시지 최대 길이를 넘었습니다.";
					
				echo json_encode($retResult);
				exit;
			}
			
			$driver_manager = new CDriverManager();
			
			$imgUrl = "";
			if (isset($_FILES['push_img']) && is_uploaded_file($_FILES['push_img']['tmp_name'])) {
				$fileNameArr = explode(".", basename($_FILES['push_img']['name']));
				$uploadfile = "push/" . $fileNameArr[0] . "_" . time() . "." . $fileNameArr[1];
					
				if (move_uploaded_file($_FILES['push_img']['tmp_name'], CONF_PATH_UPLOAD . $uploadfile)) {
					$imgUrl = "http://115.68.122.108/upload/" . $uploadfile;
				}
			}
			
			$push_content = array();
			$push_content["data"] = array(
				"type" => "notice",
				"title" => $push_title,
				"message" => $push_message,
				"imgUrl" => $imgUrl
			);
			
			// 헤더 부분
			$headers = array(
				'Content-Type:application/json',
				'Authorization:key=' . GOOGLE_SERVER_KEY
			);
			
			$limit = 1000;
			$skip = 0;
			$retPush = 0;
			$hasNext = true;
			while($hasNext) {
				$ret_gcm_list = $driver_manager->getRegIdList($database->drivers, $recv_user_type, $recv_user_list, $skip, $limit);
				
				if($ret_gcm_list == null || count($ret_gcm_list) <= 0) {
					$hasNext = false;
					break;
				}
					
				$push_content['registration_ids'] = $ret_gcm_list;
								
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($push_content));
				$response = curl_exec($ch);
				curl_close($ch);
			
				// 푸시 전송 결과 반환.
				$obj = json_decode($response);
			
				if($obj != null) {
					// 푸시 전송시 성공 수량 반환.
					$retPush += $obj->success;

					if($obj->failure > 0) {
						debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $response);
					}
				} else {
					debug_mesg("E", __CLASS__, __FUNCTION__, __LINE__, $response);
				}
				
				$skip += $limit;
			}
			
			$retResult["data"] = $retPush;
		} else {
			$retResult = array("result" => "ERROR", "msg" => "정상적인 호출이 아닙니다.");
		}
	} catch (Exception $e) {
		$retResult["result"] = "ERROR";
		$retResult["msg"] = $e->getMessage();
	}
	
	echo json_encode($retResult);
	exit;
?>
