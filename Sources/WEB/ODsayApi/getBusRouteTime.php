<?php
	header("Content-Type:text/html; charset=utf-8");
	require_once("./db.php");

	$busroutes = $db->busroutes;
	$busstations = $db->busstations;
	
	$busRouteData = file_get_contents("./busRouteByRouteName.json");
	$busRouteData = json_decode($busRouteData, true);
	
	$noDataList = array();
	$noDataInLocalList = array();
	
	foreach ($busRouteData as $key => $row) {
		if(count($row) > 0) {
			foreach ($row as $routeData) {
				if(isset($routeData["busRouteId"])) {
					if($routeData["busRouteId"] != "0") {
						$localData = $busroutes->findOne(array('busRouteId' => $routeData["busRouteId"]));
				
						if(isset($localData)) {
							$localData["firstTime"] = $routeData["firstTime_ODsay"];
							$localData["lastTime"] = $routeData["lastTime_ODsay"];
							$localData["busRouteId_ODsay"] = $routeData["busRouteId_ODsay"];
							$localData["busRouteType_ODsay"] = $routeData["busRouteType_ODsay"];
							$localData["busRouteArea_ODsay"] = $routeData["busRouteArea_ODsay"];
							$localData["term"] = $routeData["term"];
								
							$busroutes->save($localData);
								
							updateBusStation($busstations, $localData["busRouteId"], $localData["firstTime"], $localData["lastTime"]);
								
							echo $key . "[" . $localData["busRouteId"] . "] updated\n";
						} else {
							$noDataInLocalList[] = $key;
							echo $key . " has no data in localDB;\n";
						}
					} else {
						$noDataInLocalList[] = $key;
						echo $key . " has no data in localDB;\n";
					}
				} else {
					$noDataList[] = $key;
					echo $key . " has no data;\n";
				}				
			}
		}
	}
	
	echo "<pre>noDataList";
	print_r(json_encode($noDataList, JSON_UNESCAPED_UNICODE));
	
	echo "<pre>noDataInLocalList";
	print_r(json_encode($noDataInLocalList, JSON_UNESCAPED_UNICODE));
	
	function updateBusStation($_busstations, $_busRouteId, $_first_time, $_last_time) {
		$busStationCursor = $_busstations->find(array("busRouteId" => $_busRouteId))->sort(array("stationSequence" => 1));
		
		$prev_lat = null;
		$prev_lng = null;
		$prev_first_time = $_first_time . ":00";
		$prev_last_time = $_last_time . ":00";
		
		$time_per_meter = floatval(180 / 1000);
		
		foreach ($busStationCursor as $row) {
			$increase_time = 0;
			$dist = 0;
			
			if($prev_lat != null) {
				$dist = get_distance($prev_lat, $prev_lng, $row["gpsY"], $row["gpsX"]);
				$increase_time = ($dist * $time_per_meter);
			}
			
			$first_time = date("H:i:s", strtotime(date("Ymd") . " " . $prev_first_time) + $increase_time);
			$last_time = date("H:i:s", strtotime(date("Ymd") . " " . $prev_last_time) + $increase_time);
			
			$row["startTime"] = substr($first_time, 0, 5);
			$row["endTime"] = substr($last_time, 0, 5);
			
			$_busstations->save($row);
			
			$prev_lat = $row["gpsY"];
			$prev_lng = $row["gpsX"];
				
			$prev_first_time = $first_time;
			$prev_last_time = $last_time;
		}
	}
	
	function get_distance($lat1, $lon1, $lat2, $lon2) {
		/* WGS84 stuff */
		$a = 6378137;
		$b = 6356752.3142;
		$f = 1/298.257223563;
		/* end of WGS84 stuff */
	
		$L = deg2rad($lon2-$lon1);
		$U1 = atan((1-$f) * tan(deg2rad($lat1)));
		$U2 = atan((1-$f) * tan(deg2rad($lat2)));
		$sinU1 = sin($U1);
		$cosU1 = cos($U1);
		$sinU2 = sin($U2);
		$cosU2 = cos($U2);
	
		$lambda = $L;
		$lambdaP = 2*pi();
		$iterLimit = 20;
		while ((abs($lambda-$lambdaP) > pow(10, -12)) && ($iterLimit-- > 0)) {
			$sinLambda = sin($lambda);
			$cosLambda = cos($lambda);
			$sinSigma = sqrt(($cosU2*$sinLambda) * ($cosU2*$sinLambda) + ($cosU1*$sinU2-$sinU1*$cosU2*$cosLambda) * ($cosU1*$sinU2-$sinU1*$cosU2*$cosLambda));
	
			if ($sinSigma == 0) {
				return 0;
			}
	
			$cosSigma   = $sinU1*$sinU2 + $cosU1*$cosU2*$cosLambda;
			$sigma      = atan2($sinSigma, $cosSigma);
			$sinAlpha   = $cosU1 * $cosU2 * $sinLambda / $sinSigma;
			$cosSqAlpha = 1 - $sinAlpha*$sinAlpha;
			$cos2SigmaM = $cosSigma - 2*$sinU1*$sinU2/$cosSqAlpha;
	
			if (is_nan($cos2SigmaM)) {
				$cos2SigmaM = 0;
			}
	
			$C = $f/16*$cosSqAlpha*(4+$f*(4-3*$cosSqAlpha));
			$lambdaP = $lambda;
			$lambda = $L + (1-$C) * $f * $sinAlpha *($sigma + $C*$sinSigma*($cos2SigmaM+$C*$cosSigma*(-1+2*$cos2SigmaM*$cos2SigmaM)));
		}
	
		if ($iterLimit == 0) {
			// formula failed to converge
			return NaN;
		}
	
		$uSq = $cosSqAlpha * ($a*$a - $b*$b) / ($b*$b);
		$A = 1 + $uSq/16384*(4096+$uSq*(-768+$uSq*(320-175*$uSq)));
		$B = $uSq/1024 * (256+$uSq*(-128+$uSq*(74-47*$uSq)));
		$deltaSigma = $B*$sinSigma*($cos2SigmaM+$B/4*($cosSigma*(-1+2*$cos2SigmaM*$cos2SigmaM)- $B/6*$cos2SigmaM*(-3+4*$sinSigma*$sinSigma)*(-3+4*$cos2SigmaM*$cos2SigmaM)));
	
		// 		return round($b*$A*($sigma-$deltaSigma) / 1000);
		return round($b*$A*($sigma-$deltaSigma));
	
	
		/* sphere way */
		$distance = rad2deg(acos(sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($lon1 - $lon2))));
	
		$distance *= 111.18957696; // Convert to km
	
		return $distance;
	}
	
	exit;
	
	
?>