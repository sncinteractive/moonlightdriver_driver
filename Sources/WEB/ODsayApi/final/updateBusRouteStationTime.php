<?php
	header("Content-Type:text/html; charset=utf-8");
	require_once("../db.php");

	$busroutes = $db->busroutes;
	$busstations = $db->busstations;
	$busroutestations = $db->busroutestations;
	
	$busRouteCursor = $busroutes->find()->sort(array("busRouteId" => 1))->skip(13051);
	
	$i = 0;
	foreach ($busRouteCursor as $route_row) {
		updateBusStation($busroutestations, $busstations, $route_row["busRouteId"], $route_row["firstTime"], $route_row["lastTime"]);
		
		echo "(" . (++$i) . ") " . $route_row["busRouteName"] . "[" . $route_row["busRouteId"] . "]\n";
	}
	
	function updateBusStation($_busroutestations, $_busstations, $_busRouteId, $_first_time, $_last_time) {
		$busRouteStationCursor = $_busroutestations->find(array("busRouteId" => $_busRouteId))->sort(array("stationSequence" => 1));
		
		$isNextDayFirstTime = false;
		$isNextDayLastTime = false;
		
		if(intval(substr($_first_time, 0, 2)) > 23) {
			$isNextDayFirstTime = true;
			$first_time_hour = intval(substr($_first_time, 0, 2)) - 24;
			if($first_time_hour < 10) {
				$first_time_hour = '0' . $first_time_hour;
			}
			
			$_first_time = $first_time_hour . substr($_first_time, 2, 3);
		}
		
		if(intval(substr($_last_time, 0, 2)) > 23) {
			$isNextDayLastTime = true;
			$last_time_hour = intval(substr($_last_time, 0, 2)) - 24;
			if($last_time_hour < 10) {
				$last_time_hour = '0' . $last_time_hour;
			}
			
			$_last_time = $last_time_hour . substr($_last_time, 2, 3);
		}
		
		$prev_lat = null;
		$prev_lng = null;
		$prev_first_time = $_first_time . ":00";
		$prev_last_time = $_last_time . ":00";
		
		$time_per_meter = floatval(120 / 1000);
		
		foreach ($busRouteStationCursor as $row) {
			$station = $_busstations->findOne(array("arsId" => $row["arsId"]));
			
			$increase_time = 0;
			$dist = 0;
			
			if($prev_lat != null) {
				$dist = get_distance($prev_lat, $prev_lng, $station["gpsY"], $station["gpsX"]);
				$increase_time = ($dist * $time_per_meter);
			}
			
			$first_time = date("H:i:s", strtotime(date("Ymd") . " " . $prev_first_time) + $increase_time);
			$last_time = date("H:i:s", strtotime(date("Ymd") . " " . $prev_last_time) + $increase_time);
			$converted_first_time = $first_time;
			$converted_last_time = $last_time;
			
			if(!$isNextDayFirstTime && intval(substr($prev_first_time, 0, 2)) > intval(substr($first_time, 0, 2))) {
				$isNextDayFirstTime = true;
			}
			
			if(!$isNextDayLastTime && intval(substr($prev_last_time, 0, 2)) > intval(substr($last_time, 0, 2))) {
				$isNextDayLastTime = true;
			}
			
			if($isNextDayFirstTime) {
				$firstTimeHour = strval(intval(substr($converted_first_time, 0, 2)) + 24);
				$converted_first_time = $firstTimeHour . substr($converted_first_time, 2, 3);
			}
			
			if($isNextDayLastTime) {
				$lastTimeHour = strval(intval(substr($converted_last_time, 0, 2)) + 24);
				$converted_last_time = $lastTimeHour . substr($last_time, 2, 3);
			}
			
			$row["startTime"] = substr($converted_first_time, 0, 5);
			$row["endTime"] = substr($converted_last_time, 0, 5);
			$row["location"] = array(
				"type" => "Point",
				"coordinates" => array($station["gpsX"], $station["gpsY"])
			);
			
			if(strlen($row["startTime"]) != 5) {
				echo "ERROR : " . $row["startTime"] . "\n";
			}
			
			if(strlen($row["endTime"]) != 5) {
				echo "ERROR : " . $row["endTime"] . "\n";
			}
			
			$_busroutestations->save($row);
			
			$prev_lat = $station["gpsY"];
			$prev_lng = $station["gpsX"];
				
			$prev_first_time = $first_time;
			$prev_last_time = $last_time;
		}
	}
	
	function get_distance($lat1, $lon1, $lat2, $lon2) {
		/* WGS84 stuff */
		$a = 6378137;
		$b = 6356752.3142;
		$f = 1/298.257223563;
		/* end of WGS84 stuff */
	
		$L = deg2rad($lon2-$lon1);
		$U1 = atan((1-$f) * tan(deg2rad($lat1)));
		$U2 = atan((1-$f) * tan(deg2rad($lat2)));
		$sinU1 = sin($U1);
		$cosU1 = cos($U1);
		$sinU2 = sin($U2);
		$cosU2 = cos($U2);
	
		$lambda = $L;
		$lambdaP = 2*pi();
		$iterLimit = 20;
		while ((abs($lambda-$lambdaP) > pow(10, -12)) && ($iterLimit-- > 0)) {
			$sinLambda = sin($lambda);
			$cosLambda = cos($lambda);
			$sinSigma = sqrt(($cosU2*$sinLambda) * ($cosU2*$sinLambda) + ($cosU1*$sinU2-$sinU1*$cosU2*$cosLambda) * ($cosU1*$sinU2-$sinU1*$cosU2*$cosLambda));
	
			if ($sinSigma == 0) {
				return 0;
			}
	
			$cosSigma   = $sinU1*$sinU2 + $cosU1*$cosU2*$cosLambda;
			$sigma      = atan2($sinSigma, $cosSigma);
			$sinAlpha   = $cosU1 * $cosU2 * $sinLambda / $sinSigma;
			$cosSqAlpha = 1 - $sinAlpha*$sinAlpha;
			$cos2SigmaM = $cosSigma - 2*$sinU1*$sinU2/$cosSqAlpha;
	
			if (is_nan($cos2SigmaM)) {
				$cos2SigmaM = 0;
			}
	
			$C = $f/16*$cosSqAlpha*(4+$f*(4-3*$cosSqAlpha));
			$lambdaP = $lambda;
			$lambda = $L + (1-$C) * $f * $sinAlpha *($sigma + $C*$sinSigma*($cos2SigmaM+$C*$cosSigma*(-1+2*$cos2SigmaM*$cos2SigmaM)));
		}
	
		if ($iterLimit == 0) {
			// formula failed to converge
			return NaN;
		}
	
		$uSq = $cosSqAlpha * ($a*$a - $b*$b) / ($b*$b);
		$A = 1 + $uSq/16384*(4096+$uSq*(-768+$uSq*(320-175*$uSq)));
		$B = $uSq/1024 * (256+$uSq*(-128+$uSq*(74-47*$uSq)));
		$deltaSigma = $B*$sinSigma*($cos2SigmaM+$B/4*($cosSigma*(-1+2*$cos2SigmaM*$cos2SigmaM)- $B/6*$cos2SigmaM*(-3+4*$sinSigma*$sinSigma)*(-3+4*$cos2SigmaM*$cos2SigmaM)));
	
		// 		return round($b*$A*($sigma-$deltaSigma) / 1000);
		return round($b*$A*($sigma-$deltaSigma));
	
	
		/* sphere way */
		$distance = rad2deg(acos(sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($lon1 - $lon2))));
	
		$distance *= 111.18957696; // Convert to km
	
		return $distance;
	}
	
	exit;
	
	
?>