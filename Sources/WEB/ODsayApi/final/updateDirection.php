<?php
	header("Content-Type:text/html; charset=utf-8");
	require_once("../db.php");
	
	$SERVICE_ID = "0aab2ced55dc3c47f901b51414464099";
	
	$busstations = $db->busstations;
	
	$skipCount = 0;
	while(true) {
		$busStationCursor = $busstations->find(array("localStationId" => array('$exists' => false)))->sort(array("stationIndex" => 1))->skip($skipCount)->limit(1);
		
		foreach ($busStationCursor as $row) {
			$stationName = str_replace(" ", "", $row["stationName"]);
			$limit = 10;
			$start = 1;
			
			$isUpdated = false;
			while(true) {
				echo $stationName . "[" . $row["stationIndex"] . "]\n";
			
				$retJson = file_get_contents("http://dev.odsay.com/denny_test/appletree/v1/0/Bus/Station/Search.asp?stationName=" . $stationName . "&stationClass=1&svcid=" . $SERVICE_ID . "&startNo=" . $start . "&displayCnt=" . $limit . "&output=json");
				$ret = str_replace("[,", "[", $retJson);
			
				$retList = json_decode($ret, true);
					
				if(isset($retList["result"]) && $retList["result"] != null) {
					$totalCount = $retList["result"]["totalCount"];
			
					if(isset($retList)) {
						$start = $start + $limit;
			
						foreach ($retList["result"]["station"] as $stations) {
							$row["localStationId"] = $stations["localStationID"];
							$row["stationId"] = $stations["stationID"];
							$row["arsId"] = $stations["arsID"];
			
							$busstations->update(array("arsId" => $stations["stationID"]), array('$set' => array("localStationId" => $stations["localStationID"], "stationId" => $stations["stationID"], "arsId" => $stations["arsID"])));
			
							echo $stations["stationName"] . "[" . $stations["stationID"] . "] - " . $stations["localStationID"];
							echo "\n";
							
							if($stations["stationID"] == $row) {
								$isUpdated = true;
							}
						}
							
						if(count($retList["result"]["station"]) < 10) {
							echo "\n";
							break;
						}
					}
				} else {
					echo "\n";
					break;
				}
			}
			
			if(!$isUpdated) {
				$skipCount++;
			}
		}
	}
?>
