package kr.moonlightdriver.driver.Network.NetData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 * Created by youngmin on 2015-11-05.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShuttlePathData {
	@JsonProperty("busId")
	private String mShuttleId;
	@JsonProperty("name")
	private String mShuttleName;
	@JsonProperty("desc")
	private String mDescription;
	@JsonProperty("phone")
	private String mPhone;
	@JsonProperty("comment")
	private ArrayList<ShuttleChatData> mShuttleChatData;
	@JsonProperty("category")
	private int mCategory;
	@JsonProperty("firstTime")
	private String mFirstTime;
	@JsonProperty("lastTime")
	private String mLastTime;
	@JsonProperty("saturdayFirstTime")
	private String mSaturdayFirstTime;
	@JsonProperty("saturdayLastTime")
	private String mSaturdayLastTime;
	@JsonProperty("sundayFirstTime")
	private String mSundayFirstTime;
	@JsonProperty("sundayLastTime")
	private String mSundayLastTime;
	@JsonProperty("runFirstTime")
	private String mRunFirstTime;
	@JsonProperty("runLastTime")
	private String mRunLastTime;
	@JsonProperty("saturdayRunFirstTime")
	private String mSaturdayRunFirstTime;
	@JsonProperty("saturdayRunLastTime")
	private String mSaturdayRunLastTime;
	@JsonProperty("sundayRunFirstTime")
	private String mSundayRunFirstTime;
	@JsonProperty("sundayRunLastTime")
	private String mSundayRunLastTime;
	@JsonProperty("holidayDate")
	private String mHolidayDate;
	@JsonProperty("term")
	private int mTerm;
	@JsonProperty("totalTime")
	private int mTotalTime;
	@JsonProperty("lineColor")
	private String mLineColor;
	@JsonProperty("lists")
	private ArrayList<ShuttlePathPointData> mShuttlePathPointData;
	@JsonProperty("realTimeCategoryName")
	private String mRealTimeCategoryName;

	private String mStartStation;
	private String mEndStation;

	public ShuttlePathData() {}

	public ShuttlePathData(int mCategory, String mDescription, String mEndStation, String mFirstTime, String mHolidayDate, String mLastTime, String mLineColor, String mPhone, String mRealTimeCategoryName, String mRunFirstTime, String mRunLastTime, String mSaturdayFirstTime, String mSaturdayLastTime, String mSaturdayRunFirstTime, String mSaturdayRunLastTime, ArrayList<ShuttleChatData> mShuttleChatData, String mShuttleId, String mShuttleName, ArrayList<ShuttlePathPointData> mShuttlePathPointData, String mStartStation, String mSundayFirstTime, String mSundayLastTime, String mSundayRunFirstTime, String mSundayRunLastTime, int mTerm, int mTotalTime) {
		this.mCategory = mCategory;
		this.mDescription = mDescription;
		this.mEndStation = mEndStation;
		this.mFirstTime = mFirstTime;
		this.mHolidayDate = mHolidayDate;
		this.mLastTime = mLastTime;
		this.mLineColor = mLineColor;
		this.mPhone = mPhone;
		this.mRealTimeCategoryName = mRealTimeCategoryName;
		this.mRunFirstTime = mRunFirstTime;
		this.mRunLastTime = mRunLastTime;
		this.mSaturdayFirstTime = mSaturdayFirstTime;
		this.mSaturdayLastTime = mSaturdayLastTime;
		this.mSaturdayRunFirstTime = mSaturdayRunFirstTime;
		this.mSaturdayRunLastTime = mSaturdayRunLastTime;
		this.mShuttleChatData = mShuttleChatData;
		this.mShuttleId = mShuttleId;
		this.mShuttleName = mShuttleName;
		this.mShuttlePathPointData = mShuttlePathPointData;
		this.mStartStation = mStartStation;
		this.mSundayFirstTime = mSundayFirstTime;
		this.mSundayLastTime = mSundayLastTime;
		this.mSundayRunFirstTime = mSundayRunFirstTime;
		this.mSundayRunLastTime = mSundayRunLastTime;
		this.mTerm = mTerm;
		this.mTotalTime = mTotalTime;
	}

	public String getmLineColor() {
		return mLineColor;
	}

	public void setmLineColor(String mLineColor) {
		this.mLineColor = mLineColor;
	}

	public String getmEndStation() {
		return mEndStation;
	}

	public void setmEndStation(String mEndStation) {
		this.mEndStation = mEndStation;
	}

	public String getmStartStation() {
		return mStartStation;
	}

	public void setmStartStation(String mStartStation) {
		this.mStartStation = mStartStation;
	}

	public int getmCategory() {
		return mCategory;
	}

	public void setmCategory(int mCategory) {
		this.mCategory = mCategory;
	}

	public String getmDescription() {
		return mDescription;
	}

	public void setmDescription(String mDescription) {
		this.mDescription = mDescription;
	}

	public String getmFirstTime() {
		return mFirstTime;
	}

	public void setmFirstTime(String mFirstTime) {
		this.mFirstTime = mFirstTime;
	}

	public String getmLastTime() {
		return mLastTime;
	}

	public void setmLastTime(String mLastTime) {
		this.mLastTime = mLastTime;
	}

	public ArrayList<ShuttleChatData> getmShuttleChatData() {
		return mShuttleChatData;
	}

	public void setmShuttleChatData(ArrayList<ShuttleChatData> mShuttleChatData) {
		this.mShuttleChatData = mShuttleChatData;
	}

	public String getmShuttleId() {
		return mShuttleId;
	}

	public void setmShuttleId(String mShuttleId) {
		this.mShuttleId = mShuttleId;
	}

	public String getmShuttleName() {
		return mShuttleName;
	}

	public void setmShuttleName(String mShuttleName) {
		this.mShuttleName = mShuttleName;
	}

	public ArrayList<ShuttlePathPointData> getmShuttlePathPointData() {
		return mShuttlePathPointData;
	}

	public void setmShuttlePathPointData(ArrayList<ShuttlePathPointData> mShuttlePathPointData) {
		this.mShuttlePathPointData = mShuttlePathPointData;
	}

	public int getmTerm() {
		return mTerm;
	}

	public void setmTerm(int mTerm) {
		this.mTerm = mTerm;
	}

	public int getmTotalTime() {
		return mTotalTime;
	}

	public void setmTotalTime(int mTotalTime) {
		this.mTotalTime = mTotalTime;
	}

	public String getmRealTimeCategoryName() {
		return mRealTimeCategoryName;
	}

	public void setmRealTimeCategoryName(String mRealTimeCategoryName) {
		this.mRealTimeCategoryName = mRealTimeCategoryName;
	}

	public String getmSaturdayFirstTime() {
		return mSaturdayFirstTime;
	}

	public void setmSaturdayFirstTime(String mSaturdayFirstTime) {
		this.mSaturdayFirstTime = mSaturdayFirstTime;
	}

	public String getmSaturdayLastTime() {
		return mSaturdayLastTime;
	}

	public void setmSaturdayLastTime(String mSaturdayLastTime) {
		this.mSaturdayLastTime = mSaturdayLastTime;
	}

	public String getmSundayFirstTime() {
		return mSundayFirstTime;
	}

	public void setmSundayFirstTime(String mSundayFirstTime) {
		this.mSundayFirstTime = mSundayFirstTime;
	}

	public String getmSundayLastTime() {
		return mSundayLastTime;
	}

	public void setmSundayLastTime(String mSundayLastTime) {
		this.mSundayLastTime = mSundayLastTime;
	}

	public String getmHolidayDate() {
		return mHolidayDate;
	}

	public void setmHolidayDate(String mHolidayDate) {
		this.mHolidayDate = mHolidayDate;
	}

	public String getmRunFirstTime() {
		return mRunFirstTime;
	}

	public void setmRunFirstTime(String mRunFirstTime) {
		this.mRunFirstTime = mRunFirstTime;
	}

	public String getmRunLastTime() {
		return mRunLastTime;
	}

	public void setmRunLastTime(String mRunLastTime) {
		this.mRunLastTime = mRunLastTime;
	}

	public String getmSaturdayRunFirstTime() {
		return mSaturdayRunFirstTime;
	}

	public void setmSaturdayRunFirstTime(String mSaturdayRunFirstTime) {
		this.mSaturdayRunFirstTime = mSaturdayRunFirstTime;
	}

	public String getmSaturdayRunLastTime() {
		return mSaturdayRunLastTime;
	}

	public void setmSaturdayRunLastTime(String mSaturdayRunLastTime) {
		this.mSaturdayRunLastTime = mSaturdayRunLastTime;
	}

	public String getmSundayRunFirstTime() {
		return mSundayRunFirstTime;
	}

	public void setmSundayRunFirstTime(String mSundayRunFirstTime) {
		this.mSundayRunFirstTime = mSundayRunFirstTime;
	}

	public String getmSundayRunLastTime() {
		return mSundayRunLastTime;
	}

	public void setmSundayRunLastTime(String mSundayRunLastTime) {
		this.mSundayRunLastTime = mSundayRunLastTime;
	}

	public String getmPhone() {
		return mPhone;
	}

	public void setmPhone(String mPhone) {
		this.mPhone = mPhone;
	}
}
