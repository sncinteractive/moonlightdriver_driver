package kr.moonlightdriver.driver.Network.NetData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by youngmin on 2015-11-05.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class FrequentlyUsedWordData {
	@JsonProperty("wordId")
	private String mWordId;
	@JsonProperty("word")
	private String mWord;
	@JsonProperty("usedCount")
	private int mUsedCount;
	@JsonProperty("priority")
	private int mPriority;

	public FrequentlyUsedWordData() {}

	public FrequentlyUsedWordData(int mPriority, int mUsedCount, String mWord, String mWordId) {
		this.mPriority = mPriority;
		this.mUsedCount = mUsedCount;
		this.mWord = mWord;
		this.mWordId = mWordId;
	}

	public int getmPriority() {
		return mPriority;
	}

	public void setmPriority(int mPriority) {
		this.mPriority = mPriority;
	}

	public int getmUsedCount() {
		return mUsedCount;
	}

	public void setmUsedCount(int mUsedCount) {
		this.mUsedCount = mUsedCount;
	}

	public String getmWord() {
		return mWord;
	}

	public void setmWord(String mWord) {
		this.mWord = mWord;
	}

	public String getmWordId() {
		return mWordId;
	}

	public void setmWordId(String mWordId) {
		this.mWordId = mWordId;
	}
}
