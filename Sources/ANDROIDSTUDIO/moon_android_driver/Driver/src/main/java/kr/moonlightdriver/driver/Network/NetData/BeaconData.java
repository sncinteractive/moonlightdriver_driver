package kr.moonlightdriver.driver.Network.NetData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by lk on 2016. 10. 2..
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class BeaconData {
    @JsonProperty("_id")
    private String mId;
    @JsonProperty("beaconMac")
    private String mBeaconMac;
    @JsonProperty("busId")
    private String mBusId;
    @JsonProperty("busNum")
    private String mBusNum;
    @JsonProperty("text")
    private String mText;
    @JsonProperty("lastTime")
    private String mLastTime;
    @JsonProperty("createdAt")
    private String mCreatedAt;
    @JsonProperty("locations")
    public ShuttleLocationData mLocation;

    public BeaconData(){}

    public BeaconData(String mId, String mBeaconMac, String mBusId, String mBusNum, String mText, String mLastTime, String mCreatedAt, ShuttleLocationData mLocation) {
        this.mId = mId;
        this.mBeaconMac = mBeaconMac;
        this.mBusId = mBusId;
        this.mBusNum = mBusNum;
        this.mText = mText;
        this.mLastTime = mLastTime;
        this.mCreatedAt = mCreatedAt;
        this.mLocation = mLocation;
    }

    public String getmId() {
        return mId;
    }

    public String getmBeaconMac() {
        return mBeaconMac;
    }

    public String getmBusId() {
        return mBusId;
    }

    public String getmBusNum() {
        return mBusNum;
    }

    public String getmText() {
        return mText;
    }

    public String getmLastTime() {
        return mLastTime;
    }

    public String getmCreatedAt() {
        return mCreatedAt;
    }

    public ShuttleLocationData getLocation() {
        return mLocation;
    }
}
