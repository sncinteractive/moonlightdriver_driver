package kr.moonlightdriver.driver.Network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import kr.moonlightdriver.driver.Network.NetData.ResultData;

/**
 * Created by lk on 2017. 4. 24..
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class BusTakeAck  extends NetAPI {
    @JsonProperty("result")
    private ResultData mResultData;

    public ResultData getmResultData() {
        return mResultData;
    }

    public BusTakeAck(){}

    public BusTakeAck(ResultData mResultData) {

        this.mResultData = mResultData;
    }
}
