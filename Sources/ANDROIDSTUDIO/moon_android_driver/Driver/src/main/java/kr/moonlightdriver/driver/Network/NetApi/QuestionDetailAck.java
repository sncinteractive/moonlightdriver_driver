package kr.moonlightdriver.driver.Network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

import kr.moonlightdriver.driver.Network.NetData.QuestionAnswerData;
import kr.moonlightdriver.driver.Network.NetData.QuestionData;
import kr.moonlightdriver.driver.Network.NetData.ResultData;

/**
 * Created by youngmin on 2015-10-13.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class QuestionDetailAck extends NetAPI {
	@JsonProperty("result")
	private ResultData mResultData;

	@JsonProperty("question")
	private QuestionData mQuestionDetailData;

	@JsonProperty("answer")
	private ArrayList<QuestionAnswerData> mQuestionAnswerDataList;

	public QuestionDetailAck() {}

	public QuestionDetailAck(ArrayList<QuestionAnswerData> mQuestionAnswerDataList, QuestionData mQuestionDetailData, ResultData mResultData) {
		this.mQuestionAnswerDataList = mQuestionAnswerDataList;
		this.mQuestionDetailData = mQuestionDetailData;
		this.mResultData = mResultData;
	}

	public ArrayList<QuestionAnswerData> getmQuestionAnswerDataList() {
		return mQuestionAnswerDataList;
	}

	public void setmQuestionAnswerDataList(ArrayList<QuestionAnswerData> mQuestionAnswerDataList) {
		this.mQuestionAnswerDataList = mQuestionAnswerDataList;
	}

	public QuestionData getmQuestionDetailData() {
		return mQuestionDetailData;
	}

	public void setmQuestionDetailData(QuestionData mQuestionDetailData) {
		this.mQuestionDetailData = mQuestionDetailData;
	}

	public ResultData getmResultData() {
		return mResultData;
	}

	public void setmResultData(ResultData mResultData) {
		this.mResultData = mResultData;
	}
}
