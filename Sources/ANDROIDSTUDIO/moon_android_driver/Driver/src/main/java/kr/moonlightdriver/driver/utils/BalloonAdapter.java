package kr.moonlightdriver.driver.utils;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.Marker;

import kr.moonlightdriver.driver.R;

/**
 * Created by youngmin on 2015-12-30.
 */
public class BalloonAdapter implements InfoWindowAdapter {
	LayoutInflater mInflater = null;
	private TextView mBalloonInfoText;

	public BalloonAdapter(LayoutInflater _inflater) {
		this.mInflater = _inflater;
	}

	@Override
	public View getInfoWindow(Marker _marker) {
		try {
			View v = mInflater.inflate(R.layout.balloon, null);

			if(_marker != null) {
				mBalloonInfoText = (TextView) v.findViewById(R.id.balloon_info_text);
				mBalloonInfoText.setText(_marker.getTitle());
			}

			return v;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public View getInfoContents(Marker marker) {
		return null;
	}
}
