package kr.moonlightdriver.driver.Network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

import kr.moonlightdriver.driver.Network.NetData.BusRouteListDataByStationId;
import kr.moonlightdriver.driver.Network.NetData.BusStationListData;
import kr.moonlightdriver.driver.Network.NetData.ResultData;

/**
 * Created by youngmin on 2015-10-13.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BusRouteListByStationIdAck extends NetAPI {
	@JsonProperty("result")
	private ResultData mResultData;

	@JsonProperty("busRouteList")
	private ArrayList<BusRouteListDataByStationId> mBusRouteDataListByStationId;

	@JsonProperty("stationInfo")
	private BusStationListData mBusStationDetailData;

	public BusRouteListByStationIdAck() {}

	public BusRouteListByStationIdAck(ArrayList<BusRouteListDataByStationId> mBusRouteDataListByStationId, BusStationListData mBusStationDetailData, ResultData mResultData) {
		this.mBusRouteDataListByStationId = mBusRouteDataListByStationId;
		this.mBusStationDetailData = mBusStationDetailData;
		this.mResultData = mResultData;
	}

	public ArrayList<BusRouteListDataByStationId> getmBusRouteDataListByStationId() {
		return mBusRouteDataListByStationId;
	}

	public void setmBusRouteDataListByStationId(ArrayList<BusRouteListDataByStationId> mBusRouteDataListByStationId) {
		this.mBusRouteDataListByStationId = mBusRouteDataListByStationId;
	}

	public BusStationListData getmBusStationDetailData() {
		return mBusStationDetailData;
	}

	public void setmBusStationDetailData(BusStationListData mBusStationDetailData) {
		this.mBusStationDetailData = mBusStationDetailData;
	}

	public ResultData getmResultData() {
		return mResultData;
	}

	public void setmResultData(ResultData mResultData) {
		this.mResultData = mResultData;
	}
}
