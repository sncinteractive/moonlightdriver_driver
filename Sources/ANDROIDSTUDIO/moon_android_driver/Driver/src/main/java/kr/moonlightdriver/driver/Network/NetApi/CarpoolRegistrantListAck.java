package kr.moonlightdriver.driver.Network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

import kr.moonlightdriver.driver.Network.NetData.CarpoolRegistrantData;
import kr.moonlightdriver.driver.Network.NetData.DriverPointData;
import kr.moonlightdriver.driver.Network.NetData.ResultData;

/**
 * Created by youngmin on 2015-10-13.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CarpoolRegistrantListAck extends NetAPI {
	@JsonProperty("result")
	private ResultData mResultData;
	@JsonProperty("taxiCarpoolRegistrant")
	private ArrayList<CarpoolRegistrantData> mCarpoolRegistrantDataList;
	@JsonProperty("driverList")
	private ArrayList<DriverPointData> mNearDriverList;

	public CarpoolRegistrantListAck() {}

	public CarpoolRegistrantListAck(ArrayList<CarpoolRegistrantData> mCarpoolRegistrantDataList, ArrayList<DriverPointData> mNearDriverList, ResultData mResultData) {
		this.mCarpoolRegistrantDataList = mCarpoolRegistrantDataList;
		this.mNearDriverList = mNearDriverList;
		this.mResultData = mResultData;
	}

	public ArrayList<CarpoolRegistrantData> getmCarpoolRegistrantDataList() {
		return mCarpoolRegistrantDataList;
	}

	public void setmCarpoolRegistrantDataList(ArrayList<CarpoolRegistrantData> mCarpoolRegistrantDataList) {
		this.mCarpoolRegistrantDataList = mCarpoolRegistrantDataList;
	}

	public ArrayList<DriverPointData> getmNearDriverList() {
		return mNearDriverList;
	}

	public void setmNearDriverList(ArrayList<DriverPointData> mNearDriverList) {
		this.mNearDriverList = mNearDriverList;
	}

	public ResultData getmResultData() {
		return mResultData;
	}

	public void setmResultData(ResultData mResultData) {
		this.mResultData = mResultData;
	}
}
