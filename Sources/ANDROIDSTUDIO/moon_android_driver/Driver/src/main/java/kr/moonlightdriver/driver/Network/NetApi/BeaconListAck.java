package kr.moonlightdriver.driver.Network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

import kr.moonlightdriver.driver.Network.NetData.BeaconData;
import kr.moonlightdriver.driver.Network.NetData.ResultData;

/**
 * Created by lk on 2016. 10. 2..
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class BeaconListAck extends NetAPI {
    @JsonProperty("result")
    private ResultData mResultData;
    @JsonProperty("beacon_list")
    private ArrayList<BeaconData> mBeaconListData;

    public BeaconListAck(){}

    public BeaconListAck(ResultData mResultData, ArrayList<BeaconData> mBeaconListData) {
        this.mResultData = mResultData;
        this.mBeaconListData = mBeaconListData;
    }

    public ResultData getmResultData() {
        return mResultData;
    }

    public ArrayList<BeaconData> getmBeaconListData() {
        return mBeaconListData;
    }
}
