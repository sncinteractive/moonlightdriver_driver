package kr.moonlightdriver.driver.Network.NetData;

/**
 * Created by youngmin on 2015-12-31.
 */
public class BusRouteListData {
	private String mRouteName;
	private String mBusRouteId;
	private String mDirection;
	private String mNextStation;
	private String mMsg1;
	private String mMsg2;
	private int mRouteType;
	private String mRouteTypeName;
	private String mLocalBusRouteId;
	private String mBusRoutArea;

	public BusRouteListData() {}

	public BusRouteListData(int _routeType, String _routeTypeName, String _routeName, String _busRouteId, String _direction, String _nextStation, String _msg1, String _msg2, String _localBusRouteId, String _busRouteArea) {
		this.mRouteType = _routeType;
		this.mRouteName = _routeName;
		this.mBusRouteId = _busRouteId;
		this.mDirection = _direction;
		this.mNextStation = _nextStation;
		this.mMsg1 = _msg1;
		this.mMsg2 = _msg2;
		this.mRouteTypeName = _routeTypeName;
		this.mLocalBusRouteId = _localBusRouteId;
		this.mBusRoutArea = _busRouteArea;
	}

	public String getmBusRoutArea() {
		return mBusRoutArea;
	}

	public void setmBusRoutArea(String mBusRoutArea) {
		this.mBusRoutArea = mBusRoutArea;
	}

	public String getmLocalBusRouteId() {
		return mLocalBusRouteId;
	}

	public void setmLocalBusRouteId(String mLocalBusRouteId) {
		this.mLocalBusRouteId = mLocalBusRouteId;
	}

	public String getmBusRouteId() {
		return mBusRouteId;
	}

	public void setmBusRouteId(String mBusRouteId) {
		this.mBusRouteId = mBusRouteId;
	}

	public String getmDirection() {
		return mDirection;
	}

	public void setmDirection(String mDirection) {
		this.mDirection = mDirection;
	}

	public String getmMsg1() {
		return mMsg1;
	}

	public void setmMsg1(String mMsg1) {
		this.mMsg1 = mMsg1;
	}

	public String getmMsg2() {
		return mMsg2;
	}

	public void setmMsg2(String mMsg2) {
		this.mMsg2 = mMsg2;
	}

	public String getmNextStation() {
		return mNextStation;
	}

	public void setmNextStation(String mNextStation) {
		this.mNextStation = mNextStation;
	}

	public String getmRouteName() {
		return mRouteName;
	}

	public void setmRouteName(String mRouteName) {
		this.mRouteName = mRouteName;
	}

	public int getmRouteType() {
		return mRouteType;
	}

	public void setmRouteType(int mRouteType) {
		this.mRouteType = mRouteType;
	}

	public String getmRouteTypeName() {
		return mRouteTypeName;
	}

	public void setmRouteTypeName(String mRouteTypeName) {
		this.mRouteTypeName = mRouteTypeName;
	}
}
