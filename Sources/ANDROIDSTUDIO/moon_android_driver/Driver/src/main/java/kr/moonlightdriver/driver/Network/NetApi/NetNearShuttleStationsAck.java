package kr.moonlightdriver.driver.Network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

import kr.moonlightdriver.driver.Network.NetData.BusStationListData;
import kr.moonlightdriver.driver.Network.NetData.CarpoolRegistrantData;
import kr.moonlightdriver.driver.Network.NetData.DriverPointData;
import kr.moonlightdriver.driver.Network.NetData.ResultData;
import kr.moonlightdriver.driver.Network.NetData.ShuttleLineColorData;
import kr.moonlightdriver.driver.Network.NetData.ShuttleLineData;
import kr.moonlightdriver.driver.Network.NetData.StationInfoData;
import kr.moonlightdriver.driver.Network.NetData.SubwayStationListData;

/**
 * Created by youngmin on 2015-10-13.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class NetNearShuttleStationsAck extends NetAPI {
	@JsonProperty("result")
	private ResultData mResultData;

	@JsonProperty("shuttleMarkerPoints")
	private ArrayList<StationInfoData> mShuttleMarkerPoints;

	@JsonProperty("shuttleLinePoints")
	private ArrayList<ShuttleLineData> mShuttleLinePoints;

	@JsonProperty("shuttles")
	private ArrayList<ShuttleLineColorData> mShuttleLineColorData;

	@JsonProperty("taxiCarpoolRegistrant")
	private ArrayList<CarpoolRegistrantData> mCarpoolRegistrantDataList;
	@JsonProperty("driverList")
	private ArrayList<DriverPointData> mNearDriverList;
	@JsonProperty("busStationList")
	private ArrayList<BusStationListData> mBusStationDataList;
	@JsonProperty("subwayStationList")
	private ArrayList<SubwayStationListData> mSubwayStationDataList;

	public NetNearShuttleStationsAck() {}

	public NetNearShuttleStationsAck(ResultData mResultData, ArrayList<StationInfoData> mShuttleMarkerPoints, ArrayList<ShuttleLineData> mShuttleLinePoints, ArrayList<ShuttleLineColorData> mShuttleLineColorData, ArrayList<CarpoolRegistrantData> mCarpoolRegistrantDataList, ArrayList<DriverPointData> mNearDriverList, ArrayList<BusStationListData> mBusStationDataList, ArrayList<SubwayStationListData> mSubwayStationDataList) {
		this.mResultData = mResultData;
		this.mShuttleMarkerPoints = mShuttleMarkerPoints;
		this.mShuttleLinePoints = mShuttleLinePoints;
		this.mShuttleLineColorData = mShuttleLineColorData;
		this.mCarpoolRegistrantDataList = mCarpoolRegistrantDataList;
		this.mNearDriverList = mNearDriverList;
		this.mBusStationDataList = mBusStationDataList;
		this.mSubwayStationDataList = mSubwayStationDataList;
	}

	public ResultData getmResultData() {
		return mResultData;
	}

	public void setmResultData(ResultData mResultData) {
		this.mResultData = mResultData;
	}

	public ArrayList<StationInfoData> getmShuttleMarkerPoints() {
		return mShuttleMarkerPoints;
	}

	public void setmShuttleMarkerPoints(ArrayList<StationInfoData> mShuttleMarkerPoints) {
		this.mShuttleMarkerPoints = mShuttleMarkerPoints;
	}

	public ArrayList<ShuttleLineData> getmShuttleLinePoints() {
		return mShuttleLinePoints;
	}

	public void setmShuttleLinePoints(ArrayList<ShuttleLineData> mShuttleLinePoints) {
		this.mShuttleLinePoints = mShuttleLinePoints;
	}

	public ArrayList<ShuttleLineColorData> getmShuttleLineColorData() {
		return mShuttleLineColorData;
	}

	public void setmShuttleLineColorData(ArrayList<ShuttleLineColorData> mShuttleLineColorData) {
		this.mShuttleLineColorData = mShuttleLineColorData;
	}

	public ArrayList<CarpoolRegistrantData> getmCarpoolRegistrantDataList() {
		return mCarpoolRegistrantDataList;
	}

	public void setmCarpoolRegistrantDataList(ArrayList<CarpoolRegistrantData> mCarpoolRegistrantDataList) {
		this.mCarpoolRegistrantDataList = mCarpoolRegistrantDataList;
	}

	public ArrayList<DriverPointData> getmNearDriverList() {
		return mNearDriverList;
	}

	public void setmNearDriverList(ArrayList<DriverPointData> mNearDriverList) {
		this.mNearDriverList = mNearDriverList;
	}

	public ArrayList<BusStationListData> getmBusStationDataList() {
		return mBusStationDataList;
	}

	public void setmBusStationDataList(ArrayList<BusStationListData> mBusStationDataList) {
		this.mBusStationDataList = mBusStationDataList;
	}

	public ArrayList<SubwayStationListData> getmSubwayStationDataList() {
		return mSubwayStationDataList;
	}

	public void setmSubwayStationDataList(ArrayList<SubwayStationListData> mSubwayStationDataList) {
		this.mSubwayStationDataList = mSubwayStationDataList;
	}
}
