package kr.moonlightdriver.driver.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;
import com.skt.Tmap.TMapData;
import com.skt.Tmap.TMapPOIItem;

import org.xml.sax.SAXException;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.xml.parsers.ParserConfigurationException;

import kr.moonlightdriver.driver.Network.NetApi.CarpoolInfoAck;
import kr.moonlightdriver.driver.Network.NetApi.NetAPI;
import kr.moonlightdriver.driver.Network.NetApi.NetResponse;
import kr.moonlightdriver.driver.Network.NetApi.NetResponseCallback;
import kr.moonlightdriver.driver.Network.NetApi.RegisterTaxiCarpoolAck;
import kr.moonlightdriver.driver.Network.NetClient.NetClient;
import kr.moonlightdriver.driver.Network.NetData.CarpoolData;
import kr.moonlightdriver.driver.Network.NetData.DriverPointData;
import kr.moonlightdriver.driver.Network.NetData.ResultData;
import kr.moonlightdriver.driver.R;
import kr.moonlightdriver.driver.activity.MainActivity;
import kr.moonlightdriver.driver.utils.Global;
import kr.moonlightdriver.driver.utils.SVUtil;

/**
 * Created by User on 2014-09-30.
 * 택시카풀 신청 페이지

 */
public class CarpoolMakeFragment extends Fragment implements View.OnClickListener {
	private final int DEFAULT_PEOPLE = 2;
	private final CharSequence[] DEPARTURE_TIME_LIST = {"5분 후", "10분 후", "20분 후 ", "30분 후", "1시간 후"};
	private final long[] DEPARTURE_TIME_VALUE = {5*60000, 10*60000, 20*60000, 30*60000, 60*60000};

	private RadioButton mRadioBtnHasTaxi;
	private RadioButton mRadioBtnNotHasTaxi;
	private TextView mTextViewDepartureTime;
	private ImageButton mBtnSearchStartLocation;
	private ImageButton mBtnSearchEndLocation;
	private Button mBtnRegister;
	private ImageButton mImgBtnMapSearchStartLocation;
	private ImageButton mImgBtnMapSearchEndLocation;
	private ImageButton mImgBtnClose;
	private CheckBox mCheckBoxIsPush;
	private Spinner mSelectPeopleSpinner;
	private EditText mEditTextEndLocation;
	private EditText mEditTextStartLocation;
	private EditText mEditTextPrice;

	private SimpleDateFormat mDateFormat;
	private TMapData mTMapData;
	private CharSequence[] mPOIItems;

	private double mStartLat;
	private double mStartLon;
	private double mEndLat;
	private double mEndLon;
	private long mDepartureTime;
	private int mCurrentParticipant;
	private int mParticipantCount;

	private String mConvertedAddress;
	private String mTaxiId;
	private String mEditMode;

	private MainActivity mMainActivity;

	private View mView;

	private boolean mIsFinishFragment;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		try {
			mView = inflater.inflate(R.layout.frag_carpool_make, container, false);

			mMainActivity = (MainActivity)getActivity();

			mIsFinishFragment = false;

			Bundle bundle = CarpoolMakeFragment.this.getArguments();
			mTaxiId = bundle.getString("taxiId", null);

			if(mTaxiId != null) {
				mEditMode = "edit";
			} else {
				mEditMode = "make";
			}

			mStartLat = mMainActivity.mCurrentLocation.latitude;
			mStartLon = mMainActivity.mCurrentLocation.longitude;
			mEndLat = 0;
			mEndLon = 0;
			mParticipantCount = DEFAULT_PEOPLE; // default 2명
			mCurrentParticipant = 0;
			mDateFormat = new SimpleDateFormat("HH:mm");
			mTMapData = new TMapData();

			mRadioBtnNotHasTaxi = (RadioButton) mView.findViewById(R.id.frag_carpoolmake_RB1);
			mRadioBtnHasTaxi = (RadioButton) mView.findViewById(R.id.frag_carpoolmake_RB2);
			mBtnSearchStartLocation = (ImageButton) mView.findViewById(R.id.carpoolmake_search1Btn);
			mBtnSearchEndLocation = (ImageButton) mView.findViewById(R.id.carpoolmake_search2Btn);
			mBtnRegister = (Button) mView.findViewById(R.id.frag_carpoolmake_registerBtn);
			mImgBtnClose = (ImageButton) mView.findViewById(R.id.carpoolmake_frag_closeBtn);
			mImgBtnMapSearchStartLocation = (ImageButton) mView.findViewById(R.id.carpoolmake_map1Btn);
			mImgBtnMapSearchEndLocation = (ImageButton) mView.findViewById(R.id.carpoolmake_map2Btn);
			mCheckBoxIsPush = (CheckBox) mView.findViewById(R.id.frag_carpoolmake_pushCheckBox);
			mTextViewDepartureTime = (TextView) mView.findViewById(R.id.frag_carpoolmake_timeTV);
			TextView textViewTitle = (TextView) mView.findViewById(R.id.carpoolmake_act_titleTV);
			mEditTextPrice = (EditText) mView.findViewById(R.id.frag_carpoolmake_feeET);
			mEditTextStartLocation = (EditText) mView.findViewById(R.id.carpoolmake_startET);
			mEditTextEndLocation = (EditText) mView.findViewById(R.id.carpoolmake_endET);
			mSelectPeopleSpinner = (Spinner)mView.findViewById(R.id.taxi_reg_recruitment);

			mImgBtnClose.setOnClickListener(this);
			mImgBtnMapSearchStartLocation.setOnClickListener(this);
			mImgBtnMapSearchEndLocation.setOnClickListener(this);
			mBtnRegister.setOnClickListener(this);
			mBtnSearchStartLocation.setOnClickListener(this);
			mBtnSearchEndLocation.setOnClickListener(this);
			mTextViewDepartureTime.setClickable(true);
			mTextViewDepartureTime.setOnClickListener(this);

			//  모집 인원수를 선택할 수 있는 스피너 초기화
			String[] recruitmentString = getResources().getStringArray(R.array.recruitment_list); // 2명, 3명, 4명
			mSelectPeopleSpinner.setAdapter(new ArrayAdapter<>(mMainActivity, android.R.layout.simple_spinner_dropdown_item, recruitmentString));

			mSelectPeopleSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
				@Override
				public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
					mParticipantCount = position + DEFAULT_PEOPLE;
					if(mParticipantCount < mCurrentParticipant) {
						SVUtil.showSimpleDialog(mMainActivity, "현재 동승 신청한 인원보다 많은 인원을 선택해주세요.");
						return;
					}

					mSelectPeopleSpinner.setSelection(position);
				}

				@Override
				public void onNothingSelected(AdapterView<?> parent) { }
			});

			if(mEditMode.equals("edit")) {
				textViewTitle.setText("택시카풀 수정");
				mBtnRegister.setText("수정완료");

				getCarpoolInfo(mTaxiId);
			} else {
				textViewTitle.setText("택시카풀 생성");
				mBtnRegister.setText("등록하기");

				mEditTextEndLocation.requestFocus();

				//  (출발 예정시간을 현재시간에서 20분 후로 기본 설정한다)
				Calendar calendar = Calendar.getInstance();
				Date now = calendar.getTime();

				calendar.add(Calendar.MINUTE, 20);

				Date startTime = calendar.getTime();
				mDepartureTime = startTime.getTime();
				long gap = (startTime.getTime() - now.getTime()) / (1000 * 60);

				mTextViewDepartureTime.setText(String.format("%d분후 출발예정(%s)", gap, mDateFormat.format(startTime)));

				mConvertedAddress = "";

				new ConvertGpsToAddressTask().execute();
			}
		} catch (InflateException e) {
			e.printStackTrace();
		}

		return mView;
	}

	private class ConvertGpsToAddressTask extends AsyncTask<Void, Void, String> {
		@Override
		protected String doInBackground(Void... params) {
			String retString = "";
			try {
				retString = mTMapData.convertGpsToAddress(mMainActivity.mCurrentLocation.latitude, mMainActivity.mCurrentLocation.longitude);
			} catch (IOException | ParserConfigurationException | SAXException e) {
				e.printStackTrace();
			}

			return retString;
		}

		@Override
		protected void onPostExecute(String _retString) {
			try {
				if(mIsFinishFragment) {
					return;
				}

				String[] retSplit = _retString.split(" ");

				for (int i = 2; i < retSplit.length; i++) {
					mConvertedAddress += (retSplit[i] + " ");
				}

				if(mView != null) {
					mEditTextStartLocation.setText(mConvertedAddress);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void getCarpoolInfo(String _taxiId) {
		SVUtil.showProgressDialog(mMainActivity, "로딩중...");

		RequestParams params = new RequestParams();
		params.add("taxiId", _taxiId);

		String url = Global.URL_CARPOOL_INFO.replaceAll(":taxiId", _taxiId);

		NetClient.send(mMainActivity, url, "POST", params, new CarpoolInfoAck(), new NetResponseCallback(new NetResponse() {
			@Override
			public void onResponse(NetAPI _netAPI) {
				SVUtil.hideProgressDialog();

				try {
					if (_netAPI != null && !mIsFinishFragment) {
						CarpoolInfoAck retNetApi = (CarpoolInfoAck) _netAPI;
						ResultData result = retNetApi.getmResultData();
						if (result.getmCode() != 100) {
							SVUtil.showDialogWithListener(mMainActivity, result.getmDetail(), "확인", new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									returnToPrevFragment();
								}
							});

							return;
						}

						CarpoolData retData = retNetApi.getmCarpoolInfo();
						ArrayList<DriverPointData> driverPointData = retNetApi.getmDriverPointData();

						String price = retData.getmPrice() + "";
						mCurrentParticipant = driverPointData.size();

						if (retData.ismIsCatch()) {
							mRadioBtnHasTaxi.setChecked(true);
							mRadioBtnNotHasTaxi.setChecked(false);
						} else {
							mRadioBtnHasTaxi.setChecked(false);
							mRadioBtnNotHasTaxi.setChecked(true);
						}

						mEditTextStartLocation.setText(retData.getmStartLocation().getmLocationName());
						mStartLat = retData.getmStartLocation().getmLocations().getmCoordinates().get(1);
						mStartLon = retData.getmStartLocation().getmLocations().getmCoordinates().get(0);
						mEditTextEndLocation.setText(retData.getmEndLocation().getmLocationName());
						mEndLat = retData.getmEndLocation().getmLocations().getmCoordinates().get(1);
						mEndLon = retData.getmEndLocation().getmLocations().getmCoordinates().get(0);
						mEditTextPrice.setText(price);
						mParticipantCount = retData.getmDriverListData().getmMax();
						mSelectPeopleSpinner.setSelection(mParticipantCount - DEFAULT_PEOPLE);
						mDepartureTime = retData.getmDepartureTime();
						mTextViewDepartureTime.setText(mDateFormat.format(new Date(mDepartureTime)));
						mCheckBoxIsPush.setChecked(retData.ismIsPush());
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}));
	}

	@Override
	public void onClick(View view) {
		try {
			if (view == mBtnSearchStartLocation) {
				if (mEditTextStartLocation.getText().toString().length() < 2 ) {
					SVUtil.showSimpleDialog(mMainActivity, "검색어를 2글자 이상 입력해주세요.");
				} else if (mEditTextStartLocation.getText().toString().equals("(현재위치)")) {
					SVUtil.showSimpleDialog(mMainActivity, "검색어를 입력해주세요.");
				} else {
					StartSearchByText(1);
				}
			} else if (view == mBtnSearchEndLocation) {
				if (mEditTextEndLocation.getText().toString().length() < 2) {
					SVUtil.showSimpleDialog(mMainActivity, "검색어를 2글자 이상 입력해주세요.");
				} else {
					StartSearchByText(2);
				}
			} else if (view == mImgBtnClose) {
				returnToPrevFragment();
			} else if (view == mBtnRegister) {
				if (mStartLat == 0) {
					SVUtil.showSimpleDialog(mMainActivity, "출발지를 선택(출발지를 적은 후 돋보기 아이콘을 클릭)해주세요.");
					return;
				}

				if (mEndLat == 0) {
					SVUtil.showSimpleDialog(mMainActivity, "도착지를 선택(도착지를 적은 후 돋보기 아이콘을 클릭)해주세요.");
					return;
				}

				registerOrEditTaxiCarpool();
			} else if (view == mImgBtnMapSearchStartLocation) {
				showMapSearch("start");
			} else if (view == mImgBtnMapSearchEndLocation) {
				showMapSearch("end");
			} else if (view == mTextViewDepartureTime) {
				SVUtil.showSingleChoiceItemDialog(mMainActivity, "출발시간", DEPARTURE_TIME_LIST, -1, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						mDepartureTime = Calendar.getInstance().getTime().getTime() + DEPARTURE_TIME_VALUE[i];
						String retDepartureTime = DEPARTURE_TIME_LIST[i] + " 출발예정(" + mDateFormat.format(mDepartureTime) + ")";
						mTextViewDepartureTime.setText(retDepartureTime);

						dialogInterface.dismiss();
					}
				});
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void returnToPrevFragment() {
		getFragmentManager().popBackStack();
	}

	private void showMapSearch(String _searchType) {
		try {
			SVUtil.hideSoftInput(mMainActivity);

			double search_point_lat = mMainActivity.mCurrentLocation.latitude;
			double search_point_lng = mMainActivity.mCurrentLocation.longitude;

			switch (_searchType) {
				case "start" :
					search_point_lat = mStartLat;
					search_point_lng = mStartLon;
					break;
				case "end" :
					if(mEndLat > 0  && mEndLon > 0) {
						search_point_lat = mEndLat;
						search_point_lng = mEndLon;
					}
					break;
			}

			MapSearchFragment frag = new MapSearchFragment();
			frag.setTargetFragment(this, 1);

			Bundle args = new Bundle();
			args.putString("searchType", _searchType);
			args.putDouble("lat", search_point_lat);
			args.putDouble("lng", search_point_lng);
			frag.setArguments(args);

			FragmentTransaction transaction = getFragmentManager().beginTransaction();
			transaction.add(R.id.mainFrameLayout, frag, Global.FRAG_MAP_SEARCH_TAG);
			transaction.addToBackStack(null);
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		try {
			if(mIsFinishFragment) {
				return;
			}

			if(requestCode == 1 && resultCode == Activity.RESULT_OK) {
				if(data != null && mView != null) {
					String searchType = data.getStringExtra("searchType");
					String address = data.getStringExtra("address");
					double lat = data.getDoubleExtra("lat", 0);
					double lng = data.getDoubleExtra("lng", 0);

					if (searchType.equals("start")) {
						mEditTextStartLocation.setText(address);
						mStartLat = lat;
						mStartLon = lng;
					} else if (searchType.equals("end")) {
						mEditTextEndLocation.setText(address);
						mEndLat = lat;
						mEndLon = lng;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void registerOrEditTaxiCarpool() {
		try {
			String phoneNumber = mMainActivity.mDriverInfo.getmPhone();
			String registrant = phoneNumber.substring(phoneNumber.length() - 4, phoneNumber.length());
			String startText = mEditTextStartLocation.getText().toString();
			String endText = mEditTextEndLocation.getText().toString();
			int money = Integer.parseInt(mEditTextPrice.getText().toString());
			int isCatch = mRadioBtnHasTaxi.isChecked() ? 1 : 0;
			int isPush = mCheckBoxIsPush.isChecked() ? 1 : 0;

			String url = Global.URL_REGISTER_CARPOOL;
			if(mEditMode.equals("edit")) {
				url = Global.URL_EDIT_CARPOOL;
			}

			SVUtil.showProgressDialog(mMainActivity, "등록중...");

			RequestParams params = new RequestParams();
			params.add("driverId", mMainActivity.mDriverInfo.getmDriverId());
			params.add("isCatch", isCatch + "");
			params.add("isPush", isPush + "");
			params.add("phone", phoneNumber);
			params.add("start_lat", mStartLat + "");
			params.add("start_lng", mStartLon + "");
			params.add("start_text", startText);
			params.add("stop_lat", mEndLat + "");
			params.add("stop_lng", mEndLon + "");
			params.add("stop_text", endText);
			params.add("max", mParticipantCount+ "");
			params.add("time", mDepartureTime + "");
			params.add("divide", "0");
			params.add("money", money + "");
			params.add("registrant", registrant);
			if(mTaxiId != null) {
				params.add("taxiId", mTaxiId);
			}

			NetClient.send(mMainActivity, url, "POST", params, new RegisterTaxiCarpoolAck(), new NetResponseCallback(new NetResponse() {
				@Override
				public void onResponse(NetAPI _netAPI) {
					SVUtil.hideProgressDialog();

					try {
						if (_netAPI != null && !mIsFinishFragment) {
							RegisterTaxiCarpoolAck retNetApi = (RegisterTaxiCarpoolAck) _netAPI;
							ResultData result = retNetApi.getmResultData();
							String message = "택시 카풀 등록이 완료 되었습니다.";
							if (mEditMode.equals("edit")) {
								message = "택시 카풀 수정이 완료되었습니다.";
							}

							if (result.getmCode() != 100) {
								message = result.getmDetail();
							}

							SVUtil.showDialogWithListener(mMainActivity, message, "확인", new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									returnToPrevFragment();
								}
							});
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void StartSearchByText(final int index) {
		try {
			SVUtil.hideSoftInput(mMainActivity);
			SVUtil.showProgressDialog(mMainActivity, "검색중...");

			String searchText;

			if (index == 1) {
				searchText = mEditTextStartLocation.getText().toString();
			} else {
				searchText = mEditTextEndLocation.getText().toString();
			}

			mTMapData.findAllPOI(searchText, new TMapData.FindAllPOIListenerCallback() {
				@Override
				public void onFindAllPOI(ArrayList<TMapPOIItem> tMapPOIItems) {
					SVUtil.hideProgressDialog();

					try {
						if (mIsFinishFragment) {
							return;
						}

						final ArrayList<TMapPOIItem> poi_list = new ArrayList<>();
						final ArrayList<String> nameList = new ArrayList<>();

						for (TMapPOIItem item : tMapPOIItems) {
							poi_list.add(item);
							nameList.add(item.getPOIName());
						}

						mPOIItems = nameList.toArray(new CharSequence[nameList.size()]);

						new Handler(Looper.getMainLooper()).post(new Runnable() {
							@Override
							public void run() {
								SVUtil.showItemDialog(mMainActivity, "검색 결과", mPOIItems, new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialogInterface, int i) {
										try {
											String addressName = poi_list.get(i).lowerAddrName;

											if (poi_list.get(i).firstNo != null) {
												addressName += " " + poi_list.get(i).firstNo;
											}

											if (poi_list.get(i).secondNo != null) {
												addressName += "-" + poi_list.get(i).secondNo;
											}

											String retAddressName = nameList.get(i) + "(" + addressName + ")";

											if (!mIsFinishFragment) {
												if (index == 1) { // 출발지
													mEditTextStartLocation.setText(retAddressName);
													mStartLat = poi_list.get(i).getPOIPoint().getLatitude();
													mStartLon = poi_list.get(i).getPOIPoint().getLongitude();
												} else { // 도착지
													mEditTextEndLocation.setText(retAddressName);
													mEndLat = poi_list.get(i).getPOIPoint().getLatitude();
													mEndLon = poi_list.get(i).getPOIPoint().getLongitude();
												}
											}
										} catch (Exception e) {
											e.printStackTrace();
										}
									}
								});
							}
						});
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	@Override
	 public void onStart() {
		super.onStart();

		mIsFinishFragment = false;
	}

	@Override
	public void onResume() {
		super.onResume();

		try {
			mMainActivity.changeButtonImage(mMainActivity.mImgBtnCarpool);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		mTMapData = null;
		mIsFinishFragment = true;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();

		try {
			if(mView != null){
				ViewGroup parent = (ViewGroup) mView.getParent();
				if(parent != null){
					parent.removeView(mView);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		setTargetFragment(null, -1);
	}
}
