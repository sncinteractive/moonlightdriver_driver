package kr.moonlightdriver.driver.fragment;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import kr.moonlightdriver.driver.Network.NetApi.NetAPI;
import kr.moonlightdriver.driver.Network.NetApi.NetResponse;
import kr.moonlightdriver.driver.Network.NetApi.NetResponseCallback;
import kr.moonlightdriver.driver.Network.NetApi.TwoInOneListAck;
import kr.moonlightdriver.driver.Network.NetClient.NetClient;
import kr.moonlightdriver.driver.Network.NetData.ResultData;
import kr.moonlightdriver.driver.Network.NetData.TwoInOneListData;
import kr.moonlightdriver.driver.R;
import kr.moonlightdriver.driver.activity.MainActivity;
import kr.moonlightdriver.driver.utils.Global;
import kr.moonlightdriver.driver.utils.SVUtil;

/**
 * Created by youngmin on 2015-07-22.
 */
public class TwoInOneFragment extends Fragment {
	private static final String TAG = "2in1 Fragment";
	private final int LIMIT_COUNT = 20;

	private MainActivity mMainActivity;
	private TextView m2in1ContentNoItemTextView;
	private ContentAdapter m2in1ContentAdapter;
	private ArrayList<TwoInOneListData> m2in1ContentList;

	private int mPageNum;
	private int mPreLast;

	private View mView;

	private boolean mIsFinishFragment;

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		try {
			mView = inflater.inflate(R.layout.frag_2in1, container, false);
			mMainActivity = (MainActivity) getActivity();

			mIsFinishFragment = false;

			mPageNum = 0;
			mPreLast = 0;
			m2in1ContentList = new ArrayList<>();
			ListView twoInOneContentListView = (ListView) mView.findViewById(R.id.content_2in1_listview);
			m2in1ContentNoItemTextView = (TextView) mView.findViewById(R.id.content_2in1_no_item_text_view);

			m2in1ContentAdapter = new ContentAdapter(mMainActivity, R.layout.row_2in1_content, m2in1ContentList);
			twoInOneContentListView.setAdapter(m2in1ContentAdapter);
			twoInOneContentListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					try {
						TwoInOneListData contentInfoBox = m2in1ContentList.get(position);

						Bundle args = new Bundle();
						args.putString("boardId", contentInfoBox.getmBoardId());

						TwoInOneDetailFragment frag = new TwoInOneDetailFragment();
						frag.setArguments(args);

						mMainActivity.replaceFragment(frag, R.id.mainFrameLayout, Global.FRAG_TWOINONE_DETAIL_TAG, true);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			twoInOneContentListView.setOnScrollListener(new AbsListView.OnScrollListener() {
				@Override
				public void onScrollStateChanged(AbsListView view, int scrollState) {}

				@Override
				public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
					try {
						if (firstVisibleItem == 0 && visibleItemCount <= totalItemCount) {
							return;
						}

						int lastItem = firstVisibleItem + visibleItemCount;
						if (lastItem == totalItemCount && mPreLast != lastItem) {
							mPreLast = lastItem;
							mPageNum = mPageNum + 1;
							get2in1ContentList(mPageNum);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			ImageButton btnWrite = (ImageButton) mView.findViewById(R.id.btn_2in1_write);
			btnWrite.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						TwoInOneWriteFragment frag = new TwoInOneWriteFragment();

						Bundle args = new Bundle();
						args.putString("boardId", null);

						frag.setArguments(args);

						mMainActivity.replaceFragment(frag, R.id.mainFrameLayout, Global.FRAG_TWOINONE_WRITE_TAG, true);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		} catch(InflateException e) {
			e.printStackTrace();
		}

		return mView;
	}

	private void get2in1ContentList(int _page) {
		SVUtil.showProgressDialog(mMainActivity, "로딩중...");

		String url = Global.URL_BOARD_LIST + "?pageNum=" + _page + "&limit=" + LIMIT_COUNT;
		NetClient.send(mMainActivity, url, "GET", null, new TwoInOneListAck(), new NetResponseCallback(new NetResponse() {
			@Override
			public void onResponse(NetAPI _netAPI) {
				SVUtil.hideProgressDialog();

				try {
					if (_netAPI != null && !mIsFinishFragment) {
						TwoInOneListAck retNetApi = (TwoInOneListAck) _netAPI;
						ResultData result = retNetApi.getmResultData();

						if (result.getmCode() != 100) {
							SVUtil.showSimpleDialog(mMainActivity, result.getmDetail());
						} else {
							ArrayList<TwoInOneListData> retData = retNetApi.getmTwoInOneListData();
							int listSize = retData.size();
							for (int i = 0; i < listSize; i++) {
								m2in1ContentList.add(retData.get(i));
							}

							if (m2in1ContentList.size() > 0) {
								m2in1ContentNoItemTextView.setVisibility(View.GONE);
							} else {
								m2in1ContentNoItemTextView.setVisibility(View.VISIBLE);
							}

							m2in1ContentAdapter.notifyDataSetChanged();
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}));
	}

	private class ViewHolder2in1Content {
		public ImageView mImageViewRowIcon;
		public TextView mTextViewTitle;
		public TextView mTextViewDate;
		public TextView mTextViewCommentCount;
	}

	private class ContentAdapter extends ArrayAdapter<TwoInOneListData> {
		ArrayList<TwoInOneListData> mContentInfoList;
		private int mRowId;
		private ViewHolder2in1Content mHolder;

		public ContentAdapter(Context _context, int _rowId, ArrayList<TwoInOneListData> _contentInfoList) {
			super(_context, _rowId, _contentInfoList);
			this.mContentInfoList = _contentInfoList;
			this.mRowId = _rowId;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;

			try {
				if (v == null) {
					LayoutInflater li = (LayoutInflater) mMainActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					v = li.inflate(mRowId, null);
					mHolder = new ViewHolder2in1Content();
					mHolder.mImageViewRowIcon = (ImageView) v.findViewById(R.id.icon_2in1_content);
					mHolder.mTextViewTitle = (TextView) v.findViewById(R.id.title_2in1_content);
					mHolder.mTextViewDate = (TextView) v.findViewById(R.id.date_2in1_content);
					mHolder.mTextViewCommentCount = (TextView) v.findViewById(R.id.comment_2in1_content);

					v.setTag(mHolder);
				} else {
					mHolder = (ViewHolder2in1Content) v.getTag();
				}

				TwoInOneListData contentInfoBox = mContentInfoList.get(position);

				if(contentInfoBox.getmCategory().equals("find")) {
					mHolder.mImageViewRowIcon.setImageResource(R.drawable.detail_gp_category02);
				} else {
					mHolder.mImageViewRowIcon.setImageResource(R.drawable.detail_gp_category01);
				}

				String dateString = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(contentInfoBox.getmCreatedDate())) + " | " + contentInfoBox.getmNickName();
				String commentCount = contentInfoBox.getmCommentCount() + "";

				mHolder.mTextViewTitle.setText(contentInfoBox.getmTitle());
				mHolder.mTextViewDate.setText(dateString);
				mHolder.mTextViewCommentCount.setText(commentCount);
			} catch (Exception e) {
				e.printStackTrace();
			}

			return v;
		}
	}

	@Override
	public void onStart() {
		super.onStart();

		mIsFinishFragment = false;
	}

	@Override
	public void onResume() {
		super.onResume();

		try {
			mPageNum = 0;
			m2in1ContentList.clear();
			get2in1ContentList(mPageNum);

			mMainActivity.changeButtonImage(mMainActivity.mImgBtn2in1);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onPause() {
		super.onPause();

		SVUtil.hideProgressDialog();

		mIsFinishFragment = true;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();

		try {
			if(mView != null){
				ViewGroup parent = (ViewGroup) mView.getParent();
				if(parent!=null){
					parent.removeView(mView);
				}
			}

			mMainActivity.mFragmentTwoInOne = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		setTargetFragment(null, -1);
	}
}
