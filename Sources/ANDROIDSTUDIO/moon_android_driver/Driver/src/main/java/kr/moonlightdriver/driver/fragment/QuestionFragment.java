package kr.moonlightdriver.driver.fragment;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import kr.moonlightdriver.driver.Network.NetApi.NetAPI;
import kr.moonlightdriver.driver.Network.NetApi.NetResponse;
import kr.moonlightdriver.driver.Network.NetApi.NetResponseCallback;
import kr.moonlightdriver.driver.Network.NetApi.QuestionListAck;
import kr.moonlightdriver.driver.Network.NetClient.NetClient;
import kr.moonlightdriver.driver.Network.NetData.QuestionData;
import kr.moonlightdriver.driver.Network.NetData.ResultData;
import kr.moonlightdriver.driver.R;
import kr.moonlightdriver.driver.activity.MainActivity;
import kr.moonlightdriver.driver.utils.Global;
import kr.moonlightdriver.driver.utils.SVUtil;

/**
 * Created by youngmin on 2015-07-22.
 */
public class QuestionFragment extends Fragment {
	private static final String TAG = "QuestionFragment";
	private static final int LIMIT_COUNT = 20;

	private MainActivity mMainActivity;
	private TextView mQuestionNoItemTextView;
	private QuestionAdapter mQuestionAdapter;
	private ArrayList<QuestionData> mQuestionContentList;

	private int mPageNum;
	private int mPreLast;

	private View mView;

	private boolean mIsFinishFragment;

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		try {
			mView = inflater.inflate(R.layout.frag_question, container, false);
			mMainActivity = (MainActivity) getActivity();

			mIsFinishFragment = false;

			mPageNum = 0;
			mPreLast = 0;
			mQuestionContentList = new ArrayList<>();
			
			final ListView questionContentListView = (ListView) mView.findViewById(R.id.question_listview);
			mQuestionNoItemTextView = (TextView) mView.findViewById(R.id.question_no_item_text_view);

			mQuestionAdapter = new QuestionAdapter(mMainActivity, R.layout.row_question_list, mQuestionContentList);
			questionContentListView.setAdapter(mQuestionAdapter);
			questionContentListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					try {
						QuestionData questionData = mQuestionContentList.get(position);

						if(questionData.getmDriverId().equals(mMainActivity.mDriverInfo.getmDriverId())) {
							Bundle args = new Bundle();
							args.putString("questionId", questionData.getmQuestionId());

							QuestionDetailFragment frag = new QuestionDetailFragment();
							frag.setArguments(args);

							mMainActivity.replaceFragment(frag, R.id.mainFrameLayout, Global.FRAG_QUESTION_DETAIL_TAG, true);
						} else {
							SVUtil.showSimpleDialog(mMainActivity, "등록자만 확인 가능한 비밀글입니다.");
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			questionContentListView.setOnScrollListener(new AbsListView.OnScrollListener() {
				@Override
				public void onScrollStateChanged(AbsListView view, int scrollState) {
				}

				@Override
				public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
					try {
						if (firstVisibleItem == 0 && visibleItemCount <= totalItemCount) {
							return;
						}

						int lastItem = firstVisibleItem + visibleItemCount;
						if (lastItem == totalItemCount && mPreLast != lastItem) {
							mPreLast = lastItem;
							mPageNum = mPageNum + 1;
							getQuestionList(mPageNum);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			Button btnWrite = (Button) mView.findViewById(R.id.btn_question_write);
			btnWrite.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						mMainActivity.replaceFragment(new QuestionWriteFragment(), R.id.mainFrameLayout, Global.FRAG_QUESTION_WRITE_TAG, true);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		} catch(InflateException e) {
			e.printStackTrace();
		}

		return mView;
	}

	private void getQuestionList(int _page) {
		SVUtil.showProgressDialog(mMainActivity, "로딩중...");

		String url = Global.URL_QUESTION_LIST + "?pageNum=" + _page + "&limit=" + LIMIT_COUNT;
		NetClient.send(mMainActivity, url, "GET", null, new QuestionListAck(), new NetResponseCallback(new NetResponse() {
			@Override
			public void onResponse(NetAPI _netAPI) {
				SVUtil.hideProgressDialog();

				try {
					if (_netAPI != null && !mIsFinishFragment) {
						QuestionListAck retNetApi = (QuestionListAck) _netAPI;
						ResultData result = retNetApi.getmResultData();

						if (result.getmCode() != 100) {
							SVUtil.showSimpleDialog(mMainActivity, result.getmDetail());
						} else {
							ArrayList<QuestionData> retQuestionData = retNetApi.getmQuestionDataList();

							int listSize = retQuestionData.size();
							for (int i = 0; i < listSize; i++) {
								mQuestionContentList.add(retQuestionData.get(i));
							}

							if (mQuestionContentList.size() > 0) {
								mQuestionNoItemTextView.setVisibility(View.GONE);
							} else {
								mQuestionNoItemTextView.setVisibility(View.VISIBLE);
							}

							mQuestionAdapter.notifyDataSetChanged();
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}));
	}

	private class ViewHolderQuestionList {
		public TextView mTextViewStatus;
		public TextView mTextViewTitle;
		public TextView mTextViewDate;
		public TextView mTextViewAnswerCount;
	}

	private class QuestionAdapter extends ArrayAdapter<QuestionData> {
		ArrayList<QuestionData> mQuestionList;
		private int mRowId;
		private ViewHolderQuestionList mHolder;

		public QuestionAdapter(Context _context, int _rowId, ArrayList<QuestionData> _questionList) {
			super(_context, _rowId, _questionList);
			this.mQuestionList = _questionList;
			this.mRowId = _rowId;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;

			try {
				if (v == null) {
					LayoutInflater li = (LayoutInflater) mMainActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					v = li.inflate(mRowId, null);

					mHolder = new ViewHolderQuestionList();
					mHolder.mTextViewStatus = (TextView) v.findViewById(R.id.row_question_list_status);
					mHolder.mTextViewTitle = (TextView) v.findViewById(R.id.row_question_list_title);
					mHolder.mTextViewDate = (TextView) v.findViewById(R.id.row_question_list_date);
					mHolder.mTextViewAnswerCount = (TextView) v.findViewById(R.id.row_question_list_answer_count);

					v.setTag(mHolder);
				} else {
					mHolder = (ViewHolderQuestionList) v.getTag();
				}

				QuestionData questionData = mQuestionList.get(position);

				String status = "[답변준비]";
				mHolder.mTextViewStatus.setTextColor(Color.parseColor("#545454"));
				if(questionData.getmAnswerCount() > 0) {
					status = "[답변완료]";
					mHolder.mTextViewStatus.setTextColor(Color.parseColor("#ff9049"));
				}

				String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(questionData.getmCreatedDate()));
				String answerCount = questionData.getmAnswerCount() + "";

				mHolder.mTextViewStatus.setText(status);
				mHolder.mTextViewTitle.setText(questionData.getmTitle());
				mHolder.mTextViewDate.setText(date);
				mHolder.mTextViewAnswerCount.setText(answerCount);
			} catch (Exception e) {
				e.printStackTrace();
			}

			return v;
		}
	}

	@Override
	public void onStart() {
		super.onStart();

		mIsFinishFragment = false;
	}

	@Override
	public void onResume() {
		super.onResume();

		try {
			mPageNum = 0;
			mQuestionContentList.clear();
			getQuestionList(mPageNum);

			mMainActivity.changeButtonImage(mMainActivity.mImgBtnQuestion);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onPause() {
		super.onPause();

		SVUtil.hideProgressDialog();

		mIsFinishFragment = true;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();

		try {
			if(mView != null){
				ViewGroup parent = (ViewGroup) mView.getParent();
				if(parent!=null){
					parent.removeView(mView);
				}
			}

			mMainActivity.mFragmentQuestion = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		setTargetFragment(null, -1);
	}
}
