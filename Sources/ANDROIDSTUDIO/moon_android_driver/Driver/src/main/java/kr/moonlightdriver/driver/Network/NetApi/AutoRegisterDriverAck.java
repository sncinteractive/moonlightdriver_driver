package kr.moonlightdriver.driver.Network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import kr.moonlightdriver.driver.Network.NetData.DriverData;
import kr.moonlightdriver.driver.Network.NetData.ResultData;

/**
 * Created by youngmin on 2015-10-13.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AutoRegisterDriverAck extends NetAPI {
	@JsonProperty("result")
	private ResultData mResultData;
	@JsonProperty("driver")
	private DriverData mDriverData;

	public AutoRegisterDriverAck() {}

	public AutoRegisterDriverAck(DriverData mDriverData, ResultData mResultData) {
		this.mDriverData = mDriverData;
		this.mResultData = mResultData;
	}

	public DriverData getmDriverData() {
		return mDriverData;
	}

	public void setmDriverData(DriverData mDriverData) {
		this.mDriverData = mDriverData;
	}

	public ResultData getmResultData() {
		return mResultData;
	}

	public void setmResultData(ResultData mResultData) {
		this.mResultData = mResultData;
	}
}
