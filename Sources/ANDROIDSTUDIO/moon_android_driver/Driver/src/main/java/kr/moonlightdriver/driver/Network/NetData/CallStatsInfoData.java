package kr.moonlightdriver.driver.Network.NetData;

/**
 * Created by youngmin on 2015-12-31.
 */
public class CallStatsInfoData {
	private String mSiName;
	private String mGuName;
	private String mDongName;
	private int mCallCount;

	public CallStatsInfoData() {}

	public CallStatsInfoData(String _siName, String _guName, String _dongName, int _callCount) {
		this.mSiName = _siName;
		this.mGuName = _guName;
		this.mDongName = _dongName;
		this.mCallCount = _callCount;
	}

	public int getmCallCount() {
		return mCallCount;
	}

	public void setmCallCount(int mCallCount) {
		this.mCallCount = mCallCount;
	}

	public String getmDongName() {
		return mDongName;
	}

	public void setmDongName(String mDongName) {
		this.mDongName = mDongName;
	}

	public String getmGuName() {
		return mGuName;
	}

	public void setmGuName(String mGuName) {
		this.mGuName = mGuName;
	}

	public String getmSiName() {
		return mSiName;
	}

	public void setmSiName(String mSiName) {
		this.mSiName = mSiName;
	}
}
