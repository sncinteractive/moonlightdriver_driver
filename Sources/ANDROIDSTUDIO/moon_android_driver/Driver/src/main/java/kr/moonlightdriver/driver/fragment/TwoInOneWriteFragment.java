package kr.moonlightdriver.driver.fragment;

import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import kr.moonlightdriver.driver.Network.NetApi.NetAPI;
import kr.moonlightdriver.driver.Network.NetApi.NetResponse;
import kr.moonlightdriver.driver.Network.NetApi.NetResponseCallback;
import kr.moonlightdriver.driver.Network.NetApi.NoResponseDataAck;
import kr.moonlightdriver.driver.Network.NetApi.TwoInOneContentDetailAck;
import kr.moonlightdriver.driver.Network.NetClient.NetClient;
import kr.moonlightdriver.driver.Network.NetData.ResultData;
import kr.moonlightdriver.driver.Network.NetData.TwoInOneContentDetailData;
import kr.moonlightdriver.driver.R;
import kr.moonlightdriver.driver.activity.MainActivity;
import kr.moonlightdriver.driver.utils.Global;
import kr.moonlightdriver.driver.utils.SVUtil;

public class TwoInOneWriteFragment extends Fragment {
	private EditText mTitle;
	private EditText mNickName;
	private EditText mAge;
	private TextView mCareer;
	private TextView mDivide;
	private TextView mRange;
	private TextView mStart;
	private TextView mTime;
	private TextView mTarget;
	private TextView mCar;
	private TextView mContact;
	private TextView mEtc;
	private RadioGroup mCategoryRadioGroup;
	private RadioGroup mSexRadioGroup;

	private String mCategory;
	private String mSex;

	private MainActivity mMainActivity;
	private View mView;

	private boolean mIsFinishFragment;
	private String mBoardId;
	private String mEditMode;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		try {
			mView = inflater.inflate(R.layout.frag_2in1_write, container, false);

			mMainActivity = (MainActivity) getActivity();

			mIsFinishFragment = false;
			Bundle bundle = TwoInOneWriteFragment.this.getArguments();
			mBoardId = bundle.getString("boardId", null);

			if(mBoardId != null) {
				mEditMode = "edit";
			} else {
				mEditMode = "make";
			}

			ImageButton btnBack = (ImageButton) mView.findViewById(R.id.write2in1_btn_back);
			mTitle = (EditText) mView.findViewById(R.id.write2in1_title);
			mCategoryRadioGroup = (RadioGroup) mView.findViewById(R.id.write2in1_category);
			mNickName = (EditText) mView.findViewById(R.id.write2in1_nickname);
			mSexRadioGroup = (RadioGroup) mView.findViewById(R.id.write2in1_sex);
			mAge = (EditText) mView.findViewById(R.id.write2in1_age);
			mCareer = (EditText) mView.findViewById(R.id.write2in1_career);
			mDivide = (EditText) mView.findViewById(R.id.write2in1_divide);
			mRange = (EditText) mView.findViewById(R.id.write2in1_range);
			mStart = (EditText) mView.findViewById(R.id.write2in1_start);
			mTime = (EditText) mView.findViewById(R.id.write2in1_time);
			mTarget = (EditText) mView.findViewById(R.id.write2in1_target);
			mCar = (EditText) mView.findViewById(R.id.write2in1_car);
			mContact = (EditText) mView.findViewById(R.id.write2in1_contact);
			mEtc = (EditText) mView.findViewById(R.id.write2in1_etc);
			ImageButton btnRegisterContent = (ImageButton) mView.findViewById(R.id.btn_write2in1_content);

			mCategoryRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(RadioGroup group, int checkedId) {
					try {
						switch (checkedId) {
							case R.id.write2in1_find :
								mCategory = "find";
								break;
							case R.id.write2in1_support :
								mCategory = "support";
								break;
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			mSexRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(RadioGroup group, int checkedId) {
					try {
						switch (checkedId) {
							case R.id.write2in1_male:
								mSex = "male";
								break;
							case R.id.write2in1_female:
								mSex = "female";
								break;
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			btnBack.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if(mEditMode.equals("edit")) {
						moveToTwoInOneDetailFragment();
					} else {
						moveToTwoInOneFragment();
					}
//					getFragmentManager().popBackStack();
				}
			});

			btnRegisterContent.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						String checkRet = checkParams();
						if(checkRet.equals("OK")) {
							register2in1Content();
						} else {
							SVUtil.showSimpleDialog(mMainActivity, checkRet);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			if(mEditMode.equals("edit")) {
				get2in1ContentInfo(mBoardId);
			}
		} catch(InflateException e) {
			e.printStackTrace();
		}

		return mView;
	}

	private void get2in1ContentInfo(String _boardId) {
		SVUtil.showProgressDialog(mMainActivity, "로딩중...");

		String url = Global.URL_BOARD_INFO.replaceAll(":boardId", _boardId);
		NetClient.send(mMainActivity, url, "GET", null, new TwoInOneContentDetailAck(), new NetResponseCallback(new NetResponse() {
			@Override
			public void onResponse(NetAPI _netAPI) {
				try {
					SVUtil.hideProgressDialog();

					if (_netAPI != null && !mIsFinishFragment) {
						TwoInOneContentDetailAck retNetApi = (TwoInOneContentDetailAck) _netAPI;
						ResultData result = retNetApi.getmResultData();

						if (result.getmCode() != 100) {
							SVUtil.showSimpleDialog(mMainActivity, result.getmDetail());
						} else {
							TwoInOneContentDetailData contentDetailInfo = retNetApi.getmTwoInOneContentDetailData();

							if (contentDetailInfo.getmCategory().equals("find")) {
								mCategoryRadioGroup.check(R.id.write2in1_find);
							} else {
								mCategoryRadioGroup.check(R.id.write2in1_support);
							}

							if (contentDetailInfo.getmSex().equals("male")) {
								mSexRadioGroup.check(R.id.write2in1_male);
							} else {
								mSexRadioGroup.check(R.id.write2in1_female);
							}

							mTitle.setText(contentDetailInfo.getmTitle());
							mNickName.setText(contentDetailInfo.getmNickName());
							mAge.setText(contentDetailInfo.getmAge());
							mCareer.setText(contentDetailInfo.getmCareer());
							mDivide.setText(contentDetailInfo.getmDivide());
							mRange.setText(contentDetailInfo.getmRange());
							mStart.setText(contentDetailInfo.getmStart());
							mTime.setText(contentDetailInfo.getmTime());
							mTarget.setText(contentDetailInfo.getmTarget());
							mCar.setText(contentDetailInfo.getmCar());
							mContact.setText(contentDetailInfo.getmContact());
							mEtc.setText(contentDetailInfo.getmEtc());
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}));
	}

	private void register2in1Content() {
		try {
			SVUtil.hideSoftInput(mMainActivity);

			SVUtil.showProgressDialog(mMainActivity, "등록중...");

			RequestParams params = new RequestParams();
			params.add("driverId", mMainActivity.mDriverInfo.getmDriverId());
			params.add("title", mTitle.getText().toString());
			params.add("category", mCategory);
			params.add("nickName", mNickName.getText().toString());
			params.add("sex", mSex);
			params.add("age", mAge.getText().toString());
			params.add("career", mCareer.getText().toString());
			params.add("divide", mDivide.getText().toString());
			params.add("range", mRange.getText().toString());
			params.add("start", mStart.getText().toString());
			params.add("time", mTime.getText().toString());
			params.add("target", mTarget.getText().toString());
			params.add("car", mCar.getText().toString());
			params.add("contact", mContact.getText().toString());
			params.add("etc", mEtc.getText().toString());

			if(mBoardId != null) {
				params.add("boardId", mBoardId);
			}

			String url = Global.URL_BOARD_WRITE_CONTENT;
			if(mEditMode.equals("edit")) {
				url = Global.URL_BOARD_EDIT_CONTENT;
			}

			NetClient.send(mMainActivity, url, "POST", params, new NoResponseDataAck(), new NetResponseCallback(new NetResponse() {
				@Override
				public void onResponse(NetAPI _netAPI) {
					try {
						SVUtil.hideProgressDialog();

						if (_netAPI != null && !mIsFinishFragment) {
							NoResponseDataAck retNetApi = (NoResponseDataAck) _netAPI;
							ResultData result = retNetApi.getmResultData();

							if (result.getmCode() != 100) {
								SVUtil.showSimpleDialog(mMainActivity, result.getmDetail());
							} else {
								String message = "등록 되었습니다.";
								if(mEditMode.equals("edit")) {
									message = "수정 되었습니다.";
								}

								SVUtil.showDialogWithListener(mMainActivity, message, "확인", new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										if(mEditMode.equals("edit")) {
											moveToTwoInOneDetailFragment();
										} else {
											moveToTwoInOneFragment();
										}
									}
								});
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String checkParams() {
		try {
			if(mMainActivity.mDriverInfo.getmDriverId() == null || mMainActivity.mDriverInfo.getmDriverId().isEmpty()) {
				return "기사로 등록한 후 입력 가능합니다.";
			} else if(mTitle.getText().toString().isEmpty()) {
				return "제목을 입력해주세요.";
			} else if(mCategory == null || mCategory.isEmpty()) {
				return "꽁지 구함/지원 여부를 선택해주세요.";
			} else if(mNickName.getText().toString().isEmpty()) {
				return "닉네임을 입력해주세요.";
			} else if(mSex == null || mSex.isEmpty()) {
				return "성별을 선택해주세요.";
			} else if(mAge.getText().toString().isEmpty()) {
				return "나이를 입력해주세요.";
			} else if(mCareer.getText().toString().isEmpty()) {
				return "경력을 입력해주세요.";
			} else if(mDivide.getText().toString().isEmpty()) {
				return "수입배분을 입력해주세요.";
			} else if(mRange.getText().toString().isEmpty()) {
				return "활동범위를 입력해주세요.";
			} else if(mStart.getText().toString().isEmpty()) {
				return "출발지를 입력해주세요.";
			} else if(mTime.getText().toString().isEmpty()) {
				return "업무시간을 입력해주세요.";
			} else if(mTarget.getText().toString().isEmpty()) {
				return "찾는대상을 입력해주세요.";
			} else if(mCar.getText().toString().isEmpty()) {
				return "보유/희망 차종을 입력해주세요.";
			} else if(mContact.getText().toString().isEmpty()) {
				return "연락처를 입력해주세요.";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "OK";
	}

	private void moveToTwoInOneFragment() {
		try {
			mMainActivity.mFragmentTwoInOne = new TwoInOneFragment();
			mMainActivity.replaceFragment(mMainActivity.mFragmentTwoInOne, R.id.mainFrameLayout, Global.FRAG_TWOINONE_TAG, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void moveToTwoInOneDetailFragment() {
		try {
			TwoInOneDetailFragment frag = new TwoInOneDetailFragment();

			Bundle args = new Bundle();
			args.putString("boardId", mBoardId);

			frag.setArguments(args);

			mMainActivity.replaceFragment(frag, R.id.mainFrameLayout, Global.FRAG_TWOINONE_TAG, false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onStart() {
		super.onStart();

		mIsFinishFragment = false;
	}

	@Override
	public void onResume() {
		super.onResume();

		try {
			mMainActivity.changeButtonImage(mMainActivity.mImgBtn2in1);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onPause() {
		super.onPause();

		mIsFinishFragment = true;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();

		try {
			if(mView != null){
				ViewGroup parent = (ViewGroup) mView.getParent();
				if(parent!=null){
					parent.removeView(mView);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		setTargetFragment(null, -1);
	}
}
