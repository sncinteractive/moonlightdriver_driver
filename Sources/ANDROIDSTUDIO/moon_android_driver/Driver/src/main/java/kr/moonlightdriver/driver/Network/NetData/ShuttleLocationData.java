package kr.moonlightdriver.driver.Network.NetData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 * Created by youngmin on 2015-10-07.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShuttleLocationData {
	@JsonProperty("type")
	public String mType;

	@JsonProperty("coordinates")
	public ArrayList<Double> mCoordinates;

	public ShuttleLocationData() {}

	public ShuttleLocationData(ArrayList<Double> mCoordinates, String mType) {
		this.mCoordinates = mCoordinates;
		this.mType = mType;
	}

	public ArrayList<Double> getmCoordinates() {
		return mCoordinates;
	}

	public void setmCoordinates(ArrayList<Double> mCoordinates) {
		this.mCoordinates = mCoordinates;
	}

	public String getmType() {
		return mType;
	}

	public void setmType(String mType) {
		this.mType = mType;
	}
}
