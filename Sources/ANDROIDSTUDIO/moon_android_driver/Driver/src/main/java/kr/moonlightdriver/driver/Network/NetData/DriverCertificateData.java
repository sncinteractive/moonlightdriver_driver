package kr.moonlightdriver.driver.Network.NetData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by youngmin on 2015-10-28.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DriverCertificateData {
    @JsonProperty("automatic")
    private boolean mIsAutomatic;

    @JsonProperty("number")
    private String mNumber;

    public DriverCertificateData() {}

    public DriverCertificateData(boolean mIsAutomatic, String mNumber) {
        this.mIsAutomatic = mIsAutomatic;
        this.mNumber = mNumber;
    }

    public boolean ismIsAutomatic() {
        return mIsAutomatic;
    }

    public void setmIsAutomatic(boolean mIsAutomatic) {
        this.mIsAutomatic = mIsAutomatic;
    }

    public String getmNumber() {
        return mNumber;
    }

    public void setmNumber(String mNumber) {
        this.mNumber = mNumber;
    }
}
