package kr.moonlightdriver.driver.Network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import kr.moonlightdriver.driver.Network.NetData.ResultData;
import kr.moonlightdriver.driver.Network.NetData.TwoInOneContentDetailData;

/**
 * Created by youngmin on 2015-10-13.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TwoInOneContentDetailAck extends NetAPI {
	@JsonProperty("result")
	private ResultData mResultData;
	@JsonProperty("content")
	private TwoInOneContentDetailData mTwoInOneContentDetailData;

	public TwoInOneContentDetailAck() {}

	public TwoInOneContentDetailAck(ResultData mResultData, TwoInOneContentDetailData mTwoInOneContentDetailData) {
		this.mResultData = mResultData;
		this.mTwoInOneContentDetailData = mTwoInOneContentDetailData;
	}

	public TwoInOneContentDetailData getmTwoInOneContentDetailData() {
		return mTwoInOneContentDetailData;
	}

	public void setmTwoInOneContentDetailData(TwoInOneContentDetailData mTwoInOneContentDetailData) {
		this.mTwoInOneContentDetailData = mTwoInOneContentDetailData;
	}

	public ResultData getmResultData() {
		return mResultData;
	}

	public void setmResultData(ResultData mResultData) {
		this.mResultData = mResultData;
	}
}
