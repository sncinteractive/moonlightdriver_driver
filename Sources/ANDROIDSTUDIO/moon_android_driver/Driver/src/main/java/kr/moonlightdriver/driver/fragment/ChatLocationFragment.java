package kr.moonlightdriver.driver.fragment;


import android.app.Fragment;
import android.content.DialogInterface;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;
import java.util.List;

import kr.moonlightdriver.driver.Network.NetApi.NearDriverListAck;
import kr.moonlightdriver.driver.Network.NetApi.NetAPI;
import kr.moonlightdriver.driver.Network.NetApi.NetResponse;
import kr.moonlightdriver.driver.Network.NetApi.NetResponseCallback;
import kr.moonlightdriver.driver.Network.NetApi.ShuttlePathAck;
import kr.moonlightdriver.driver.Network.NetClient.NetClient;
import kr.moonlightdriver.driver.Network.NetData.DriverPointData;
import kr.moonlightdriver.driver.Network.NetData.ResultData;
import kr.moonlightdriver.driver.Network.NetData.ShuttlePathData;
import kr.moonlightdriver.driver.Network.NetData.ShuttlePathPointData;
import kr.moonlightdriver.driver.R;
import kr.moonlightdriver.driver.activity.MainActivity;
import kr.moonlightdriver.driver.utils.BalloonAdapter;
import kr.moonlightdriver.driver.utils.Global;
import kr.moonlightdriver.driver.utils.SVUtil;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatLocationFragment extends Fragment {
	private static final String TAG = "ChatLocationFragment";
	ImageButton mBtnCloseX;
	ImageButton mBtnZoomIn;
	ImageButton mBtnZoomOut;

	private GoogleMap mGoogleMap;
	private Marker mShowInfoWindowMarker;
	private ArrayList<Marker> mShuttleStationMarkers;
	private ArrayList<Marker> mArrowMarkers;
	private ArrayList<ShuttlePathPointData> mShuttlePathPointData;
	private static ShuttlePathData mShuttlePathData;
	private BitmapDescriptor mArrowBitmapDescriptor;
	private Polyline mShuttlePolyLine;

	private ArrayList<Marker> mDriverMarkerList;

	private String mNickname;
	private LatLng mPosition;

	private View mView;

	private MainActivity mMainActivity;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		try {
			mView = inflater.inflate(R.layout.frag_chat_location, container, false);

			mMainActivity = (MainActivity) getActivity();

			Bundle bundle = ChatLocationFragment.this.getArguments();
			mNickname = bundle.getString("nickName");
			double lat = bundle.getDouble("lat", 0.0);
			double lng = bundle.getDouble("lng", 0.0);
			String shuttleId = bundle.getString("shuttleId", "");
			mPosition = new LatLng(lat, lng);

			mBtnCloseX = (ImageButton) mView.findViewById(R.id.btnCloseX);
			mBtnZoomIn = (ImageButton) mView.findViewById(R.id.btnChatLocationZoomIn);
			mBtnZoomOut = (ImageButton) mView.findViewById(R.id.btnChatLocationZoomOut);

			mShuttleStationMarkers = new ArrayList<>();
			mArrowMarkers = new ArrayList<>();
			mShuttlePathPointData = new ArrayList<>();
			mDriverMarkerList = new ArrayList<>();

			setUpMapIfNeeded();

			getShuttlePath(shuttleId);
		} catch (InflateException e) {
			e.printStackTrace();
		}

		return mView;
	}

	private void setUpMapIfNeeded() {
		try {
			if (mGoogleMap == null) {
				mGoogleMap = ((SupportMapFragment) MainActivity.mSupportFragmentManager.findFragmentById(R.id.chat_location_map)).getMap();
				if (mGoogleMap != null) {
					setUpMap();
				}
			}

//			if (mGoogleMap == null) {
//				SupportMapFragment mapFragment = (SupportMapFragment) MainActivity.mSupportFragmentManager.findFragmentById(R.id.chat_location_map);
//				mapFragment.getMapAsync(this);
//			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

//	@Override
//	public void onMapReady(GoogleMap googleMap) {
//		try {
//			mGoogleMap = googleMap;
//			setUpMap();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	private void setUpMap() {
		try {
			mGoogleMap.getUiSettings().setRotateGesturesEnabled(false);
			mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
			mGoogleMap.getUiSettings().setCompassEnabled(false);
			mGoogleMap.setInfoWindowAdapter(new BalloonAdapter(mMainActivity.getLayoutInflater()));

			mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mPosition, 16.23f));

			addMarker("[" + mNickname + "] 님의 현위치");

			mBtnZoomIn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					mGoogleMap.animateCamera(CameraUpdateFactory.zoomIn());
				}
			});

			mBtnZoomOut.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					mGoogleMap.animateCamera(CameraUpdateFactory.zoomOut());
				}
			});

			mBtnCloseX.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					closeFragment();
				}
			});

			mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
				@Override
				public boolean onMarkerClick(Marker marker) {
					try {
						marker.showInfoWindow();
						mShowInfoWindowMarker = marker;
					} catch (Exception e) {
						e.printStackTrace();
					}

					return false;
				}
			});

			mGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
				@Override
				public void onMapClick(LatLng latLng) {
					try {
						if (mShowInfoWindowMarker != null) {
							mShowInfoWindowMarker.hideInfoWindow();
							mShowInfoWindowMarker = null;
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			mGoogleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
				@Override
				public void onCameraChange(CameraPosition cameraPosition) {
					try {
						if (mGoogleMap == null) {
							return;
						}

						float range = getDriverRange();
						if(range > 0 && range <= 2000) {
							getNearDriverList(cameraPosition.target, range);
						} else {
							removeDriverMarkers();
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private float getDriverRange() {
		float range;

		try {
			LatLngBounds currentScreen = mGoogleMap.getProjection().getVisibleRegion().latLngBounds;

			float[] dist = new float[2];
			Location.distanceBetween(currentScreen.southwest.latitude, currentScreen.southwest.longitude, currentScreen.northeast.latitude, currentScreen.northeast.longitude, dist);

			range = dist[0];

			if(range < 1000) {
				range = 1000;
			}
		} catch (Exception e) {
			e.printStackTrace();

			range = 0f;
		}

		return range;
	}

	private void getNearDriverList(LatLng _center, float range) {
		try {
			RequestParams params = new RequestParams();
			params.add("lat", _center.latitude + "");
			params.add("lng", _center.longitude + "");
			params.add("range", range + "");

//			SVUtil.log("error", TAG, "lat : " + _center.latitude + ", lng : " + _center.longitude + ", range : " + range);
//			SVUtil.log("error", TAG, "params : " + params.toString());

			NetClient.send(mMainActivity, Global.URL_DRIVER_FIND, "POST", params, new NearDriverListAck(), new NetResponseCallback(new NetResponse() {
				@Override
				public void onResponse(NetAPI _netAPI) {
					try {
						removeDriverMarkers();

						if (_netAPI != null) {
							NearDriverListAck retNetApi = (NearDriverListAck) _netAPI;
							ResultData result = retNetApi.getmResultData();
							if (result.getmCode() == 100) {
								ArrayList<DriverPointData> retDriverList = retNetApi.getmDriverPointData();

								for (DriverPointData driverPoint : retDriverList) {
									String markerText = driverPoint.getmPhoneNumber().substring(driverPoint.getmPhoneNumber().length() - 4, driverPoint.getmPhoneNumber().length()) + " 기사님";
									LatLng markerPosition = new LatLng(driverPoint.getmLocation().getmCoordinates().get(1), driverPoint.getmLocation().getmCoordinates().get(0));
									Marker m = mGoogleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_driver_40)).position(markerPosition).title(markerText).anchor(0.5f, 1.0f));

									mDriverMarkerList.add(m);
								}
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void removeDriverMarkers() {
		try {
			if(mDriverMarkerList != null && mDriverMarkerList.size() > 0) {
				for (Marker m : mDriverMarkerList) {
					m.remove();
				}

				mDriverMarkerList.clear();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void getShuttlePath(String _shuttleId) {
		SVUtil.showProgressDialog(mMainActivity, "로딩중...");

		String url = Global.URL_SHUTTLE_PATH.replace(":busId", _shuttleId);
		NetClient.send(mMainActivity, url, "GET", null, new ShuttlePathAck(), new NetResponseCallback(new NetResponse() {
			@Override
			public void onResponse(NetAPI _netAPI) {
				try {
					SVUtil.hideProgressDialog();

					mShuttlePathPointData.clear();

					if (_netAPI != null) {
						ShuttlePathAck retNetApi = (ShuttlePathAck) _netAPI;
						ResultData result = retNetApi.getmResultData();
						if (result.getmCode() != 100) {
							SVUtil.showDialogWithListener(mMainActivity, result.getmDetail(), "확인", new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									closeFragment();
								}
							});

							return;
						}

						mShuttlePathData = retNetApi.getmShuttlePathData();

						ArrayList<ShuttlePathPointData> shuttlePathPointData = mShuttlePathData.getmShuttlePathPointData();
						int listSize = shuttlePathPointData.size();

						for (int i = 0; i < listSize; i++) {
							shuttlePathPointData.get(i).setmFirstTime(SVUtil.convertTime(shuttlePathPointData.get(i).getmFirstTime()));
							shuttlePathPointData.get(i).setmLastTime(SVUtil.convertTime(shuttlePathPointData.get(i).getmLastTime()));

							mShuttlePathPointData.add(shuttlePathPointData.get(i));
						}

						drawShuttlePath(mShuttlePathPointData);

						mShuttlePathData.setmStartStation(SVUtil.getShuttleStartStation(mShuttlePathPointData));
						mShuttlePathData.setmEndStation(SVUtil.getShuttleEndStation(mShuttlePathPointData));
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}));
	}

	private void drawShuttlePath (ArrayList<ShuttlePathPointData> _shuttlePathPointData) {
		try {
			PolylineOptions opt = new PolylineOptions();
			opt.color(Color.parseColor("#0000ff"));
			opt.width(20f);

			initShuttlePathView();

			int listSize = _shuttlePathPointData.size();
			LatLng pathPoint;
			ShuttlePathPointData prevStationName = null;
			for(int i = 0; i < listSize; i++) {
				if(mGoogleMap == null) {
					return;
				}

				ShuttlePathPointData shuttlePathPointItem = _shuttlePathPointData.get(i);
				pathPoint = new LatLng(shuttlePathPointItem.getmShuttleLocationData().getmCoordinates().get(1), shuttlePathPointItem.getmShuttleLocationData().getmCoordinates().get(0));

				opt.add(pathPoint);

				if (!shuttlePathPointItem.getmShuttlePointName().equals("__point")) { // 셔틀 정류장
					mShuttleStationMarkers.add(mGoogleMap.addMarker(new MarkerOptions().position(pathPoint).icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_shuttle_30)).snippet("shuttle_station").anchor(0.5f, 1.0f)));
				}

				if(prevStationName != null && !prevStationName.getmShuttlePointName().equals("__point") && shuttlePathPointItem.getmShuttlePointName().equals("__point")) {
					LatLng prevPoint = new LatLng(prevStationName.getmShuttleLocationData().getmCoordinates().get(1), prevStationName.getmShuttleLocationData().getmCoordinates().get(0));
					mArrowMarkers.add(addShuttleLineArrowMarker(pathPoint, SVUtil.getRotationDegrees(prevPoint, pathPoint), "#0000ff"));
				}

				prevStationName = shuttlePathPointItem;
			}

			mShuttlePolyLine = mGoogleMap.addPolyline(opt);
//			moveToBounds(mShuttlePolyLine);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void moveToBounds(Polyline p) {
		try {
			LatLngBounds.Builder builder = new LatLngBounds.Builder();
			List<LatLng> arr = p.getPoints();

			for(int i = 0; i < arr.size();i++){
				builder.include(arr.get(i));
			}

			builder.include(mPosition);

			LatLngBounds bounds = builder.build();
			int padding = 40; // offset from edges of the map in pixels

			mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, padding));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void initShuttlePathView() {
		try {
			if (mShuttlePolyLine != null) {
				mShuttlePolyLine.remove();
			}

			int listSize = mShuttleStationMarkers.size();
			for (int i = 0; i < listSize; i++) {
				mShuttleStationMarkers.get(i).remove();
			}

			mShuttleStationMarkers.clear();

			int markerSize = mArrowMarkers.size();
			for(int i = 0; i < markerSize; i++) {
				mArrowMarkers.get(i).remove();
			}

			mArrowMarkers.clear();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private Marker addShuttleLineArrowMarker(LatLng _position, float _rotateDegree, String _color) {
		try {
			if(mArrowBitmapDescriptor == null) {
				mArrowBitmapDescriptor = SVUtil.createShuttleArrowBitmap(50, 50, _color);
			}

			return mGoogleMap.addMarker(new MarkerOptions().position(_position).icon(mArrowBitmapDescriptor).anchor(0.5f, 0.5f).rotation(_rotateDegree).flat(true));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private void closeFragment() {
		try {
			mMainActivity.getFragmentManager().beginTransaction().remove(ChatLocationFragment.this).commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void addMarker(String _title) {
		try {
//		IconGenerator iconFactory = new IconGenerator(mMainActivity);
//		MarkerOptions markerOptions = new MarkerOptions().
//				icon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon(_title))).
//				position(mPosition).anchor(iconFactory.getAnchorU(), iconFactory.getAnchorV());
//
//		mGoogleMap.addMarker(markerOptions);

			Marker m = mGoogleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_taxi_passenger_40)).position(mPosition).title(_title).anchor(0.5f, 1.0f));
			m.showInfoWindow();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void destroyView() {
		try {
			if(mGoogleMap != null) {
				mGoogleMap.clear();
				mGoogleMap = null;
			}

			SupportMapFragment f = (SupportMapFragment) MainActivity.mSupportFragmentManager.findFragmentById(R.id.chat_location_map);
			if(f != null) {
				MainActivity.mSupportFragmentManager.beginTransaction().remove(f).commitAllowingStateLoss();
			}

			if(mView != null){
				ViewGroup parent = (ViewGroup) mView.getParent();
				if(parent!=null){
					parent.removeView(mView);
				}
			}

			closeFragment();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();

		destroyView();
	}
}
