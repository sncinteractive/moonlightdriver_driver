package kr.moonlightdriver.driver.utils;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by User on 2014-09-24.
 * Global Define Variable & functions class
 */
public class Global {
	public static final String APP_DOWNLOAD_URL = "https://play.google.com/store/apps/details?id=kr.moonlightdriver.driver";
	public static final String DATABASE_NAME = "bus_data.db";
	public static final int DATABASE_VERSION = 1;

	public static final String ENCRYPT_KEY = "ekfqlcrltk";
	public static final String SECRET_KEY = "KJC_MLD";

	public static final int UPDATE_DISTANCE = 10;
	public static final int REQ_LOCATION_TIME = 1;   // GPS 갱신 시간 : 1초
	public static final int REQ_LOCATION_DISTANCE = 10;  // GPS 갱신 거리 : 10m
	public static final long UPDATE_LOCATION_RESUME_TIME = 30 * 1000;
	public static final long SHUTTLE_POSITION_REFRESH_TIME = 5 * 1000;  // 셔틀 실시가 위치 조회 갱신 시간 : 5초

	public static final int HTTP_TIMEOUT_MILLIS = 10 * 1000;

	public static final float MIN_SCREEN_WITH = 580f;
	public static final float MIN_SCREEN_HEIGHT = 825f;

	public static final long DOUBLE_PRESS_INTERVAL = 500;

	public static final int ACCESS_LOCATION_REQ_CODE = 1;
	public static final int PHONE_NUMBER_REQ_CODE = 2;
	public static final int CALL_PHONE_REQ_CODE = 3;

	public static final String PROPERTY_REG_ID = "registration_id";
	public static final String PROPERTY_APP_VERSION = "appVersion";
	public static final String SENDER_ID = "563199847799";
	public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	public static final String PROPERTY_FIRST_LAUNCH = "first_launch";
	public static final String PROPERTY_NO_SHOW_NOTICE = "no_show_notice";
	public static final String PROPERTY_YN_CARPOOL_PUSH = "Y";

	//  Preference name
	public static final String PREF_NAME = "MOONLIGHT_PREF";

//	public static final String TMAP_API_KEY		 = "013703a7-d144-36c8-a049-9dafe4e4417b";   //  T-map api key
	public static final String TMAP_API_KEY		 = "c2269a8b-d379-49bc-bf4c-6c224cf2d1e2";   //  T-map api key

	public static final String HEADQUARTER_NUMBER = "070-7585-3101";

	public static final LatLng SEOUL_CITY_HALL = new LatLng(37.566221, 126.977921);
	public static final int BUS_START_HOURS = 3;

	// REAL SVR
	public static final String SERVER_HOST = "http://115.68.104.30:8888";
	public static final String WOKITIKI_SERVER_HOST = "http://115.68.122.108:7030";
//	public static final String WOKITIKI_SERVER_HOST = "http://150.95.135.222:7030";

	// DEV SVR
//	public static final String SERVER_HOST = "http://dev.sncinteractive.com:8888";
//	public static final String WOKITIKI_SERVER_HOST = "http://dev.sncinteractive.com:7030";

	public static final String URL_REQ_LOCATION = Global.SERVER_HOST + "/shuttle/:busId/req_location";
	public static final String URL_RES_LOCATION = Global.SERVER_HOST + "/shuttle/:busId/res_location";
	public static final String URL_BOARD_LIST = Global.SERVER_HOST + "/board/list";
	public static final String URL_BOARD_INFO = Global.SERVER_HOST + "/board/:boardId/getone";
	public static final String URL_BOARD_WRITE_CONTENT = Global.SERVER_HOST + "/board/write/content";
	public static final String URL_BOARD_EDIT_CONTENT = Global.SERVER_HOST + "/board/edit/content";
	public static final String URL_BOARD_DELETE = Global.SERVER_HOST + "/board/delete";
	public static final String URL_BOARD_WRITE_COMMENT = Global.SERVER_HOST + "/board/write/comment";
	public static final String URL_SHUTTLE_STATION = Global.SERVER_HOST + "/busnew2/station";
	public static final String URL_SHUTTLE_STATION2 = Global.SERVER_HOST + "/busnew2/station2";
	public static final String URL_SHUTTLE_STATION_UPDATED = Global.SERVER_HOST + "/busnew2/station/updated";
	public static final String URL_SHUTTLE_STATION_UPDATED2 = Global.SERVER_HOST + "/busnew2/station/updated2";
	public static final String URL_SHUTTLE_STATION_UPDATED3 = Global.SERVER_HOST + "/busnew2/station/updated3";
	public static final String URL_CALL_HISTORY = Global.SERVER_HOST + "/call/history/list";
	public static final String URL_SHUTTLE_LIST_BY_ID = Global.SERVER_HOST + "/shuttle/:arsId/list";
	public static final String URL_CHECK_VERSION = Global.SERVER_HOST + "/driver/version";
	public static final String URL_AUTO_REGISTER_DRIVER = Global.SERVER_HOST + "/driver/register/auto";
	public static final String URL_UPDATE_DRIVER_LOCATION = Global.SERVER_HOST + "/driver/update";
	public static final String URL_UPDATE_DRIVER_CONFIRM_DATA = Global.SERVER_HOST + "/driver/update/confirm";
	public static final String URL_DRIVER_DATA_CHECK = Global.SERVER_HOST + "/driver/check";
	public static final String URL_DRIVER_FIND = Global.SERVER_HOST + "/driver/find";
	public static final String URL_DRIVER_LOGOUT = Global.SERVER_HOST + "/driver/logout";
	public static final String URL_CARPOOL_LIST = Global.SERVER_HOST + "/taxi/list";
	public static final String URL_CARPOOL_REGISTRANT_LIST = Global.SERVER_HOST + "/taxi/list/registrant";
	public static final String URL_CARPOOL_INFO = Global.SERVER_HOST + "/taxi/:taxiId/get";
	public static final String URL_CARPOOL_PARTICIPATE_REQ = Global.SERVER_HOST + "/taxi/:taxiId/register";
	public static final String URL_REGISTER_CARPOOL = Global.SERVER_HOST + "/taxi";
	public static final String URL_EDIT_CARPOOL = Global.SERVER_HOST + "/taxi/edit";
	public static final String URL_DELETE_CARPOOL = Global.SERVER_HOST + "/taxi/delete";
	public static final String URL_CARPOOL_DECLINE = Global.SERVER_HOST + "/taxi/:taxiId/decline";
	public static final String URL_CARPOOL_ACCEPT = Global.SERVER_HOST + "/taxi/:taxiId/accept";
	public static final String URL_SHUTTLE_LIST = Global.SERVER_HOST + "/shuttle/list";
	public static final String URL_SHUTTLE_CATEGORY = Global.SERVER_HOST + "/shuttle/category";
	public static final String URL_SHUTTLE_PATH = Global.SERVER_HOST + "/busnew2/:busId/points";
	public static final String URL_START_CHAT = Global.SERVER_HOST + "/busnew2/:busId/:driverId/start";
	public static final String URL_END_CHAT = Global.SERVER_HOST + "/busnew2/:busId/:driverId/end";
	public static final String URL_ADD_SHUTTLE_COMMENT = Global.SERVER_HOST + "/busnew/:busId/comment";
	public static final String URL_REGISTER_VIRTUAL_NUMBER = Global.SERVER_HOST + "/virtual_number/register";
	public static final String URL_UNREGISTER_VIRTUAL_NUMBER = Global.SERVER_HOST + "/virtual_number/unregister";
	public static final String URL_FREQUENTLY_USED_WORD_LIST = Global.SERVER_HOST + "/shuttle/frequently/used";
	public static final String URL_NOTICE_LIST = Global.SERVER_HOST + "/notice/list";
	public static final String URL_QUESTION_SEND = Global.SERVER_HOST + "/question/send";
	public static final String URL_QUESTION_LIST = Global.SERVER_HOST + "/question/list";
	public static final String URL_QUESTION_DETAIL = Global.SERVER_HOST + "/question/info/answer/:questionId";
	public static final String URL_PUBLIC_TRANSIT_LIST = Global.SERVER_HOST + "/public/transit/near/station/list";
	public static final String URL_BUS_ROUTE_LIST_BY_STATION_ID = Global.SERVER_HOST + "/public/transit/route/list";
	public static final String URL_BUS_STATION_LIST_BY_ROUTE_ID = Global.SERVER_HOST + "/public/transit/station/list";
	public static final String URL_BUS_ROUTE_DETAIL = Global.SERVER_HOST + "/public/transit/route/detail";
	public static final String URL_NIGHT_BUS_ROUTE_LIST = Global.SERVER_HOST + "/public/transit/nightbus/list";
	public static final String URL_SHUTTLE_REAL_TIME = Global.SERVER_HOST + "/shuttle/realtime/position";
	public static final String URL_SHUTTLE_TRANSFER = Global.SERVER_HOST + "/shuttle/transfer";
	public static final String URL_SHUTTLE_BEACON = Global.SERVER_HOST + "/beacon";
	public static final String URL_BEACON_REGISTRANT_LIST = Global.SERVER_HOST + "/beacon/list";
	public static final String URL_BEACON_CHECK_REAL_TIME = Global.SERVER_HOST + "/beacon/isrealtime/:busId";


	public static final String URL_NEAR_SHUTTLE_STATION = Global.SERVER_HOST + "/near/station/shuttle";





	public static final String[] CALL_HISTORY_TYPE = new String[]{ "지난달 콜 수", "최근 3개월간 콜 수(평균)", "최근 1년간 콜 수(평균)" };

	public static final String FRAG_OUTBACK_TAG = "OUTBACK";
	public static final String FRAG_SHUTTLE_TAG = "SHUTTLE";
	public static final String FRAG_CARPOOL_TAG = "CARPOOL";
	public static final String FRAG_TWOINONE_TAG = "TWOINONE";
	public static final String FRAG_FINDPATH_TAG = "FINDPATH";
	public static final String FRAG_CALLSTATS_TAG = "CALLSTATS";
	public static final String FRAG_QUESTION_TAG = "QUESTION";
	public static final String FRAG_QUESTION_DETAIL_TAG = "QUESTION_DETAIL";
	public static final String FRAG_QUESTION_WRITE_TAG = "QUESTION_WRITE";
	public static final String FRAG_SHUTTLE_CHATTING_TAG = "SHUTTLE_CHATTING";
	public static final String FRAG_SHUTTLE_STATION_TAG = "SHUTTLE_STATION";
	public static final String FRAG_CHAT_LOCATION_TAG = "CHAT_LOCATION";
	public static final String FRAG_NIGHT_BUS_INFO_TAG = "NIGHT_BUS_INFO";
	public static final String FRAG_CARPOOL_MAKE_TAG = "CARPOOL_MAKE";
	public static final String FRAG_CARPOOL_DETAIL_TAG = "CARPOOL_DETAIL";
	public static final String FRAG_BUS_STATION_INFO_TAG = "BUS_STATION_INFO";
	public static final String FRAG_BUS_INFO_TAG = "BUS_INFO";
	public static final String FRAG_TWOINONE_WRITE_TAG = "TWOINONE_WRITE";
	public static final String FRAG_TWOINONE_DETAIL_TAG = "TWOINONE_DETAIL";
	public static final String FRAG_MAP_SEARCH_TAG = "MAP_SEARCH";

	public static final String TRANSPORT_SERVICE_KEY = "SeXIq%2F47tYT4doM%2Fnu5I8dhNBjD%2BGySm0ayiAsDEWHd4ZCOFl5qQDHiT6UIp7PunXRsJ%2FJtRKAJQb1i%2F0yIsZg%3D%3D";
	public static final String SEOUL_BUS_HOST = "http://210.96.13.82:8099";
	public static final String BUS_STATION_INFO_BY_ARSID = Global.SEOUL_BUS_HOST + "/api/rest/stationinfo/getStationByUid?arsId=";
	public static final String BUS_POSITION_BY_ROUTE_ID = Global.SEOUL_BUS_HOST + "/api/rest/buspos/getBusPosByRtid?busRouteId=";
	public static final String BUS_STATION_BY_ROUTE_ID = Global.SEOUL_BUS_HOST + "/api/rest/busRouteInfo/getStaionByRoute?busRouteId=";
	public static final String BUS_ROUTE_PATH = "http://topis.seoul.go.kr/renewal/ajaxData/getBusData.jsp?mode=routLine&rout_id=";

	public static final String GBUS_HOST = "http://openapi.gbis.go.kr";
	public static final String GBUS_POSITION_DATA_URL = Global.GBUS_HOST + "/ws/rest/buslocationservice";
	public static final String GBUS_ARRIVAL_DATA_URL = Global.GBUS_HOST + "/ws/rest/busarrivalservice/station";
}
