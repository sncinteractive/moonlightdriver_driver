package kr.moonlightdriver.driver.Network.NetData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 * Created by youngmin on 2015-10-28.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TwoInOneContentDetailData {
	@JsonProperty("boardId")
	private String mBoardId;

	@JsonProperty("driverId")
	private String mDriverId;

	@JsonProperty("nickName")
	private String mNickName;

	@JsonProperty("title")
	private String mTitle;

	@JsonProperty("category")
	private String mCategory;

	@JsonProperty("sex")
	private String mSex;

	@JsonProperty("age")
	private String mAge;

	@JsonProperty("career")
	private String mCareer;

	@JsonProperty("divide")
	private String mDivide;

	@JsonProperty("range")
	private String mRange;

	@JsonProperty("start")
	private String mStart;

	@JsonProperty("time")
	private String mTime;

	@JsonProperty("target")
	private String mTarget;

	@JsonProperty("car")
	private String mCar;

	@JsonProperty("contact")
	private String mContact;

	@JsonProperty("etc")
	private String mEtc;

	@JsonProperty("comment")
	private ArrayList<TwoInOneCommentData> mTwoInOneCommentDataList;

	@JsonProperty("createdDate")
	private Long mCreatedDate;

	public TwoInOneContentDetailData() {}

	public TwoInOneContentDetailData(String mAge, String mBoardId, String mCar, String mCareer, String mCategory, String mContact, Long mCreatedDate, String mDivide, String mDriverId, String mEtc, String mNickName, String mRange, String mSex, String mStart, String mTarget, String mTime, String mTitle, ArrayList<TwoInOneCommentData> mTwoInOneCommentDataList) {
		this.mAge = mAge;
		this.mBoardId = mBoardId;
		this.mCar = mCar;
		this.mCareer = mCareer;
		this.mCategory = mCategory;
		this.mContact = mContact;
		this.mCreatedDate = mCreatedDate;
		this.mDivide = mDivide;
		this.mDriverId = mDriverId;
		this.mEtc = mEtc;
		this.mNickName = mNickName;
		this.mRange = mRange;
		this.mSex = mSex;
		this.mStart = mStart;
		this.mTarget = mTarget;
		this.mTime = mTime;
		this.mTitle = mTitle;
		this.mTwoInOneCommentDataList = mTwoInOneCommentDataList;
	}

	public String getmAge() {
		return mAge;
	}

	public void setmAge(String mAge) {
		this.mAge = mAge;
	}

	public String getmBoardId() {
		return mBoardId;
	}

	public void setmBoardId(String mBoardId) {
		this.mBoardId = mBoardId;
	}

	public String getmCar() {
		return mCar;
	}

	public void setmCar(String mCar) {
		this.mCar = mCar;
	}

	public String getmCategory() {
		return mCategory;
	}

	public void setmCategory(String mCategory) {
		this.mCategory = mCategory;
	}

	public String getmContact() {
		return mContact;
	}

	public void setmContact(String mContact) {
		this.mContact = mContact;
	}

	public Long getmCreatedDate() {
		return mCreatedDate;
	}

	public void setmCreatedDate(Long mCreatedDate) {
		this.mCreatedDate = mCreatedDate;
	}

	public String getmDivide() {
		return mDivide;
	}

	public void setmDivide(String mDivide) {
		this.mDivide = mDivide;
	}

	public String getmDriverId() {
		return mDriverId;
	}

	public void setmDriverId(String mDriverId) {
		this.mDriverId = mDriverId;
	}

	public String getmEtc() {
		return mEtc;
	}

	public void setmEtc(String mEtc) {
		this.mEtc = mEtc;
	}

	public String getmNickName() {
		return mNickName;
	}

	public void setmNickName(String mNickName) {
		this.mNickName = mNickName;
	}

	public String getmRange() {
		return mRange;
	}

	public void setmRange(String mRange) {
		this.mRange = mRange;
	}

	public String getmSex() {
		return mSex;
	}

	public void setmSex(String mSex) {
		this.mSex = mSex;
	}

	public String getmStart() {
		return mStart;
	}

	public void setmStart(String mStart) {
		this.mStart = mStart;
	}

	public String getmTarget() {
		return mTarget;
	}

	public void setmTarget(String mTarget) {
		this.mTarget = mTarget;
	}

	public String getmTime() {
		return mTime;
	}

	public void setmTime(String mTime) {
		this.mTime = mTime;
	}

	public String getmTitle() {
		return mTitle;
	}

	public void setmTitle(String mTitle) {
		this.mTitle = mTitle;
	}

	public ArrayList<TwoInOneCommentData> getmTwoInOneCommentDataList() {
		return mTwoInOneCommentDataList;
	}

	public void setmTwoInOneCommentDataList(ArrayList<TwoInOneCommentData> mTwoInOneCommentDataList) {
		this.mTwoInOneCommentDataList = mTwoInOneCommentDataList;
	}

	public String getmCareer() {
		return mCareer;
	}

	public void setmCareer(String mCareer) {
		this.mCareer = mCareer;
	}
}
