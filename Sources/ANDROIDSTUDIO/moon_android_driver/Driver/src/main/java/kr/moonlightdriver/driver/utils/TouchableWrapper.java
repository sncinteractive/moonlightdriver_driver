package kr.moonlightdriver.driver.utils;

import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.FrameLayout;

/**
 * Created by youngmin on 2015-12-06.
 */
public class TouchableWrapper extends FrameLayout {
	private static final String TAG = "TouchableWrapper";
	private MapGestureDetector mMapGestureDetector;
	private GestureDetector mGestureDetector;

	public TouchableWrapper(Context _context) {
		super(_context);

		try {
			mMapGestureDetector = (MapGestureDetector) _context;

			mGestureDetector = new GestureDetector(_context, new MapGestureListener(){
				@Override
				public void onFlingDetected(double _angle, float _velocityX, float _velocityY) {
					mMapGestureDetector.onFlingDetected(_angle, _velocityX, _velocityY);
				}

				@Override
				public void onScrollingDetected() {
					mMapGestureDetector.onScrollingDetected();
				}
			});
		} catch (ClassCastException e) {
			throw new ClassCastException(_context.toString() + " must implement MapGestureDetect");
		}
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {
		mGestureDetector.onTouchEvent(event);
		return super.dispatchTouchEvent(event);
	}

	public interface MapGestureDetector {
		void onFlingDetected(double _angle, float _velocityX, float _velocityY);
		void onScrollingDetected();
	}
}
