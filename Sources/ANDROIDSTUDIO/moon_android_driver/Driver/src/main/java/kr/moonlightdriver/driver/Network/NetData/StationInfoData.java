package kr.moonlightdriver.driver.Network.NetData;

import android.graphics.Point;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by youngmin on 2015-10-07.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class StationInfoData {
	@JsonProperty("text")
	private String mStationName;
	@JsonProperty("arsId")
	private String mArsId;
	@JsonProperty("distance")
	private double mDistance;
	@JsonProperty("locations")
	private ShuttleLocationData mShuttleLocationData;
	@JsonProperty("stationType")
	private String mStationType;
	@JsonProperty("busId")
	private String mBusId;
	@JsonProperty("index")
	private int mStationSeq;
	@JsonProperty("busRouteArea")
	private String mBusRouteArea;
	@JsonProperty("exceptOutback")
	private int mExceptOutback;
	@JsonProperty("firstTime")
	private String mFirstTime;
	@JsonProperty("lastTime")
	private String mLastTime;
	@JsonProperty("saturdayFirstTime")
	private String mSaturdayFirstTime;
	@JsonProperty("saturdayLastTime")
	private String mSaturdayLastTime;
	@JsonProperty("sundayFirstTime")
	private String mSundayFirstTime;
	@JsonProperty("sundayLastTime")
	private String mSundayLastTime;
	@JsonProperty("runFirstTime")
	private String mRunFirstTime;
	@JsonProperty("runLastTime")
	private String mRunLastTime;
	@JsonProperty("saturdayRunFirstTime")
	private String mSaturdayRunFirstTime;
	@JsonProperty("saturdayRunLastTime")
	private String mSaturdayRunLastTime;
	@JsonProperty("sundayRunFirstTime")
	private String mSundayRunFirstTime;
	@JsonProperty("sundayRunLastTime")
	private String mSundayRunLastTime;
	@JsonProperty("holidayDate")
	private String mHolidayDate;
	@JsonProperty("isVisible")
	private boolean mIsVisible;

	private Point mBoundsIndex;
	private String mStationId;

	public StationInfoData() {
		this.mIsVisible = true;
	}

	public StationInfoData(String mArsId, Point mBoundsIndex, String mBusId, String mBusRouteArea, double mDistance, int mExceptOutback, String mFirstTime, String mHolidayDate, boolean mIsVisible, String mLastTime, String mRunFirstTime, String mRunLastTime, String mSaturdayFirstTime, String mSaturdayLastTime, String mSaturdayRunFirstTime, String mSaturdayRunLastTime, ShuttleLocationData mShuttleLocationData, String mStationId, String mStationName, int mStationSeq, String mStationType, String mSundayFirstTime, String mSundayLastTime, String mSundayRunFirstTime, String mSundayRunLastTime) {
		this.mArsId = mArsId;
		this.mBoundsIndex = mBoundsIndex;
		this.mBusId = mBusId;
		this.mBusRouteArea = mBusRouteArea;
		this.mDistance = mDistance;
		this.mExceptOutback = mExceptOutback;
		this.mFirstTime = mFirstTime;
		this.mHolidayDate = mHolidayDate;
		this.mIsVisible = mIsVisible;
		this.mLastTime = mLastTime;
		this.mRunFirstTime = mRunFirstTime;
		this.mRunLastTime = mRunLastTime;
		this.mSaturdayFirstTime = mSaturdayFirstTime;
		this.mSaturdayLastTime = mSaturdayLastTime;
		this.mSaturdayRunFirstTime = mSaturdayRunFirstTime;
		this.mSaturdayRunLastTime = mSaturdayRunLastTime;
		this.mShuttleLocationData = mShuttleLocationData;
		this.mStationId = mStationId;
		this.mStationName = mStationName;
		this.mStationSeq = mStationSeq;
		this.mStationType = mStationType;
		this.mSundayFirstTime = mSundayFirstTime;
		this.mSundayLastTime = mSundayLastTime;
		this.mSundayRunFirstTime = mSundayRunFirstTime;
		this.mSundayRunLastTime = mSundayRunLastTime;
	}

	public StationInfoData(String mArsId, String mBusId, String mBusRouteArea, double mDistance, int mExceptOutback, String mFirstTime, String mLastTime, ShuttleLocationData mShuttleLocationData, String mStationName, int mStationSeq,
	                       String mStationType, boolean mIsVisible, String mStationId) {
		this.mArsId = mArsId;
		this.mBoundsIndex = mBoundsIndex;
		this.mBusId = mBusId;
		this.mBusRouteArea = mBusRouteArea;
		this.mDistance = mDistance;
		this.mExceptOutback = mExceptOutback;
		this.mFirstTime = mFirstTime;
		this.mIsVisible = mIsVisible;
		this.mLastTime = mLastTime;
		this.mShuttleLocationData = mShuttleLocationData;
		this.mStationId = mStationId;
		this.mStationName = mStationName;
		this.mStationSeq = mStationSeq;
		this.mStationType = mStationType;
		this.mHolidayDate = "";
		this.mRunFirstTime = "";
		this.mRunLastTime = "";
		this.mSaturdayFirstTime = "";
		this.mSaturdayLastTime = "";
		this.mSaturdayRunFirstTime = "";
		this.mSaturdayRunLastTime = "";
		this.mSundayFirstTime = "";
		this.mSundayLastTime = "";
		this.mSundayRunFirstTime = "";
		this.mSundayRunLastTime = "";
	}

	public String getmStationId() {
		return mStationId;
	}

	public void setmStationId(String mStationId) {
		this.mStationId = mStationId;
	}

	public boolean ismIsVisible() {
		return mIsVisible;
	}

	public void setmIsVisible(boolean mIsVisible) {
		this.mIsVisible = mIsVisible;
	}

	public Point getmBoundsIndex() {
		return mBoundsIndex;
	}

	public void setmBoundsIndex(Point mBoundsIndex) {
		this.mBoundsIndex = mBoundsIndex;
	}

	public int getmExceptOutback() {
		return mExceptOutback;
	}

	public void setmExceptOutback(int mExceptOutback) {
		this.mExceptOutback = mExceptOutback;
	}

	public String getmArsId() {
		return mArsId;
	}

	public void setmArsId(String mArsId) {
		this.mArsId = mArsId;
	}

	public String getmBusId() {
		return mBusId;
	}

	public void setmBusId(String mBusId) {
		this.mBusId = mBusId;
	}

	public String getmBusRouteArea() {
		return mBusRouteArea;
	}

	public void setmBusRouteArea(String mBusRouteArea) {
		this.mBusRouteArea = mBusRouteArea;
	}

	public double getmDistance() {
		return mDistance;
	}

	public void setmDistance(double mDistance) {
		this.mDistance = mDistance;
	}

	public ShuttleLocationData getmShuttleLocationData() {
		return mShuttleLocationData;
	}

	public void setmShuttleLocationData(ShuttleLocationData mShuttleLocationData) {
		this.mShuttleLocationData = mShuttleLocationData;
	}

	public String getmStationName() {
		return mStationName;
	}

	public void setmStationName(String mStationName) {
		this.mStationName = mStationName;
	}

	public int getmStationSeq() {
		return mStationSeq;
	}

	public void setmStationSeq(int mStationSeq) {
		this.mStationSeq = mStationSeq;
	}

	public String getmStationType() {
		return mStationType;
	}

	public void setmStationType(String mStationType) {
		this.mStationType = mStationType;
	}

	public LatLng getPosition() {
		if(this.getmShuttleLocationData() == null) {
			return null;
		} else {
			return new LatLng(this.getmShuttleLocationData().getmCoordinates().get(1), this.getmShuttleLocationData().getmCoordinates().get(0));
		}
	}

	public String getmFirstTime() {
		return mFirstTime;
	}

	public void setmFirstTime(String mFirstTime) {
		this.mFirstTime = mFirstTime;
	}

	public String getmLastTime() {
		return mLastTime;
	}

	public void setmLastTime(String mLastTime) {
		this.mLastTime = mLastTime;
	}

	public String getmRunFirstTime() {
		return mRunFirstTime;
	}

	public void setmRunFirstTime(String mRunFirstTime) {
		this.mRunFirstTime = mRunFirstTime;
	}

	public String getmRunLastTime() {
		return mRunLastTime;
	}

	public void setmRunLastTime(String mRunLastTime) {
		this.mRunLastTime = mRunLastTime;
	}

	public String getmSaturdayFirstTime() {
		return mSaturdayFirstTime;
	}

	public void setmSaturdayFirstTime(String mSaturdayFirstTime) {
		this.mSaturdayFirstTime = mSaturdayFirstTime;
	}

	public String getmSaturdayLastTime() {
		return mSaturdayLastTime;
	}

	public void setmSaturdayLastTime(String mSaturdayLastTime) {
		this.mSaturdayLastTime = mSaturdayLastTime;
	}

	public String getmSaturdayRunFirstTime() {
		return mSaturdayRunFirstTime;
	}

	public void setmSaturdayRunFirstTime(String mSaturdayRunFirstTime) {
		this.mSaturdayRunFirstTime = mSaturdayRunFirstTime;
	}

	public String getmSaturdayRunLastTime() {
		return mSaturdayRunLastTime;
	}

	public void setmSaturdayRunLastTime(String mSaturdayRunLastTime) {
		this.mSaturdayRunLastTime = mSaturdayRunLastTime;
	}

	public String getmSundayFirstTime() {
		return mSundayFirstTime;
	}

	public void setmSundayFirstTime(String mSundayFirstTime) {
		this.mSundayFirstTime = mSundayFirstTime;
	}

	public String getmSundayLastTime() {
		return mSundayLastTime;
	}

	public void setmSundayLastTime(String mSundayLastTime) {
		this.mSundayLastTime = mSundayLastTime;
	}

	public String getmSundayRunFirstTime() {
		return mSundayRunFirstTime;
	}

	public void setmSundayRunFirstTime(String mSundayRunFirstTime) {
		this.mSundayRunFirstTime = mSundayRunFirstTime;
	}

	public String getmSundayRunLastTime() {
		return mSundayRunLastTime;
	}

	public void setmSundayRunLastTime(String mSundayRunLastTime) {
		this.mSundayRunLastTime = mSundayRunLastTime;
	}

	public String getmHolidayDate() {
		return mHolidayDate;
	}

	public void setmHolidayDate(String mHolidayDate) {
		this.mHolidayDate = mHolidayDate;
	}
}
