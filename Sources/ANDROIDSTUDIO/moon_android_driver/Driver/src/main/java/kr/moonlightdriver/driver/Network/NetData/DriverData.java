package kr.moonlightdriver.driver.Network.NetData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Created by youngmin on 2015-10-28.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DriverData {
	@JsonProperty("driverId")
	private String mDriverId;

	@JsonProperty("gcmId")
	private String mGcmId;

	@JsonProperty("phone")
	private String mPhone;

	@JsonProperty("cert")
	private DriverCertificateData mCert;

	@JsonProperty("isValid")
	private boolean mIsValid;

	@JsonProperty("macAddr")
	private String mMacAddr;

	@JsonProperty("star")
	private int mStar;

	@JsonProperty("starnum")
	private int mStarNum;

	@JsonProperty("version")
	private String mVersion;

	@JsonProperty("name")
	private String mDriverName;

	@JsonProperty("ynLogin")
	private String mYnLogin;

	@JsonProperty("driverLicense")
	private String mDriverLicense;

	@JsonProperty("insuranceCompanyId")
	private String mInsuranceCompanyId;

	@JsonProperty("insuranceNumber")
	private String mInsuranceNumber;

	public DriverData() {}

	public DriverData(DriverCertificateData mCert, String mDriverId, String mDriverLicense, String mDriverName, String mGcmId, String mInsuranceCompanyId, String mInsuranceNumber, boolean mIsValid, String mMacAddr, String mPhone, int mStar, int mStarNum, String mVersion, String mYnLogin) {
		this.mCert = mCert;
		this.mDriverId = mDriverId;
		this.mDriverLicense = mDriverLicense;
		this.mDriverName = mDriverName;
		this.mGcmId = mGcmId;
		this.mInsuranceCompanyId = mInsuranceCompanyId;
		this.mInsuranceNumber = mInsuranceNumber;
		this.mIsValid = mIsValid;
		this.mMacAddr = mMacAddr;
		this.mPhone = mPhone;
		this.mStar = mStar;
		this.mStarNum = mStarNum;
		this.mVersion = mVersion;
		this.mYnLogin = mYnLogin;
	}

	public String getmYnLogin() {
		return mYnLogin;
	}

	public void setmYnLogin(String mYnLogin) {
		this.mYnLogin = mYnLogin;
	}

	public String getmDriverLicense() {
		return mDriverLicense;
	}

	public void setmDriverLicense(String mDriverLicense) {
		this.mDriverLicense = mDriverLicense;
	}

	public String getmDriverName() {
		return mDriverName;
	}

	public void setmDriverName(String mDriverName) {
		this.mDriverName = mDriverName;
	}

	public String getmInsuranceCompanyId() {
		return mInsuranceCompanyId;
	}

	public void setmInsuranceCompanyId(String mInsuranceCompanyId) {
		this.mInsuranceCompanyId = mInsuranceCompanyId;
	}

	public String getmInsuranceNumber() {
		return mInsuranceNumber;
	}

	public void setmInsuranceNumber(String mInsuranceNumber) {
		this.mInsuranceNumber = mInsuranceNumber;
	}

	public DriverCertificateData getmCert() {
		return mCert;
	}

	public void setmCert(DriverCertificateData mCert) {
		this.mCert = mCert;
	}

	public String getmDriverId() {
		return mDriverId;
	}

	public void setmDriverId(String mDriverId) {
		this.mDriverId = mDriverId;
	}

	public String getmGcmId() {
		return mGcmId;
	}

	public void setmGcmId(String mGcmId) {
		this.mGcmId = mGcmId;
	}

	public boolean ismIsValid() {
		return mIsValid;
	}

	public void setmIsValid(boolean mIsValid) {
		this.mIsValid = mIsValid;
	}

	public String getmMacAddr() {
		return mMacAddr;
	}

	public void setmMacAddr(String mMacAddr) {
		this.mMacAddr = mMacAddr;
	}

	public String getmPhone() {
		return mPhone;
	}

	public void setmPhone(String mPhone) {
		this.mPhone = mPhone;
	}

	public int getmStar() {
		return mStar;
	}

	public void setmStar(int mStar) {
		this.mStar = mStar;
	}

	public int getmStarNum() {
		return mStarNum;
	}

	public void setmStarNum(int mStarNum) {
		this.mStarNum = mStarNum;
	}

	public String getmVersion() {
		return mVersion;
	}

	public void setmVersion(String mVersion) {
		this.mVersion = mVersion;
	}
}
