package kr.moonlightdriver.driver.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;

import com.google.android.gms.maps.model.LatLng;
import com.skt.Tmap.TMapData;
import com.skt.Tmap.TMapPOIItem;

import org.xml.sax.SAXException;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;

import kr.moonlightdriver.driver.Network.NetData.LocationData;
import kr.moonlightdriver.driver.R;
import kr.moonlightdriver.driver.activity.MainActivity;
import kr.moonlightdriver.driver.utils.Global;
import kr.moonlightdriver.driver.utils.SVUtil;

public class FindPathFragment extends Fragment {
	private static final String TAG = "FindPathFragment";
	private EditText mStartLocationText;
	private EditText mEndLocationText;
	private EditText mSpecificSearchText;

	private ArrayList<LocationData> mSearchResultList;

	private LocationData mStartLocation;
	private LocationData mEndLocation;
	private LocationData mSpecificLocation;

	private int mSearchType;

	private TMapData mTmapData;

	private View mView;

	private MainActivity mMainActivity;

	private boolean mIsFinishFragment;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		try {
			mView = inflater.inflate(R.layout.frag_findpath, container, false);

			mMainActivity = (MainActivity) getActivity();

			mIsFinishFragment = false;

			mStartLocationText = (EditText) mView.findViewById(R.id.findPath_startET);
			mEndLocationText = (EditText) mView.findViewById(R.id.findPath_endET);
			mSpecificSearchText = (EditText) mView.findViewById(R.id.find_path_search_edit_text);

			ImageButton btnStartSearchImage = (ImageButton) mView.findViewById(R.id.findPath_map1Btn);
			ImageButton btnEndSearchImage = (ImageButton) mView.findViewById(R.id.findPath_map2Btn);
			ImageButton btnSpecificSearchImage = (ImageButton) mView.findViewById(R.id.find_path_btn_search_map);
			ImageButton btnStartSearch = (ImageButton) mView.findViewById(R.id.findPath_search1Btn);
			ImageButton btnEndSearch = (ImageButton) mView.findViewById(R.id.findPath_search2Btn);
			ImageButton btnTotalSearchCar = (ImageButton) mView.findViewById(R.id.findPath_total_search_car);
			ImageButton btnTotalSearchBus = (ImageButton) mView.findViewById(R.id.findPath_total_search_bus);
			ImageButton btnTotalSearchWalk = (ImageButton) mView.findViewById(R.id.findPath_total_search_walk);
			ImageButton btnSpecificSearch = (ImageButton) mView.findViewById(R.id.find_path_btn_search);
			ImageButton btnSpecificExecute = (ImageButton) mView.findViewById(R.id.find_path_btn_execute);

			btnStartSearch.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						if (mStartLocationText.getText().toString().length() < 2) {
							SVUtil.showSimpleDialog(mMainActivity, "검색어를 2글자 이상 입력해주세요.");
						} else if (mStartLocationText.getText().toString().equals("(현재위치)")) {
							SVUtil.showSimpleDialog(mMainActivity, "검색어를 입력해주세요.");
						} else {
							mSearchType = 1;
							new SearchByTextTask().execute();
						}

						SVUtil.hideSoftInput(mMainActivity);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			btnStartSearchImage.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					showMapSearch("start");
				}
			});

			btnEndSearchImage.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					showMapSearch("end");
				}
			});

			btnEndSearch.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						if (mEndLocationText.getText().toString().length() < 2) {
							SVUtil.showSimpleDialog(mMainActivity, "검색어를 2글자 이상 입력해주세요.");
						} else {
							mSearchType = 2;
							new SearchByTextTask().execute();
						}

						SVUtil.hideSoftInput(mMainActivity);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			btnTotalSearchCar.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					launchDaumWeb("carRoute");
				}
			});

			btnTotalSearchBus.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					launchDaumWeb("publicRoute");
				}
			});

			btnTotalSearchWalk.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					launchDaumWeb("walkRoute");
				}
			});

			btnSpecificSearch.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						if (mSpecificSearchText.getText().toString().length() < 2) {
							SVUtil.showSimpleDialog(mMainActivity, "검색어를 2글자 이상 입력해주세요.");
						} else {
							mSearchType = 3;
							new SearchByTextTask().execute();
						}

						SVUtil.hideSoftInput(mMainActivity);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			btnSpecificSearchImage.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					showMapSearch("specific");
				}
			});

			btnSpecificExecute.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						if(mSpecificLocation == null) {
							SVUtil.showSimpleDialog(mMainActivity, "위치를 입력해주세요.");
							return;
						}

						showSpecificPointSearch();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			mSearchResultList = new ArrayList<>();

			mSearchType = 0;
			mTmapData = new TMapData();
		} catch(InflateException e) {
			e.printStackTrace();
		}

		return mView;
	}

	private void getCurrentAddress() {
		new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				String retString = "";
				try {
					retString = mTmapData.convertGpsToAddress(mMainActivity.mCurrentLocation.latitude, mMainActivity.mCurrentLocation.longitude);
				} catch (IOException | ParserConfigurationException | SAXException e) {
					e.printStackTrace();
				}

				return retString;
			}

			@Override
			protected void onPostExecute(String _retString) {
				try {
					if(mIsFinishFragment) {
						return;
					}

					String[] retSplit = _retString.split(" ");
					String convertedAddress = "";

					for (int i = 2; i < retSplit.length; i++) {
						convertedAddress += (retSplit[i] + " ");
					}

					if(mView != null) {
						String startLocation = "현위치 : " + convertedAddress;
						mStartLocationText.setText(startLocation);
						mStartLocation = new LocationData(convertedAddress, mMainActivity.mCurrentLocation);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}.execute();
	}

	private void showSpecificPointSearch() {
		try {
			mMainActivity.mFragmentOutback = new OutbackFragment();

			Bundle args = new Bundle();
			args.putString("from", "findpath");
			args.putString("positionName", mSpecificLocation.getmLocationName());
			args.putDouble("lat", mSpecificLocation.getmPosition().latitude);
			args.putDouble("lng", mSpecificLocation.getmPosition().longitude);

			mMainActivity.mFragmentOutback.setArguments(args);

			mMainActivity.replaceFragment(mMainActivity.mFragmentOutback, R.id.mainFrameLayout, Global.FRAG_OUTBACK_TAG, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void showMapSearch(String _searchType) {
		try {
			double search_point_lat = mMainActivity.mCurrentLocation.latitude;
			double search_point_lng = mMainActivity.mCurrentLocation.longitude;

			switch (_searchType) {
				case "start" :
					if(mStartLocation != null) {
						search_point_lat = mStartLocation.getmPosition().latitude;
						search_point_lng = mStartLocation.getmPosition().longitude;
					}
					break;
				case "end" :
					if(mEndLocation != null) {
						search_point_lat = mEndLocation.getmPosition().latitude;
						search_point_lng = mEndLocation.getmPosition().longitude;
					}
					break;
				case "specific" :
					if(mSpecificLocation != null) {
						search_point_lat = mSpecificLocation.getmPosition().latitude;
						search_point_lng = mSpecificLocation.getmPosition().longitude;
					}
					break;
			}

			MapSearchFragment frag = new MapSearchFragment();
			frag.setTargetFragment(this, 1);

			Bundle args = new Bundle();
			args.putString("searchType", _searchType);
			args.putDouble("lat", search_point_lat);
			args.putDouble("lng", search_point_lng);

			frag.setArguments(args);

			FragmentTransaction transaction = getFragmentManager().beginTransaction();
			transaction.add(R.id.mainFrameLayout, frag, Global.FRAG_MAP_SEARCH_TAG);
			transaction.addToBackStack(null);
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		try {
			if(mIsFinishFragment) {
				return;
			}

			if(requestCode == 1 && resultCode == Activity.RESULT_OK) {
				if(data != null) {
					String searchType = data.getStringExtra("searchType");
					String address = data.getStringExtra("address");
					double lat = data.getDoubleExtra("lat", 0);
					double lng = data.getDoubleExtra("lng", 0);

					switch (searchType) {
						case "start" :
							mStartLocationText.setText(address);
							mStartLocation = new LocationData(address, new LatLng(lat, lng));
							break;
						case "end" :
							mEndLocationText.setText(address);
							mEndLocation = new LocationData(address, new LatLng(lat, lng));
							break;
						case "specific" :
							mSpecificSearchText.setText(address);
							mSpecificLocation = new LocationData(address, new LatLng(lat, lng));
							break;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	class SearchByTextTask extends AsyncTask<Void, Integer, ArrayList<TMapPOIItem>> {
		private String mSearchText;
		@Override
		protected void onPreExecute() {
			try {
				SVUtil.showProgressDialog(mMainActivity, "검색중...");
				if (mSearchType == 1) {
					mSearchText = mStartLocationText.getText().toString();
				} else if (mSearchType == 2) {
					mSearchText = mEndLocationText.getText().toString();
				} else if (mSearchType == 3) {
					mSearchText = mSpecificSearchText.getText().toString();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		protected ArrayList<TMapPOIItem> doInBackground(Void... params) {
			ArrayList<TMapPOIItem> ret = null;

			try {
				ret = mTmapData.findAllPOI(mSearchText);
			} catch (IOException | ParserConfigurationException | SAXException e) {
				e.printStackTrace();
			}

			return ret;
		}

		@Override
		protected void onPostExecute(ArrayList<TMapPOIItem> result) {
			SVUtil.hideProgressDialog();

			try {
				mSearchResultList.clear();

				if(mIsFinishFragment) {
					return;
				}

				if (result != null) {
					for (TMapPOIItem item : result) {
						mSearchResultList.add(new LocationData(item.getPOIName(), new LatLng(item.getPOIPoint().getLatitude(), item.getPOIPoint().getLongitude())));
					}

					final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(mMainActivity, R.layout.row_joined_drivers);

					for (LocationData box : mSearchResultList) {
						arrayAdapter.add(box.getmLocationName());
					}

					SVUtil.showAdapterDialog(mMainActivity, "검색 결과", arrayAdapter, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int position) {
							if (mSearchResultList.size() > 0) {
//							SVUtil.log("error", TAG, "item : " + mSearchResultList.get(position).getmLocationName());

								if (mSearchType == 1) {
									mStartLocationText.setText(mSearchResultList.get(position).getmLocationName());
									mStartLocation = mSearchResultList.get(position);
								} else if (mSearchType == 2) {
									mEndLocationText.setText(mSearchResultList.get(position).getmLocationName());
									mEndLocation = mSearchResultList.get(position);
								} else if (mSearchType == 3) {
									mSpecificSearchText.setText(mSearchResultList.get(position).getmLocationName());
									mSpecificLocation = mSearchResultList.get(position);
								}
							}

							mSearchResultList.clear();
						}
					}, "취소", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});
				} else {
					SVUtil.showSimpleDialog(mMainActivity, "검색 결과가 없습니다.");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void launchDaumWeb(String _action) {
		try {
			mSearchType = 0;
			if(mStartLocation == null || mStartLocation.getmLocationName() == null || mStartLocation.getmPosition() == null) {
				SVUtil.showSimpleDialog(mMainActivity, "출발지를 선택(출발지를 적은 후 돋보기 아이콘을 클릭)해주세요.");
				return;
			}

			if(mEndLocation == null || mEndLocation.getmLocationName() == null || mEndLocation.getmPosition() == null) {
				SVUtil.showSimpleDialog(mMainActivity, "도착지를 선택(도착지를 적은 후 돋보기 아이콘을 클릭)해주세요.");
				return;
			}

			String urlStr = "";
			urlStr += "http://m.map.daum.net/actions/" + _action + "?";
			urlStr += "startLoc=" + mStartLocation.getmLocationName() + "&";
			urlStr += "endLoc=" + mEndLocation.getmLocationName() + "&";
			urlStr += "sx=" + mStartLocation.getmPosition().longitude + "&";
			urlStr += "sy=" + mStartLocation.getmPosition().latitude + "&";
			urlStr += "ex=" + mEndLocation.getmPosition().longitude + "&";
			urlStr += "ey=" + mEndLocation.getmPosition().latitude + "&";
			urlStr += "service=TOPIS" + "&";
			urlStr += "inputCoordSystem=WGS84";

//		SVUtil.log("error", TAG, "url : " + urlStr);

			Uri uri = Uri.parse(urlStr);
			startActivity(new Intent(Intent.ACTION_VIEW, uri));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onStart() {
		super.onStart();

		try {
			mIsFinishFragment = false;

			mStartLocationText.setText("");
			mEndLocationText.setText("");
			mSpecificSearchText.setText("");

			mEndLocationText.requestFocus();

			mStartLocation = null;
			mEndLocation = null;
			mSpecificLocation = null;

			getCurrentAddress();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onResume() {
		super.onResume();

		try {
			mMainActivity.changeButtonImage(mMainActivity.mImgBtnFindPath);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		setTargetFragment(null, -1);
	}

	@Override
	public void onPause() {
		super.onPause();

		mIsFinishFragment = true;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();

		try {
			if(mView != null){
				ViewGroup parent = (ViewGroup) mView.getParent();
				if(parent != null) {
					parent.removeView(mView);
				}
			}

			mMainActivity.mFragmentFindPath = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
