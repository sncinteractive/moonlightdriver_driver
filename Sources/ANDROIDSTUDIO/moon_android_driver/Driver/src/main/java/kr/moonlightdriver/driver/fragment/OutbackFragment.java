package kr.moonlightdriver.driver.fragment;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.GestureDetector;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.kevinsawicki.http.HttpRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.ui.IconGenerator;
import com.skt.Tmap.TMapData;
import com.skt.Tmap.TMapPOIItem;
import com.skt.Tmap.TMapPoint;
import com.skt.Tmap.TMapPolyLine;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.MultiPolygon;

import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;

import kr.moonlightdriver.driver.Network.NetApi.BeaconListAck;
import kr.moonlightdriver.driver.Network.NetApi.CarpoolRegistrantListAck;
import kr.moonlightdriver.driver.Network.NetApi.NetAPI;
import kr.moonlightdriver.driver.Network.NetApi.NetNearShuttleStationsAck;
import kr.moonlightdriver.driver.Network.NetApi.NetResponse;
import kr.moonlightdriver.driver.Network.NetApi.NetResponseCallback;
import kr.moonlightdriver.driver.Network.NetApi.PublicTransitStationListAck;
import kr.moonlightdriver.driver.Network.NetApi.ShuttleRealTimeDataAck;
import kr.moonlightdriver.driver.Network.NetClient.NetClient;
import kr.moonlightdriver.driver.Network.NetData.BeaconData;
import kr.moonlightdriver.driver.Network.NetData.BusStationListData;
import kr.moonlightdriver.driver.Network.NetData.CarpoolRegistrantData;
import kr.moonlightdriver.driver.Network.NetData.DriverPointData;
import kr.moonlightdriver.driver.Network.NetData.LocationData;
import kr.moonlightdriver.driver.Network.NetData.OutbackBoundsData;
import kr.moonlightdriver.driver.Network.NetData.ResultData;
import kr.moonlightdriver.driver.Network.NetData.ShuttleLineColorData;
import kr.moonlightdriver.driver.Network.NetData.ShuttleLineData;
import kr.moonlightdriver.driver.Network.NetData.ShuttleLocationData;
import kr.moonlightdriver.driver.Network.NetData.ShuttleRealTimeData;
import kr.moonlightdriver.driver.Network.NetData.StationInfoData;
import kr.moonlightdriver.driver.Network.NetData.SubwayStationListData;
import kr.moonlightdriver.driver.R;
import kr.moonlightdriver.driver.activity.MainActivity;
import kr.moonlightdriver.driver.utils.Global;
import kr.moonlightdriver.driver.utils.MapGestureListener;
import kr.moonlightdriver.driver.utils.SVUtil;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * Created by youngmin on 2015-07-22.
 */
public class OutbackFragment extends Fragment implements SensorEventListener, View.OnTouchListener {
	private static final String TAG = "OutbackFragment_new";
	private static final LatLng LIMIT_BOUNDS_NE = new LatLng(83.91177715928563, -175.8688546344638);
	private static final LatLng LIMIT_BOUNDS_SW = new LatLng(-43.27909581852835, 57.56865844130516);
	private static final LatLngBounds LIMIT_BOUNDS = new LatLngBounds(LIMIT_BOUNDS_SW, LIMIT_BOUNDS_NE);
	private static final float MIN_ZOOM  = 14f;  //16.23f;
	private static final float CHANGE_ICON_ZOOM  = 16.23f;//15.25f;  //16.23f;
	private static final int SCREEN_BOUNDS_NUMBER = 4;
	private static final int CHECK_BOUNDS_NUMBER = 7;
	private static final double HOLE_RADIUS = 1d;
	private static final double HOLE_POINTS = 50d;
	private static final int MSG_2 = 1;
	private final int[] mDistanceList = { 1000, 2000, 3000, 4000, 5000 };
	private String[] mDistanceTextList;

	public boolean mExpectedOutsideOfBounds;

	private MainActivity mMainActivity;
	private GoogleMap mGoogleMap;
	private View mView;

	private Marker mCloseMarker;
	private Marker mLongClickPositionMarker;
	private Marker mFindWayStartMarker;
	private Marker mMeasureMarker;
	private Marker mCurrentPositionOuterMarker;
	private Marker mCurrentPositionInnerMarker;
	private Marker mSearchPositionMarker;
//	private Marker mCurrentPositionOuterCircleMarker;
	private LatLngBounds mCurrentScreen;
	private LatLng mLastCenterPosition;
	private LatLng mCurrentMyPosition;
	private LatLng mDefaultCenterPosition;
	private LatLng mMeasureStartLatLng;
	private LatLng mFindWayStartLatLng;
	private LatLng mFindWayEndLatLng;
	private Polygon mOutbackPolygon;
	private Polyline mFindWayPolyline;
	private Polyline mPastPathPolyline;

	private GestureDetector mGestureDetector;

	private int mCurrentDistance;
	private int mSelectedDistance;
	private float mLastZoom;
	private float mSearchRadius;
	private double mTotalDistance;

	public boolean mIsFragmentRunning;
	public boolean mIsFinishFragment;
	private boolean mIsUpdateRunningStationList;
	private boolean mIsShowShuttleLineColorList;
	private boolean mIsShowOutbackPolygon;
	private boolean mIsMapRotation;
	private boolean mIsShowDriver;
	private boolean mIsShowPublicTransit;
//	private boolean mIsAnimationToCurrentPosition;

	public String mFragmentFrom;
	public String mCurrentPositionName;
	private String mLongClickStatus;

	private enum MarkerDisplayMode {
		ALL,    // 모두 표시
		SMALL_ICON, // 셔틀 경로 표시 제외 작은 아이콘 표시
		NOTHING // 아무것도 표시 안함
	}
	private MarkerDisplayMode mMarkerDisplayMode;

//	private JSONObject mShuttleColorList;
	private JSONObject mShuttleColorArrowBitmapDescriptor;
	private OutbackBoundsData mOutbackBoundsData;

	private SensorManager mSensorManager;
	private Sensor mAccelerometer;
	private Sensor mMagnetometer;
	float[] mLastAccelerometer;
	float[] mLastMagnetometer;

	private float mOuterMapBearing;
	private float mInnerMapBearing;
	private float mDeclination;

	private ShuttleLineColorAdapter mShuttleLineColorAdapter;
	private OutbackStationAdapter mOutbackStationAdapter;

	private ArrayList<ShuttleLineColorData> mShuttleLineColorList;
	private ArrayList<StationInfoData> mRunningStationList;
	private ArrayList<StationInfoData> mShowRunningStationList;
	private ArrayList<Marker> mShuttleStationMarkers;
	private ArrayList<Polyline> mShuttleLineList;
	private ArrayList<Marker> mArrowMarkers;
	private ArrayList<StationInfoData> mDriverList;
	private ArrayList<StationInfoData> mPublicTransitList;
	private HashMap<String,BeaconData> mBeaconList;
	private ArrayList<Marker> mDriverMarkers;
	private ArrayList<Marker> mPublicTransitMarkers;

	private ArrayList<Marker> mDistanceMakers;
	private ArrayList<Polyline> mDistancePolyline;

	private Button mBtnShowStationList;
	private Button mBtnAroundMap;
	private ImageButton mBtnCurrentPosition;
	private ImageButton mBtnClose;
	private ImageButton mBtnZoomIn;
	private ImageButton mBtnZoomOut;
	private ImageButton mBtnCall;
	private LinearLayout mOutbackStationListLayout;
	private TextView mOutbackStationListDirection;
	private TextView mEmptyTextView;
	private Button mBtnDistance;
	private TextView mTVTouchDesc;
	private Button mBtnOnOff;
	private Button mBtnMapRotation;
	private Button mBtnDriverOnOff;
	private Button mBtnPublicTransitOnOff;
	private LinearLayout mBtnShowShuttleLineColor;
	private ListView mShuttleColorLineListView;
	private StickyListHeadersListView mOutbackListView;

	private ImageButton mBtnSearchShuttlePath;
	private ImageButton mBtnSearchShuttlePathExecute;
	private LinearLayout mSearchShuttlePathLayout;
	private EditText mDepartFromText;
	private EditText mArriveAtText;
	private ListView mSearchResultListView;
	private ImageButton mBtnSearchShuttlePathDepartFrom;
	private ImageButton mBtnSearchShuttlePathArriveAt;
	private ArrayAdapter<String> mSearchResultAdapter;
	private ArrayList<LocationData> mSearchResultList;

	private LocationData mDepartFromLocation;
	private LocationData mArriveAtLocation;
	private int mSearchType;

	private boolean isSearchShuttlePathOn;

	private NearStationListTask mSearchNearStationListTask;
	private ShowOutbackPolygonTask mShowOutbackPolygonTask;
	private UpdateShuttleLineListTask mUpdateShuttleLineListTask;
	private UpdatedStationListTask mUpdatedStationListTask;

	public static long mStartTime;

	private boolean mIsShowRealTimeShuttlePosition;
	private boolean mIsRetrievingShuttlePosition;
	private ArrayList<Marker> mShuttleRealTimeMarkers;
	private ArrayList<Marker> mShuttleRealTimeBeaconMarkers;

	private TMapData mTmapData;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		try {
//			SVUtil.log("error", TAG, "onCreateView()");

			mView = inflater.inflate(R.layout.frag_outback, container, false);

			mMainActivity = (MainActivity) getActivity();

			mGestureDetector = new GestureDetector(mMainActivity, new MapGestureListener());

			mTmapData = new TMapData();

			init();

			initView();

			setUpMapIfNeeded();
		} catch(InflateException e) {
			e.printStackTrace();
		}

		return mView;
	}

	private void setUpMapIfNeeded() {
		try {
			if (mGoogleMap == null) {
				mGoogleMap = ((SupportMapFragment) MainActivity.mSupportFragmentManager.findFragmentById(R.id.outback_map)).getMap();
				if (mGoogleMap != null) {
					setUpMap();
				}
			}

//			if (mGoogleMap == null) {
//				SupportMapFragment mapFragment = (SupportMapFragment) MainActivity.mSupportFragmentManager.findFragmentById(R.id.outback_map);
//				mapFragment.getMapAsync(this);
//			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

//	@Override
//	public void onMapReady(GoogleMap googleMap) {
//		try {
//			mGoogleMap = googleMap;
//			setUpMap();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	private void setUpMap() {
		try {
			mGoogleMap.getUiSettings().setRotateGesturesEnabled(true);
			mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
			mGoogleMap.getUiSettings().setCompassEnabled(true);

			if(mFragmentFrom.equals("main")) {
				mCurrentPositionOuterMarker = mGoogleMap.addMarker(new MarkerOptions().position(mCurrentMyPosition).icon(SVUtil.getMarkerIcon(getResources(), R.drawable.ic_current_location_outer)).anchor(0.5f, 0.5f));
				mCurrentPositionInnerMarker = mGoogleMap.addMarker(new MarkerOptions().position(mCurrentMyPosition).icon(SVUtil.getMarkerIcon(getResources(), R.drawable.ic_current_location_inner)).anchor(0.5f, 0.5f));
//		        mCurrentPositionOuterCircleMarker = mGoogleMap.addMarker(new MarkerOptions().position(mCurrentMyPosition).icon(SVUtil.getMarkerIcon(getResources(), R.drawable.ic_current_location_outer_circl)).anchor(0.5f, 0.5f));
			} else if(mFragmentFrom.equals("findpath")) {
				mCurrentPositionOuterMarker = mGoogleMap.addMarker(new MarkerOptions().position(mMainActivity.mCurrentLocation).icon(SVUtil.getMarkerIcon(getResources(), R.drawable.ic_current_location_outer)).anchor(0.5f, 0.5f));
				mCurrentPositionInnerMarker = mGoogleMap.addMarker(new MarkerOptions().position(mMainActivity.mCurrentLocation).icon(SVUtil.getMarkerIcon(getResources(), R.drawable.ic_current_location_inner)).anchor(0.5f, 0.5f));

				IconGenerator iconFactory = new IconGenerator(mMainActivity);
				iconFactory.setStyle(IconGenerator.STYLE_ORANGE);
				mSearchPositionMarker = mGoogleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon(mCurrentPositionName))).position(mCurrentMyPosition).anchor(iconFactory.getAnchorU(), iconFactory.getAnchorV()).snippet(""));
			}

			mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mDefaultCenterPosition, CHANGE_ICON_ZOOM));

			setEventListener();

//		    drawPastPath();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void initView() {
		try {
			mEmptyTextView = (TextView) mView.findViewById(R.id.empty_text_view);
			mBtnCurrentPosition = (ImageButton) mView.findViewById(R.id.frg_outback_map_currentPosIB);
			mBtnShowStationList = (Button) mView.findViewById(R.id.btnOutbackStationList);
			mBtnClose = (ImageButton) mView.findViewById(R.id.frg_outback_btn_close);
			mBtnAroundMap = (Button) mView.findViewById(R.id.btnOutbackAroundMap);
			mBtnZoomIn = (ImageButton) mView.findViewById(R.id.btnOutbackZoomIn);
			mBtnZoomOut = (ImageButton) mView.findViewById(R.id.btnOutbackZoomOut);
			mOutbackStationListLayout = (LinearLayout) mView.findViewById(R.id.frgOutbackStationListLayout);
			mOutbackListView = (StickyListHeadersListView) mView.findViewById(R.id.frgOutbackListView);
			mTVTouchDesc = (TextView) mView.findViewById(R.id.textview_touch_desc);
			mBtnDistance = (Button) mView.findViewById(R.id.frgOutbackBtnDistance);
			mBtnMapRotation = (Button) mView.findViewById(R.id.frg_map_rotation_onoffBtn);
			mBtnCall = (ImageButton) mView.findViewById(R.id.btnOutbackCall);
			mOutbackStationListDirection = (TextView) mView.findViewById(R.id.btn_shuttle_line_color_direction);
			mBtnOnOff = (Button) mView.findViewById(R.id.frg_outback_onoffBtn);
			mBtnShowShuttleLineColor = (LinearLayout) mView.findViewById(R.id.btn_shuttle_line_color);
			mShuttleColorLineListView = (ListView) mView.findViewById(R.id.shuttle_color_list);
			mBtnDriverOnOff = (Button) mView.findViewById(R.id.frg_map_driver_onoffBtn);
			mBtnPublicTransitOnOff = (Button) mView.findViewById(R.id.frg_map_transit_onoffBtn);

			mBtnSearchShuttlePath = (ImageButton) mView.findViewById(R.id.btnSearchShuttlePath);
			mBtnSearchShuttlePathExecute = (ImageButton) mView.findViewById(R.id.btnSearchShuttlePathExecute);
			mSearchShuttlePathLayout = (LinearLayout) mView.findViewById(R.id.searchShuttlePathLayout);
			mSearchShuttlePathLayout.setVisibility(View.GONE);
			mDepartFromText = (EditText) mView.findViewById(R.id.shuttlePathDepartFromET);
			mArriveAtText = (EditText) mView.findViewById(R.id.shuttlePathArriveAtET);
			mSearchResultListView = (ListView) mView.findViewById(R.id.searchResultList);
			mBtnSearchShuttlePathDepartFrom = (ImageButton) mView.findViewById(R.id.btnSearchShuttlePathDepartFrom);
			mBtnSearchShuttlePathArriveAt = (ImageButton) mView.findViewById(R.id.btnSearchShuttlePathArriveAt);

			mSearchResultAdapter = new ArrayAdapter<>(mMainActivity, android.R.layout.simple_list_item_1);
			mSearchResultListView.setAdapter(mSearchResultAdapter);

			mShuttleLineColorAdapter = new ShuttleLineColorAdapter(mMainActivity, R.layout.row_shuttle_line_color, mShuttleLineColorList);
			mShuttleColorLineListView.setAdapter(mShuttleLineColorAdapter);

			mOutbackStationAdapter = new OutbackStationAdapter(mMainActivity, R.layout.row_near_station, mShowRunningStationList);
			mOutbackListView.setAdapter(mOutbackStationAdapter);

			mBtnClose.setVisibility(View.GONE);
			if(mFragmentFrom.equals("findpath")) {
				mBtnClose.setVisibility(View.VISIBLE);
			}

			changeOutbackButtonBg(mIsShowOutbackPolygon);

			changeMapRotationButtonBg(mIsMapRotation);

			changeDriverOnOffButtonBg(mIsShowDriver);

			changePublicTransitOnOffButtonBg();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void init() {
		try {
			mFragmentFrom = "main";
			mCurrentPositionName = "";
			mCurrentMyPosition = mMainActivity.mCurrentLocation;
			mDefaultCenterPosition = mMainActivity.mCurrentLocation;

			mDepartFromLocation = null;
			mArriveAtLocation = null;

			Bundle bundle = OutbackFragment.this.getArguments();
			if(bundle != null) {
				mFragmentFrom = bundle.getString("from", "main");
				mCurrentPositionName = bundle.getString("positionName", "");
				mCurrentMyPosition = new LatLng(bundle.getDouble("lat"), bundle.getDouble("lng"));
				mDefaultCenterPosition = new LatLng(bundle.getDouble("centerLat", 0), bundle.getDouble("centerLng", 0));
				if(mDefaultCenterPosition.latitude == 0) {
					mDefaultCenterPosition = mCurrentMyPosition;
				}
			}

			mSensorManager = (SensorManager) mMainActivity.getSystemService(Context.SENSOR_SERVICE);
			mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
			mMagnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

//			mIsAnimationToCurrentPosition = false;
			mIsMapRotation = false;
			mIsShowDriver = true;
			mIsShowPublicTransit = false;
			mIsShowOutbackPolygon = false;
			mIsFragmentRunning = false;
			mExpectedOutsideOfBounds = false;
			mLastCenterPosition = null;
			mIsFinishFragment = false;
			mIsUpdateRunningStationList = true;
			mIsShowRealTimeShuttlePosition = false;
			mIsRetrievingShuttlePosition = false;
			isSearchShuttlePathOn = false;
			mSearchRadius = 0f;
			mMarkerDisplayMode = MarkerDisplayMode.ALL;
			mLastZoom = CHANGE_ICON_ZOOM;

			mShuttleLineColorList = new ArrayList<>();
			mShowRunningStationList = new ArrayList<>();
			mShuttleStationMarkers = new ArrayList<>();
			mShuttleLineList = new ArrayList<>();
			mArrowMarkers = new ArrayList<>();
			mRunningStationList = new ArrayList<>();
			mShuttleRealTimeMarkers = new ArrayList<>();
			mShuttleRealTimeBeaconMarkers = new ArrayList<>();
			mSearchResultList = new ArrayList<>();
			mBeaconList = new HashMap();

			mShuttleColorArrowBitmapDescriptor = new JSONObject();

			mOutbackBoundsData = new OutbackBoundsData(CHECK_BOUNDS_NUMBER, SCREEN_BOUNDS_NUMBER);

			mSearchType = 0;
			mCurrentDistance = 0;
			mIsShowShuttleLineColorList = false;
			mDeclination = 0f;
			mOuterMapBearing = 0f;
			mInnerMapBearing = 0f;

			mLongClickStatus = "wait";
			mDistanceMakers = new ArrayList<>();
			mDistancePolyline = new ArrayList<>();

			int listLength = mDistanceList.length;

			mDistanceTextList = new String[listLength];
			for(int i = 0; i < listLength; i++) {
				float distance = (mDistanceList[i] / 1000);
				if(distance < 1) {
					mDistanceTextList[i] = distance + "km";
				} else {
					mDistanceTextList[i] = ((int) distance) + "km";
				}
			}

//		setShuttleColor();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setEventListener() {
		try {
			mSearchResultListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					if (mSearchResultList.size() > 0) {
						if (mSearchType == 1) {
							mDepartFromText.setText(mSearchResultList.get(position).getmLocationName());
							mDepartFromLocation = mSearchResultList.get(position);
						} else if (mSearchType == 2) {
							mArriveAtText.setText(mSearchResultList.get(position).getmLocationName());
							mArriveAtLocation = mSearchResultList.get(position);
						}

						if(mDepartFromLocation != null && mArriveAtLocation != null) {
							mBtnSearchShuttlePath.setVisibility(View.GONE);
							isSearchShuttlePathOn = false;
							mBtnSearchShuttlePath.setBackgroundResource(R.drawable.shape_button_bg_off);

							mBtnSearchShuttlePathExecute.setVisibility(View.VISIBLE);
						}
					}

					mSearchResultList.clear();
					mSearchResultAdapter.clear();
				}
			});

			mBtnSearchShuttlePathDepartFrom.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (mDepartFromText.getText().toString().length() < 2) {
						SVUtil.showSimpleDialog(mMainActivity, "검색어를 2글자 이상 입력해주세요.");
					} else {
						mSearchType = 1;
						new SearchByTextTask(mDepartFromText.getText().toString()).execute();
						SVUtil.hideSoftInput(mMainActivity);
					}
				}
			});

			mBtnSearchShuttlePathArriveAt.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (mArriveAtText.getText().toString().length() < 2) {
						SVUtil.showSimpleDialog(mMainActivity, "검색어를 2글자 이상 입력해주세요.");
					} else {
						mSearchType = 2;
						new SearchByTextTask(mArriveAtText.getText().toString()).execute();
						SVUtil.hideSoftInput(mMainActivity);
					}
				}
			});

			mBtnSearchShuttlePath.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						isSearchShuttlePathOn = !isSearchShuttlePathOn;
						changeSearchShuttlePathButtonBg();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			mBtnSearchShuttlePathExecute.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						SearchShuttlePathResultFragment frag = new SearchShuttlePathResultFragment();

						Bundle args = new Bundle();
						args.putString("departFromAddress", mDepartFromLocation.getmLocationName());
						args.putDouble("departFromLat", mDepartFromLocation.getmPosition().latitude);
						args.putDouble("departFromLng", mDepartFromLocation.getmPosition().longitude);
						args.putString("arriveAtAddress", mArriveAtLocation.getmLocationName());
						args.putDouble("arriveAtLat", mArriveAtLocation.getmPosition().latitude);
						args.putDouble("arriveAtLng", mArriveAtLocation.getmPosition().longitude);

						frag.setArguments(args);

						FragmentTransaction transaction = getFragmentManager().beginTransaction();
						transaction.add(R.id.mainFrameLayout, frag);
						transaction.addToBackStack(null);
						transaction.commit();

						isSearchShuttlePathOn = true;
						changeSearchShuttlePathButtonBg();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			mBtnCall.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					SVUtil.showDialogWithListener(mMainActivity, "알림", "상황실 전화 연결은 유료로 추가 요금이 발생할수 있습니다.\n연결 하시겠습니까?", "연결", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();

							mMainActivity.makePhoneCall();
						}
					}, "취소", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});
				}
			});

			mBtnZoomIn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
//					mGoogleMap.animateCamera(CameraUpdateFactory.zoomIn());
					mGoogleMap.moveCamera(CameraUpdateFactory.zoomIn());
				}
			});

			mBtnZoomOut.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
//					mGoogleMap.animateCamera(CameraUpdateFactory.zoomOut());
					mGoogleMap.moveCamera(CameraUpdateFactory.zoomOut());
				}
			});

			mBtnClose.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						mMainActivity.mFragmentFindPath = new FindPathFragment();
						mMainActivity.replaceFragment(mMainActivity.mFragmentFindPath, R.id.mainFrameLayout, Global.FRAG_FINDPATH_TAG, false);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			mShuttleColorLineListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					try {
						mMainActivity.checkDriverConfirmData(mShuttleLineColorList.get(position).getmBusId(), mFragmentFrom);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			mBtnShowShuttleLineColor.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						mIsShowShuttleLineColorList = !mIsShowShuttleLineColorList;

						if (mIsShowShuttleLineColorList) {
							mOutbackStationListDirection.setText("▲");
							mShuttleColorLineListView.setVisibility(View.VISIBLE);
						} else {
							mOutbackStationListDirection.setText("▼");
							mShuttleColorLineListView.setVisibility(View.GONE);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			mBtnDistance.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					SVUtil.showSingleChoiceItemDialog(mMainActivity, "검색 범위 선택", mDistanceTextList, mCurrentDistance, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {
							mSelectedDistance = whichButton;
						}
					}, "확인", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {
							try {
								if (mIsFinishFragment) {
									return;
								}

								mCurrentDistance = mSelectedDistance;
								showNearStationList();

								int selectedDistance = mDistanceList[mCurrentDistance];
								if (mShowRunningStationList.size() < 1) {
									SVUtil.showSimpleDialog(mMainActivity, (selectedDistance / 1000) + "km 반경 내에 정류장 없음");
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}, "취소", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {
							dialog.dismiss();
						}
					});
				}
			});

			mBtnMapRotation.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						mIsMapRotation = !mIsMapRotation;

						changeMapRotationButtonBg(mIsMapRotation);

//						SVUtil.log("error", TAG, "from btnMapRotation click");

						updateCamera(true);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			mBtnDriverOnOff.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						mIsShowDriver = !mIsShowDriver;

						changeDriverOnOffButtonBg(mIsShowDriver);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			mBtnPublicTransitOnOff.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					try {
						mIsShowPublicTransit = !mIsShowPublicTransit;

						changePublicTransitOnOffButtonBg();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			mBtnOnOff.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						mStartTime = System.currentTimeMillis();

						mIsShowOutbackPolygon = !mIsShowOutbackPolygon;

						changeOutbackButtonBg(mIsShowOutbackPolygon);

						if (mIsShowOutbackPolygon) {
							if (mGoogleMap.getCameraPosition().zoom < CHANGE_ICON_ZOOM) {
//								mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mCurrentScreen.getCenter().latitude, mCurrentScreen.getCenter().longitude), CHANGE_ICON_ZOOM));
								mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mCurrentScreen.getCenter().latitude, mCurrentScreen.getCenter().longitude), CHANGE_ICON_ZOOM));
							} else {
								checkExpectedOutsideOfBounds(mCurrentScreen.getCenter(), 0f, 0f);
								getNearStationList(mCurrentScreen, mIsUpdateRunningStationList, mExpectedOutsideOfBounds);
							}
						} else {
							removeOutbackPolygon();
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			mBtnCurrentPosition.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						if (mFragmentFrom.equals("main")) {
							mCurrentMyPosition = mMainActivity.mCurrentLocation;
						}

						checkExpectedOutsideOfBounds(mCurrentMyPosition, 0f, 0f);

						mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mCurrentMyPosition, CHANGE_ICON_ZOOM));
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			mOutbackListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					try {
						StationInfoData selectedItem = mShowRunningStationList.get(position);

						switch (selectedItem.getmStationType()) {
							case "bus":
								moveToBusStationInfoFragment(selectedItem.getmStationId(), selectedItem.getmArsId(), selectedItem.getmBusRouteArea(), selectedItem.getmStationName());
								break;
							case "shuttle":
								moveToShuttleStationInfoFragment(selectedItem.getmStationId(), selectedItem.getmArsId(), selectedItem.getmBusRouteArea(), selectedItem.getmStationName());
								break;
							case "subway":
								launchDaumWeb(selectedItem.getmStationId());
								break;
							case "carpool":
								moveToCarpoolDetailFragment(selectedItem.getmStationId());
								break;
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			mBtnShowStationList.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						mOutbackStationListLayout.setVisibility(View.VISIBLE);
						getUpdatedStationList();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			mBtnAroundMap.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						mOutbackStationListLayout.setVisibility(View.GONE);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			mGoogleMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
				@Override
				public void onMapLongClick(LatLng latLng) {
					try {
						switch (mLongClickStatus) {
							case "startFindWay":
								finishFindWay(latLng);
								break;
							case "startMeasure":
								calculateMeasure(latLng);
								break;
							default:
								initDistanceObject();
								showLongClickMenu(latLng);
								break;
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			mGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
				@Override
				public void onMapClick(LatLng latLng) {
					try {
						switch (mLongClickStatus) {
							case "startFindWay":
								finishFindWay(latLng);
								break;
							case "startMeasure":
								calculateMeasure(latLng);
								break;
							default:
								initDistanceObject();
								break;
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
				@Override
				public boolean onMarkerClick(Marker marker) {
					try {
						if (marker.getSnippet() == null) {
							return false;
						}

						String marker_snippet = marker.getSnippet();
						String[] marker_snippet_array = marker_snippet.split("_");

						switch (marker_snippet_array[0]) {
							case "findway":
								if (marker_snippet_array[1].equals("start")) {
									mFindWayStartLatLng = marker.getPosition();
									mLongClickStatus = "startFindWay";

									initDistanceObject();

									makeClickPointMarker(R.drawable.ic_start_findpath, mFindWayStartLatLng);
									makeCloseMarker(marker.getPosition(), -0.4f, 2.4f);

									mTVTouchDesc.setText("도착지 설정.");
								}
								break;
							case "measure":
								initDistanceObject();
								mMeasureStartLatLng = marker.getPosition();
								makeClickPointMarker(R.drawable.ic_start_measure, mMeasureStartLatLng);
								makeCloseMarker(marker.getPosition(), -0.4f, 2.4f);
								mLongClickStatus = "startMeasure";

								mTVTouchDesc.setText("거리를 잴 위치 선택");
								break;
							case "close":
								initDistanceObject();
								mLongClickStatus = "wait";

								mTVTouchDesc.setText("길게 누르면 길찾기 / 거리재기 가능");
								break;
							case "bus":
								switch (mLongClickStatus) {
									case "startFindWay":
										finishFindWay(marker.getPosition());
										break;
									case "startMeasure":
										calculateMeasure(marker.getPosition());
										break;
									default:
										moveToBusStationInfoFragment(marker_snippet_array[1], marker_snippet_array[2], marker_snippet_array[3], marker_snippet_array[4]);
										break;
								}
								break;
							case "shuttle":
								switch (mLongClickStatus) {
									case "startFindWay":
										finishFindWay(marker.getPosition());
										break;
									case "startMeasure":
										calculateMeasure(marker.getPosition());
										break;
									default:
										moveToShuttleStationInfoFragment(marker_snippet_array[1], marker_snippet_array[2], marker_snippet_array[3], marker_snippet_array[4]);
										break;
								}
								break;
							case "subway":
								switch (mLongClickStatus) {
									case "startFindWay":
										finishFindWay(marker.getPosition());
										break;
									case "startMeasure":
										calculateMeasure(marker.getPosition());
										break;
									default:
										launchDaumWeb(marker_snippet_array[1]);
										break;
								}
								break;
							case "carpool":
								switch (mLongClickStatus) {
									case "startFindWay":
										finishFindWay(marker.getPosition());
										break;
									case "startMeasure":
										calculateMeasure(marker.getPosition());
										break;
									default:
										moveToCarpoolDetailFragment(marker_snippet_array[1]);
										break;
								}
								break;
						}
					} catch (Exception e) {
						e.printStackTrace();
					}

					return false;
				}
			});

			mGoogleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
				@Override
				public void onCameraChange(CameraPosition cameraPosition) {
					try {
						if (mGoogleMap == null || mIsFinishFragment) {
							return;
						}

						setCurrentLocation();

						float currentZoom = cameraPosition.zoom;

						if (!checkRefreshDistance(mCurrentScreen.getCenter(), mLastCenterPosition, currentZoom, mLastZoom)) {
							setIsUpdateRunningStationList(false);
							return;
						}

						getNearStationList(mCurrentScreen, mIsUpdateRunningStationList, mExpectedOutsideOfBounds);
						mExpectedOutsideOfBounds = false;
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private void getCurrentAddress() {
		new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				String retString = "";
				try {
					retString = mTmapData.convertGpsToAddress(mMainActivity.mCurrentLocation.latitude, mMainActivity.mCurrentLocation.longitude);
				} catch (IOException | ParserConfigurationException | SAXException e) {
					e.printStackTrace();
				}

				return retString;
			}

			@Override
			protected void onPostExecute(String _retString) {
				try {
					if(mIsFinishFragment) {
						return;
					}

					String[] retSplit = _retString.split(" ");
					String convertedAddress = "";

					for (int i = 2; i < retSplit.length; i++) {
						convertedAddress += (retSplit[i] + " ");
					}

					if(mView != null) {
						mDepartFromText.setText(convertedAddress);
						mDepartFromLocation = new LocationData(convertedAddress, mMainActivity.mCurrentLocation);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}.execute();
	}

	class SearchByTextTask extends AsyncTask<Void, Integer, ArrayList<TMapPOIItem>> {
		private String mSearchText;

		public SearchByTextTask(String _searchText) {
			this.mSearchText = _searchText;
		}

		@Override
		protected void onPreExecute() {
			try {
				SVUtil.showProgressDialog(mMainActivity, "검색중...");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		protected ArrayList<TMapPOIItem> doInBackground(Void... params) {
			ArrayList<TMapPOIItem> ret = null;

			try {
				ret = mTmapData.findAllPOI(this.mSearchText);
			} catch (IOException | ParserConfigurationException | SAXException e) {
				e.printStackTrace();
			}

			return ret;
		}

		@Override
		protected void onPostExecute(ArrayList<TMapPOIItem> result) {
			SVUtil.hideProgressDialog();

			try {
				mSearchResultList.clear();
				mSearchResultAdapter.clear();

				if(mIsFinishFragment) {
					return;
				}

				if (result != null) {
					for (TMapPOIItem item : result) {
						mSearchResultList.add(new LocationData(item.getPOIName(), new LatLng(item.getPOIPoint().getLatitude(), item.getPOIPoint().getLongitude())));
						mSearchResultAdapter.add(item.getPOIName());
					}
				} else {
					SVUtil.showSimpleDialog(mMainActivity, "검색 결과가 없습니다.");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void setIsUpdateRunningStationList(boolean _isUpdateRunningStationList) {
		try {
			mIsUpdateRunningStationList = true;

			if(mFragmentFrom.equals("main")) {
				mIsUpdateRunningStationList = _isUpdateRunningStationList;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private boolean checkRefreshDistance(LatLng _currentCenter, LatLng _lastCenter, float _currentZoom, float _lastZoom) {
		try {
			if (_lastCenter != null) {
				float[] dist = new float[2];
				Location.distanceBetween(_currentCenter.latitude, _currentCenter.longitude, _lastCenter.latitude, _lastCenter.longitude, dist);

				if (dist[0] <= Global.UPDATE_DISTANCE && _lastZoom == _currentZoom) {
					return false;
				}
			}

			mLastCenterPosition = _currentCenter;
			mLastZoom = _currentZoom;

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	private void setDisplayMode() {
		try {
			float zoom = mGoogleMap.getCameraPosition().zoom;
			mMarkerDisplayMode = MarkerDisplayMode.ALL;
			if (zoom < CHANGE_ICON_ZOOM) {
				mMarkerDisplayMode = MarkerDisplayMode.SMALL_ICON;

				if (zoom < MIN_ZOOM) {
					mMarkerDisplayMode = MarkerDisplayMode.NOTHING;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void checkExpectedOutsideOfBounds(LatLng _center, float _velocityX, float _velocityY) {
		try {
			mExpectedOutsideOfBounds = false;

			if(!mIsShowOutbackPolygon) {
				return;
			}

			LatLng calculatedCenterPosition = _center;
			if(calculatedCenterPosition == null) {
				calculatedCenterPosition = getExpectedCenterPosition(_velocityX, _velocityY);
			}

			float[] screenSize = SVUtil.getScreenSize(mCurrentScreen);

			LatLngBounds expectedBounds = SVUtil.getExtendedOutbackBounds(screenSize[0] / 2, screenSize[1] / 2, calculatedCenterPosition);

			if(!mOutbackBoundsData.isScreenInMaxOutbackCheckBounds(3, expectedBounds)) {
				mExpectedOutsideOfBounds = true;
				removeOutbackPolygon();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private LatLng getExpectedCenterPosition(float _velocityX, float _velocityY) {
		try {
			Projection projection = mGoogleMap.getProjection();
			LatLng currentCenterPosition = mCurrentScreen.getCenter();

			Point currentCenterPoint = projection.toScreenLocation(currentCenterPosition);
			currentCenterPoint.x = currentCenterPoint.x + Math.round(Math.round((_velocityX * 0.2)) * -1);
			currentCenterPoint.y = currentCenterPoint.y + Math.round(Math.round((_velocityY * 0.2)) * -1);

			return projection.fromScreenLocation(currentCenterPoint);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public void getNearStationList(LatLngBounds _currentScreen, boolean _isUpdateRunningStationList, boolean _isShowLoadingMsg) {
		try {
			mStartTime = System.currentTimeMillis();

			setDisplayMode();

			if(_isUpdateRunningStationList) {
				setIsUpdateRunningStationList(false);
				getUpdatedShuttleLineList();
			}

			if(mMarkerDisplayMode == MarkerDisplayMode.ALL) {
				if(mIsShowOutbackPolygon) {
					if(_isShowLoadingMsg) {
						removeOutbackPolygon();
						SVUtil.showProgressDialog(mMainActivity, "오지정보 로딩중...");
						mOutbackBoundsData.createOutbackCheckBounds(_currentScreen);
						getStationMarkerList(_currentScreen);
					} else if(!mOutbackBoundsData.isScreenInMaxOutbackCheckBounds(3, _currentScreen)) {
						mOutbackBoundsData.createOutbackCheckBounds(_currentScreen);
						getStationMarkerList(_currentScreen);
					}
				} else {
					removeOutbackPolygon();

					changeOutbackButtonBg(false);

					getStationMarkerList(_currentScreen);
				}
			} else if(mMarkerDisplayMode == MarkerDisplayMode.SMALL_ICON) {
				removeOutbackPolygon();

				changeOutbackButtonBg(false);

				removeShuttleLine();

				getStationMarkerList(_currentScreen);
			} else {
				removeOutbackPolygon();

				changeOutbackButtonBg(false);

				removeShuttleLine();

				removeShuttleMarkers();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void getUpdatedStationList() {
		try {
			if(mUpdatedStationListTask != null) {
				mUpdatedStationListTask.cancel(true);
				mUpdatedStationListTask = null;
			}

			mUpdatedStationListTask = new UpdatedStationListTask();
			mUpdatedStationListTask.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private class UpdatedStationListTask extends AsyncTask<Void, Void, String> {
		private boolean mIsCanceled = false;
		private LatLng mCurrentPosition;

		public UpdatedStationListTask() {
			try {
				this.mCurrentPosition = mCurrentMyPosition;
				if(!mFragmentFrom.equals("main")) {
					this.mCurrentPosition = mCurrentScreen.getCenter();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			mRunningStationList.clear();
			mBeaconList.clear();

			mShowRunningStationList.clear();
			mOutbackStationAdapter.notifyDataSetChanged();

			mStartTime = System.currentTimeMillis();

			SVUtil.log("error", TAG, "==================== [update]주변 정류장 조회 시작 ====================");
		}

		@Override
		protected String doInBackground(Void... params) {
			ShuttleLocationData location;
//			StationInfoData shuttle_item;
			int selectedDistance = mDistanceList[mDistanceList.length - 1];

			long startTime = 0;

			try {
				if(!this.mIsCanceled) {
					// 셔틀 리스트 조회
					String url = Global.URL_SHUTTLE_STATION_UPDATED3 + "?lat=" + this.mCurrentPosition.latitude + "&lng=" + this.mCurrentPosition.longitude + "&range=" + selectedDistance;
					SVUtil.log("error", TAG, "==================== [update]주변 정류장 조회  ==================== : " + url);
					startTime = System.currentTimeMillis();
					NetNearShuttleStationsAck retShuttleNetApi = getNearShuttleStationList(url);
					printExecutionTime("==================== [update]주변 정류장 조회 완료  ==================== ", startTime, System.currentTimeMillis());
					startTime = System.currentTimeMillis();

					if (retShuttleNetApi != null) {
						ArrayList<StationInfoData> shuttleMarkerPoints = retShuttleNetApi.getmShuttleMarkerPoints();

						for (StationInfoData shuttle_item : shuttleMarkerPoints) {
							if (mIsFinishFragment || this.mIsCanceled) {
								return null;
							}

							shuttle_item.setmIsVisible(true);
							shuttle_item.setmStationId(shuttle_item.getmArsId());

							mRunningStationList.add(shuttle_item);
						}
					}

					BeaconListAck retBeaconNetApi = getNearBeaconList(this.mCurrentPosition, selectedDistance);
					if(retBeaconNetApi != null){
						ResultData result = retBeaconNetApi.getmResultData();

						if(result.getmCode() == 100){
							ArrayList<BeaconData> retData = retBeaconNetApi.getmBeaconListData();

							int listSize = retData.size();
							for(int i=0; i<listSize; i++){
								if(mIsFinishFragment || this.mIsCanceled){
									return null;
								}

								BeaconData beaconDataItem = retData.get(i);
								mBeaconList.put(beaconDataItem.getmBeaconMac(), beaconDataItem);
								Log.e("UPDATE", "UPDATE BEACON");
							}
						}
					}

					// 카풀 리스트 조회
					ArrayList<CarpoolRegistrantData> retData = retShuttleNetApi.getmCarpoolRegistrantDataList();
					if(retData != null) {
						for(CarpoolRegistrantData carpoolRegistrantDataItem : retData) {
							if (mIsFinishFragment || this.mIsCanceled) {
								return null;
							}

							float[] dist = new float[2];
							Location.distanceBetween(this.mCurrentPosition.latitude, this.mCurrentPosition.longitude, carpoolRegistrantDataItem.getmLocation().getmCoordinates().get(1),
									carpoolRegistrantDataItem.getmLocation().getmCoordinates().get(0), dist);

							mRunningStationList.add(new StationInfoData(carpoolRegistrantDataItem.getmTaxiId(), "", "", dist[0], 0, "", "", carpoolRegistrantDataItem.getmLocation(),
									"등록자 " + carpoolRegistrantDataItem.getmRegistrant() + " 기사님", 0, "carpool", true, carpoolRegistrantDataItem.getmTaxiId()));
						}
					}

					// 버스 정류장 리스트 조회
					ArrayList<BusStationListData> retBusData = retShuttleNetApi.getmBusStationDataList();

					if(retBusData != null) {
						for(BusStationListData busStationListItem : retBusData) {
							if (mIsFinishFragment || this.mIsCanceled) {
								return null;
							}

							location = new ShuttleLocationData();
							location.mCoordinates = new ArrayList<>();
							location.mCoordinates.add(busStationListItem.getmGpsX());
							location.mCoordinates.add(busStationListItem.getmGpsY());

							mRunningStationList.add(new StationInfoData(busStationListItem.getmArsId(), "", busStationListItem.getmBusRouteArea(), busStationListItem.getmDistance(), 0, busStationListItem.getmStartTime(),
									busStationListItem.getmEndTime(), location, busStationListItem.getmStationName(), 0, "bus", true, busStationListItem.getmStationId() + ""));
						}
					}

					// 지하철 정류장 조회
					ArrayList<SubwayStationListData> retSubwayData = retShuttleNetApi.getmSubwayStationDataList();

					if(retShuttleNetApi.getmSubwayStationDataList() != null) {
						for(SubwayStationListData subwayStationListItem : retSubwayData) {
							if (mIsFinishFragment || this.mIsCanceled) {
								return null;
							}

							if(subwayStationListItem.getmStationCodeDaum() != null && !subwayStationListItem.getmStationCodeDaum().isEmpty()) {
								subwayStationListItem.setmStationCode(subwayStationListItem.getmStationCodeDaum());
							}

							mRunningStationList.add(new StationInfoData(subwayStationListItem.getmStationCode(), "", "서울", subwayStationListItem.getmDistance(), 0, subwayStationListItem.getmFirstTime(),
									subwayStationListItem.getmLastTime(), subwayStationListItem.getmLocation(), subwayStationListItem.getmStationName(), 0, "subway", true, subwayStationListItem.getmStationCode()));
						}
					}

					printExecutionTime("==================== [update]주변 정류장 파싱 완료  ==================== ", startTime, System.currentTimeMillis());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onCancelled() {
			this.mIsCanceled = true;
		}

		@Override
		protected void onPostExecute(String s) {
			super.onPostExecute(s);

			try {
				if(!this.mIsCanceled && !mIsFinishFragment) {
					showNearStationList();

					if (mShowRunningStationList.size() < 1) {
						int selectedDistance = mDistanceList[mCurrentDistance];
						SVUtil.showSimpleDialog(mMainActivity, (selectedDistance / 1000) + "km 반경 내에 정류장 없음");
					}
				} else {
					SVUtil.hideProgressDialog();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			printExecutionTime("==================== [update]주변 정류장 조회 완료  ==================== ", mStartTime, System.currentTimeMillis());
		}
	}

	private void getUpdatedShuttleLineList() {
		try {
			if(mUpdateShuttleLineListTask != null) {
				mUpdateShuttleLineListTask.cancel(true);
				mUpdateShuttleLineListTask = null;
			}

			mUpdateShuttleLineListTask = new UpdateShuttleLineListTask();
			mUpdateShuttleLineListTask.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private class UpdateShuttleLineListTask extends AsyncTask<Void, Void, String> {
		private boolean mIsCanceled = false;
		private LatLng mCurrentPosition;

		public UpdateShuttleLineListTask() {
			try {
				this.mCurrentPosition = mCurrentMyPosition;
				if(!mFragmentFrom.equals("main")) {
					this.mCurrentPosition = mCurrentScreen.getCenter();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			mRunningStationList.clear();
			mShuttleLineColorList.clear();
			mBeaconList.clear();

			mStartTime = System.currentTimeMillis();

			SVUtil.log("error", TAG, "==================== [update]셔틀 노선 조회 시작 ====================");
		}

		@Override
		protected String doInBackground(Void... params) {
			int selectedDistance = mDistanceList[mDistanceList.length - 1];

			long startTime = 0;

			try {
				if(!this.mIsCanceled) {
					// 셔틀 리스트 조회
					String url = Global.URL_SHUTTLE_STATION_UPDATED2 + "?lat=" + this.mCurrentPosition.latitude + "&lng=" + this.mCurrentPosition.longitude + "&range=" + selectedDistance;
					SVUtil.log("error", TAG, "==================== [update]셔틀 노선 조회  ==================== : " + url);
					startTime = System.currentTimeMillis();
					NetNearShuttleStationsAck retShuttleNetApi = getNearShuttleStationList(url);
					printExecutionTime("==================== [update]셔틀 노선 조회 완료  ==================== ", startTime, System.currentTimeMillis());
					startTime = System.currentTimeMillis();

					if (retShuttleNetApi != null) {
						mShuttleLineColorList.addAll((ArrayList<ShuttleLineColorData>)retShuttleNetApi.getmShuttleLineColorData().clone());
					}
					printExecutionTime("==================== [update]셔틀 노선 파싱 완료  ==================== ", startTime, System.currentTimeMillis());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onCancelled() {
			this.mIsCanceled = true;
		}

		@Override
		protected void onPostExecute(String s) {
			super.onPostExecute(s);

			try {
				if(!this.mIsCanceled && !mIsFinishFragment) {
					if(mShuttleLineColorList.size() > 0) {
						mBtnShowShuttleLineColor.setVisibility(View.VISIBLE);
					} else {
						mBtnShowShuttleLineColor.setVisibility(View.GONE);
					}

					mShuttleLineColorAdapter.notifyDataSetChanged();
				} else {
					SVUtil.hideProgressDialog();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			printExecutionTime("==================== [update]셔틀 노선 조회 완료  ==================== ", mStartTime, System.currentTimeMillis());
		}
	}

	private void getStationMarkerList(LatLngBounds _currentScreen) {
		try {
			if(mSearchNearStationListTask != null) {
				mSearchNearStationListTask.cancel(true);
				mSearchNearStationListTask = null;
			}

			if(mShowOutbackPolygonTask != null) {
				mShowOutbackPolygonTask.cancel(true);
				mShowOutbackPolygonTask = null;
			}

			mSearchNearStationListTask = new NearStationListTask(_currentScreen, mIsShowOutbackPolygon);
			mSearchNearStationListTask.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private class NearStationListTask extends AsyncTask<Void, Void, String> {
		private boolean mIsCanceled = false;
		private boolean mIsShowOutbackPolygon;

		private ArrayList<ShuttleLineData> mStationLine;
		private ArrayList<ShuttleLineColorData> mStationLineColorData;
		private ArrayList<StationInfoData> mShuttleMarkerDataList;
		private LatLngBounds mCurrentScreen;

		public NearStationListTask(LatLngBounds _currentScreen, boolean _isShowOutbackPolygon) {
			this.mIsShowOutbackPolygon = _isShowOutbackPolygon;
			this.mCurrentScreen = _currentScreen;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			this.mStationLine = new ArrayList<>();
			this.mShuttleMarkerDataList = new ArrayList<>();
			this.mStationLineColorData = new ArrayList<>();
			mBeaconList.clear();

			mStartTime = System.currentTimeMillis();

			SVUtil.log("error", TAG, "==================== 주변 정류장 조회 시작 ====================");
		}

		@Override
		protected String doInBackground(Void... params) {
			ShuttleLocationData location;
			StationInfoData shuttle_item;

			float searchRange = mSearchRadius;

			if(this.mIsShowOutbackPolygon) {
				searchRange = mOutbackBoundsData.getMaxSearchRange();
			}

			try {
				if(!this.mIsCanceled) {
					long startTime = System.currentTimeMillis();
					String url = Global.URL_SHUTTLE_STATION2 + "?lat=" + this.mCurrentScreen.getCenter().latitude + "&lng=" + this.mCurrentScreen.getCenter().longitude + "&range=" + searchRange;
					SVUtil.log("error", TAG, "==================== 셔틀 리스트 조회  ==================== : " + url);
					NetNearShuttleStationsAck retShuttleNetApi = getNearShuttleStationList(url);
					printExecutionTime("==================== 셔틀 리스트 조회 완료  ==================== ", startTime, System.currentTimeMillis());
					startTime = System.currentTimeMillis();

					mPublicTransitList = new ArrayList<>();
					if (retShuttleNetApi != null) {
						boolean isShowShuttlePosition = false;

						// 셔틀 노선 색 리스트 조회
						if(retShuttleNetApi.getmShuttleLineColorData() != null) {
							for(ShuttleLineColorData shuttleLineColorData : retShuttleNetApi.getmShuttleLineColorData()) {
								if (mIsFinishFragment || this.mIsCanceled) {
									return null;
								}

								this.mStationLineColorData.add(shuttleLineColorData);

								if(shuttleLineColorData.getmRealTimeCategoryName() != null && !shuttleLineColorData.getmRealTimeCategoryName().isEmpty()) {
									isShowShuttlePosition = true;
								}
							}
						}

						mIsShowRealTimeShuttlePosition = isShowShuttlePosition;

						// 셔틀 노선 리스트 조회
						ArrayList<ShuttleLineData> shuttleLinePoints = retShuttleNetApi.getmShuttleLinePoints();
						if(shuttleLinePoints != null) {
							for(ShuttleLineData shuttleLineDataItem : shuttleLinePoints) {
								this.mStationLine.add(shuttleLineDataItem);

								for(StationInfoData s : shuttleLineDataItem.getmShuttleLinePointDataList()) {
									if(s.getmStationName().equals("__point")) {
										continue;
									}

									if(s.getmExceptOutback() == 0) {
										this.mShuttleMarkerDataList.add(s);
									}

									if(this.mIsShowOutbackPolygon && s.ismIsVisible()) {
										mOutbackBoundsData.addToOutbackBoundsAreaList(s.getPosition());
									}
								}
							}
						}

						// 카풀 리스트 조회
						mDriverList = new ArrayList<>();

						if(retShuttleNetApi.getmCarpoolRegistrantDataList() != null) {
							for(CarpoolRegistrantData carpoolRegistrantDataItem : retShuttleNetApi.getmCarpoolRegistrantDataList()) {
								if (mIsFinishFragment || this.mIsCanceled) {
									return null;
								}

								float[] dist = new float[2];
								Location.distanceBetween(this.mCurrentScreen.getCenter().latitude, this.mCurrentScreen.getCenter().longitude, carpoolRegistrantDataItem.getmLocation().getmCoordinates().get(1),
										carpoolRegistrantDataItem.getmLocation().getmCoordinates().get(0), dist);

								if(dist[0] <= searchRange) {
									shuttle_item = new StationInfoData(carpoolRegistrantDataItem.getmTaxiId(), "", "", dist[0], 0, "", "", carpoolRegistrantDataItem.getmLocation(),
											"등록자 " + carpoolRegistrantDataItem.getmRegistrant() + " 기사님", 0, "carpool", true, carpoolRegistrantDataItem.getmTaxiId());

									this.mShuttleMarkerDataList.add(shuttle_item);

									if(this.mIsShowOutbackPolygon) {
										mOutbackBoundsData.addToOutbackBoundsAreaList(shuttle_item.getPosition());
									}
								}
							}
						}

						// 주변 기사 리스트 조회
						ArrayList<DriverPointData> nearDriverList = retShuttleNetApi.getmNearDriverList();
						if(nearDriverList != null) {
							for(DriverPointData driverData : nearDriverList) {
								if(driverData.getmPhoneNumber().equals(mMainActivity.mUserPhoneNumber)) {
									continue;
								}

								float[] dist = new float[2];
								Location.distanceBetween(this.mCurrentScreen.getCenter().latitude, this.mCurrentScreen.getCenter().longitude, driverData.getmLocation().getmCoordinates().get(1),
										driverData.getmLocation().getmCoordinates().get(0), dist);

								if(dist[0] <= searchRange) {
									String registrant = driverData.getmPhoneNumber().substring(driverData.getmPhoneNumber().length() - 4, driverData.getmPhoneNumber().length());
									shuttle_item = new StationInfoData(driverData.getmDriverId(), "", "", dist[0], 0, "", "", driverData.getmLocation(), registrant + " 기사님", 0, "driver", true, driverData.getmDriverId());

									mDriverList.add(shuttle_item);
								}
							}
						}

						// 버스 정류장 리스트 조회
						ArrayList<BusStationListData> retBusData = retShuttleNetApi.getmBusStationDataList();

						if(retBusData != null) {
							for(BusStationListData busStationListItem : retBusData) {
								if (mIsFinishFragment || this.mIsCanceled) {
									return null;
								}

								location = new ShuttleLocationData();
								location.mCoordinates = new ArrayList<>();
								location.mCoordinates.add(busStationListItem.getmGpsX());
								location.mCoordinates.add(busStationListItem.getmGpsY());

								shuttle_item = new StationInfoData(busStationListItem.getmArsId(), "", busStationListItem.getmBusRouteArea(), busStationListItem.getmDistance(), 0, busStationListItem.getmStartTime(),
										busStationListItem.getmEndTime(), location, busStationListItem.getmStationName(), 0, "bus", true, busStationListItem.getmStationId() + "");

								mPublicTransitList.add(shuttle_item);

								if(this.mIsShowOutbackPolygon) {
									mOutbackBoundsData.addToOutbackBoundsAreaList(shuttle_item.getPosition());
								}
							}
						}

						// 지하철 정류장 조회
						ArrayList<SubwayStationListData> retSubwayData = retShuttleNetApi.getmSubwayStationDataList();

						if(retSubwayData != null) {
							for(SubwayStationListData subwayStationListItem : retSubwayData) {
								if (mIsFinishFragment || this.mIsCanceled) {
									return null;
								}

								if(subwayStationListItem.getmStationCodeDaum() != null && !subwayStationListItem.getmStationCodeDaum().isEmpty()) {
									subwayStationListItem.setmStationCode(subwayStationListItem.getmStationCodeDaum());
								}

								shuttle_item = new StationInfoData(subwayStationListItem.getmStationCode(), "", "서울", subwayStationListItem.getmDistance(), 0, subwayStationListItem.getmFirstTime(),
										subwayStationListItem.getmLastTime(), subwayStationListItem.getmLocation(), subwayStationListItem.getmStationName(), 0, "subway", true, subwayStationListItem.getmStationCode());

								mPublicTransitList.add(shuttle_item);

								if(this.mIsShowOutbackPolygon) {
									mOutbackBoundsData.addToOutbackBoundsAreaList(shuttle_item.getPosition());
								}
							}
						}
					}

					BeaconListAck retBeaconNetApi = getNearBeaconList(this.mCurrentScreen.getCenter(), searchRange);
					printExecutionTime(TAG + " NearStationListTask (get data from server beacon List) ", startTime, System.currentTimeMillis());
					if(retBeaconNetApi != null){
						ResultData result = retBeaconNetApi.getmResultData();
						if(result.getmCode() == 100){
							ArrayList<BeaconData> retData = retBeaconNetApi.getmBeaconListData();
							int listSize = retData.size();
							for(int i=0; i<listSize; i++){
								if(mIsFinishFragment || this.mIsCanceled){
									return null;
								}
								mBeaconList.put(retData.get(i).getmBeaconMac(), retData.get(i));
								Log.e("UPDATE", "NEAR Station BEACON");
							}
						}
					}
					printExecutionTime("==================== 셔틀 리스트 파싱 완료  ==================== ", startTime, System.currentTimeMillis());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onCancelled() {
			this.mIsCanceled = true;
		}

		@Override
		protected void onPostExecute(String s) {
			super.onPostExecute(s);

			try {
				if(!this.mIsCanceled && !mIsFinishFragment) {
					removeShuttleMarkers();

					long startTime = System.currentTimeMillis();
					int listSize = this.mShuttleMarkerDataList.size();
					for(int i = 0; i < listSize; i++) {
						addOneShuttleStationMarker(this.mShuttleMarkerDataList.get(i));
					}
					printExecutionTime("==================== 셔틀 정류장 표시 완료  ==================== ", startTime, System.currentTimeMillis());

					startTime = System.currentTimeMillis();
					drawBeaconPosition(new ArrayList<>(mBeaconList.values()));
					printExecutionTime("==================== 비콘 표시 완료  ==================== ", startTime, System.currentTimeMillis());

					startTime = System.currentTimeMillis();
					if(mIsShowDriver) {
						drawDriverMarker(mDriverList);
					}
					printExecutionTime("==================== 기사 위치 표시 완료  ==================== ", startTime, System.currentTimeMillis());

					startTime = System.currentTimeMillis();
					if(mIsShowPublicTransit) {
						drawPublicTransitMarker(mPublicTransitList);
					}
					printExecutionTime("==================== 대중교통 정류장 표시 완료  ==================== ", startTime, System.currentTimeMillis());

					startTime = System.currentTimeMillis();
					if (mMarkerDisplayMode == MarkerDisplayMode.ALL) {
						drawShuttleStationPolyline(this.mStationLine, this.mStationLineColorData);
					}
					printExecutionTime("==================== 셔틀 노선 표시 완료  ==================== ", startTime, System.currentTimeMillis());

					if(mIsShowRealTimeShuttlePosition) {
						if(!mIsRetrievingShuttlePosition) {
							getRealTimeShuttlePosition("onPostExecute()");
						}
					} else {
						mIsRetrievingShuttlePosition = false;
						removeRealTimeMarker();
					}

					finishLoading();
				} else {
					SVUtil.hideProgressDialog();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			printExecutionTime("==================== 주변 정류장 조회 완료  ==================== ", mStartTime, System.currentTimeMillis());
		}
	}

	private void removeRealTimeMarker() {
		try {
			int realTimeMarkerSize = mShuttleRealTimeMarkers.size();
			for(int i = 0; i < realTimeMarkerSize; i++) {
				mShuttleRealTimeMarkers.get(i).remove();
			}

			mShuttleRealTimeMarkers.clear();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void removeRealTimeBeaconMarker() {
		try {
			int realTimeMarkerSize = mShuttleRealTimeBeaconMarkers.size();
			for(int i = 0; i < realTimeMarkerSize; i++) {
				mShuttleRealTimeBeaconMarkers.get(i).remove();
			}

			mShuttleRealTimeBeaconMarkers.clear();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void drawBeaconPosition(ArrayList<BeaconData> list){

		IconGenerator iconFactory = new IconGenerator(mMainActivity);
		LatLng shuttle_position;
		removeRealTimeBeaconMarker();
		int listSize = list.size();
		for(int i=0; i<listSize; i++){
			shuttle_position = new LatLng(list.get(i).getLocation().getmCoordinates().get(1), list.get(i).getLocation().getmCoordinates().get(0));
			addRealTimeBeaconMarker(iconFactory, list.get(i).getmText() + "", shuttle_position, "beacon_position");
		}
	}

	private void getRealTimeShuttlePosition(String _from) {
		try {
			removeRealTimeMarker();

			if(!mIsShowRealTimeShuttlePosition) {
				mIsRetrievingShuttlePosition = false;
				return;
			}

			mIsRetrievingShuttlePosition = true;

			NetClient.send(mMainActivity, Global.URL_SHUTTLE_REAL_TIME, "GET", null, new ShuttleRealTimeDataAck(), new NetResponseCallback(new NetResponse() {
				@Override
				public void onResponse(NetAPI _netAPI) {
					try {
						SVUtil.hideProgressDialog();

						if (_netAPI != null && !mIsFinishFragment) {
							ShuttleRealTimeDataAck retNetApi = (ShuttleRealTimeDataAck) _netAPI;
							ResultData result = retNetApi.getmResultData();
							if (result.getmCode() == 100) {
								ArrayList<ShuttleRealTimeData> retData = retNetApi.getmShuttleRealTimeData();

								IconGenerator iconFactory = new IconGenerator(mMainActivity);
								LatLng shuttle_position;
								for (ShuttleRealTimeData item : retData) {
									shuttle_position = new LatLng(item.getmShuttleLocationData().getmCoordinates().get(1), item.getmShuttleLocationData().getmCoordinates().get(0));
									addRealTimeShuttleMarker(iconFactory, item.getmRealTimeCategoryName(), shuttle_position, "shuttle_position");
								}
							}

							RealTimeShuttleHandler.sendEmptyMessageDelayed(MSG_2, Global.SHUTTLE_POSITION_REFRESH_TIME);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private Handler RealTimeShuttleHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			try {
				switch (msg.what) {
					case MSG_2 :
						getRealTimeShuttlePosition("RealTimeShuttleHandler");
						break;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};

	private void addRealTimeShuttleMarker(IconGenerator _iconFactory, String _title, LatLng _position, String _snippet) {
		try {
			MarkerOptions markerOptions = new MarkerOptions().icon(BitmapDescriptorFactory.fromBitmap(_iconFactory.makeIcon(_title))).position(_position).anchor(_iconFactory.getAnchorU(), 1.2f).snippet("");

			mShuttleRealTimeMarkers.add(mGoogleMap.addMarker(new MarkerOptions().position(_position).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_realtime_shuttle)).snippet(_snippet).anchor(0.5f, 0.5f)));
			mShuttleRealTimeMarkers.add(mGoogleMap.addMarker(markerOptions));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void addRealTimeBeaconMarker(IconGenerator _iconFactory, String _title, LatLng _position, String _snippet) {
		try {
			MarkerOptions markerOptions = new MarkerOptions().icon(BitmapDescriptorFactory.fromBitmap(_iconFactory.makeIcon(_title))).position(_position).anchor(_iconFactory.getAnchorU(), 1.2f).snippet("");
			mShuttleRealTimeBeaconMarkers.add(mGoogleMap.addMarker(new MarkerOptions().position(_position).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_realtime_shuttle)).snippet(_snippet).anchor(0.5f, 0.5f)));
			mShuttleRealTimeBeaconMarkers.add(mGoogleMap.addMarker(markerOptions));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private NetNearShuttleStationsAck getNearShuttleStationList(String _url) {
		NetNearShuttleStationsAck retShuttleNetApi = null;
		Reader reader = null;
		JsonParser jp = null;

		try {
			HttpRequest request = HttpRequest.get(_url);
//			SVUtil.log("error", TAG, "shuttle station url : " + _url);
			request.acceptGzipEncoding().uncompress(true);
			reader = request.reader();
			ObjectMapper mapper = new ObjectMapper();

			jp = mapper.getFactory().createParser(reader);
			retShuttleNetApi = mapper.readValue(jp, NetNearShuttleStationsAck.class);

			reader.close();
			reader = null;
			jp.close();
			jp = null;
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				if(reader != null) {
					reader.close();
				}

				if(jp != null && !jp.isClosed()) {
					jp.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return retShuttleNetApi;
	}

	private BeaconListAck getNearBeaconList(LatLng _center, float _maxRange) {
		BeaconListAck retBeaconNetApi = null;
		Reader reader = null;
		JsonParser jp = null;

		try {
			String url = Global.URL_BEACON_REGISTRANT_LIST + "?lat=" + _center.latitude + "&lng=" + _center.longitude + "&range=" + _maxRange;
//			SVUtil.log("error", TAG, "carpool registrant url : " + url);
			HttpRequest request = HttpRequest.get(url);
			request.acceptGzipEncoding().uncompress(true);
			reader = request.reader();
			ObjectMapper mapper = new ObjectMapper();

			jp = mapper.getFactory().createParser(reader);
			retBeaconNetApi = mapper.readValue(jp, BeaconListAck.class);

			reader.close();
			reader = null;
			jp.close();
			jp = null;
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				if(reader != null) {
					reader.close();
				}

				if(jp != null && !jp.isClosed()) {
					jp.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return retBeaconNetApi;
	}

	private CarpoolRegistrantListAck getNearCarpoolList(LatLng _center, float _maxRange) {
		CarpoolRegistrantListAck retCarpoolNetApi = null;
		Reader reader = null;
		JsonParser jp = null;

		try {
			String url = Global.URL_CARPOOL_REGISTRANT_LIST + "?lat=" + _center.latitude + "&lng=" + _center.longitude + "&range=" + _maxRange;
//			SVUtil.log("error", TAG, "carpool registrant url : " + url);
			HttpRequest request = HttpRequest.get(url);
			request.acceptGzipEncoding().uncompress(true);
			reader = request.reader();
			ObjectMapper mapper = new ObjectMapper();

			jp = mapper.getFactory().createParser(reader);
			retCarpoolNetApi = mapper.readValue(jp, CarpoolRegistrantListAck.class);

			reader.close();
			reader = null;
			jp.close();
			jp = null;
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				if(reader != null) {
					reader.close();
				}

				if(jp != null && !jp.isClosed()) {
					jp.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return retCarpoolNetApi;
	}

	private PublicTransitStationListAck getNearBusStationList(LatLng _center, float _maxRange) {
		PublicTransitStationListAck retBusStationListNetApi = null;
		Reader reader = null;
		JsonParser jp = null;

		try {
			String url = Global.URL_PUBLIC_TRANSIT_LIST + "?lat=" + _center.latitude + "&lng=" + _center.longitude + "&range=" + _maxRange;
//			SVUtil.log("error", TAG, "bus station registrant url : " + url);
			HttpRequest request = HttpRequest.get(url);
			request.acceptGzipEncoding().uncompress(true);
			reader = request.reader();
			ObjectMapper mapper = new ObjectMapper();

			jp = mapper.getFactory().createParser(reader);
			retBusStationListNetApi = mapper.readValue(jp, PublicTransitStationListAck.class);
			reader.close();
			reader = null;
			jp.close();
			jp = null;
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				if(reader != null) {
					reader.close();
				}

				if(jp != null && !jp.isClosed()) {
					jp.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return retBusStationListNetApi;
	}

	private void finishLoading() {
		try {
//		SVUtil.log("error", TAG, "finishLoading() station count : " + mRunningStationList.size() + ", marker count : " + mShuttleStationMarkers.size());

			if(mShowOutbackPolygonTask != null) {
				mShowOutbackPolygonTask.cancel(true);
				mShowOutbackPolygonTask = null;
			}

			if (mIsShowOutbackPolygon) {
				mShowOutbackPolygonTask = new ShowOutbackPolygonTask();
				mShowOutbackPolygonTask.execute();
			} else {
				SVUtil.hideProgressDialog();
				removeOutbackPolygon();
			}

			if(!mMainActivity.mIsSplashRemoved) {
				mMainActivity.mIsSplashRemoved = true;
				mMainActivity.finishFirstLoading();
			}

//			printExecutionTime("finishLoading() OutbackFragment_new total execution time : ", System.currentTimeMillis());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void printExecutionTime(String _tag, long _startTime, long _endTime) {
		try {
			long execTime = _endTime - _startTime;
			SVUtil.log("error", TAG, _tag + " execution time : " + (execTime) + "ms");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private class ShowOutbackPolygonTask extends AsyncTask<Void, Void, ArrayList<ArrayList<LatLng>>> {
		private boolean mIsCanceled = false;
		private boolean mIsShowPolygon;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			this.mIsCanceled = false;
			this.mIsShowPolygon = false;
		}

		@Override
		protected ArrayList<ArrayList<LatLng>> doInBackground(Void... params) {
			ArrayList<ArrayList<LatLng>> holes = new ArrayList<>();

			try {
				if(mIsFinishFragment || this.mIsCanceled) {
					return null;
				}

				this.mIsShowPolygon = mOutbackBoundsData.isShowOutbackPolygon();
				if(this.mIsShowPolygon) {
					holes = createPolygons();
				}
			} catch(Exception e) {
				e.printStackTrace();
			}

			return holes;
		}

		@Override
		protected void onCancelled() {
			super.onCancelled();

			this.mIsCanceled = true;
		}

		@Override
		protected void onPostExecute(ArrayList<ArrayList<LatLng>> _holes) {
			super.onPostExecute(_holes);

			try {
				if(mIsFinishFragment || this.mIsCanceled) {
					return;
				}

				removeOutbackPolygon();

				if(this.mIsShowPolygon) {
					drawBackgroundPolygon(_holes);
				}

				SVUtil.hideProgressDialog();

//				printExecutionTime("ShowOutbackPolygonTask OutbackFragment_new total execution time : ", System.currentTimeMillis());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void removeOutbackPolygon() {
		try {
			if(mOutbackPolygon != null) {
				mOutbackPolygon.remove();
				mOutbackPolygon = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void drawBackgroundPolygon(ArrayList<ArrayList<LatLng>> _holes) {
		try {
			if(mGoogleMap != null && !mIsFinishFragment) {
				PolygonOptions options = new PolygonOptions();
				options.add(LIMIT_BOUNDS.northeast);
				options.add(new LatLng(LIMIT_BOUNDS.southwest.latitude, LIMIT_BOUNDS.northeast.longitude));
				options.add(LIMIT_BOUNDS.southwest);
				options.add(new LatLng(LIMIT_BOUNDS.northeast.latitude, LIMIT_BOUNDS.southwest.longitude));

				options.strokeColor(Color.rgb(30, 30, 30));
				options.strokeWidth(0);
				options.fillColor(Color.argb(120, 255, 0, 0));

				if(_holes != null && _holes.size() > 0) {
					ArrayList<LatLng> hole;
					int holeSize = _holes.size();
					for(int i = 0; i < holeSize; i++) {
						hole = _holes.get(i);
						if(hole != null && hole.size() > 0) {
							options.addHole(hole);
						}
					}
				}

				mOutbackPolygon = mGoogleMap.addPolygon(options);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	private ArrayList<ArrayList<LatLng>> createPolygons() {
		com.vividsolutions.jts.geom.Polygon polygon;
		ArrayList<LatLng> outer;
		ArrayList<ArrayList<LatLng>> holes = new ArrayList<>();
		Coordinate[] outerCoordinates;
		Coordinate[] innerCoordinates;

		try {
			Geometry unionPoly = mOutbackBoundsData.getPolygonsByStation(HOLE_RADIUS, HOLE_POINTS);
			if(unionPoly == null) {
				return null;
			}

			if(unionPoly instanceof com.vividsolutions.jts.geom.Polygon) {
				outer = new ArrayList<>();
				polygon = (com.vividsolutions.jts.geom.Polygon)unionPoly;
				outerCoordinates = polygon.getExteriorRing().reverse().getCoordinates();
				for (Coordinate outerCoordinate : outerCoordinates){
					outer.add(new LatLng(outerCoordinate.y, outerCoordinate.x));
				}

				int interiorRingCount = polygon.getNumInteriorRing();
				if(interiorRingCount > 0) {
					for(int i = 0; i < interiorRingCount; i++) {
						innerCoordinates = polygon.getInteriorRingN(i).reverse().getCoordinates();
						for (Coordinate innerCoordinate : innerCoordinates){
							outer.add(new LatLng(innerCoordinate.y, innerCoordinate.x));
						}
					}
				}

				holes.add(outer);
			} else if(unionPoly instanceof MultiPolygon) {
				int geoCount = unionPoly.getNumGeometries();
				for(int i = 0; i < geoCount; i++) {
					outer = new ArrayList<>();
					polygon = (com.vividsolutions.jts.geom.Polygon) unionPoly.getGeometryN(i);
					outerCoordinates = polygon.getExteriorRing().reverse().getCoordinates();
					for (Coordinate outerCoordinate : outerCoordinates) {
						outer.add(new LatLng(outerCoordinate.y, outerCoordinate.x));
					}

					int interiorRingCount = polygon.getNumInteriorRing();
					if(interiorRingCount > 0) {
						for(int j = 0; j < interiorRingCount; j++) {
							innerCoordinates = polygon.getInteriorRingN(i).reverse().getCoordinates();
							for (Coordinate innerCoordinate : innerCoordinates){
								outer.add(new LatLng(innerCoordinate.y, innerCoordinate.x));
							}
						}
					}

					holes.add(outer);
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
			holes = null;
		}

		return holes;
	}

//	private LatLngBounds getExtendScreen(LatLng _center, double _extend_range) {
//		LatLngBounds extendsScreen;
//		ArrayList<LatLng> extendsCoordinates = SVUtil.getCircleCoordinatesLatLng(_center, _extend_range, 4);
//		extendsScreen = new LatLngBounds(new LatLng(extendsCoordinates.get(3).latitude, extendsCoordinates.get(2).longitude),
//				new LatLng(extendsCoordinates.get(1).latitude, extendsCoordinates.get(0).longitude));
//
//		return extendsScreen;
//	}

	private boolean isValidShuttleTime() {
		boolean isValidTime = false;

		try {
			Calendar rightNow = Calendar.getInstance();

			int hours = rightNow.get(Calendar.HOUR_OF_DAY);
			int minutes = rightNow.get(Calendar.MINUTE);

			if(hours >= 0 && hours < 4) {
				isValidTime = true;
			} else if(hours == 23 && minutes >= 30) {
				isValidTime = true;
			} else if(hours == 4 && minutes <= 38) {
				isValidTime = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

//		return true;
		return isValidTime;
	}

	private String getShuttleLineColorByBusId(ArrayList<ShuttleLineColorData> _shuttleLineColor, String _busId) {
		try {
			for (ShuttleLineColorData lineColor : _shuttleLineColor) {
				if(lineColor.getmBusId().equals(_busId)) {
					return lineColor.getmLineColor();
				}
			}

//			SVUtil.log("error", TAG, "busId : " + _busId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	private void drawShuttleStationPolyline(ArrayList<ShuttleLineData> _stationLine, ArrayList<ShuttleLineColorData> _shuttleLineColor) {
		try {
			removeShuttleLine();

			int listSize = _stationLine.size();
			for(int j = 0; j < listSize; j++) {
				ShuttleLineData lineDataItem = _stationLine.get(j);
				ArrayList<StationInfoData> infoItemList = lineDataItem.getmShuttleLinePointDataList();

				Collections.sort(infoItemList, stationSeqSort);

				String color = getShuttleLineColorByBusId(_shuttleLineColor, lineDataItem.getmShuttleLineIdData().getmBusId());

				StationInfoData prevStation = null;
				int infoListSize = infoItemList.size();
				StationInfoData item;
				PolylineOptions opt = null;

				for(int i = 0; i < infoListSize; i++) {
					if(mGoogleMap == null) {
						return;
					}

					if(opt == null) {
						opt = new PolylineOptions();
					}

					item = infoItemList.get(i);

					if((prevStation != null && ((prevStation.getmStationSeq() + 1) != item.getmStationSeq())) || (i == (infoListSize - 1))) {
						opt.width(10);

//						SVUtil.log("error", TAG, "color : " + color);
						opt.color(Color.parseColor(color));

						mShuttleLineList.add(mGoogleMap.addPolyline(opt));

						opt = null;
					} else {
						opt.add(item.getPosition());
					}

					if(prevStation != null && !prevStation.getmStationName().equals("__point") && item.getmStationName().equals("__point")) {
						addShuttleLineArrowMarker(item.getmBusId(), item.getPosition(), SVUtil.getRotationDegrees(prevStation.getPosition(), item.getPosition()), color);
					}

					prevStation = item;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void addShuttleLineArrowMarker(String _busId, LatLng _position, float _rotateDegree, String _color) {
		try {
			BitmapDescriptor markerIcon = (BitmapDescriptor) mShuttleColorArrowBitmapDescriptor.opt(_busId);
			if(markerIcon == null) {
				markerIcon= SVUtil.createShuttleArrowBitmap(50, 50, _color);
				mShuttleColorArrowBitmapDescriptor.put(_busId, markerIcon);
			}

			mArrowMarkers.add(mGoogleMap.addMarker(new MarkerOptions().position(_position).icon(markerIcon).anchor(0.5f, 0.5f).rotation(_rotateDegree).flat(true)));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void addOneShuttleStationMarker(StationInfoData _item) {
		try {
			String snippet = _item.getmStationType() + "_" + _item.getmStationId() + "_" + _item.getmArsId() + "_" + _item.getmBusRouteArea() + "_" + _item.getmStationName();
			BitmapDescriptor markerIcon = getStationMarkerIconByStationType(_item.getmStationType(), _item.ismIsVisible());

			if(markerIcon != null) {
				mShuttleStationMarkers.add(mGoogleMap.addMarker(new MarkerOptions().position(_item.getPosition()).snippet(snippet).icon(markerIcon).anchor(0.5f, 1.0f)));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private BitmapDescriptor getStationMarkerIconByStationType(String _stationType, boolean _isVisible) {
		BitmapDescriptor markerDefaultIconDescriptor = null;

		try {
			switch (_stationType) {
				case "bus" :
					if(mMarkerDisplayMode == MarkerDisplayMode.ALL) {
						if(_isVisible) {
							markerDefaultIconDescriptor = SVUtil.getMarkerIcon(getResources(), R.drawable.pin_bus_40);
						} else {
							markerDefaultIconDescriptor = SVUtil.getMarkerIcon(getResources(), R.drawable.pin_bus_40_disabled);
						}
					} else {
						if(_isVisible) {
							markerDefaultIconDescriptor = SVUtil.getMarkerIcon(getResources(), R.drawable.pin_bus_15);
						} else {
							markerDefaultIconDescriptor = SVUtil.getMarkerIcon(getResources(), R.drawable.pin_bus_15_disabled);
						}
					}
					break;
				case "subway" :
					if(mMarkerDisplayMode == MarkerDisplayMode.ALL) {
						if(_isVisible) {
							markerDefaultIconDescriptor = SVUtil.getMarkerIcon(getResources(), R.drawable.pin_subway_40);
						} else {
							markerDefaultIconDescriptor = SVUtil.getMarkerIcon(getResources(), R.drawable.pin_subway_40_disabled);
						}
					} else {
						if(_isVisible) {
							markerDefaultIconDescriptor = SVUtil.getMarkerIcon(getResources(), R.drawable.pin_subway_15);
						} else {
							markerDefaultIconDescriptor = SVUtil.getMarkerIcon(getResources(), R.drawable.pin_subway_15_disabled);
						}
					}
					break;
				case "shuttle" :
					if(mMarkerDisplayMode == MarkerDisplayMode.ALL) {
						if(_isVisible) {
							markerDefaultIconDescriptor = SVUtil.getMarkerIcon(getResources(), R.drawable.pin_shuttle_40);
						} else {
							markerDefaultIconDescriptor = SVUtil.getMarkerIcon(getResources(), R.drawable.pin_shuttle_40_disabled);
						}
					} else {
						if(_isVisible) {
							markerDefaultIconDescriptor = SVUtil.getMarkerIcon(getResources(), R.drawable.pin_shuttle_15);
						} else {
							markerDefaultIconDescriptor = SVUtil.getMarkerIcon(getResources(), R.drawable.pin_shuttle_15_disabled);
						}
					}
					break;
				case "carpool" :
					if(mMarkerDisplayMode == MarkerDisplayMode.ALL) {
						markerDefaultIconDescriptor = SVUtil.getMarkerIcon(getResources(), R.drawable.pin_taxi_registrant_40);
					} else {
						markerDefaultIconDescriptor = SVUtil.getMarkerIcon(getResources(), R.drawable.pin_taxi_registrant_15);
					}
					break;
				case "driver" :
					if(mMarkerDisplayMode == MarkerDisplayMode.ALL) {
						markerDefaultIconDescriptor = SVUtil.getMarkerIcon(getResources(), R.drawable.pin_driver_40);
					} else {
						markerDefaultIconDescriptor = SVUtil.getMarkerIcon(getResources(), R.drawable.pin_driver_25);
					}
					break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return markerDefaultIconDescriptor;
	}

	private void removeShuttleMarkers() {
		try {
			if(mShuttleStationMarkers != null) {
				int listSize = mShuttleStationMarkers.size();
				for(int i = 0; i < listSize; i++) {
					mShuttleStationMarkers.get(i).remove();
				}

				mShuttleStationMarkers.clear();
			}

			removeDriverMarker();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void removeShuttleLine() {
		try {
			if(mShuttleLineList != null) {
				int listSize = mShuttleLineList.size();
				for(int i = 0; i < listSize; i++) {
					mShuttleLineList.get(i).remove();
				}

				mShuttleLineList.clear();
			}

			if(mArrowMarkers != null) {
				int markerSize = mArrowMarkers.size();
				for(int i = 0; i < markerSize; i++) {
					mArrowMarkers.get(i).remove();
				}

				mArrowMarkers.clear();
			}

			mShuttleColorArrowBitmapDescriptor = new JSONObject();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setCurrentLocation() {
		try {
			mCurrentScreen = mGoogleMap.getProjection().getVisibleRegion().latLngBounds;

			float[] dist = new float[2];
			Location.distanceBetween(mCurrentScreen.southwest.latitude, mCurrentScreen.southwest.longitude, mCurrentScreen.northeast.latitude, mCurrentScreen.northeast.longitude, dist);

			mSearchRadius = dist[0];

			if(mSearchRadius < 600) {
				mSearchRadius = 600;
			}

//		SVUtil.log("error", TAG, "setCurrentLocation() distance between screen SW and NE : " + mSearchRadius);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void changeDriverOnOffButtonBg(boolean _status) {
		try {
			mIsShowDriver = _status;

			if(mIsShowDriver) {
				mBtnDriverOnOff.setText("기사\nON");
				mBtnDriverOnOff.setBackgroundResource(R.drawable.shape_button_bg_on);

				drawDriverMarker(mDriverList);
			} else {
				mBtnDriverOnOff.setText("기사\nOFF");
				mBtnDriverOnOff.setBackgroundResource(R.drawable.shape_button_bg_off);

				removeDriverMarker();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void changePublicTransitOnOffButtonBg() {
		try {
			if(mIsShowPublicTransit) {
				mBtnPublicTransitOnOff.setText("버스\nON");
				mBtnPublicTransitOnOff.setBackgroundResource(R.drawable.shape_button_bg_on);

				drawPublicTransitMarker(mPublicTransitList);
			} else {
				mBtnPublicTransitOnOff.setText("버스\nOFF");
				mBtnPublicTransitOnOff.setBackgroundResource(R.drawable.shape_button_bg_off);

				removePublicTransitMarker();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void drawDriverMarker(ArrayList<StationInfoData> _driverList) {
		try {
			if(_driverList == null || mGoogleMap == null) {
				return;
			}

			if(mDriverMarkers == null) {
				mDriverMarkers = new ArrayList<>();
			}

			for(StationInfoData _item : _driverList) {
				String snippet = _item.getmStationType() + "_" + _item.getmStationId() + "_" + _item.getmArsId() + "_" + _item.getmBusRouteArea() + "_" + _item.getmStationName();
				BitmapDescriptor markerIcon = getStationMarkerIconByStationType(_item.getmStationType(), _item.ismIsVisible());

//				SVUtil.log("error", TAG, snippet);
				if(markerIcon != null) {
					mDriverMarkers.add(mGoogleMap.addMarker(new MarkerOptions().position(_item.getPosition()).snippet(snippet).icon(markerIcon).anchor(0.5f, 1.0f)));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void removeDriverMarker() {
		try {
			if(mDriverMarkers != null && mDriverMarkers.size() > 0) {
				for(Marker m : mDriverMarkers) {
					m.remove();
				}

				mDriverMarkers.clear();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void drawPublicTransitMarker(ArrayList<StationInfoData> _publicTransitList) {
		try {
			if(_publicTransitList == null || mGoogleMap == null) {
				return;
			}

			if(mPublicTransitMarkers == null) {
				mPublicTransitMarkers = new ArrayList<>();
			}

			for(StationInfoData _item : _publicTransitList) {
				String snippet = _item.getmStationType() + "_" + _item.getmStationId() + "_" + _item.getmArsId() + "_" + _item.getmBusRouteArea() + "_" + _item.getmStationName();
				BitmapDescriptor markerIcon = getStationMarkerIconByStationType(_item.getmStationType(), _item.ismIsVisible());

//				SVUtil.log("error", TAG, snippet);
				if(markerIcon != null) {
					mPublicTransitMarkers.add(mGoogleMap.addMarker(new MarkerOptions().position(_item.getPosition()).snippet(snippet).icon(markerIcon).anchor(0.5f, 1.0f)));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void removePublicTransitMarker() {
		try {
			if(mPublicTransitMarkers != null && mPublicTransitMarkers.size() > 0) {
				for(Marker m : mPublicTransitMarkers) {
					m.remove();
				}

				mPublicTransitMarkers.clear();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void changeOutbackButtonBg(boolean _status) {
		try {
			mIsShowOutbackPolygon = _status;

			if(mIsShowOutbackPolygon) {
				mBtnOnOff.setText("오지\nON");
				mBtnOnOff.setBackgroundResource(R.drawable.shape_button_bg_on);
			} else {
				mBtnOnOff.setText("오지\nOFF");
				mBtnOnOff.setBackgroundResource(R.drawable.shape_button_bg_off);

				mOutbackBoundsData.initDefaultData();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void changeMapRotationButtonBg(boolean _status) {
		try {
			mIsMapRotation = _status;

			if(mIsMapRotation) {
				mBtnMapRotation.setText("지도\n회전\nON");
				mBtnMapRotation.setBackgroundResource(R.drawable.shape_button_bg_on);
				mEmptyTextView.setVisibility(View.VISIBLE);
			} else {
				mBtnMapRotation.setText("지도\n회전\nOFF");
				mBtnMapRotation.setBackgroundResource(R.drawable.shape_button_bg_off);
				mEmptyTextView.setVisibility(View.GONE);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void changeSearchShuttlePathButtonBg() {
		try {
			if(isSearchShuttlePathOn) {
				mBtnSearchShuttlePath.setBackgroundResource(R.drawable.shape_button_bg_on);
				mSearchShuttlePathLayout.setVisibility(View.VISIBLE);
				initSearchField();
				getCurrentAddress();
			} else {
				mBtnSearchShuttlePath.setBackgroundResource(R.drawable.shape_button_bg_off);
				mSearchShuttlePathLayout.setVisibility(View.GONE);
				initSearchField();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void initSearchField() {
		try {
			mDepartFromText.setText("");
			mArriveAtText.setText("");
			mDepartFromLocation = null;
			mArriveAtLocation = null;
			mBtnSearchShuttlePathExecute.setVisibility(View.GONE);
			mBtnSearchShuttlePath.setVisibility(View.VISIBLE);

			mSearchResultList.clear();
			mSearchResultAdapter.clear();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void drawPastPath() {
		try {
			if(mPastPathPolyline != null) {
				mPastPathPolyline.remove();
				mPastPathPolyline = null;
			}

			PolylineOptions opt = new PolylineOptions();
			opt.color(Color.DKGRAY);
			opt.width(15);
			opt.addAll(mMainActivity.mPastPathList);

			mPastPathPolyline = mGoogleMap.addPolyline(opt);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void changeMyLocation(float _declination) {
		try {
			SVUtil.log("error", TAG, "===== changeMyLocation() =====");

			if(mIsFinishFragment) {
				return;
			}

			if(mFragmentFrom != null && mFragmentFrom.equals("main")) {
				mDeclination = _declination;
				mCurrentMyPosition = mMainActivity.mCurrentLocation;

				checkExpectedOutsideOfBounds(mCurrentMyPosition, 0f, 0f);

				if(mGoogleMap != null) {
					setIsUpdateRunningStationList(true);
					mCurrentPositionOuterMarker.setPosition(mCurrentMyPosition);
					mCurrentPositionInnerMarker.setPosition(mCurrentMyPosition);

					mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(mCurrentMyPosition));

					updateCamera(false);

//				    drawPastPath();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void finishFindWay(LatLng _latLng) {
		try {
			mFindWayEndLatLng = _latLng;
			mLongClickStatus = "wait";
			mTVTouchDesc.setText("길게 누르면 길찾기 / 거리재기 가능");

			makeClickPointMarker(R.drawable.ic_finish_findpath, mFindWayEndLatLng);
			makeCloseMarker(mFindWayEndLatLng, -0.4f, 2.4f);

			drawPedestrianPath(mFindWayStartLatLng, mFindWayEndLatLng);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void makeClickPointMarker(int _resource_id, LatLng _position) {
		try {
			if(mGoogleMap == null) {
				return;
			}

			MarkerOptions markerOptions = new MarkerOptions().icon(SVUtil.getMarkerIcon(getResources(), _resource_id)).position(_position).anchor(0.5f, 1.0f);

			Marker m = mGoogleMap.addMarker(markerOptions);
			mDistanceMakers.add(m);

			Marker m2 = mGoogleMap.addMarker(new MarkerOptions().icon(SVUtil.getMarkerIcon(getResources(), R.drawable.marker_point)).position(_position).anchor(0.5f, 0.5f));
			mDistanceMakers.add(m2);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void makeDistanceMarker(LatLng _position, String _text, int _rotate, int _contentRotate, float _u, float _v) {
		try {
			if(mGoogleMap == null) {
				return;
			}

			IconGenerator iconFactory = new IconGenerator(mMainActivity);
			iconFactory.setRotation(_rotate);
			iconFactory.setContentRotation(_contentRotate);
			MarkerOptions markerOptions = new MarkerOptions().
					icon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon(_text))).
					position(_position).
					anchor(iconFactory.getAnchorU() + _u, iconFactory.getAnchorV() + _v);

			Marker m = mGoogleMap.addMarker(markerOptions);
			mDistanceMakers.add(m);

			Marker m2 = mGoogleMap.addMarker(new MarkerOptions().icon(SVUtil.getMarkerIcon(getResources(), R.drawable.marker_point)).position(_position).anchor(0.5f, 0.5f));
			mDistanceMakers.add(m2);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void showLongClickMenu(LatLng _position) {
		try {
			if(mGoogleMap == null) {
				return;
			}

			mLongClickPositionMarker = mGoogleMap.addMarker(new MarkerOptions().icon(SVUtil.getMarkerIcon(getResources(), R.drawable.map_pin_red)).position(_position).anchor(0.5f, 1.0f));

			MarkerOptions markerOptions = new MarkerOptions().
					icon(SVUtil.getMarkerIcon(getResources(), R.drawable.ic_menu_findpath)).
					position(_position).anchor(-0.2f, 1.0f).snippet("findway_start");

			mFindWayStartMarker = mGoogleMap.addMarker(markerOptions);

			markerOptions = new MarkerOptions().
					icon(SVUtil.getMarkerIcon(getResources(), R.drawable.ic_menu_measure)).
					position(_position).anchor(1.2f, 1.0f).snippet("measure_start");

			mMeasureMarker = mGoogleMap.addMarker(markerOptions);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void initDistanceObject() {
		try {
			if(mLongClickPositionMarker != null) {
				mLongClickPositionMarker.remove();
			}

			if(mCloseMarker != null) {
				mCloseMarker.remove();
			}

			if(mMeasureMarker != null) {
				mMeasureMarker.remove();
			}

			if(mFindWayStartMarker != null) {
				mFindWayStartMarker.remove();
			}

			if(mFindWayPolyline != null) {
				mFindWayPolyline.remove();
			}

			if(mDistanceMakers != null && mDistanceMakers.size() > 0) {
				int distanceMarkerSize = mDistanceMakers.size();
				for(int i = 0; i < distanceMarkerSize; i++) {
					mDistanceMakers.get(i).remove();
				}
			}

			if(mDistancePolyline != null && mDistancePolyline.size() > 0) {
				int polylineSize = mDistancePolyline.size();
				for(int i = 0; i < polylineSize; i++) {
					mDistancePolyline.get(i).remove();
				}
			}

			mTotalDistance = 0d;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void drawDistancePolyline(LatLng _start, LatLng _end) {
		try {
			if(mGoogleMap == null) {
				return;
			}

			PolylineOptions opt = new PolylineOptions();
			opt.add(_start);
			opt.add(_end);
			opt.width(10);
			opt.color(Color.parseColor("#2b3d4f"));

			mDistancePolyline.add(mGoogleMap.addPolyline(opt));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void showNearStationList() {
		try {
			mShowRunningStationList.clear();

			int listCount = mRunningStationList.size();
			int selectedDistance = mDistanceList[mCurrentDistance];

			for (int i = 0; i < listCount; i++) {
				StationInfoData item = mRunningStationList.get(i);
				if (item.getmDistance() <= selectedDistance) {
					mShowRunningStationList.add(item);
				}
			}

			Collections.sort(mShowRunningStationList, stationTypeSort);

			mOutbackStationAdapter.notifyDataSetChanged();

			String distance = (selectedDistance / 1000) + "km";
			mBtnDistance.setText(distance);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void moveToBusStationInfoFragment(String _stationId, String _arsId, String _busRouteArea, String _stationName) {
		try {
			Bundle args = new Bundle();
			args.putString("stationId", _stationId);
			args.putString("arsId", _arsId);
			args.putString("busRouteArea", _busRouteArea);
			args.putString("stationName", _stationName);
			args.putString("from", mFragmentFrom);
			args.putString("positionName", mCurrentPositionName);
			args.putDouble("centerLat", mCurrentScreen.getCenter().latitude);
			args.putDouble("centerLng", mCurrentScreen.getCenter().longitude);
			args.putDouble("positionLat", mCurrentMyPosition.latitude);
			args.putDouble("positionLng", mCurrentMyPosition.longitude);

			BusStationInfoFragment frag = new BusStationInfoFragment();
			frag.setArguments(args);

			mMainActivity.replaceFragment(frag, R.id.mainFrameLayout, Global.FRAG_BUS_STATION_INFO_TAG, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void moveToShuttleStationInfoFragment(String _stationId, String _arsId, String _busRouteArea, String _stationName) {
		try {
			Bundle args = new Bundle();
			args.putString("stationId", _stationId);
			args.putString("arsId", _arsId);
			args.putString("busRouteArea", _busRouteArea);
			args.putString("stationName", _stationName);
			args.putString("from", mFragmentFrom);
			args.putString("positionName", mCurrentPositionName);
			args.putDouble("centerLat", mCurrentScreen.getCenter().latitude);
			args.putDouble("centerLng", mCurrentScreen.getCenter().longitude);
			args.putDouble("positionLat", mCurrentMyPosition.latitude);
			args.putDouble("positionLng", mCurrentMyPosition.longitude);

			ShuttleStationInfoFragment frag = new ShuttleStationInfoFragment();
			frag.setArguments(args);

			mMainActivity.replaceFragment(frag, R.id.mainFrameLayout, Global.FRAG_SHUTTLE_STATION_TAG, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void launchDaumWeb(String _stationCode) {
		try {
			String urlStr = "";
			urlStr += "http://m.map.daum.net/actions/detailInfoView?id=SES" + _stationCode;

//		SVUtil.log("error", TAG, "url : " + urlStr);

			Uri uri = Uri.parse(urlStr);

			startActivity(new Intent(Intent.ACTION_VIEW, uri));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void moveToCarpoolDetailFragment(String _taxiId) {
		try {
			mMainActivity.changeButtonImage(mMainActivity.mImgBtnCarpool);

			Bundle carpoolArgs = new Bundle();
			carpoolArgs.putString("taxiId", _taxiId);

			CarpoolDetailFragment frag = new CarpoolDetailFragment();
			frag.setArguments(carpoolArgs);

			mMainActivity.replaceFragment(frag, R.id.mainFrameLayout, Global.FRAG_CARPOOL_DETAIL_TAG, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void calculateMeasure(LatLng _latLng) {
		try {
			String strDist = calculateDistance(mMeasureStartLatLng, _latLng);
			makeDistanceMarker(_latLng, strDist, 0, 0, 0f, 0f);
			makeCloseMarker(_latLng, -1.0f, 2.0f);
			drawDistancePolyline(mMeasureStartLatLng, _latLng);

			mMeasureStartLatLng = _latLng;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String calculateDistance(LatLng _start, LatLng _end) {
		try {
			Location startLoc = new Location("start");
			Location endLoc = new Location("end");

			startLoc.setLatitude(_start.latitude);
			startLoc.setLongitude(_start.longitude);
			endLoc.setLatitude(_end.latitude);
			endLoc.setLongitude(_end.longitude);

			mTotalDistance = mTotalDistance + startLoc.distanceTo(endLoc);
			double distance = mTotalDistance;

			String strDist;
			if(distance >= 1000) {
				distance = distance / 1000.0;
				strDist = String.format("%.2f", distance) + "km";
			} else {
				strDist = String.format("%.0fm", distance);
			}

			return strDist;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	private void makeCloseMarker(LatLng position, float _u, float _v) {
		try {
			if(mCloseMarker != null) {
				mCloseMarker.remove();
			}

			if(mGoogleMap == null) {
				return;
			}

			MarkerOptions markerOptions = new MarkerOptions().
					icon(SVUtil.getMarkerIcon(getResources(), R.drawable.ic_delete_all)).
					position(position).anchor(_u, _v).snippet("close");

			mCloseMarker = mGoogleMap.addMarker(markerOptions);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void drawPedestrianPath(LatLng _start, LatLng _end) {
		try {
			if (mFindWayPolyline != null) {
				mFindWayPolyline.remove();
			}

			new RouteSearchTask(_start, _end).execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	class RouteSearchTask extends AsyncTask<Void, Void, ArrayList<LatLng>> {
		private LatLng mStartPoint;
		private LatLng mEndPoint;
		private int mDistance;
		private int mTime;

		public RouteSearchTask(LatLng _startPoint, LatLng _endPoint) {
			this.mStartPoint = _startPoint;
			this.mEndPoint = _endPoint;
			this.mDistance = 0;
			this.mTime = 0;
		}

		@Override
		protected ArrayList<LatLng> doInBackground(Void... params) {
			TMapPoint start = new TMapPoint(this.mStartPoint.latitude, this.mStartPoint.longitude); // 기사의 위치
			TMapPoint end = new TMapPoint(this.mEndPoint.latitude, this.mEndPoint.longitude); // 고객의 위치
			TMapData data = new TMapData();
			ArrayList<LatLng> googlePolylinePath = new ArrayList<>();

			try {
				Document doc = data.findPathDataAllType(TMapData.TMapPathType.PEDESTRIAN_PATH, start, end);
				NodeList childNodes = doc.getElementsByTagName("Document").item(0).getChildNodes();
				int childSize = childNodes.getLength();

				for(int i = 0; i < childSize; i++) {
					Node childItem = childNodes.item(i);
					if(childItem.getNodeName().equals("tmap:totalDistance")) {
						mDistance = Integer.parseInt(childItem.getFirstChild().getNodeValue());
					} else if (childItem.getNodeName().equals("tmap:totalTime")) {
						mTime = Integer.parseInt(childItem.getFirstChild().getNodeValue());
					}
				}

				TMapPolyLine path = data.findPathDataWithType(TMapData.TMapPathType.PEDESTRIAN_PATH, start, end);
				ArrayList<TMapPoint> tmapPolylinePath = path.getLinePoint();

				int pathSize = tmapPolylinePath.size();
				for (int i = 0; i < pathSize; i++) {
					googlePolylinePath.add(new LatLng(tmapPolylinePath.get(i).getLatitude(), tmapPolylinePath.get(i).getLongitude()));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return googlePolylinePath;
		}

		@Override
		protected void onPostExecute(ArrayList<LatLng> result) {
			try {
				if(mIsFinishFragment) {
					return;
				}

				if (result.size() > 0) {
					PolylineOptions opt = new PolylineOptions();
					opt.color(Color.BLUE);
					opt.width(15);
					opt.addAll(result);

					mFindWayPolyline = mGoogleMap.addPolyline(opt);

					String totalTime = SVUtil.getTotalTimeString(mTime);
					String totalDist;
					if(mDistance >= 1000) {
						double distances = mDistance / 1000.0;
						totalDist = String.format("%.2f", distances) + "km";
					} else {
						totalDist = mDistance + "m";
					}

					makeDistanceMarker(mFindWayEndLatLng, "도보거리 : " + totalDist + "\n" + "소요시간 : " + totalTime, 180, -180, 0f, 0f);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private class ShuttleLineColorViewHolder {
		public TextView mTextViewColor;
		public TextView mTextViewShuttleName;
	}

	private class ShuttleLineColorAdapter extends ArrayAdapter<ShuttleLineColorData> {
		ArrayList<ShuttleLineColorData> mShuttleLineColorList;
		private int mRowId;
		private ShuttleLineColorViewHolder mHolder;

		public ShuttleLineColorAdapter(Context _context, int _rowId, ArrayList<ShuttleLineColorData> _shuttleLineColorList) {
			super(_context, _rowId, _shuttleLineColorList);
			this.mShuttleLineColorList = _shuttleLineColorList;
			this.mRowId = _rowId;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			try {
				if (convertView == null) {
					LayoutInflater li = (LayoutInflater) mMainActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					convertView = li.inflate(mRowId, null);
					mHolder = new ShuttleLineColorViewHolder();
					mHolder.mTextViewShuttleName = (TextView) convertView.findViewById(R.id.row_shuttle_color_shuttle_name);
					mHolder.mTextViewColor = (TextView) convertView.findViewById(R.id.row_shuttle_color_color);

					convertView.setTag(mHolder);
				} else {
					mHolder = (ShuttleLineColorViewHolder) convertView.getTag();
				}

				ShuttleLineColorData box = mShuttleLineColorList.get(position);
				mHolder.mTextViewShuttleName.setText(box.getmShuttleName());
				mHolder.mTextViewColor.setBackgroundColor(Color.parseColor(box.getmLineColor()));
			} catch (Exception e) {
				e.printStackTrace();
			}

			return convertView;
		}
	}

	private class ViewHolder {
		public TextView mTextViewStationName;
		public TextView mTextViewArsId;
		public TextView mTextViewDistance;
	}

	private class ViewHolder_header {
		TextView titleTV;
	}

	private class OutbackStationAdapter extends ArrayAdapter<StationInfoData> implements StickyListHeadersAdapter {
		ArrayList<StationInfoData> mStationInfoList;
		private int mRowId;
		private ViewHolder mHolder;

		public OutbackStationAdapter(Context _context, int _rowId, ArrayList<StationInfoData> _stationInfoList) {
			super(_context, _rowId, _stationInfoList);
			this.mStationInfoList = _stationInfoList;
			this.mRowId = _rowId;
		}

		@Override
		public View getHeaderView(int i, View view, ViewGroup viewGroup) {
			try {
				ViewHolder_header holder;

				if (view == null) {
					holder = new ViewHolder_header();
					view = mMainActivity.getLayoutInflater().inflate(R.layout.row_shuttle_list_header, viewGroup, false);
					holder.titleTV = (TextView) view.findViewById(R.id.row_shuttle_list_headerTV);
					view.setTag(holder);
				} else {
					holder = (ViewHolder_header) view.getTag();
				}

				if (getHeaderId(i) == 0) {
					holder.titleTV.setText("시내버스/마을버스/심야 정거장");
				} else if (getHeaderId(i) == 1) {
					holder.titleTV.setText("지하철역");
				} else if (getHeaderId(i) == 2) {
					holder.titleTV.setText("대리셔틀");
				} else if (getHeaderId(i) == 3) {
					holder.titleTV.setText("택시카풀 등록자");
				} else {
					holder.titleTV.setText("기타");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return view;
		}

		@Override
		public long getHeaderId(int i) {
			try {
				if (mShowRunningStationList.get(i).getmStationType().equals("bus")) {
					return 0;
				} else if (mShowRunningStationList.get(i).getmStationType().equals("subway")) {
					return 1;
				} else if (mShowRunningStationList.get(i).getmStationType().equals("shuttle")) {
					return 2;
				} else if (mShowRunningStationList.get(i).getmStationType().equals("carpool")) {
					return 3;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return 4;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;

			try {
				if (v == null) {
					LayoutInflater li = (LayoutInflater) mMainActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					v = li.inflate(mRowId, null);
					mHolder = new ViewHolder();
					mHolder.mTextViewStationName = (TextView) v.findViewById(R.id.row_near_station_nameTV);
					mHolder.mTextViewArsId = (TextView) v.findViewById(R.id.row_near_station_arsIdTV);
					mHolder.mTextViewDistance = (TextView) v.findViewById(R.id.row_near_station_distTV);

					v.setTag(mHolder);
				} else {
					mHolder = (ViewHolder) v.getTag();
				}

				StationInfoData box = mStationInfoList.get(position);
				String arsId = "[" + box.getmArsId() + "]";

				double distance = box.getmDistance();
				String strDist;
				if(distance >= 1000) {
					distance = distance / 1000.0;
					strDist = String.format("%.2f", distance) + "km";
				} else {
					strDist = (int) distance + "m";
				}

				mHolder.mTextViewStationName.setText(box.getmStationName());
				mHolder.mTextViewArsId.setText(arsId);
				mHolder.mTextViewDistance.setText(strDist);
			} catch (Exception e) {
				e.printStackTrace();
			}

			return v;
		}
	}

	private void updateCamera(boolean _isAnimation) {
		try {
			if(mIsFinishFragment || mGoogleMap == null) {
				return;
			}

			CameraPosition cameraPosition;
			float outer_bearing = mOuterMapBearing + mDeclination;
			float innerBearing = getPositionInnerMarkerBearing();
			float innerDiff = Math.abs(mInnerMapBearing - innerBearing);
			if(innerDiff >= 5) {
				mInnerMapBearing = innerBearing;
			}

			cameraPosition = mGoogleMap.getCameraPosition();

			if(mIsMapRotation) {
				mCurrentPositionInnerMarker.setRotation(0f);

				cameraPosition = CameraPosition.builder(cameraPosition).bearing(mInnerMapBearing).build();

				if(_isAnimation) {
					mGoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
				} else {
					mGoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
				}

				outer_bearing = outer_bearing - mInnerMapBearing;
				mCurrentPositionOuterMarker.setRotation(outer_bearing);
			} else {
				cameraPosition = CameraPosition.builder(cameraPosition).bearing(0f).build();

				mGoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

				mCurrentPositionInnerMarker.setRotation(mInnerMapBearing);
				mCurrentPositionOuterMarker.setRotation(outer_bearing);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private float getPositionInnerMarkerBearing() {
		float innerBearing = 0f;

		try {
			if(mMainActivity.mPastPathList != null && mMainActivity.mPastPathList.size() > 2) {
				LatLng last = mMainActivity.mPastPathList.get(mMainActivity.mPastPathList.size() - 1);
				LatLng pre_last = mMainActivity.mPastPathList.get(mMainActivity.mPastPathList.size() - 2);

//				innerBearing = SVUtil.getRotationDegrees(pre_last, last);

				float[] dist = new float[3];
				Location.distanceBetween(pre_last.latitude, pre_last.longitude, last.latitude, last.longitude, dist);

//				SVUtil.log("error", TAG, "last : " + last.toString() + ", pre_last : " + pre_last.toString() + ", dist : [" + dist[0] + ", " + dist[1] + ", " + dist[2] + "]");

				if(dist.length == 2) {
					innerBearing = dist[1];
				} else if(dist.length == 3) {
					innerBearing = dist[2];
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return innerBearing;
	}

	private final Comparator<StationInfoData> stationSeqSort = new Comparator<StationInfoData>() {
		@Override
		public int compare(StationInfoData box, StationInfoData box2) {
			return box.getmStationSeq() - box2.getmStationSeq();
		}
	};

	private final Comparator<StationInfoData> stationTypeSort = new Comparator<StationInfoData>() {
		@Override
		public int compare(StationInfoData box, StationInfoData box2) {
			int comp1 = box.getmStationType().equals("shuttle") ? 0 : (box.getmStationType().equals("subway") ? 1 : (box.getmStationType().equals("carpool") ? 2 : 3));
			int comp2 = box2.getmStationType().equals("shuttle") ? 0 : (box2.getmStationType().equals("subway") ? 1 : (box2.getmStationType().equals("carpool") ? 2 : 3));
			int comp_id = comp1 - comp2;

			if(comp_id == 0) {
				return (int)(box.getmDistance() - box2.getmDistance());
			}

			return comp_id;
		}
	};

	@Override
	public void onSensorChanged(SensorEvent event) {
		try {
			if(mIsFinishFragment) {
				return;
			}

			if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
				mLastAccelerometer = event.values;
			} else if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
				mLastMagnetometer = event.values;
			}

			if (mLastAccelerometer != null && mLastMagnetometer != null) {
				float R[] = new float[9];
				float I[] = new float[9];
				boolean success = SensorManager.getRotationMatrix(R, I, mLastAccelerometer, mLastMagnetometer);
				if (success) {
					float orientation[] = new float[3];
					SensorManager.getOrientation(R, orientation);
					float azimuthInRadians = orientation[0];
					float azimuthInDegrees = (float)Math.toDegrees(azimuthInRadians);

					float diff = Math.abs(mOuterMapBearing - azimuthInDegrees);
					if(diff >= 10) {
						mOuterMapBearing = azimuthInDegrees;
						updateCamera(false);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) { }

	private void destroyView() {
		try {
			if(mGoogleMap != null) {
				mGoogleMap.clear();
				mGoogleMap = null;
			}

			SupportMapFragment f = (SupportMapFragment) MainActivity.mSupportFragmentManager.findFragmentById(R.id.outback_map);
			if(f != null) {
				MainActivity.mSupportFragmentManager.beginTransaction().remove(f).commitAllowingStateLoss();
			}

			if(mView != null){
				ViewGroup parent = (ViewGroup) mView.getParent();
				if(parent!=null) {
					parent.removeView(mView);
				}

				mView = null;
			}

//			mMainActivity.mFragmentOutback = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onStart() {
		super.onStart();

		mIsFinishFragment = false;
		mIsFragmentRunning = true;
	}

	@Override
	public void onResume() {
		super.onResume();

		try {
			if(mSensorManager != null) {
				mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_GAME);
				mSensorManager.registerListener(this, mMagnetometer, SensorManager.SENSOR_DELAY_GAME);
			}

			mMainActivity.changeButtonImage(mMainActivity.mImgBtnOutback);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onPause() {
		super.onPause();

		try {
			SVUtil.hideProgressDialog();

			mIsFinishFragment = true;
			mIsFragmentRunning = false;

			if(mSensorManager != null) {
				mSensorManager.unregisterListener(this, mAccelerometer);
				mSensorManager.unregisterListener(this, mMagnetometer);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onStop() {
		super.onStop();
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		destroyView();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		setTargetFragment(null, -1);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onDetach() {
		super.onDetach();
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		return mGestureDetector.onTouchEvent(event);
	}
}
