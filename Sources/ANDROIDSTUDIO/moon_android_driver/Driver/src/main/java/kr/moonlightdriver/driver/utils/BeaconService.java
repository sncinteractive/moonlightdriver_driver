package kr.moonlightdriver.driver.utils;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.MacAddress;
import com.estimote.sdk.Region;
import com.google.android.gms.maps.model.LatLng;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

import kr.moonlightdriver.driver.Network.NetApi.BeaconDataAck;
import kr.moonlightdriver.driver.Network.NetApi.BusTakeAck;
import kr.moonlightdriver.driver.Network.NetApi.NetAPI;
import kr.moonlightdriver.driver.Network.NetApi.NetResponse;
import kr.moonlightdriver.driver.Network.NetApi.NetResponseCallback;
import kr.moonlightdriver.driver.Network.NetApi.ShuttlePathAck;
import kr.moonlightdriver.driver.Network.NetClient.NetClient;
import kr.moonlightdriver.driver.Network.NetData.BeaconData;
import kr.moonlightdriver.driver.Network.NetData.ResultData;
import kr.moonlightdriver.driver.Network.NetData.ShuttlePathData;
import kr.moonlightdriver.driver.Network.NetData.ShuttlePathPointData;
import kr.moonlightdriver.driver.R;
import kr.moonlightdriver.driver.activity.MainActivity;

public class BeaconService extends Service {

    private static final int BEACON_RANGE = -80;
    private static final int SEC = 1000;

    public static LatLng mNewlocation = null;
    private LatLng mLastLocation = null;

    private BeaconManager beaconManager;
    private Region region;
    private int status = 0;
    private long lastUpdateTime;
    private long createTime;
    private boolean isTakeOffTask = false;
    private boolean isSending = false;

    private String mShuttleId;
    private String mShuttleName;
    private String mBusNumId;
    private String driverId;
    private MacAddress beaconMacAddress;
    private MacAddress currentMacAddress;  // 지금 처리중인 맥 어드레스

    private HashSet<String> disableBeaconSet;
    private ArrayList<ShuttlePathPointData> mShuttlePathPointData;
    private Runnable takeOffNoti;
    private Runnable sendLocation;
    private Handler handler;


    public void onCreate() {
        super.onCreate();

        TelephonyManager tMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        driverId = tMgr.getLine1Number();

        handler = new Handler();
        disableBeaconSet = new HashSet<>();
        mShuttlePathPointData = new ArrayList<>();

        takeOffNoti = new Runnable() {
            @Override
            public void run() {
                status = 0;
                createTime = 0;
                sendServer("takeOffBus");
                handler.removeCallbacks(sendLocation);
                isSending = false;
            }
        };

        sendLocation = new Runnable() {
            @Override
            public void run() {
                checkGPSChange();
                connectHttpServer();
            }
        };

        beaconManager = new BeaconManager(getApplicationContext());
        region = new Region(
                "beacon",
                UUID.fromString("fda50693-a4e2-4fb1-afcf-c6eb07647825"), // 본인이 연결할 Beacon의 ID와 Major / Minor Code를 알아야 한다.
                null, null);

        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                beaconManager.startMonitoring(region);
            }
        });


        beaconManager.setRangingListener(new BeaconManager.RangingListener() {
            @Override
            public void onBeaconsDiscovered(Region region, List<Beacon> list) {
                if (!list.isEmpty()) {  // 만약 비콘이 연결 되면
                    if (createTime == 0) {
                        reset();
                        createTime = System.currentTimeMillis();
                    } else {
                        lastUpdateTime = System.currentTimeMillis();
                    }

                    // 시간 설정 끝
                    Beacon nearestBeacon = list.get(0);
                    Log.d("Airport", "Nearest places: " + nearestBeacon.getRssi() + "  / " + nearestBeacon.getMacAddress() + " / staus : " + status + " / lastUpdateTime" + ((lastUpdateTime - createTime) / 1000));

                    if (beaconMacAddress == null) {       // 최초이면?
                        if (!disableBeaconSet.contains(nearestBeacon.getMacAddress().toStandardString()))
                            getBeaconData(nearestBeacon.getMacAddress());
                        return;
                    } else if (!beaconMacAddress.toStandardString().equals(nearestBeacon.getMacAddress().toStandardString())) { // 비콘이 새로 연결되었다면??
                        if (!isTakeOffTask && currentMacAddress != null) {
                            Log.e("하차대기", "하차대기 + currentMAc" + currentMacAddress.toStandardString());
                            handler.postDelayed(takeOffNoti, 180 * SEC);
                            isTakeOffTask = true;
                        }
                        getBeaconData(nearestBeacon.getMacAddress());
                        return;
                    }

                    //만약 범위 안에 있으면?
                    if (list.get(0).getRssi() > BEACON_RANGE) {
                        if (lastUpdateTime - createTime > 2 * SEC & status == 0) {
                            showNotification("달빛기사", "셔틀 탑승 가능(" + mShuttleName + ")", 100);
                            status = 1;
                        } else if (lastUpdateTime - createTime > 30 * SEC & status == 1) {
                            connectHttpServer();
                            currentMacAddress = beaconMacAddress;
                            status = 2;
                            sendServer("takeOnBus");
                        } else {
                            if(!isSending)
                                connectHttpServer();
                            //checkGPSChange();
                            handler.removeCallbacks(takeOffNoti);
                            isTakeOffTask = false;
                        }

                        // 만약 범위 밖에 있다면?
                    } else {
                        if (!isTakeOffTask && currentMacAddress != null) {
                            Log.e("하차대기", "하차대기 / 범위 밖+ currentMAc " + currentMacAddress.toStandardString());
                            handler.postDelayed(takeOffNoti, 180 * SEC);
                            handler.removeCallbacks(sendLocation);
                            isTakeOffTask = true;
                            isSending = false;
                        } else if (currentMacAddress == null) { // 연결 된 적이 없음
                            reset();
                        }
                    }
                }
            }
        });

        beaconManager.setMonitoringListener(new BeaconManager.MonitoringListener() {

            @Override
            public void onEnteredRegion(Region r, List<Beacon> list) {
                Log.e("Application", "onEnter Region");
                beaconManager.startRanging(region);
            }

            @Override
            public void onExitedRegion(Region r) {
                Log.e("Application", "onExited Region");
                beaconManager.stopRanging(region);

            }
        });

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void reset() {
        createTime = 0;
        lastUpdateTime = 0;
        status = 0;
    }

    private void sendServer(final String what) {
        if (mNewlocation == null) {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Log.e("Application", "위치가 잡히지 않아 대기중");
                    sendServer(what);
                }
            }, 10 * SEC);
        } else {
            Log.e("Application", what);
            RequestParams params = new RequestParams();
            params.add("driverId", driverId);
            params.add("beaconId", currentMacAddress.toStandardString());
            params.add("lat", mNewlocation.latitude + "");
            params.add("lng", mNewlocation.longitude + "");
            NetClient.send(this, "http://150.95.135.222:8888/beacon/" + what, "POST", params, new BusTakeAck(), new NetResponseCallback(new NetResponse() {
                @Override
                public void onResponse(NetAPI _netAPI) {
                    BusTakeAck retNetApi = (BusTakeAck) _netAPI;
                    ResultData result = retNetApi.getmResultData();
                    Log.e("Response", result.getmDetail());

                    if (what.equals("takeOffBus")) {
                        showNotification("달빛기사", "하차하셨습니다", 100);
                        currentMacAddress = null;
                    } else
                        processTakeOnBus();
                }
            }));
        }
    }

    private void processTakeOnBus() {
        String url = Global.URL_SHUTTLE_PATH.replace(":busId", mShuttleId);
        NetClient.send(this, url, "GET", null, new ShuttlePathAck(), new NetResponseCallback(new NetResponse() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onResponse(NetAPI _netAPI) {
                try {
                    SVUtil.hideProgressDialog();

                    if (_netAPI != null) {
                        ShuttlePathAck retNetApi = (ShuttlePathAck) _netAPI;
                        ResultData result = retNetApi.getmResultData();
                        if (result.getmCode() != 100) {
                            return;
                        }
                        Log.e("aa", result.getmDetail());

                        ShuttlePathData mShuttlePathData = retNetApi.getmShuttlePathData();
                        final String shuttleName = mShuttlePathData.getmShuttleName();
                        ArrayList<ShuttlePathPointData> shuttlePathPointData = mShuttlePathData.getmShuttlePathPointData();
                        int listSize = shuttlePathPointData.size();

                        double distance = 999999999;
                        String pointName = "";
                        for (int i = 0; i < listSize; i++) {
                            if (!shuttlePathPointData.get(i).getmShuttlePointName().equals("__point")) {
                                mShuttlePathPointData.add(shuttlePathPointData.get(i));
                                ArrayList<Double> point = shuttlePathPointData.get(i).getmShuttleLocationData().getmCoordinates();
                                Log.e("t", shuttlePathPointData.get(i).getmShuttlePointName() + "");
                                if (calcrateDistance(point.get(0), mNewlocation.latitude, point.get(1), mNewlocation.longitude) < distance) {
                                    distance = calcrateDistance(point.get(1), mNewlocation.latitude, point.get(0), mNewlocation.longitude);
                                    pointName = shuttlePathPointData.get(i).getmShuttlePointName();
                                    Log.e("aa", point.get(1) + " / " + mNewlocation.latitude + " /  " + point.get(0) + " / " + mNewlocation.longitude);
                                }
                            }
                        }
                        showNotification("달빛기사", shuttleName + "셔틀 탑승 (" + pointName + ")", 100);

                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Intent notifyIntent = new Intent(BeaconService.this, MainActivity.class);
                                notifyIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                notifyIntent.putExtra("actionFragment", "ShuttlePointListDetail");
                                notifyIntent.putExtra("actionId", mShuttleId);
                                PendingIntent pendingIntent = PendingIntent.getActivity(BeaconService.this, 0, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                                Notification.Builder notification = new Notification.Builder(BeaconService.this)
                                        .setSmallIcon(R.drawable.ic_launcher)
                                        .setContentTitle("셔틀 정거장 안내")
                                        .setContentText(shuttleName + " 노선 정거장 (클릭)")
                                        .setAutoCancel(true)
                                        .setContentIntent(pendingIntent);

                                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                                notificationManager.notify(110, notification.build());
                            }
                        }, 60000);


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }));
    }

    private double calcrateDistance(double x1, double x2, double y1, double y2) {
        return (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2);
    }

    private void checkGPSChange() {
        if (mNewlocation == null) {

        } else {
            if (mLastLocation == null) {
                mLastLocation = mNewlocation;
                Log.e("aa", "lat=" + mLastLocation.latitude + "&lng=" + mLastLocation.longitude + " / " + mBusNumId + "/" + driverId);

            } else if (!mNewlocation.equals(mLastLocation)) {
                mLastLocation = mNewlocation;
                Log.e("aa", "lat=" + mLastLocation.latitude + "&lng=" + mLastLocation.longitude + " / " + mBusNumId + "/" + driverId);

                Log.e("aa", makeGPSParams().toString());
                NetClient.send(getApplicationContext(), Global.WOKITIKI_SERVER_HOST + "/shuttleBeacon", "POST", makeGPSParams(), new BeaconDataAck(), new NetResponseCallback(new NetResponse() {
                    @Override
                    public void onResponse(NetAPI _netAPI) {
                        try {
                            if (_netAPI != null) {
                                BeaconDataAck retNetApi = (BeaconDataAck) _netAPI;
                                ResultData result = retNetApi.getmResultData();

                                if (result.getmCode() == 100) {
                                } else {
                                    SVUtil.log("error", "TAG", result.getmDetail());
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e("Error", "1");
                        }
                    }
                }));
            }
        }
    }

    public void showNotification(String title, String message, int id) {
        Intent notifyIntent = new Intent(this, MainActivity.class);
        SharedPreferences preferences = getSharedPreferences("pref", MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        Log.e("aa", "일치 여부 " + " : " + preferences.getString("noti_shuttleId", "").equals(mShuttleId) + " / status : 저장값 " + preferences.getInt("noti_status", 0) + " / 실제값 " + status + " / 시간 차이 " + Math.abs(System.currentTimeMillis() - preferences.getLong("noti_time", 0)) + " / 메시지 " + message);
        if(message.equals("하차하셨습니다")){
            ;   // 하차도 무시하지 않음
        } else if (Math.abs(System.currentTimeMillis() - preferences.getLong("noti_time", 0)) > 60 * 60 * SEC) {   // 1시간
            ;   // 시간이 오래되었으면 상관 없음
        } else if (preferences.getString("noti_shuttleId", "").equals(mShuttleId) && preferences.getInt("noti_status", 0) >= status) {
            return;
        }

        if (message.equals("하차하셨습니다")) {
            editor.putString("noti_shuttleId", "");
            editor.putInt("noti_status", 0);
            editor.putLong("noti_time", 0);
        } else {
            editor.putInt("noti_status", status);
            editor.putString("noti_shuttleId", mShuttleId);
            editor.putLong("noti_time", System.currentTimeMillis());
        }


        editor.commit();

        notifyIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        long[] pattern;
        if (message.equals("승차알림 - 근처에 버스가 있습니다.")) {
            pattern = new long[1];
            pattern[0] = 0;
        } else {
            pattern = new long[4];
            pattern[0] = 100;
            pattern[1] = 100;
            pattern[2] = 100;
            pattern[3] = 100;
        }
        PendingIntent pendingIntent = PendingIntent.getActivities(this, 0,
                new Intent[]{notifyIntent}, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification notification = new Notification.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setVibrate(pattern)
                .setContentIntent(pendingIntent)
                .build();
        stopForeground(false);
        startForeground(id, notification);
    }

    private void connectHttpServer() {
        SVUtil.log("error", "SockJS", "connectHttpServer");
        handler.postDelayed(sendLocation, 3000);
        isSending = true;
    }

    public RequestParams makeGPSParams() {
        RequestParams obj = new RequestParams();
        try {
            obj.put("channel_id", mShuttleId);
            obj.put("senderId", driverId);
            obj.put("beaconMac", beaconMacAddress.toStandardString());
            obj.put("msg", mLastLocation.latitude + "@" + mLastLocation.longitude + "@" + mBusNumId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("RequestParams", "send : " + obj.toString());

        return obj;
    }


    public void getBeaconData(final MacAddress beaconMac) {
        SVUtil.log("error", "TAG", "정보 요청");
        try {
            NetClient.send(getApplicationContext(), Global.SERVER_HOST + "/beacon" + "/" + beaconMac.toStandardString(), "GET", null, new BeaconDataAck(), new NetResponseCallback(new NetResponse() {
                @Override
                public void onResponse(NetAPI _netAPI) {
                    try {
                        if (_netAPI != null) {
                            BeaconDataAck retNetApi = (BeaconDataAck) _netAPI;
                            ResultData result = retNetApi.getmResultData();

                            if (result.getmCode() == 100) {
                                BeaconData beaconData = retNetApi.getmBeaconData();
                                //setLocationUpdateTime(_newLocation);
                                SVUtil.log("error", "TAG", beaconData.getmBusId() + " // " + beaconData.getmBusId());

                                mShuttleId = beaconData.getmBusId();
                                mShuttleName = beaconData.getmText();
                                mBusNumId = beaconData.getmBusNum();
                                //connectHttpServer();
                                if (status == 0) {
                                    showNotification("달빛기사", "승차알림 - 근처에 버스가 있습니다.", 100);
                                }
                                if (currentMacAddress == null)
                                    reset();
                                beaconMacAddress = beaconMac;

                            } else {
                                SVUtil.log("error", "TAG", result.getmDetail());
//                                reset();
//
//
//                                if (!isTakeOffTask && currentMacAddress != null) {
//                                    Log.e("하차대기", "하차대기 / 새 비콘이 잡혔으나 탐색되지 않음. + currentMAc " + currentMacAddress.toStandardString());
//                                    handler.postDelayed(takeOffNoti, 10000);
//                                    isTakeOffTask = true;
//                                } else if (!isTakeOffTask) {
//                                    reset();
//                                }

                                disableBeaconSet.add(beaconMac.toStandardString());
                                beaconMacAddress = null;
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e("Error", "1");
                    }
                }
            }));
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Error", "2");
        }
    }
}
