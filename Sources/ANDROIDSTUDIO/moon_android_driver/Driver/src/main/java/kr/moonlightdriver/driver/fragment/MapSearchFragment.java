package kr.moonlightdriver.driver.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.skt.Tmap.TMapData;

import org.xml.sax.SAXException;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import kr.moonlightdriver.driver.R;
import kr.moonlightdriver.driver.activity.MainActivity;

/**
 * Created by redjjol on 23/10/14.
 */
public class MapSearchFragment extends Fragment {
	private static final String TAG = "MapSearchFragment";
	private TMapData mTmapData;

	private ImageButton mBtnConvert;
	private Button mBtnSet;
	private ImageButton mBtnZoomIn;
	private ImageButton mBtnZoomOut;

	private LatLng mCurrentPosition;
	private String mSearchType;

	private View mView;
	private GoogleMap mGoogleMap;

	private MainActivity mMainActivity;

	private boolean mIsFinishFragment;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		try {
			mView = inflater.inflate(R.layout.frag_map_search, container, false);

			mMainActivity = (MainActivity) getActivity();

			mIsFinishFragment = false;

			Bundle bundle = MapSearchFragment.this.getArguments();
			mSearchType = bundle.getString("searchType", "");
			double lat = bundle.getDouble("lat", mMainActivity.mCurrentLocation.latitude);
			double lng = bundle.getDouble("lng", mMainActivity.mCurrentLocation.longitude);
			mCurrentPosition = new LatLng(lat, lng);

			mBtnConvert = (ImageButton) mView.findViewById(R.id.mapact_convertBtn);
			mBtnSet = (Button) mView.findViewById(R.id.mapact_setBtn);
			mBtnZoomIn = (ImageButton) mView.findViewById(R.id.btnMapSearchZoomIn);
			mBtnZoomOut = (ImageButton) mView.findViewById(R.id.btnMapSearchZoomOut);

			mTmapData = new TMapData();

			setUpMapIfNeeded();
		} catch(InflateException e) {
			e.printStackTrace();
		}

		return mView;
	}

	private void setUpMapIfNeeded() {
		try {
			if (mGoogleMap == null) {
				mGoogleMap = ((SupportMapFragment) MainActivity.mSupportFragmentManager.findFragmentById(R.id.map_search_map)).getMap();
				if (mGoogleMap != null) {
					setUpMap();
				}
			}

//			if (mGoogleMap == null) {
//				SupportMapFragment mapFragment = (SupportMapFragment) MainActivity.mSupportFragmentManager.findFragmentById(R.id.map_search_map);
//				mapFragment.getMapAsync(this);
//			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

//	@Override
//	public void onMapReady(GoogleMap googleMap) {
//		try {
//			mGoogleMap = googleMap;
//			setUpMap();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	private void setUpMap() {
		try {
			mGoogleMap.getUiSettings().setRotateGesturesEnabled(false);
			mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
			mGoogleMap.getUiSettings().setCompassEnabled(false);
			mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mCurrentPosition, 15));

			setEventListener();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void closeFragment() {
		try {
			mMainActivity.getFragmentManager().beginTransaction().remove(MapSearchFragment.this).commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setEventListener() {
		try {
			mBtnConvert.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					closeFragment();
				}
			});

			mBtnSet.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					new ConvertGpsToAddressTask().execute();
				}
			});

			mBtnZoomIn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					mGoogleMap.animateCamera(CameraUpdateFactory.zoomIn());
				}
			});

			mBtnZoomOut.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					mGoogleMap.animateCamera(CameraUpdateFactory.zoomOut());
				}
			});

			mGoogleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
				@Override
				public void onCameraChange(CameraPosition cameraPosition) {
					mCurrentPosition = cameraPosition.target;
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	class ConvertGpsToAddressTask extends AsyncTask<Void, Integer, String> {
		@Override
		protected void onPreExecute() {

		}

		@Override
		protected String doInBackground(Void... params) {
			String resultString = "";

			try {
				String result = mTmapData.convertGpsToAddress(mCurrentPosition.latitude, mCurrentPosition.longitude);
				String[] splitedResult = result.split(" ");

				for (int i = 2; i < splitedResult.length; i++) {
					resultString += (splitedResult[i] + " ");
				}

//				SVUtil.log("error", TAG, "result : " + result);
			} catch (IOException | ParserConfigurationException | SAXException e) {
				e.printStackTrace();
			}

			return resultString;
		}

		@Override
		protected void onPostExecute(String _addressStr) {
			try {
				if(mIsFinishFragment) {
					return;
				}

				Intent intent = new Intent();
				intent.putExtra("address", _addressStr);
				intent.putExtra("lat", mCurrentPosition.latitude);
				intent.putExtra("lng", mCurrentPosition.longitude);
				intent.putExtra("searchType", mSearchType);

				getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);

				closeFragment();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void destroyView() {
		try {
			if(mGoogleMap != null) {
				mGoogleMap.clear();
				mGoogleMap = null;
			}

			SupportMapFragment f = (SupportMapFragment) MainActivity.mSupportFragmentManager.findFragmentById(R.id.map_search_map);
			if(f != null) {
				MainActivity.mSupportFragmentManager.beginTransaction().remove(f).commitAllowingStateLoss();
			}

			if(mView != null){
				ViewGroup parent = (ViewGroup) mView.getParent();
				if(parent!=null){
					parent.removeView(mView);
				}
			}

			closeFragment();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onStart() {
		super.onStart();

		mIsFinishFragment = false;
	}

	@Override
	public void onPause() {
		super.onPause();

		mIsFinishFragment = true;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();

		destroyView();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		setTargetFragment(null, -1);
	}
}
