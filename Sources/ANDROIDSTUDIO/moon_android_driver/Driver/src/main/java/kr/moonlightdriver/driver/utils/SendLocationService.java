package kr.moonlightdriver.driver.utils;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.telephony.TelephonyManager;

import com.google.android.gms.maps.model.LatLng;
import com.loopj.android.http.RequestParams;

import java.util.Calendar;

import kr.moonlightdriver.driver.Network.NetApi.NetAPI;
import kr.moonlightdriver.driver.Network.NetApi.NetResponse;
import kr.moonlightdriver.driver.Network.NetApi.NetResponseCallback;
import kr.moonlightdriver.driver.Network.NetApi.NoResponseDataAck;
import kr.moonlightdriver.driver.Network.NetClient.NetClient;
import kr.moonlightdriver.driver.Network.NetData.ResultData;

/**
 * Created by youngmin on 2016-09-02.
 */
public class SendLocationService extends Service {
	private static final String TAG = "SendLocationService";
	public static final String ACTION = "CHANGE_LOCATION";
	public static final String CHECK_PERMISSION_REQ = "CHECK_PERMISSION_REQ";
	private LocationManager mLocationManager = null;
	private String mPhoneNumber = null;
	private String mDriverId = "";
	private LatLng mLastUpdateLocation;
	private double mLastUpdateLocationTime = 0;
	private Context mContext;
	private boolean mIsCheckGpsUpdateTime;

	private class LocationListener implements android.location.LocationListener {
		public LocationListener(String provider) {
		}

		@Override
		public void onLocationChanged(Location location) {
			try {
				SVUtil.log("error", TAG, "onLocationChanged lat: " + location.getLatitude() + ", lng : " + location.getLongitude());

				sendDataToMain(location.getLatitude(), location.getLongitude(), location.getAltitude());
				try {
					BeaconService.mNewlocation = new LatLng(location.getLatitude(), location.getLongitude());
				}catch (Exception e){

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		public void onProviderDisabled(String provider) {
		}

		@Override
		public void onProviderEnabled(String provider) {
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
		}
	}

	LocationListener[] mLocationListeners = new LocationListener[] {
			new LocationListener(LocationManager.GPS_PROVIDER),
			new LocationListener(LocationManager.NETWORK_PROVIDER)
	};

	private void setLocationUpdateTime(LatLng _newLocation) {
		try {
			if(mLastUpdateLocation != null && mLastUpdateLocationTime >= 0) {
				Calendar calendar = Calendar.getInstance();
				double nowTime = calendar.getTimeInMillis();
				double diffTimeInSeconds = ((nowTime - mLastUpdateLocationTime) / 1000d) / 3600d;

				float[] dist = new float[3];
				Location.distanceBetween(mLastUpdateLocation.latitude, mLastUpdateLocation.longitude, _newLocation.latitude, _newLocation.longitude, dist);

				double distance = dist[0] / 1000;

				double kmPerHour =  distance / diffTimeInSeconds;

				long uploadTime = SVUtil.getLocationUpdateTime(kmPerHour);

				SVUtil.log("error", TAG, "distance : " + distance + ", diffTimeInSeconds : " + diffTimeInSeconds + ", kmPerHour : " + kmPerHour + ", uploadTime : " + uploadTime + ", mIsCheckGpsUpdateTime : " + mIsCheckGpsUpdateTime);

				if(uploadTime > 1000 && mIsCheckGpsUpdateTime) {
					stopLocationUpdates();

					new android.os.Handler().postDelayed(
							new Runnable() {
								public void run() {
									startLocationUpdates();
								}
							}, uploadTime);
				}
			}

			Calendar calendar1 = Calendar.getInstance();
			mLastUpdateLocationTime = calendar1.getTimeInMillis();
			mLastUpdateLocation = new LatLng(_newLocation.latitude, _newLocation.longitude);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		super.onStartCommand(intent, flags, startId);

		try {
			mIsCheckGpsUpdateTime = true;
			mContext = getApplicationContext();

			if(intent == null) {
				sendLogout();
			} else {
				mIsCheckGpsUpdateTime = false;
				new android.os.Handler().postDelayed(
						new Runnable() {
							public void run() {
								mIsCheckGpsUpdateTime = true;
							}
						}, 30000);
			}

			initializeLocationManager();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return START_STICKY;
	}

	@Override
	public void onCreate() {
	}

	private boolean checkLocationPermission() {
//		try {
//			if(Build.VERSION.SDK_INT >= 23) {
//				if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
//						|| ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//					return false;
//				}
//			}
//		} catch(Exception e) {
//			e.printStackTrace();
//			return false;
//		}

		return true;
	}

	private boolean checkPhoneNumberPermission() {
//		try {
//			if(Build.VERSION.SDK_INT >= 23) {
//				if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED
//						|| ContextCompat.checkSelfPermission(mContext, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED) {
//					return false;
//				}
//			}
//		} catch(Exception e) {
//			e.printStackTrace();
//			return false;
//		}

		return true;
	}

	private void updateLastKnownLocation() {
		try {
			if(checkLocationPermission()) {
				Location last_location = null;

				if (mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER) != null) {
					last_location = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
				} else if (mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER) != null) {
					last_location = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
				}

				if (last_location != null) {
					sendDataToMain(last_location.getLatitude(), last_location.getLongitude(), last_location.getAltitude());
				}
			} else {
				reqCheckPermissionToMain("location_permission");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String getDriverId() {
		String driverId = "";

		try {
			SharedPreferences prefs = mContext.getSharedPreferences("moonlightdriver", Context.MODE_PRIVATE);
			driverId = prefs.getString("driverId", "");
		} catch (Exception e) {
			e.printStackTrace();
		}

		return driverId;
	}

	private void sendDataToMain(double _lat, double _lng, double _alt) {
		try {
			Intent intent = new Intent();
			intent.setAction(SendLocationService.ACTION);
			intent.putExtra("lat", _lat);
			intent.putExtra("lng", _lng);
			intent.putExtra("alt", _alt);

			sendBroadcast(intent);

			if(mDriverId.isEmpty()) {
				mDriverId = getDriverId();
			}

			if(checkPhoneNumberPermission()) {
				TelephonyManager tMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
				mPhoneNumber = tMgr.getLine1Number();

				if(!mDriverId.isEmpty() && mPhoneNumber != null && !mPhoneNumber.isEmpty()) {
					updateCurrentLocation(new LatLng(_lat, _lng));
				}
			} else {
				reqCheckPermissionToMain("phone_number_permission");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void reqCheckPermissionToMain(String _permissionType) {
		try {
			Intent intent = new Intent();
			intent.setAction(SendLocationService.CHECK_PERMISSION_REQ);
			intent.putExtra("permissionType", _permissionType);

			sendBroadcast(intent);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void updateCurrentLocation(LatLng _newLocation) {
		try {
			if(mLastUpdateLocation == null) {
				updateDriverLocation(_newLocation);
			} else {
				float[] dist = new float[2];
				Location.distanceBetween(mLastUpdateLocation.latitude, mLastUpdateLocation.longitude, _newLocation.latitude, _newLocation.longitude, dist);

				if (dist[0] >= Global.UPDATE_DISTANCE) {
					updateDriverLocation(_newLocation);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void sendLogout() {
		try {
			if(mDriverId.isEmpty()) {
				mDriverId = getDriverId();
			}

			if(mDriverId.isEmpty()) {
				return;
			}

			RequestParams params = new RequestParams();
			params.add("driverId", mDriverId);

			SVUtil.log("error", TAG, "sendLogout() from service");
			NetClient.send(mContext, Global.URL_DRIVER_LOGOUT, "POST", params, new NoResponseDataAck(), new NetResponseCallback(new NetResponse() {
				@Override
				public void onResponse(NetAPI _netAPI) {
				}
			}));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void updateDriverLocation(final LatLng _newLocation) {
		try {
			RequestParams params = new RequestParams();
			params.add("driverId", mDriverId);
			params.add("phone", mPhoneNumber);
			params.add("lat", _newLocation.latitude + "");
			params.add("lng", _newLocation.longitude + "");

			NetClient.send(mContext, Global.URL_UPDATE_DRIVER_LOCATION, "POST", params, new NoResponseDataAck(), new NetResponseCallback(new NetResponse() {
				@Override
				public void onResponse(NetAPI _netAPI) {
					try {
						if (_netAPI != null) {
							NoResponseDataAck retNetApi = (NoResponseDataAck) _netAPI;
							ResultData result = retNetApi.getmResultData();

							if (result.getmCode() == 100) {
								setLocationUpdateTime(_newLocation);
							} else {
								SVUtil.log("error", TAG, result.getmDetail());
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void stopLocationUpdates() {
		try {
			if (mLocationManager != null) {
				if(checkLocationPermission()) {
					for (LocationListener item : mLocationListeners) {
						mLocationManager.removeUpdates(item);
					}
				} else {
					reqCheckPermissionToMain("location_permission");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void startLocationUpdates() {
		try {
			if(checkLocationPermission()) {
				mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, Global.REQ_LOCATION_TIME, Global.REQ_LOCATION_DISTANCE, mLocationListeners[1]);
				mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, Global.REQ_LOCATION_TIME, Global.REQ_LOCATION_DISTANCE, mLocationListeners[0]);

				updateLastKnownLocation();
			} else {
				reqCheckPermissionToMain("location_permission");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		try {
			stopLocationUpdates();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void initializeLocationManager() {
		try {
			if (mLocationManager == null) {
				mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			}

			startLocationUpdates();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
