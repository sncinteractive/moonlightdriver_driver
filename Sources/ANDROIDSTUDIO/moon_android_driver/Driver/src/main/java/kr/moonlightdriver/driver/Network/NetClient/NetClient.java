package kr.moonlightdriver.driver.Network.NetClient;

import android.content.Context;
import android.content.DialogInterface;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.io.IOException;

import kr.moonlightdriver.driver.BuildConfig;
import kr.moonlightdriver.driver.Network.NetApi.NetAPI;
import kr.moonlightdriver.driver.Network.NetApi.NetResponseCallback;
import kr.moonlightdriver.driver.utils.SVUtil;

/**
 * Created by youngmin on 2015-10-13.
 */
public class NetClient {
	private static final String TAG = "NetClient";

	private static AsyncHttpClient mAsyncHttpClient;

	public static class NetMessageContainer {

		public Context mContext;
		public String mUrl;
		public String mMethod;
		public RequestParams mRequestParams;
		public NetAPI mNetApiAck;
		public NetResponseCallback mCallback;
		public NetMessageContainer(Context _context, String _url, String _method, RequestParams _requestParams, NetAPI _netApiAck, NetResponseCallback _callback) {
			this.mContext = _context;
			this.mUrl = _url;
			this.mMethod = _method;
			this.mRequestParams = _requestParams;
			this.mNetApiAck = _netApiAck;
			this.mCallback = _callback;
		}

	}
	public static AsyncHttpClient getAsyncHttpClient() {
		try {
			if(mAsyncHttpClient == null) {
				mAsyncHttpClient = new AsyncHttpClient();
			}

			if(BuildConfig.DEBUG) {
				mAsyncHttpClient.setLoggingEnabled(true);
			} else {
				mAsyncHttpClient.setLoggingEnabled(false);
			}

			mAsyncHttpClient.setLoggingEnabled(false);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return mAsyncHttpClient;
	}

	public static void send(Context _context, String _url, String _method, RequestParams _requestParams, NetAPI _netApiAck, NetResponseCallback _callback) {
		try {
			if(_method.toLowerCase().equals("get")) {
				NetClient.get(new NetMessageContainer(_context, _url, _method, _requestParams, _netApiAck, _callback));
			} else if(_method.toLowerCase().equals("post")) {
				NetClient.post(new NetMessageContainer(_context, _url, _method, _requestParams, _netApiAck, _callback));
			} else {
				_callback.onResponse(null);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void get(final NetMessageContainer _netMessageContainer) {
		AsyncHttpClient asyncHttpClient = getAsyncHttpClient();

		asyncHttpClient.get(_netMessageContainer.mUrl, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
				NetAPI retNetApi = null;
				try {
					ObjectMapper mapper = new ObjectMapper();
					JsonParser jp = mapper.getFactory().createParser(responseBody);
					retNetApi = mapper.readValue(jp, _netMessageContainer.mNetApiAck.getClass());

					jp.close();
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					_netMessageContainer.mCallback.onResponse(retNetApi);
				}
			}

			@Override
			public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
				SVUtil.log("error", TAG, "ERROR URL : " + _netMessageContainer.mUrl);
				error.printStackTrace();

				if (error instanceof java.net.SocketTimeoutException) {
					SVUtil.showTimeoutDialog(_netMessageContainer.mContext);
				} else {
					SVUtil.showDialogWithListener(_netMessageContainer.mContext, "알수없는 오류가 발생했습니다.\n다시 시도해 주십시요.", "확인", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							_netMessageContainer.mCallback.onResponse(null);
						}
					});
				}
			}
		});
	}

	public static void post(final NetMessageContainer _netMessageContainer) {
		AsyncHttpClient asyncHttpClient = getAsyncHttpClient();

		asyncHttpClient.post(_netMessageContainer.mUrl, _netMessageContainer.mRequestParams, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
				NetAPI retNetApi = null;

				try {
					ObjectMapper mapper = new ObjectMapper();
					JsonParser jp = mapper.getFactory().createParser(responseBody);
					retNetApi = mapper.readValue(jp, _netMessageContainer.mNetApiAck.getClass());
					jp.close();
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					_netMessageContainer.mCallback.onResponse(retNetApi);
				}
			}

			@Override
			public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
				SVUtil.log("error", TAG, "ERROR URL : " + _netMessageContainer.mUrl + ", PARAMS : " + _netMessageContainer.mRequestParams.toString());

				error.printStackTrace();

				if (error instanceof java.net.SocketTimeoutException) {
					SVUtil.showTimeoutDialog(_netMessageContainer.mContext);
				} else {
					SVUtil.showDialogWithListener(_netMessageContainer.mContext, "알수없는 오류가 발생했습니다.\n다시 시도해 주십시요.", "확인", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							_netMessageContainer.mCallback.onResponse(null);
						}
					});
				}
			}
		});
	}
}
