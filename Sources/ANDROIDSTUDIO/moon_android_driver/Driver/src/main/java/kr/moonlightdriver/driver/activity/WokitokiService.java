package kr.moonlightdriver.driver.activity;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;

import org.java_websocket.exceptions.WebsocketNotConnectedException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.Timer;
import java.util.TimerTask;

import kr.moonlightdriver.driver.R;
import kr.moonlightdriver.driver.utils.Global;
import kr.moonlightdriver.driver.utils.SVUtil;
import kr.moonlightdriver.driver.utils.SockJSImpl;
import kr.moonlightdriver.driver.utils.WebSocketUtil;

/**
 * Created by lk on 2016. 10. 14..
 */

public class WokitokiService extends Service {

    private final String TAG = "WokitokiService";
    private final int WOKITOKI_STABLE = 0;
    private final int WOKITOKI_PLAY = 1;
    private final int WOKITOKI_CHECK = 9;
    private final int RECORDER_SAMPLERATE = 8000;
    private final int RECORDER_CHANNELS_OUT = AudioFormat.CHANNEL_OUT_MONO;

    private PhoneStateCheckListener phoneCheckListener;
    private String mShuttleId;
    private String mDriverId;
    private Boolean mIsPrivate;
    private String mOtherDriverId;
    private String mShuttleName;
    boolean isMute = false;

    private SockJSImpl sockJS;
    private Timer timer;

    private NotificationCompat.Builder builder;
    private NotificationManager manager;

    private boolean wListenStarted = false;
    private int wWokitokiStateCheck = 0;

    private byte[] buffer;
    int overallBytes;

    private int minBufferSize = AudioTrack.getMinBufferSize(RECORDER_SAMPLERATE, RECORDER_CHANNELS_OUT, AudioFormat.ENCODING_PCM_16BIT);
    private AudioTrack player = new AudioTrack(AudioManager.STREAM_VOICE_CALL, 8000, RECORDER_CHANNELS_OUT, AudioFormat.ENCODING_PCM_16BIT, minBufferSize * 4, AudioTrack.MODE_STREAM);


    Timer timer2 = new Timer();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
	        //SVUtil.log("error", TAG, "onStartCommand");
	        mHandler.sendEmptyMessage(0);

	        getRoomInfos();
	        resetWokitoki();
	        connectSockJS();
	        startDeadlineTimer();
	        setPhoneCallListener();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return START_REDELIVER_INTENT;
    }

    private void setPhoneCallListener() {
	    try {
		    phoneCheckListener = new PhoneStateCheckListener(this);

		    TelephonyManager telephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
		    telephonyManager.listen(phoneCheckListener, PhoneStateListener.LISTEN_CALL_STATE);
	    } catch (Exception e) {
		    e.printStackTrace();
	    }
    }

	private void startDeadlineTimer() {
		try {
			timer2 = new Timer();
			timer2.schedule(new TimerTask() {
				@Override
				public void run() {
					Intent popupIntent = new Intent(getApplicationContext(), ServiceExtenstionDialog.class);
					PendingIntent pie = PendingIntent.getActivity(getApplicationContext(), 0, popupIntent, PendingIntent.FLAG_ONE_SHOT);

					try {
						pie.send();
					} catch (PendingIntent.CanceledException e) {
						e.printStackTrace();
					}

					if(sockJS != null) {
						sockJS.isLive = false;
					}

					timer.cancel();
					sockJS = null;
					Message msg1 = mHandler.obtainMessage();
					msg1.what = 11;
					msg1.obj = "접속이 종료되었습니다. 눌러서 다시 접속할 수 있습니다";
					mHandler.sendMessage(msg1);

					stopForeground(true);
					stopSelf();
				}
			}, 900000);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private void getRoomInfos() {
	    try {
		    SharedPreferences pref = getSharedPreferences("pref", MODE_PRIVATE);

		    mShuttleId = pref.getString("mShuttleId", "");
		    mDriverId = pref.getString("mDriverId", "");
		    mShuttleName = pref.getString("mShuttleName", "");
            mIsPrivate = pref.getBoolean("isPrivate", false);
            mOtherDriverId = pref.getString("otherDriverId", "");

	    } catch (Exception e) {
		    e.printStackTrace();
	    }
    }

    private void connectSockJS() {
        SVUtil.log("error", TAG,"connectSockJS");
        try {
            sockJS = new SockJSImpl(Global.WOKITIKI_SERVER_HOST + "/eventbus", mShuttleId, mDriverId, false) {
                @Override
                public void scheduleHeartbeat() {
	                try {
		                timer = new Timer();
		                timer.schedule(new TimerTask() {
			                @Override
			                public void run() {
				                String ping = "[\"{\\\"type\\\":\\\"ping\\\"}\"]";
				                try {
					                send(ping);
					                scheduleHeartbeat();
				                } catch (WebsocketNotConnectedException e) {
					                e.printStackTrace();
//                                SVUtil.showSimpleDialog(mMainActivity, "인터넷 연결이 불안정합니다.");
//                                mMainActivity.replaceFragment(new ShuttleFragment(), R.id.mainFrameLayout, Global.FRAG_SHUTTLE_TAG, true);
					                sockJS.isLive = false;
					                sockJS = null;
					                timer.cancel();
					                connectSockJS();
				                }

				                if (wWokitokiStateCheck == WOKITOKI_PLAY) {
					                wWokitokiStateCheck = WOKITOKI_CHECK;
				                } else if (wWokitokiStateCheck == WOKITOKI_CHECK) {
					                resetWokitoki();
				                }
			                }
		                }, 5000);
	                } catch (Exception e) {
		                e.printStackTrace();
	                }
                }

                @Override
                public void parseSockJS(String s) {
                    try {
                        s = s.replace("\\\"", "\"");
                        s = s.replace("\\\\", "\\");
                        s = s.substring(3, s.length() - 2); // a[" ~ "] 없애기
//                        SVUtil.log("error", TAG, "Reci : " + s);
                        JSONObject json = new JSONObject(s);
                        String msgType = json.getString("type");
                        if (!msgType.equals("err")) {
                            JSONObject body = new JSONObject(json.getString("body"));
                            String bodyType = body.getString("type");
                            String msg = body.getString("msg");
                            String senderId = body.getString("senderId");
                            String nickName = body.getString("senderNick");
                            boolean isPrivateMsg = body.getBoolean("isPrivateMsg");

//                            SVUtil.log("error", TAG, "isPlayCondition : " + isPlayCondition(isPrivateMsg, senderId));

                            if (isPlayCondition(isPrivateMsg, senderId)) {
                                switch (bodyType) {
                                    case "normal":
                                        playWokitoki(msg, senderId, nickName);
                                        break;
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                private void playWokitoki(String msg, String senderId, String nickName) {
                    try {
//		                SVUtil.log("error", TAG, "playWokitoki() my nick : " + mMainActivity.mDriverInfo.mDriverId() + " / " + senderId);

                        msg = WebSocketUtil.toSt(msg);

                        if (msg.equals("start")) {
                            initWokitoki();
                            Message msg1 = mHandler.obtainMessage();
                            msg1.what = 10;
                            msg1.obj = nickName + "님이 대화중입니다.";
                            mHandler.sendMessage(msg1);
                        }

                        buffer = Base64.decode(msg, Base64.URL_SAFE);

                        overallBytes += player.write(buffer, 0, buffer.length);

                        wWokitokiStateCheck = WOKITOKI_PLAY;

//		                SVUtil.log("error", TAG, "started " + wListenStarted + "  /  overallBytes : " + overallBytes);
                        if (!wListenStarted && overallBytes > minBufferSize) {
                            playSound();
                            Log.d(TAG, "재생 시작");
                        }

                        if (msg.equals("stop")) {
                            resetWokitoki();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                private void initWokitoki() {
                    try {
//		                SVUtil.log("error", TAG, "WokiToki Start");

                        resetWokitoki();
                        overallBytes = 0;

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                private void playSound() {
                    try {
                        player.play();
                        player.setPlaybackPositionUpdateListener(new AudioTrack.OnPlaybackPositionUpdateListener() {
                            @Override
                            public void onMarkerReached(AudioTrack track) {

                            }

                            @Override
                            public void onPeriodicNotification(AudioTrack track) {
                                resetWokitoki();
                            }
                        });

                        wListenStarted = true;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            sockJS.connectBlocking();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean isPlayCondition(boolean isPrivateMsg, String senderId) {
        try {
            //SVUtil.log("error", TAG, "isPlayCondition() senderId = " + senderId + " / myId  = " + mMainActivity.mDriverInfo.mDriverId() + "isMute = " +isMute + " / isPrivate = " + isPrivate + " / isPrivateMSG = " + isPrivateMsg);

            if (senderId.equals(mDriverId + "")) {
                return false;
            }

            if (isMute) {
                return false;
            }
            if (mIsPrivate) {       // 내가 비밀 모드일 떄
                return isPrivateMsg && mOtherDriverId.equals(senderId);
            } else { // 아닐 때 전체모드 일떄
            return !isPrivateMsg;        // todo 반전시켜 주어야 함
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    private void resetWokitoki() {
        try {
            resetPlayer();
            resetCheckCodes();
            Message msg1 = mHandler.obtainMessage();
            msg1.what = 10;
            msg1.obj = "워키토키 방이 실행중입니다 (" + mShuttleName + ")";
            mHandler.sendMessage(msg1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void resetCheckCodes() {
        try {
            wWokitokiStateCheck = WOKITOKI_STABLE;
            wListenStarted = false;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void resetPlayer() {
        try {
            if (player != null) {
                player.flush();
                player.stop();
                player.release();
            }

            player = new AudioTrack(AudioManager.STREAM_VOICE_CALL, 8000, RECORDER_CHANNELS_OUT, AudioFormat.ENCODING_PCM_16BIT, minBufferSize * 4, AudioTrack.MODE_STREAM);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
	        try {
		        if (builder == null) {
			        manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

			        builder = new NotificationCompat.Builder(WokitokiService.this);
			        builder.setSmallIcon(R.drawable.ic_launcher)
					        .setContentTitle("달빛기사")
					        .setContentText("워키토키 방이 실행중입니다 (" + mShuttleName + ")"); // 알림바에서 자동 삭제;
			        builder.setAutoCancel(false);
			        builder.setOngoing(true);
			        Intent intent = new Intent(getApplicationContext(), ServiceExtenstionDialog.class);
			        PendingIntent pIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
			        builder.setContentIntent(pIntent);
			        manager.notify(1, builder.build());
		        }

		        if (msg.what == 10) {
			        builder.setContentText((String) msg.obj);
			        builder.setAutoCancel(false);
			        builder.setOngoing(true);
			        manager.notify(1, builder.build());
		        }

		        if (msg.what == 11) {
			        builder.setContentText((String) msg.obj);
			        builder.setAutoCancel(true);
			        builder.setOngoing(false);
			        manager.notify(1, builder.build());
		        }
	        } catch (Exception e) {
		        e.printStackTrace();
	        }
        }
    };

    @Override
    public void onDestroy() {
        SVUtil.log("error", TAG,"onDestroy 실행");

        try {
	        resetWokitoki();
	        timer.cancel();

	        if (sockJS != null) {
		        sockJS.isLive = false;
		        sockJS = null;
	        }
        } catch (Exception e) {
	        e.printStackTrace();
        }

        Message msg1 = mHandler.obtainMessage();
        msg1.what = 11;
        msg1.obj = "접속이 종료되었습니다. 눌러서 다시 접속할 수 있습니다";
        mHandler.sendMessage(msg1);
    }

    public class PhoneStateCheckListener extends PhoneStateListener {
        WokitokiService mainService;

        PhoneStateCheckListener(WokitokiService _main) {
            this.mainService = _main;
        }

        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
	        try {
		        SVUtil.log("error", TAG, "state : " + state);
		        if (state == TelephonyManager.CALL_STATE_IDLE) {
			        AudioManager am = (AudioManager) mainService.getSystemService(MainActivity.AUDIO_SERVICE);
			        am.setSpeakerphoneOn(true);
			        isMute = false;
		        } else if (state == TelephonyManager.CALL_STATE_RINGING) {
			        isMute = true;
		        } else if (state == TelephonyManager.CALL_STATE_OFFHOOK) {
			        isMute = true;
		        }
	        } catch (Exception e) {
		        e.printStackTrace();
	        }
        }
    }
}
