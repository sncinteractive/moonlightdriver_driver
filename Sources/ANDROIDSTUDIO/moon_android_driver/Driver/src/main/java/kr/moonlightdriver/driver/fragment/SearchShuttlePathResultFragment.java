package kr.moonlightdriver.driver.fragment;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.ui.IconGenerator;
import com.loopj.android.http.RequestParams;
import com.skt.Tmap.TMapData;
import com.skt.Tmap.TMapPoint;
import com.skt.Tmap.TMapPolyLine;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import kr.moonlightdriver.driver.Network.NetApi.NetAPI;
import kr.moonlightdriver.driver.Network.NetApi.NetResponse;
import kr.moonlightdriver.driver.Network.NetApi.NetResponseCallback;
import kr.moonlightdriver.driver.Network.NetApi.NoResponseDataAck;
import kr.moonlightdriver.driver.Network.NetApi.ShuttleTransferResultAck;
import kr.moonlightdriver.driver.Network.NetClient.NetClient;
import kr.moonlightdriver.driver.Network.NetData.LocationData;
import kr.moonlightdriver.driver.Network.NetData.ResultData;
import kr.moonlightdriver.driver.Network.NetData.ShuttleTransferData;
import kr.moonlightdriver.driver.Network.NetData.ShuttleTransferLocationData;
import kr.moonlightdriver.driver.Network.NetData.ShuttleTransferPathData;
import kr.moonlightdriver.driver.Network.NetData.TransferPathShuttleInfoData;
import kr.moonlightdriver.driver.R;
import kr.moonlightdriver.driver.activity.MainActivity;
import kr.moonlightdriver.driver.utils.Global;
import kr.moonlightdriver.driver.utils.SVUtil;

/**
 * Created by redjjol on 23/10/14.
 */
public class SearchShuttlePathResultFragment extends Fragment {
	private static final String TAG = "SearchShuttlePathResultFragment";
	private static final float DEFAULT_ZOOM = 15f;

	private View mView;
	private GoogleMap mGoogleMap;

	private MainActivity mMainActivity;

	private boolean mIsFinishFragment;

	private Marker mCurrentPositionMarker;
	private LatLng mCurrentMyPosition;

	private LocationData mDepartFromLocation;
	private LocationData mArriveAtLocation;

	private ImageButton mBtnSearchResultZoomIn;
	private ImageButton mBtnSearchResultZoomOut;
	private ImageButton mBtnSearchResultCurrentPosition;
	private ImageButton mBtnSearchReset;

	private LinearLayout mShttleTransferResultLayout;
	private LinearLayout mShuttleTransferTabHost;
	private LinearLayout mShuttleTransferTransferLocationLayout;
	private TextView mShuttleTransferDepartAddress;
	private TextView mShuttleTransferArriveAddress;
	private TextView mShuttleTransferStartLocation;
	private TextView mShuttleTransferTransferLocation;
	private TextView mShuttleTransferEndLocation;

	private ArrayList<ShuttleTransferData> mShuttleTransferResultData;
	private ArrayList<Polyline> mShuttleTransferPolyLineList;
	private ArrayList<Marker> mShuttleTransferMarkerList;

	private String mVirtualNumber = "";
	private boolean mIsSecretCalling;
	private PushAlertReceiver mPushAlertReceiver;

	private LatLngBounds.Builder mBuilder;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		try {
			mView = inflater.inflate(R.layout.frag_search_shuttle_path_result, container, false);

			mMainActivity = (MainActivity) getActivity();

			mIsFinishFragment = false;
			mIsSecretCalling = false;

			mCurrentMyPosition = mMainActivity.mCurrentLocation;

			mShuttleTransferPolyLineList = new ArrayList<>();
			mShuttleTransferMarkerList = new ArrayList<>();

			Bundle bundle = SearchShuttlePathResultFragment.this.getArguments();
			mDepartFromLocation = new LocationData(bundle.getString("departFromAddress", ""), new LatLng(bundle.getDouble("departFromLat", 0), bundle.getDouble("departFromLng", 0)));
			mArriveAtLocation = new LocationData(bundle.getString("arriveAtAddress", ""), new LatLng(bundle.getDouble("arriveAtLat", 0), bundle.getDouble("arriveAtLng", 0)));

			if(mDepartFromLocation.getmLocationName().isEmpty() || mDepartFromLocation.getmPosition().latitude == 0 || mDepartFromLocation.getmPosition().longitude == 0
					|| mArriveAtLocation.getmLocationName().isEmpty() || mArriveAtLocation.getmPosition().latitude == 0 || mArriveAtLocation.getmPosition().longitude == 0) {
				SVUtil.showDialogWithListener(mMainActivity, "잘못된 요청입니다.", "확인", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						closeFragment();
					}
				});
			}

			mPushAlertReceiver = new PushAlertReceiver();
			IntentFilter intentFilter = new IntentFilter();
			intentFilter.addAction(ShuttleChattingFragment.PUSH_MSG_5);
			intentFilter.addAction(ShuttleChattingFragment.PUSH_MSG_6);

			getActivity().registerReceiver(mPushAlertReceiver, intentFilter);

			initView();

			setUpMapIfNeeded();
		} catch(InflateException e) {
			e.printStackTrace();
		}

		return mView;
	}

	private void initView() {
		try {
			mBtnSearchResultZoomIn = (ImageButton) mView.findViewById(R.id.btnSearchResultZoomIn);
			mBtnSearchResultZoomOut = (ImageButton) mView.findViewById(R.id.btnSearchResultZoomOut);
			mBtnSearchResultCurrentPosition = (ImageButton) mView.findViewById(R.id.btnSearchResultCurrentPosition);
			mBtnSearchReset = (ImageButton) mView.findViewById(R.id.btnSearchReset);

			mShttleTransferResultLayout = (LinearLayout) mView.findViewById(R.id.shuttleTransferResultLayout);
			mShttleTransferResultLayout.setVisibility(View.GONE);
			mShuttleTransferTabHost = (LinearLayout) mView.findViewById(R.id.shuttleTransferTabHost);
			mShuttleTransferTransferLocationLayout = (LinearLayout) mView.findViewById(R.id.shuttleTransferTransferLocationLayout);
			mShuttleTransferTransferLocationLayout.setVisibility(View.GONE);
			mShuttleTransferDepartAddress = (TextView) mView.findViewById(R.id.shuttleTransferDepartAddress);
			mShuttleTransferArriveAddress = (TextView) mView.findViewById(R.id.shuttleTransferArriveAddress);

			mShuttleTransferStartLocation = (TextView) mView.findViewById(R.id.shuttleTransferStartLocation);
			mShuttleTransferTransferLocation = (TextView) mView.findViewById(R.id.shuttleTransferTransferLocation);
			mShuttleTransferEndLocation = (TextView) mView.findViewById(R.id.shuttleTransferEndLocation);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setUpMapIfNeeded() {
		try {
			if (mGoogleMap == null) {
				mGoogleMap = ((SupportMapFragment) MainActivity.mSupportFragmentManager.findFragmentById(R.id.searchShuttlePathResultMap)).getMap();
				if (mGoogleMap != null) {
					setUpMap();
				}
			}

//			if (mGoogleMap == null) {
//				SupportMapFragment mapFragment = (SupportMapFragment) MainActivity.mSupportFragmentManager.findFragmentById(R.id.map_search_map);
//				mapFragment.getMapAsync(this);
//			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

//	@Override
//	public void onMapReady(GoogleMap googleMap) {
//		try {
//			mGoogleMap = googleMap;
//			setUpMap();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	private void setUpMap() {
		try {
			mGoogleMap.getUiSettings().setRotateGesturesEnabled(false);
			mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
			mGoogleMap.getUiSettings().setCompassEnabled(false);

			mCurrentPositionMarker = mGoogleMap.addMarker(new MarkerOptions().position(mCurrentMyPosition).icon(SVUtil.getMarkerIcon(getResources(), R.drawable.ic_navi_current_position)).anchor(0.5f, 0.5f));

			mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mCurrentMyPosition, DEFAULT_ZOOM));

			setEventListener();

			getSearchShuttlePathResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void getSearchShuttlePathResult() {
		try {
			mShuttleTransferDepartAddress.setText(mDepartFromLocation.getmLocationName());
			mShuttleTransferArriveAddress.setText(mArriveAtLocation.getmLocationName());

			SVUtil.showProgressDialog(mMainActivity, "셔틀경로 검색중...");

			RequestParams params = new RequestParams();
			params.add("depart_from_lat", mDepartFromLocation.getmPosition().latitude + "");
			params.add("depart_from_lng", mDepartFromLocation.getmPosition().longitude + "");
			params.add("arrive_at_lat", mArriveAtLocation.getmPosition().latitude + "");
			params.add("arrive_at_lng", mArriveAtLocation.getmPosition().longitude + "");

			NetClient.send(mMainActivity, Global.URL_SHUTTLE_TRANSFER, "POST", params, new ShuttleTransferResultAck(), new NetResponseCallback(new NetResponse() {
				@Override
				public void onResponse(NetAPI _netAPI) {
					SVUtil.hideProgressDialog();

					try {
						if (_netAPI != null && !mIsFinishFragment) {
							ShuttleTransferResultAck retNetApi = (ShuttleTransferResultAck) _netAPI;
							ResultData result = retNetApi.getmResultData();

							if (result.getmCode() != 100) {
								SVUtil.showDialogWithListener(mMainActivity, result.getmDetail(), "확인", new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										closeFragment();
									}
								});
							} else {
								mShttleTransferResultLayout.setVisibility(View.VISIBLE);

								mShuttleTransferResultData = retNetApi.getmShuttleTransferDataList();
								if(mShuttleTransferResultData.size() > 0) {
									drawTransferResultTab();
									drawTransferResult(0);
								}
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void clearTransferResult() {
		try {
			for (Marker m : mShuttleTransferMarkerList) {
				m.remove();
			}

			for(Polyline poly : mShuttleTransferPolyLineList) {
				poly.remove();
			}

			mShuttleTransferMarkerList.clear();
			mShuttleTransferPolyLineList.clear();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void drawTransferResultTab() {
		try {
			int tabCount = mShuttleTransferTabHost.getChildCount();
			for(int i = 0; i < tabCount; i++) {
				if(i < mShuttleTransferResultData.size()) {
					mShuttleTransferTabHost.getChildAt(i).setVisibility(View.VISIBLE);
					mShuttleTransferTabHost.getChildAt(i).setTag(i);
					mShuttleTransferTabHost.getChildAt(i).setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View view) {
							try {
								clearTransferResult();
								drawTransferResult(Integer.parseInt(view.getTag().toString()));
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					});
				}
			}

			mShuttleTransferTabHost.getChildAt(0).setSelected(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void drawPedestrianPolyline(int _tabIndex, double _start_lat, double _start_lng, double _end_lat, double _end_lng) {
		try {
			if(mShuttleTransferResultData.get(_tabIndex).getmPedestrianPathList().size() <= 0) {
				TMapPoint startPoint = new TMapPoint(_start_lat, _start_lng);
				TMapPoint endPoint = new TMapPoint(_end_lat, _end_lng);

				new PedestrianPathSearchTask(_tabIndex, startPoint, endPoint).execute();
			} else {
				for(ArrayList<LatLng> pathList : mShuttleTransferResultData.get(_tabIndex).getmPedestrianPathList()) {
					PolylineOptions opt = new PolylineOptions();
					opt.width(SVUtil.pxFromDp(mMainActivity, 5));
					opt.color(Color.BLUE);
					opt.addAll(pathList);

					mShuttleTransferPolyLineList.add(mGoogleMap.addPolyline(opt));

					for(LatLng path : pathList) {
						mBuilder.include(path);
					}
				}

				mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(mBuilder.build(), 120));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	class PedestrianPathSearchTask extends AsyncTask<Void, Integer, TMapPolyLine> {
		private int mDistance;
		private int mTime;
		private LatLng mEndPosition;
		private int mTabIndex;
		private TMapPoint mStart;
		private TMapPoint mEnd;

		public PedestrianPathSearchTask(int _tabIndex, TMapPoint _start, TMapPoint _end) {
			this.mTabIndex = _tabIndex;
			this.mStart = _start;
			this.mEnd = _end;
		}

		@Override
		protected void onPreExecute() {
			this.mDistance = 0;
			this.mTime = 0;
		}

		@Override
		protected TMapPolyLine doInBackground(Void... params) {
			TMapData data = new TMapData();

			try {
//				mEndPosition = new LatLng(this.mEnd.getLatitude(), this.mEnd.getLongitude());
//				Document doc = data.findPathDataAllType(TMapData.TMapPathType.PEDESTRIAN_PATH, this.mStart, this.mEnd);
//				NodeList childNodes = doc.getElementsByTagName("Document").item(0).getChildNodes();
//				int childSize = childNodes.getLength();
//
//				for(int i = 0; i < childSize; i++) {
//					Node childItem = childNodes.item(i);
//					if(childItem.getNodeName().equals("tmap:totalDistance")) {
//						mDistance = Integer.parseInt(childItem.getFirstChild().getNodeValue());
//					} else if (childItem.getNodeName().equals("tmap:totalTime")) {
//						mTime = Integer.parseInt(childItem.getFirstChild().getNodeValue());
//					}
//				}

				return data.findPathDataWithType(TMapData.TMapPathType.PEDESTRIAN_PATH, this.mStart, this.mEnd);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(TMapPolyLine result) {
			try {
				if(mIsFinishFragment) {
					return;
				}

				if (result != null && mGoogleMap != null && mView != null) {
					ArrayList<TMapPoint> tmapPolylinePath = result.getLinePoint();
					ArrayList<LatLng> googlePolylinePath = new ArrayList<>();

					int pathSize = tmapPolylinePath.size();

					googlePolylinePath.add(new LatLng(this.mStart.getLatitude(), this.mStart.getLongitude()));
					mBuilder.include(new LatLng(this.mStart.getLatitude(), this.mStart.getLongitude()));

					for (int i = 0; i < pathSize; i++) {
						googlePolylinePath.add(new LatLng(tmapPolylinePath.get(i).getLatitude(), tmapPolylinePath.get(i).getLongitude()));
						mBuilder.include(new LatLng(tmapPolylinePath.get(i).getLatitude(), tmapPolylinePath.get(i).getLongitude()));
					}

					googlePolylinePath.add(new LatLng(this.mEnd.getLatitude(), this.mEnd.getLongitude()));
					mBuilder.include(new LatLng(this.mEnd.getLatitude(), this.mEnd.getLongitude()));

					PolylineOptions opt = new PolylineOptions();
					opt.width(SVUtil.pxFromDp(mMainActivity, 5));
					opt.color(Color.BLUE);
					opt.addAll(googlePolylinePath);

					mShuttleTransferPolyLineList.add(mGoogleMap.addPolyline(opt));

					mShuttleTransferResultData.get(this.mTabIndex).getmPedestrianPathList().add(googlePolylinePath);

					mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(mBuilder.build(), 120));

//					String totalTime = SVUtil.getTotalTimeString(mTime);
//					String totalDist;
//					if(mDistance >= 1000) {
//						double distances = mDistance / 1000.0;
//						totalDist = String.format("%.2f", distances) + "km";
//					} else {
//						totalDist = mDistance + "m";
//					}

//					makeDistanceMarker(mEndPosition, "거리 : " + totalDist + "\n" + "소요시간 : " + totalTime, 180, -180);

//					moveToBounds(pathPolyline);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void drawTransferResult(int _selectedTabIndex) {
		try {
			changeTab(_selectedTabIndex);

			mBuilder = new LatLngBounds.Builder();

			ShuttleTransferData shuttleTransferData = mShuttleTransferResultData.get(_selectedTabIndex);

			mShuttleTransferMarkerList.add(mGoogleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_shuttle_transfer_start)).position(mDepartFromLocation.getmPosition()).anchor(0f, 1f)));
			mShuttleTransferMarkerList.add(mGoogleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_shuttle_transfer_end)).position(mArriveAtLocation.getmPosition()).anchor(0f, 1f)));

			mBuilder.include(mDepartFromLocation.getmPosition());
			mBuilder.include(mArriveAtLocation.getmPosition());

			drawTransferRoute(shuttleTransferData);

			String startLocationText = "";
			String endLocationText = "";
			if(shuttleTransferData.getmTransferPathList().size() == 1) {
				// 하나의 노선으로 길찾기 완료
				mShuttleTransferTransferLocationLayout.setVisibility(View.GONE);

				final TransferPathShuttleInfoData transferPathShuttleInfoData = shuttleTransferData.getmTransferPathList().get(0).getmTransferPathShuttleInfoData();
				ShuttleTransferLocationData getOnStationInfo = shuttleTransferData.getmTransferPathList().get(0).getmGetOnInfo();
				ShuttleTransferLocationData getOffStationInfo = shuttleTransferData.getmTransferPathList().get(0).getmGetOffInfo();

				mBuilder.include(new LatLng(getOnStationInfo.getmLat(), getOnStationInfo.getmLng()));
				mBuilder.include(new LatLng(getOffStationInfo.getmLat(), getOffStationInfo.getmLng()));

				startLocationText += "[" + (transferPathShuttleInfoData.getmRouteType().equals("shuttle") ? "셔틀" : "심야") + "]";
				startLocationText += " " + getOnStationInfo.getmStationName();
				startLocationText += "(" + transferPathShuttleInfoData.getmShuttleName() + ")";

				endLocationText += "[" + (transferPathShuttleInfoData.getmRouteType().equals("shuttle") ? "셔틀" : "심야") + "]";
				endLocationText += " " + getOffStationInfo.getmStationName();
				endLocationText += "(" + transferPathShuttleInfoData.getmShuttleName() + ")";

				mShuttleTransferMarkerList.add(mGoogleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_point)).position(new LatLng(getOnStationInfo.getmLat(), getOnStationInfo.getmLng())).anchor(0.5f, 0.5f)));
				mShuttleTransferMarkerList.add(mGoogleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_point)).position(new LatLng(getOffStationInfo.getmLat(), getOffStationInfo.getmLng())).anchor(0.5f, 0.5f)));

				mShuttleTransferMarkerList.add(mGoogleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_geton)).position(new LatLng(getOnStationInfo.getmLat(), getOnStationInfo.getmLng())).anchor(0.50f, 1.05f)));
				mShuttleTransferMarkerList.add(mGoogleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_getoff)).position(new LatLng(getOffStationInfo.getmLat(), getOffStationInfo.getmLng())).anchor(0.45f, -0.05f)));

				drawPedestrianPolyline(_selectedTabIndex, mDepartFromLocation.getmPosition().latitude, mDepartFromLocation.getmPosition().longitude, getOnStationInfo.getmLat(), getOnStationInfo.getmLng());
				drawPedestrianPolyline(_selectedTabIndex, getOffStationInfo.getmLat(), getOffStationInfo.getmLng(), mArriveAtLocation.getmPosition().latitude, mArriveAtLocation.getmPosition().longitude);

				mShuttleTransferStartLocation.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						try {
							showShuttleInfoDialog(transferPathShuttleInfoData);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});

				mShuttleTransferEndLocation.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						try {
							showShuttleInfoDialog(transferPathShuttleInfoData);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			} else {
				// 두개의 노선으로 길찾기 완료
				mShuttleTransferTransferLocationLayout.setVisibility(View.VISIBLE);

				ArrayList<ShuttleTransferPathData> shuttleTransferPathDataList = shuttleTransferData.getmTransferPathList();
				Collections.sort(shuttleTransferPathDataList, transferSeqSort);

				final TransferPathShuttleInfoData startTransferPathShuttleInfoData = shuttleTransferPathDataList.get(0).getmTransferPathShuttleInfoData();
				ShuttleTransferLocationData startGetOnStationInfo = shuttleTransferPathDataList.get(0).getmGetOnInfo();
				ShuttleTransferLocationData startGetOffStationInfo = shuttleTransferPathDataList.get(0).getmGetOffInfo();

				mBuilder.include(new LatLng(startGetOnStationInfo.getmLat(), startGetOnStationInfo.getmLng()));
				mBuilder.include(new LatLng(startGetOffStationInfo.getmLat(), startGetOffStationInfo.getmLng()));

				final TransferPathShuttleInfoData endTransferPathShuttleInfoData = shuttleTransferPathDataList.get(1).getmTransferPathShuttleInfoData();
				ShuttleTransferLocationData endGetOnStationInfo = shuttleTransferPathDataList.get(1).getmGetOnInfo();
				ShuttleTransferLocationData endGetOffStationInfo = shuttleTransferPathDataList.get(1).getmGetOffInfo();

				mBuilder.include(new LatLng(endGetOnStationInfo.getmLat(), endGetOnStationInfo.getmLng()));
				mBuilder.include(new LatLng(endGetOffStationInfo.getmLat(), endGetOffStationInfo.getmLng()));

				startLocationText += "[" + (startTransferPathShuttleInfoData.getmRouteType().equals("shuttle") ? "셔틀" : "심야") + "]";
				startLocationText += " " + startGetOnStationInfo.getmStationName();
				startLocationText += "(" + startTransferPathShuttleInfoData.getmShuttleName() + ")";

				endLocationText += "[" + (endTransferPathShuttleInfoData.getmRouteType().equals("shuttle") ? "셔틀" : "심야") + "]";
				endLocationText += " " + endGetOffStationInfo.getmStationName();
				endLocationText += "(" + endTransferPathShuttleInfoData.getmShuttleName() + ")";

				mShuttleTransferMarkerList.add(mGoogleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_point)).position(new LatLng(startGetOnStationInfo.getmLat(), startGetOnStationInfo.getmLng())).anchor(0.5f, 0.5f)));
				mShuttleTransferMarkerList.add(mGoogleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_point)).position(new LatLng(startGetOffStationInfo.getmLat(), startGetOffStationInfo.getmLng())).anchor(0.5f, 0.5f)));
				mShuttleTransferMarkerList.add(mGoogleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_point)).position(new LatLng(endGetOnStationInfo.getmLat(), endGetOnStationInfo.getmLng())).anchor(0.5f, 0.5f)));
				mShuttleTransferMarkerList.add(mGoogleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_point)).position(new LatLng(endGetOffStationInfo.getmLat(), endGetOffStationInfo.getmLng())).anchor(0.5f, 0.5f)));
				mShuttleTransferMarkerList.add(mGoogleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_geton)).position(new LatLng(startGetOnStationInfo.getmLat(), startGetOnStationInfo.getmLng())).anchor(0.5f, 1.05f)));
				mShuttleTransferMarkerList.add(mGoogleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_transfer)).position(new LatLng(endGetOnStationInfo.getmLat(), endGetOnStationInfo.getmLng())).anchor(1.0f, 0.5f)));
				mShuttleTransferMarkerList.add(mGoogleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_getoff)).position(new LatLng(endGetOffStationInfo.getmLat(), endGetOffStationInfo.getmLng())).anchor(0.5f, -0.05f)));

				drawPedestrianPolyline(_selectedTabIndex, mDepartFromLocation.getmPosition().latitude, mDepartFromLocation.getmPosition().longitude, startGetOnStationInfo.getmLat(), startGetOnStationInfo.getmLng());
				drawPedestrianPolyline(_selectedTabIndex, startGetOffStationInfo.getmLat(), startGetOffStationInfo.getmLng(), endGetOnStationInfo.getmLat(), endGetOnStationInfo.getmLng());
				drawPedestrianPolyline(_selectedTabIndex, endGetOffStationInfo.getmLat(), endGetOffStationInfo.getmLng(), mArriveAtLocation.getmPosition().latitude, mArriveAtLocation.getmPosition().longitude);

				if(!startGetOffStationInfo.getmStationName().equals(endGetOnStationInfo.getmStationName())) {
					drawPedestrianPolyline(_selectedTabIndex, startGetOffStationInfo.getmLat(), startGetOffStationInfo.getmLng(), endGetOnStationInfo.getmLat(), endGetOnStationInfo.getmLng());
					mShuttleTransferMarkerList.add(mGoogleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_getoff)).position(new LatLng(startGetOffStationInfo.getmLat(), startGetOffStationInfo.getmLng())).anchor(0.5f, -0.05f)));
				}

				mShuttleTransferTransferLocation.setText(shuttleTransferData.getmTransferName());

				mShuttleTransferStartLocation.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						try {
							showShuttleInfoDialog(startTransferPathShuttleInfoData);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});

				mShuttleTransferEndLocation.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						try {
							showShuttleInfoDialog(endTransferPathShuttleInfoData);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}

			mShuttleTransferStartLocation.setText(startLocationText);
			mShuttleTransferEndLocation.setText(endLocationText);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void showShuttleInfoDialog(final TransferPathShuttleInfoData _startTransferPathShuttleInfoData) {
		try {
			LayoutInflater factory = LayoutInflater.from(getActivity());
			View layoutView = factory.inflate(R.layout.popup_shuttle_transfer_info, null);
			TextView timeTV = (TextView) layoutView.findViewById(R.id.popup_shuttle_transfer_info_time);
			TextView etcTV = (TextView) layoutView.findViewById(R.id.popup_shuttle_transfer_info_etc);
			ImageButton btnCall = (ImageButton) layoutView.findViewById(R.id.popup_shuttle_transfer_info_btn_call);
			ImageButton btnShowPath = (ImageButton) layoutView.findViewById(R.id.popup_shuttle_transfer_info_btn_path);
			ImageButton btnShowStations = (ImageButton) layoutView.findViewById(R.id.popup_shuttle_transfer_info_btn_stations);
			ImageButton btnPopupClose = (ImageButton) layoutView.findViewById(R.id.popup_shuttle_transfer_btn_close);

			if(_startTransferPathShuttleInfoData.getmRouteType().equals("shuttle")) {
				btnCall.setVisibility(View.VISIBLE);
				btnShowPath.setImageResource(R.drawable.btn_shuttle_path);
				btnShowStations.setImageResource(R.drawable.btn_shuttle_station);
			} else {
				btnCall.setVisibility(View.GONE);
				btnShowPath.setImageResource(R.drawable.btn_nightbus_path);
				btnShowStations.setImageResource(R.drawable.btn_nightbus_station);
			}

			btnCall.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					try {
						registerVirtualNumber(_startTransferPathShuttleInfoData.getmBusId());
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			String timeText = "";
			timeText += "첫차 " + SVUtil.convertTime(_startTransferPathShuttleInfoData.getmFirstTime());
			timeText += ", 막차 " + SVUtil.convertTime(_startTransferPathShuttleInfoData.getmLastTime());
			timeTV.setText(timeText);

			String etcText = "";
			if(_startTransferPathShuttleInfoData.getmTerm() > 0) {
				etcText += "배차간격 " + _startTransferPathShuttleInfoData.getmTerm() + "분";
			}

			if(_startTransferPathShuttleInfoData.getmSundayFirstTime().isEmpty()) {
				if(!etcText.isEmpty()) {
					etcText += ", ";
				}

				etcText += "일요일(공휴일) 휴무";
			}

			if(_startTransferPathShuttleInfoData.getmTotalTime() > 0) {
				if(!etcText.isEmpty()) {
					etcText += ", ";
				}

				etcText += "운행시간 " + _startTransferPathShuttleInfoData.getmTotalTime() + "분";
			}

			etcTV.setText(etcText);

			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mMainActivity);
			alertDialogBuilder.setTitle(_startTransferPathShuttleInfoData.getmShuttleName());
			alertDialogBuilder.setView(layoutView);

			final AlertDialog dialog = alertDialogBuilder.show();

			btnShowPath.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					try {
						dialog.dismiss();
						mMainActivity.checkDriverConfirmData(_startTransferPathShuttleInfoData.getmBusId(), "main");
						closeFragment();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			btnShowStations.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					try {
						dialog.dismiss();
						mMainActivity.checkDriverConfirmData(_startTransferPathShuttleInfoData.getmBusId(), "main_station_list");
						closeFragment();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			btnPopupClose.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					try {
						dialog.dismiss();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			SVUtil.setDialogTitleColor(dialog, getActivity().getResources().getColor(R.color.main200));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void registerVirtualNumber(String _shuttleId) {
		try {
			RequestParams params = new RequestParams();
			params.add("type", "shuttle");
			params.add("recvId", _shuttleId);
			params.add("sendId", mMainActivity.mDriverInfo.getmDriverId());

			SVUtil.showProgressDialog(mMainActivity, "전화 연결중...");

			NetClient.send(mMainActivity, Global.URL_REGISTER_VIRTUAL_NUMBER, "POST", params, new NoResponseDataAck(), new NetResponseCallback(new NetResponse() {
				@Override
				public void onResponse(NetAPI _netAPI) {
					if (_netAPI != null && !mIsFinishFragment) {
						NoResponseDataAck retNetApi = (NoResponseDataAck) _netAPI;
						ResultData result = retNetApi.getmResultData();
						if (result.getmCode() != 100) {
							SVUtil.hideProgressDialog();
							SVUtil.showSimpleDialog(mMainActivity, result.getmDetail());
						}
					}
				}
			}));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void unregisterVirtualNumber() {
		try {
			RequestParams params = new RequestParams();
			params.add("type", "shuttle");
			params.add("virtual_number", mVirtualNumber);
			params.add("sendId", mMainActivity.mDriverInfo.getmDriverId());

			NetClient.send(mMainActivity, Global.URL_UNREGISTER_VIRTUAL_NUMBER, "POST", params, new NoResponseDataAck(), new NetResponseCallback(new NetResponse() {
				@Override
				public void onResponse(NetAPI _netAPI) {
					if (_netAPI != null && !mIsFinishFragment) {
						mIsSecretCalling = false;
						mVirtualNumber = "";

						NoResponseDataAck retNetApi = (NoResponseDataAck) _netAPI;
						ResultData result = retNetApi.getmResultData();
						if (result.getmCode() != 100) {
							SVUtil.showSimpleDialog(mMainActivity, result.getmDetail());
						}
					}
				}
			}));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private Marker drawTextMarker(LatLng _position, String _title, int _rotate, int _contentRotate) {
		try {
			IconGenerator iconFactory = new IconGenerator(mMainActivity);
			iconFactory.setRotation(_rotate);
			iconFactory.setContentRotation(_contentRotate);
			MarkerOptions markerOptions = new MarkerOptions().
					icon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon(_title))).
					position(_position).anchor(iconFactory.getAnchorU(), iconFactory.getAnchorV());

			return mGoogleMap.addMarker(markerOptions);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private void changeTab(int _selectedTabIndex) {
		try {
			int tabCount = mShuttleTransferTabHost.getChildCount();
			for(int i = 0; i < tabCount; i++) {
				mShuttleTransferTabHost.getChildAt(i).setSelected(false);
			}

			mShuttleTransferTabHost.getChildAt(_selectedTabIndex).setSelected(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void drawTransferRoute(ShuttleTransferData _shuttleTransferData) {
		try {
			ArrayList<ShuttleTransferPathData> pathItemList = _shuttleTransferData.getmTransferPathList();

			Collections.sort(pathItemList, transferSeqSort);

			int index = 0;
			for(ShuttleTransferPathData pathItem : pathItemList) {
				PolylineOptions opt = new PolylineOptions();
				if(index == 0) {
					opt.color(Color.parseColor("#ed482b"));
				} else {
					opt.color(Color.parseColor("#db2deb"));
				}

				opt.width(SVUtil.pxFromDp(mMainActivity, 9));

				for(ShuttleTransferLocationData item : pathItem.getmTransferPathShuttleInfoData().getmPathList()) {
					LatLng itemPosition = new LatLng(item.getmLat(), item.getmLng());
					opt.add(itemPosition);
					mBuilder.include(itemPosition);
				}

				mShuttleTransferPolyLineList.add(mGoogleMap.addPolyline(opt));

				index++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private final Comparator<ShuttleTransferPathData> transferSeqSort = new Comparator<ShuttleTransferPathData>() {
		@Override
		public int compare(ShuttleTransferPathData box, ShuttleTransferPathData box2) {
			return box.getmSeq() - box2.getmSeq();
		}
	};


	private void closeFragment() {
		try {
			mMainActivity.getFragmentManager().beginTransaction().remove(SearchShuttlePathResultFragment.this).commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setEventListener() {
		try {
			mBtnSearchResultZoomIn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					mGoogleMap.animateCamera(CameraUpdateFactory.zoomIn());
				}
			});

			mBtnSearchResultZoomOut.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					mGoogleMap.animateCamera(CameraUpdateFactory.zoomOut());
				}
			});

			mBtnSearchResultCurrentPosition.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					mGoogleMap.animateCamera(CameraUpdateFactory.newLatLng(mCurrentMyPosition));
				}
			});

			mBtnSearchReset.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					closeFragment();
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void makeSecretPhoneCall(String _phone_number) {
		try {
			String uri = "tel:" + _phone_number;

			mIsSecretCalling = true;

			Intent intent = new Intent(Intent.ACTION_CALL);
			intent.setData(Uri.parse(uri));

			startActivity(intent);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private class PushAlertReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			try {
				String action = intent.getAction();
				long sendDate;
				String message;
				String senderNickName;
				double lat;
				double lng;

				switch (action) {
					case ShuttleChattingFragment.PUSH_MSG_5:
						SVUtil.hideProgressDialog();

						mVirtualNumber = intent.getStringExtra("virtualNumber");
						String result = intent.getStringExtra("result");

						if(result != null && result.equals("00")) {
							makeSecretPhoneCall(mVirtualNumber);
						} else {
							SVUtil.showSimpleDialog(mMainActivity, "전화 연결에 실패했습니다.");
						}

						break;
					case ShuttleChattingFragment.PUSH_MSG_6:
						String call_state = intent.getStringExtra("call_state");

						SVUtil.log("error", TAG, "call_state : " + call_state);

						if(mIsSecretCalling && call_state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
							unregisterVirtualNumber();
						}
						break;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void destroyView() {
		try {
			if(mGoogleMap != null) {
				mGoogleMap.clear();
				mGoogleMap = null;
			}

			SupportMapFragment f = (SupportMapFragment) MainActivity.mSupportFragmentManager.findFragmentById(R.id.searchShuttlePathResultMap);
			if(f != null) {
				MainActivity.mSupportFragmentManager.beginTransaction().remove(f).commitAllowingStateLoss();
			}

			if(mView != null){
				ViewGroup parent = (ViewGroup) mView.getParent();
				if(parent!=null){
					parent.removeView(mView);
				}
			}

			getActivity().unregisterReceiver(mPushAlertReceiver);

			closeFragment();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onStart() {
		super.onStart();

		mIsFinishFragment = false;
	}

	@Override
	public void onPause() {
		super.onPause();

		mIsFinishFragment = true;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();

		destroyView();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		setTargetFragment(null, -1);
	}
}
