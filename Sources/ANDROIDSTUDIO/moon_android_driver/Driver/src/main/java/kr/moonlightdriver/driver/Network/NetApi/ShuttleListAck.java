package kr.moonlightdriver.driver.Network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

import kr.moonlightdriver.driver.Network.NetData.ResultData;
import kr.moonlightdriver.driver.Network.NetData.ShuttleListData;

/**
 * Created by youngmin on 2015-10-13.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShuttleListAck extends NetAPI {
	@JsonProperty("result")
	private ResultData mResultData;
	@JsonProperty("shuttles")
	private ArrayList<ShuttleListData> mShuttleListData;
	@JsonProperty("length")
	private int mShuttleListLength;

	public ShuttleListAck() {}

	public ShuttleListAck(ResultData mResultData, ArrayList<ShuttleListData> mShuttleListData, int mShuttleListLength) {
		this.mResultData = mResultData;
		this.mShuttleListData = mShuttleListData;
		this.mShuttleListLength = mShuttleListLength;
	}

	public ResultData getmResultData() {
		return mResultData;
	}

	public void setmResultData(ResultData mResultData) {
		this.mResultData = mResultData;
	}

	public ArrayList<ShuttleListData> getmShuttleListData() {
		return mShuttleListData;
	}

	public void setmShuttleListData(ArrayList<ShuttleListData> mShuttleListData) {
		this.mShuttleListData = mShuttleListData;
	}

	public int getmShuttleListLength() {
		return mShuttleListLength;
	}

	public void setmShuttleListLength(int mShuttleListLength) {
		this.mShuttleListLength = mShuttleListLength;
	}
}
