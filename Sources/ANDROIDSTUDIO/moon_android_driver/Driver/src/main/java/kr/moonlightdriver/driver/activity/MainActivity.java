package kr.moonlightdriver.driver.activity;

import android.animation.Animator;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.GeomagneticField;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NotificationCompat;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.loopj.android.http.RequestParams;
import com.skt.Tmap.TMapTapi;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import kr.moonlightdriver.driver.Network.NetApi.AutoRegisterDriverAck;
import kr.moonlightdriver.driver.Network.NetApi.CheckDriverConfirmDataAck;
import kr.moonlightdriver.driver.Network.NetApi.DriverConfirmDataAck;
import kr.moonlightdriver.driver.Network.NetApi.NetAPI;
import kr.moonlightdriver.driver.Network.NetApi.NetResponse;
import kr.moonlightdriver.driver.Network.NetApi.NetResponseCallback;
import kr.moonlightdriver.driver.Network.NetApi.NoResponseDataAck;
import kr.moonlightdriver.driver.Network.NetApi.NoticeListAck;
import kr.moonlightdriver.driver.Network.NetApi.ShuttleCategoryListAck;
import kr.moonlightdriver.driver.Network.NetClient.NetClient;
import kr.moonlightdriver.driver.Network.NetData.DriverData;
import kr.moonlightdriver.driver.Network.NetData.InsuranceCompanyListData;
import kr.moonlightdriver.driver.Network.NetData.NoticeData;
import kr.moonlightdriver.driver.Network.NetData.ResultData;
import kr.moonlightdriver.driver.R;
import kr.moonlightdriver.driver.fragment.CallStatsFragment;
import kr.moonlightdriver.driver.fragment.CarpoolDetailFragment;
import kr.moonlightdriver.driver.fragment.CarpoolFragment;
import kr.moonlightdriver.driver.fragment.FindPathFragment;
import kr.moonlightdriver.driver.fragment.OutbackFragment;
import kr.moonlightdriver.driver.fragment.QuestionDetailFragment;
import kr.moonlightdriver.driver.fragment.QuestionFragment;
import kr.moonlightdriver.driver.fragment.ShuttleChattingFragment;
import kr.moonlightdriver.driver.fragment.ShuttleFragment;
import kr.moonlightdriver.driver.fragment.TwoInOneDetailFragment;
import kr.moonlightdriver.driver.fragment.TwoInOneFragment;
import kr.moonlightdriver.driver.utils.BeaconService;
import kr.moonlightdriver.driver.utils.DatabaseHelper;
import kr.moonlightdriver.driver.utils.Global;
import kr.moonlightdriver.driver.utils.SVUtil;
import kr.moonlightdriver.driver.utils.SendLocationService;
import kr.moonlightdriver.driver.utils.TouchableWrapper;

public class MainActivity extends FragmentActivity implements View.OnClickListener,TouchableWrapper.MapGestureDetector {
	private static final String TAG = "MainActivity";

	public OutbackFragment mFragmentOutback;
	public ShuttleFragment mFragmentShuttle;
	public CarpoolFragment mFragmentCarpool;
	public TwoInOneFragment mFragmentTwoInOne;
	public FindPathFragment mFragmentFindPath;
	public CallStatsFragment mFragmentCallStats;
	public QuestionFragment mFragmentQuestion;
	public ShuttleChattingFragment mFragmentShuttleChatting;

	public ImageButton mImgBtnOutback;
	public ImageButton mImgBtnShuttle;
	public ImageButton mImgBtnCarpool;
	public ImageButton mImgBtn2in1;
	public ImageButton mImgBtnFindPath;
	public ImageButton mImgBtnCallStats;
	public ImageButton mImgBtnQuestion;

	public LatLng mCurrentLocation;
	private LatLng mLastUpdateLocation;

	public boolean mIsSplashRemoved = false;    // 스플래시 이미지 삭제 여부

	private Context mContext;

//	private static LocationManager mLocationManager;
//	private static LocationManager mGpsLocationManager;

//	private String mCurrentProvider;
	private int kLocationSetting = 1;
	private int kWifiSetting = 2;

	// DB
	public SQLiteDatabase mDatabase;
	public DatabaseHelper mDatabaseHelper;

	// gcm
	public String mUserPhoneNumber;
	private GoogleCloudMessaging mGcm;
	public String mRegistrationId;

	public static android.support.v4.app.FragmentManager mSupportFragmentManager;

	public DriverData mDriverInfo;
	private int mSelectedInsuranceCompany;

	private boolean mIsUpdateCurrentLocation;
	private Timer mUpdateLocationResumeTimer;

	private ArrayList<NoticeData> mNoticeDataList;
	private ArrayList<InsuranceCompanyListData> mInsuranceCompanyDataList;
	public String[] mShuttleCategoryList;

	private int mNoticeCurrentPage;
	private int mNoticeTotalPage;

	public TextView mLoadingText;
	private RelativeLayout mSplashLayout;

	private Animation mAnimationMeteor_1;
	private Animation mAnimationMeteor_2;
	private Animation mAnimationMeteor_3;
	private Animation mAnimationMeteor_4;

	private Animation mAnimationBlinkStar_1;
	private Animation mAnimationBlinkStar_2;
	private Animation mAnimationBlinkStar_3;
	private Animation mAnimationBlinkStar_4;

	private ImageView mSplashMeteor_1;
	private ImageView mSplashMeteor_2;
	private ImageView mSplashMeteor_3;
	private ImageView mSplashMeteor_4;

	private ImageView mSplashStar_1;
	private ImageView mSplashStar_2;
	private ImageView mSplashStar_3;
	private ImageView mSplashStar_4;

	private String mActionFragmentName;
	private String mActionId;

	public ArrayList<LatLng> mPastPathList;

	private long mLastPressTime;
	private boolean mIsNoShowNotice;

	private boolean mNeedUpdate = false;

	public String mYnCarpoolPush;

	private ChangeLocationReceiver mChangeLocationReceiver;

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);

		try {
//			SVUtil.log("error", TAG, "onNewIntent()");

			Bundle extras = intent.getExtras();
			getYnCarpoolPush();

			mActionFragmentName = "";
			mActionId = "";

			if(extras != null) {
				mActionFragmentName = extras.getString("actionFragment", "");
				mActionId = extras.getString("actionId", "");
			}


			if(!mActionFragmentName.isEmpty() && !mActionId.isEmpty()) {
				moveToFragmentByNotification(mActionFragmentName, mActionId);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
//		SVUtil.log("error", TAG, "start onCreate()");
		Log.e("MainActivity", "start onCreate()");
		super.onCreate(savedInstanceState);

		try {
			setContentView(R.layout.activity_main);

			mImgBtnOutback = (ImageButton) findViewById(R.id.frag_return_homeBtn);
			mImgBtnShuttle = (ImageButton) findViewById(R.id.frag_return_shuttleBtn);
			mImgBtnCarpool = (ImageButton) findViewById(R.id.frag_return_carpoolBtn);
			mImgBtn2in1 = (ImageButton) findViewById(R.id.frag_return_2in1Btn);
			mImgBtnFindPath = (ImageButton) findViewById(R.id.frag_return_naviBtn);
			mImgBtnCallStats = (ImageButton) findViewById(R.id.frag_return_callAmountBtn);
			mImgBtnQuestion = (ImageButton) findViewById(R.id.frag_return_questionBtn);

			mSplashMeteor_1 = (ImageView) findViewById(R.id.splash_meteor_1);
			mSplashMeteor_2 = (ImageView) findViewById(R.id.splash_meteor_2);
			mSplashMeteor_3 = (ImageView) findViewById(R.id.splash_meteor_3);
			mSplashMeteor_4 = (ImageView) findViewById(R.id.splash_meteor_4);

			mSplashStar_1 = (ImageView) findViewById(R.id.splash_star_1);
			mSplashStar_2 = (ImageView) findViewById(R.id.splash_star_2);
			mSplashStar_3 = (ImageView) findViewById(R.id.splash_star_3);
			mSplashStar_4 = (ImageView) findViewById(R.id.splash_star_4);

			mActionFragmentName = "";
			mActionId = "";

			Intent intent = getIntent();
			Bundle extras = intent.getExtras();
			if(extras != null) {
				mActionFragmentName = extras.getString("actionFragment", "");
				mActionId = extras.getString("actionId", "");
			}

			startMovingMeteorsAnimation();

			startBlinkStarsAnimation();

			mContext = this;

			mLastPressTime = 0;
			mIsNoShowNotice = false;
			mCurrentLocation = Global.SEOUL_CITY_HALL;
			mPastPathList = new ArrayList<>();

			mSplashLayout = (RelativeLayout) findViewById(R.id.splash_layout);
			mLoadingText = (TextView) findViewById(R.id.splash_loading_textview);

			getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

			getYnCarpoolPush();

			getUserPhoneNumber();

//			SVUtil.log("error", TAG, "end onCreate()");
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	public void getUserPhoneNumber() {
		try {
			mLoadingText.setText("사용자 정보 확인중...");

			if(checkPhoneNumberPermission()) {
				TelephonyManager tMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
				mUserPhoneNumber = tMgr.getLine1Number();

//				mUserPhoneNumber = "+821000000000";

				if(mUserPhoneNumber == null) {
					SVUtil.showDialogWithListener(mContext, "전화번호 조회에 실패했습니다.\n전화번호가 없으면 사용이 불가능합니다.", "확인", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							SVUtil.finishActivity((Activity) mContext);
						}
					});
				} else {
					checkApplicationVersion();
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	private boolean checkPhoneNumberPermission() {
		boolean isPermissionGranted = true;

//		try {
//			if(Build.VERSION.SDK_INT >= 23) {
//				if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED
//						|| ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED) {
//					ActivityCompat.requestPermissions((Activity) mContext, new String[] { Manifest.permission.READ_PHONE_STATE, Manifest.permission.READ_SMS }, Global.PHONE_NUMBER_REQ_CODE);
//					isPermissionGranted = false;
//				}
//			}
//		} catch(Exception e) {
//			e.printStackTrace();
//			isPermissionGranted = false;
//		}

		return isPermissionGranted;
	}

	public void checkApplicationVersion() {
		try {
			PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			String version = pInfo.versionName;
//			String version = "0.1.2";

//			SVUtil.log("error", TAG, "version : " + version);

			RequestParams params = new RequestParams();
			params.add("version", version);

			NetClient.send(mContext, Global.URL_CHECK_VERSION, "POST", params, new NoResponseDataAck(), new NetResponseCallback(new NetResponse() {
				@Override
				public void onResponse(NetAPI _netAPI) {
					try {
						if (_netAPI != null) {
							NoResponseDataAck retNetApi = (NoResponseDataAck) _netAPI;
							ResultData result = retNetApi.getmResultData();
							if (result.getmCode() == 100) {
								init();
							} else if(result.getmCode() == 236){
								mNeedUpdate = true;
								init();
							} else {
								SVUtil.showDialogWithListener(mContext, "버전 조회에 실패했습니다.\n사용 불가능한 버전입니다.", "확인", new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										SVUtil.finishActivity((Activity) mContext);
									}
								});
							}
						}
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
			}));
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	public void checkUserInfo() {
		try {
			// gcm regid 얻는 부분
			if (checkPlayServices()) {
				// If this check succeeds, proceed with normal processing.
				// Otherwise, prompt user to get valid Play Services APK.
				mGcm = GoogleCloudMessaging.getInstance(this);
				mRegistrationId = getRegistrationId(mContext);

				if (mRegistrationId.isEmpty()) {
					registerInBackground();
				} else {
					userLogin();
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	private boolean checkPlayServices() {
		try {
			int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
			if (resultCode != ConnectionResult.SUCCESS) {
				if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
					GooglePlayServicesUtil.getErrorDialog(resultCode, this, Global.PLAY_SERVICES_RESOLUTION_REQUEST).show();
				} else {
					SVUtil.showDialogWithListener(mContext, "지원하지 않는 단말기입니다.", "확인", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							SVUtil.finishActivity((Activity) mContext);
						}
					});
				}

				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public void getYnCarpoolPush() {
		try {
			SharedPreferences prefs = getGCMPreferences(mContext);
			mYnCarpoolPush = prefs.getString(Global.PROPERTY_YN_CARPOOL_PUSH, "Y");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void saveYnCarpoolPush() {
		try {
			SharedPreferences prefs = getGCMPreferences(mContext);
			SharedPreferences.Editor editor = prefs.edit();
			editor.putString(Global.PROPERTY_YN_CARPOOL_PUSH, mYnCarpoolPush);
			editor.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String getRegistrationId(Context _context) {
		try {
			final SharedPreferences prefs = getGCMPreferences(_context);
			String registrationId = prefs.getString(Global.PROPERTY_REG_ID, "");

			if (registrationId.isEmpty()) {
				return "";
			}

			int registeredVersion = prefs.getInt(Global.PROPERTY_APP_VERSION, Integer.MIN_VALUE);
			int currentVersion = getAppVersion(_context);
			if (registeredVersion != currentVersion) {
				return "";
			}

			return registrationId;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	public SharedPreferences getGCMPreferences(Context _context) {
		// This sample app persists the registration ID in shared preferences, but
		// how you store the regID in your app is up to you.
		return getSharedPreferences(_context.getClass().getSimpleName(), Context.MODE_PRIVATE);
	}

	/**
	 * @return Application's version code from the {@code PackageManager}.
	 */
	private static int getAppVersion(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);

			return packageInfo.versionCode;
		} catch (PackageManager.NameNotFoundException e) {
			// should never happen
			e.printStackTrace();
			throw new RuntimeException("Could not get package name: " + e);
		}
	}

	private void registerInBackground() {
		new AsyncTask<Void, Void, Void>() {
			@Override
			protected Void doInBackground(Void... voids) {
				try {
					if (mGcm == null) {
						mGcm = GoogleCloudMessaging.getInstance(mContext);
					}

					mRegistrationId = mGcm.register(Global.SENDER_ID);

					storeRegistrationId(mContext, mRegistrationId);
				} catch (Exception ex) {
					ex.printStackTrace();
				}

				return null;
			}

			@Override
			protected void onPostExecute(Void _void) {
				super.onPostExecute(_void);

				userLogin();
			}
		}.execute();
	}

	private void storeRegistrationId(Context context, String regId) {
		try {
			final SharedPreferences prefs = getGCMPreferences(context);
			int appVersion = getAppVersion(context);

			SharedPreferences.Editor editor = prefs.edit();
			editor.putString(Global.PROPERTY_REG_ID, regId);
			editor.putInt(Global.PROPERTY_APP_VERSION, appVersion);
			editor.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void startMovingMeteorsAnimation() {
		try {
			mAnimationMeteor_1 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move_meteor_1);
			mSplashMeteor_1.setVisibility(View.VISIBLE);
			mSplashMeteor_1.startAnimation(mAnimationMeteor_1);

			mAnimationMeteor_2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move_meteor_2);
			mSplashMeteor_2.setVisibility(View.VISIBLE);
			mSplashMeteor_2.startAnimation(mAnimationMeteor_2);

			mAnimationMeteor_3 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move_meteor_3);
			mSplashMeteor_3.setVisibility(View.VISIBLE);
			mSplashMeteor_3.startAnimation(mAnimationMeteor_3);

			mAnimationMeteor_4 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move_meteor_4);
			mSplashMeteor_4.setVisibility(View.VISIBLE);
			mSplashMeteor_4.startAnimation(mAnimationMeteor_4);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void startBlinkStarsAnimation() {
		try {
			mAnimationBlinkStar_1 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.blink_star_1);
			mSplashStar_1.setVisibility(View.VISIBLE);
			mSplashStar_1.startAnimation(mAnimationBlinkStar_1);

			mAnimationBlinkStar_2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.blink_star_2);
			mSplashStar_2.setVisibility(View.VISIBLE);
			mSplashStar_2.startAnimation(mAnimationBlinkStar_2);

			mAnimationBlinkStar_3 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.blink_star_3);
			mSplashStar_3.setVisibility(View.VISIBLE);
			mSplashStar_3.startAnimation(mAnimationBlinkStar_3);

			mAnimationBlinkStar_4 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.blink_star_4);
			mSplashStar_4.setVisibility(View.VISIBLE);
			mSplashStar_4.startAnimation(mAnimationBlinkStar_4);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void removeSplashFragment() {
		mSplashLayout.animate().setDuration(500).alpha(0.0f).setListener(new Animator.AnimatorListener() {
			@Override
			public void onAnimationStart(Animator animation) {
			}

			@Override
			public void onAnimationEnd(Animator animation) {
				try {
					mAnimationMeteor_1.cancel();
					mAnimationMeteor_2.cancel();
					mAnimationMeteor_3.cancel();
					mAnimationMeteor_4.cancel();

					mAnimationBlinkStar_1.cancel();
					mAnimationBlinkStar_2.cancel();
					mAnimationBlinkStar_3.cancel();
					mAnimationBlinkStar_4.cancel();

					mSplashLayout.setVisibility(View.GONE);
					mNeedUpdate = false; // todo remove
					if(mNeedUpdate) {
						new AlertDialog.Builder(mContext)
								.setIcon(android.R.drawable.ic_dialog_info)
								.setCancelable(false)
								.setTitle("업데이트")
								.setMessage("앱을 업데이트 하시겠습니까?")
								.setPositiveButton("업데이트", new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										Intent marketLaunch = new Intent(Intent.ACTION_VIEW);
										marketLaunch.setData(Uri.parse(Global.APP_DOWNLOAD_URL));
										startActivity(marketLaunch);

										SVUtil.finishActivity((Activity) mContext);

//										showPrivacyAgreePopup();
//
//										showNoticePopup();
									}
								})
								.setNegativeButton("종료", new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										SVUtil.finishActivity((Activity) mContext);
//										showPrivacyAgreePopup();
//										showNoticePopup();
									}
								})
								.show();
					} else {
						showPrivacyAgreePopup();
						showNoticePopup();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onAnimationCancel(Animator animation) {
			}

			@Override
			public void onAnimationRepeat(Animator animation) {
			}
		});
	}

	public void userLogin() {
		try {
			PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			String version = pInfo.versionName;

			RequestParams params = new RequestParams();
			params.add("gcmId", mRegistrationId);
			params.add("phone", mUserPhoneNumber);
			params.add("phone", mUserPhoneNumber);
			params.add("macAddr", "");
			params.add("version", version);

			NetClient.send(mContext, Global.URL_AUTO_REGISTER_DRIVER, "POST", params, new AutoRegisterDriverAck(), new NetResponseCallback(new NetResponse() {
				@Override
				public void onResponse(NetAPI _netAPI) {
					try {
						if (_netAPI != null) {
							AutoRegisterDriverAck retNetApi = (AutoRegisterDriverAck) _netAPI;
							ResultData result = retNetApi.getmResultData();
							if (result.getmCode() == 100) {
								mDriverInfo = retNetApi.getmDriverData();

								SVUtil.SaveDriverId(mContext, mDriverInfo.getmDriverId());

//								init();
							} else {
								SVUtil.showDialogWithListener(mContext, "사용자 인증에 실패했습니다.\n잠시후 다시 시도해주세요.", "확인", new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										SVUtil.finishActivity((Activity) mContext);
									}
								});
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void showPrivacyAgreePopup() {
		try {
			final SharedPreferences prefs = getGCMPreferences(mContext);
			boolean isFirstRun = prefs.getBoolean(Global.PROPERTY_FIRST_LAUNCH, true);

			if(!isFirstRun) {
				checkUserInfo();
				return;
			}

			LayoutInflater factory = LayoutInflater.from(this);
			View layoutView = factory.inflate(R.layout.popup_agreement, null);

			final CheckBox privacyCheck1 = (CheckBox) layoutView.findViewById(R.id.privacy_1_check);
			final CheckBox privacyCheck2 = (CheckBox) layoutView.findViewById(R.id.privacy_2_check);

			final AlertDialog dialog = SVUtil.showDialogWithListener(mContext, "개인정보 이용약관 동의", layoutView, "확인", null, "종료", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					SVUtil.finishActivity((Activity) mContext);
				}
			});

			dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (!privacyCheck1.isChecked() || !privacyCheck2.isChecked()) {
						Toast.makeText(mContext, "달빛기사 이용약관 및 개인정보 취급 방침 모두 동의해 주세요.", Toast.LENGTH_SHORT).show();
					} else {
						final SharedPreferences prefs = getGCMPreferences(mContext);
						prefs.edit().putBoolean(Global.PROPERTY_FIRST_LAUNCH, false).commit();

						dialog.dismiss();

						checkUserInfo();
					}
				}
			});

			dialog.setCancelable(false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void showNoticePopup() {
		try {
			final SharedPreferences prefs = getGCMPreferences(mContext);
			long noShowNoticeTime = prefs.getLong(Global.PROPERTY_NO_SHOW_NOTICE, 0);
			if((System.currentTimeMillis() - noShowNoticeTime) < (7 * 24 * 60 * 60 * 1000)) {
				return;
			}

			mNoticeTotalPage = mNoticeDataList.size();
			if(mNoticeTotalPage <= 0) {
				return;
			}

			AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
			LayoutInflater factory = LayoutInflater.from(this);
			View layoutView = factory.inflate(R.layout.popup_notice, null);

			builderSingle.setView(layoutView);
			builderSingle.setCancelable(false);

			final AlertDialog dialog = builderSingle.create();

			TextView noticeTitleTextView = (TextView) layoutView.findViewById(R.id.popup_notice_title_textview);
			TextView noticeContentsTextView = (TextView) layoutView.findViewById(R.id.popup_notice_contents_textview);
			ImageView noticeContentsImageView = (ImageView) layoutView.findViewById(R.id.popup_notice_contents_imageview);
			TextView noticeContentsMoreTextView = (TextView) layoutView.findViewById(R.id.popup_notice_contents_more_textview);
			TextView noticePageTextView = (TextView) layoutView.findViewById(R.id.popup_notice_page_textview);
			final Button noticePrevButton = (Button) layoutView.findViewById(R.id.popup_notice_prev_button);
			final Button noticeNextButton = (Button) layoutView.findViewById(R.id.popup_notice_next_button);
			final CheckBox isNoShowNotice = (CheckBox) layoutView.findViewById(R.id.notice_no_show);

			final NoticeData noticeItem = mNoticeDataList.get(mNoticeCurrentPage);

			isNoShowNotice.setChecked(mIsNoShowNotice);
			noticeTitleTextView.setText(noticeItem.getmTitle());

			if(noticeItem.getmContents() == null || noticeItem.getmContents().isEmpty()) {
				noticeContentsTextView.setVisibility(View.GONE);
			} else {
				noticeContentsTextView.setText(noticeItem.getmContents());
			}

			if(noticeItem.getmImage() == null || noticeItem.getmImage().isEmpty()) {
				noticeContentsImageView.setVisibility(View.GONE);
			} else {
				new DownloadImageTask(noticeContentsImageView).execute(noticeItem.getmImage());
			}

			if(noticeItem.getmLink() == null || noticeItem.getmLink().isEmpty()) {
				noticeContentsMoreTextView.setVisibility(View.GONE);
			} else {
				noticeContentsMoreTextView.setText("더보기 >>");
				noticeContentsMoreTextView.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						Uri uri = Uri.parse(noticeItem.getmLink());
						startActivity(new Intent(Intent.ACTION_VIEW, uri));
					}
				});
			}

			String pageText = (mNoticeCurrentPage + 1) + " / " + mNoticeTotalPage;
			noticePageTextView.setText(pageText);

			isNoShowNotice.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					mIsNoShowNotice = isNoShowNotice.isChecked();
				}
			});

			noticePrevButton.setEnabled(false);
			noticePrevButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (noticePrevButton.isEnabled()) {
						mNoticeCurrentPage--;
						showNoticePopup();
						dialog.dismiss();
					}
				}
			});

			noticeNextButton.setEnabled(false);
			noticeNextButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (noticeNextButton.isEnabled()) {
						mNoticeCurrentPage++;

						if (mNoticeCurrentPage < mNoticeTotalPage) {
							showNoticePopup();
						} else {
							saveNoShowNotice();
						}

						dialog.dismiss();
					}
				}
			});

			checkPageButtonEnable(noticePrevButton, noticeNextButton);

			dialog.show();

			SVUtil.setDialogTitleColor(dialog, getResources().getColor(R.color.main200));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void checkPageButtonEnable(Button _prev, Button _next) {
		try {
			_next.setEnabled(true);
			_prev.setEnabled(true);

			if(mNoticeCurrentPage + 1 == mNoticeTotalPage) {
				_next.setText("닫기");
			}

			if(mNoticeCurrentPage == 0) {
				_prev.setEnabled(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void saveNoShowNotice() {
		try {
			final SharedPreferences prefs = getGCMPreferences(mContext);
			if(mIsNoShowNotice) {
				prefs.edit().putLong(Global.PROPERTY_NO_SHOW_NOTICE, System.currentTimeMillis()).commit();
			} else {
				prefs.edit().remove(Global.PROPERTY_NO_SHOW_NOTICE).commit();
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onFlingDetected(double _angle, float _velocityX, float _velocityY) {
		try {
			if(mFragmentOutback != null && mFragmentOutback.mIsFragmentRunning) {
				mFragmentOutback.checkExpectedOutsideOfBounds(null, _velocityX, _velocityY);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onScrollingDetected() {
		try {
			mIsUpdateCurrentLocation = false;
			startUpdateLocationResumeTimer();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
		ImageView bmImage;

		public DownloadImageTask(ImageView bmImage) {
			this.bmImage = bmImage;
		}

		protected Bitmap doInBackground(String... urls) {
			String url = urls[0];
			Bitmap mIcon11 = null;

			try {
				InputStream in = new java.net.URL(url).openStream();
				mIcon11 = BitmapFactory.decodeStream(in);
			} catch (Exception e) {
				e.printStackTrace();
			}

			return mIcon11;
		}

		protected void onPostExecute(Bitmap result) {
			if(result != null) {
				bmImage.setImageBitmap(result);
			}
		}
	}

	public void startUpdateLocationResumeTimer() {
		stopUpdateLocationResumeTimer();

		mUpdateLocationResumeTimer = new Timer(true);
		mUpdateLocationResumeTimer.schedule(new TimerTask() {
			@Override
			public void run() {
				mIsUpdateCurrentLocation = true;
			}
		}, Global.UPDATE_LOCATION_RESUME_TIME);
	}

	public void stopUpdateLocationResumeTimer() {
		if(mUpdateLocationResumeTimer != null) {
			mUpdateLocationResumeTimer.cancel();
			mUpdateLocationResumeTimer = null;
		}
	}

	public void initDatabase() {
		try {
			if(mDatabase == null) {
				if(mDatabaseHelper == null) {
					mDatabaseHelper = DatabaseHelper.getInstance(this);
				}

				mDatabase = mDatabaseHelper.getReadableDatabase();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void init() {
		try {
			mLoadingText.setText("데이터 다운로드중...");
			mNoticeCurrentPage = 0;
			mNoticeTotalPage = 0;
			mSelectedInsuranceCompany = 0;
			mIsUpdateCurrentLocation = true;
			mNoticeDataList = new ArrayList<>();
			mInsuranceCompanyDataList = new ArrayList<>();

			mSupportFragmentManager = getSupportFragmentManager();

			initDatabase();


			mLoadingText.setText("데이터 로딩중...");
			if (SVUtil.GetLocationServiceStatus(this) == 0) {
				try {
					sendBroadcast(new Intent("com.skt.intent.action.GPS_TURN_ON"));
					PopUpLocationSettingDialog(2); //network provider도 켜라는 팝업창
				} catch (Exception e) {
					PopUpLocationSettingDialog(0);
				}
			} else if (SVUtil.GetLocationServiceStatus(this) == 2) {
				PopUpLocationSettingDialog(2); //network provider도 켜라는 팝업창
			} else if (SVUtil.GetLocationServiceStatus(this) == 1) {
				if (!SVUtil.GetWifiOnStatus(this)) {
					PopUpWifiSettingDialog();
				}
			}


			startLocationService();
			startBeaconService();

//			TMapView tmapview = new TMapView(mContext);
//			tmapview.setSKPMapApiKey(Global.TMAP_API_KEY);
			TMapTapi tmaptapi = new TMapTapi(mContext);
			tmaptapi.setSKTMapAuthentication (Global.TMAP_API_KEY);

			ViewGroup.LayoutParams params = mImgBtnShuttle.getLayoutParams();
			params.width = getDisplayWidth() / 5;

			mImgBtnOutback.setLayoutParams(params);
			mImgBtnShuttle.setLayoutParams(params);
			mImgBtnCarpool.setLayoutParams(params);
			mImgBtn2in1.setLayoutParams(params);
			mImgBtnFindPath.setLayoutParams(params);
			mImgBtnCallStats.setLayoutParams(params);
			mImgBtnQuestion.setLayoutParams(params);

			mImgBtnOutback.setOnClickListener(this);
			mImgBtnShuttle.setOnClickListener(this);
			mImgBtnCarpool.setOnClickListener(this);
			mImgBtn2in1.setOnClickListener(this);
			mImgBtnFindPath.setOnClickListener(this);
			mImgBtnCallStats.setOnClickListener(this);
			mImgBtnQuestion.setOnClickListener(this);

			if(!mActionFragmentName.isEmpty() && !mActionId.isEmpty()) {
				moveToFragmentByNotification(mActionFragmentName, mActionId);
			} else {
//				changeButtonImage(mImgBtnOutback);

				mFragmentOutback = new OutbackFragment();
				mFragmentOutback.setArguments(null);

				removeGoogleMapFragment(Global.FRAG_OUTBACK_TAG, R.id.outback_map);

				ChangeOutbackFragmentHandler.sendEmptyMessageDelayed(0, 100);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void startLocationService() {
		try {
			SVUtil.log("error", TAG, "startLocationService()");
			Intent serviceIntent = new Intent();
			serviceIntent.putExtra("start_from", "main");
			serviceIntent.setClass(this, SendLocationService.class);

			stopService(serviceIntent);

			startService(serviceIntent);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void startBeaconService() {
		try {
			SVUtil.log("error", TAG, "startBeaconService()");
			Intent serviceIntent = new Intent();
			serviceIntent.setClass(this, BeaconService.class);

			stopService(serviceIntent);

			startService(serviceIntent);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void finishFirstLoading() {
		try {
			getShuttleCategory();
			getNotice();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void getNotice() {
		try {
			NetClient.send(mContext, Global.URL_NOTICE_LIST, "GET", null, new NoticeListAck(), new NetResponseCallback(new NetResponse() {
				@Override
				public void onResponse(NetAPI _netAPI) {
					try {
						if (_netAPI != null) {
							NoticeListAck retNetApi = (NoticeListAck) _netAPI;
							ResultData result = retNetApi.getmResultData();
							if (result.getmCode() == 100) {
								mNoticeDataList = retNetApi.getmNoticeDataList();
							}
						}

						removeSplashFragment();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void getShuttleCategory() {
		try {
			NetClient.send(mContext, Global.URL_SHUTTLE_CATEGORY, "GET", null, new ShuttleCategoryListAck(), new NetResponseCallback(new NetResponse() {
				@Override
				public void onResponse(NetAPI _netAPI) {
					try {
						if (_netAPI != null) {
							ShuttleCategoryListAck retNetApi = (ShuttleCategoryListAck) _netAPI;
							ResultData result = retNetApi.getmResultData();
							if (result.getmCode() == 100) {
								ArrayList<String> retCategory = retNetApi.getmShuttleCategoryList();
								mShuttleCategoryList = new String[retCategory.size()];

								for(int i = 0; i < retCategory.size(); i++) {
									mShuttleCategoryList[i] = retCategory.get(i);
								}
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private int getDisplayWidth() {
		DisplayMetrics dm = new DisplayMetrics();

		try {
			getWindowManager().getDefaultDisplay().getMetrics(dm);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return dm.widthPixels;
	}

	private void PopUpLocationSettingDialog(int status) {
		String msg = "";

		if (status == 0) {
			msg = "달빛기사에서 내 위치 정보를 사용하려면, 단말기의 설정에서 '위치 서비스' 사용을 허용해주세요.";
		} else if (status == 2) {
			msg = "위치서비스에서 무선 네트워크 사용을 허용해주세요. 위치를 빠르게 찾을 수 있습니다.";
		}

		SVUtil.showDialogWithListener(mContext, "위치 서비스", msg, "설정하기", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				try {
					Intent viewIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
					startActivityForResult(viewIntent, kLocationSetting);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}, "취소", null);
	}

	private void PopUpWifiSettingDialog() {
		SVUtil.showDialogWithListener(mContext, "Wi-Fi", "Wi-Fi를 켜면 위치 정확도를 높일 수 있습니다.", "설정하기", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				try {
					Intent viewIntent = new Intent(Settings.ACTION_WIFI_SETTINGS);
					startActivityForResult(viewIntent, kWifiSetting);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}, "취소", null);
	}

	public void addCurrentLocationToPastPath(LatLng _currentLocation) {
		try {
//			if(mPastPathList.size() > 0) {
//				float[] dist = new float[2];
//				Location.distanceBetween(mPastPathList.get(mPastPathList.size() - 1).latitude, mPastPathList.get(mPastPathList.size() - 1).longitude, _currentLocation.latitude, _currentLocation.longitude, dist);
//
//				if(dist[0] >= 50) {
//					return;
//				}
//			}

			mPastPathList.add(_currentLocation);

			if(mPastPathList.size() > 200) {
				mPastPathList.remove(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		try {
			if (requestCode == kLocationSetting) {
				SVUtil.log("error", TAG, "startLocationService() from onActivityResult() kLocationSetting");
				startLocationService();
			} else if (requestCode == kWifiSetting) {
				if (SVUtil.GetWifiOnStatus(this)) {
					SVUtil.log("error", TAG, "startLocationService() from onActivityResult() kWifiSetting");
					startLocationService();
				} else {
					PopUpWifiSettingDialog();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void changeButtonImage(View _btnSelected) {
		try {
			mImgBtnOutback.setSelected(false);
			mImgBtnShuttle.setSelected(false);
			mImgBtnCarpool.setSelected(false);
			mImgBtn2in1.setSelected(false);
			mImgBtnFindPath.setSelected(false);
			mImgBtnCallStats.setSelected(false);
			mImgBtnQuestion.setSelected(false);

			_btnSelected.setSelected(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onClick(View _btnSelected) {
		try {
			SVUtil.hideSoftInput(MainActivity.this);

			if (_btnSelected == mImgBtnOutback) { // 주변 정류장
				if (mFragmentOutback == null || !mFragmentOutback.mFragmentFrom.equals("main")) {
					mFragmentOutback = new OutbackFragment();
					removeGoogleMapFragment(Global.FRAG_OUTBACK_TAG, R.id.outback_map);
				}

				ChangeOutbackFragmentHandler.sendEmptyMessageDelayed(0, 100);
			} else if (_btnSelected == mImgBtnShuttle) { // 셔틀/심야
				if (mFragmentShuttle == null) {
					mFragmentShuttle = new ShuttleFragment();
				}

				replaceFragment(mFragmentShuttle, R.id.mainFrameLayout, Global.FRAG_SHUTTLE_TAG, true);
			} else if (_btnSelected == mImgBtnCarpool) { // 택시카풀
				if (mFragmentCarpool == null) {
					mFragmentCarpool = new CarpoolFragment();
				}

				replaceFragment(mFragmentCarpool, R.id.mainFrameLayout, Global.FRAG_CARPOOL_TAG, true);
			} else if (_btnSelected == mImgBtn2in1) { // 2인1조
				if (mFragmentTwoInOne == null) {
					mFragmentTwoInOne = new TwoInOneFragment();
				}

				replaceFragment(mFragmentTwoInOne, R.id.mainFrameLayout, Global.FRAG_TWOINONE_TAG, true);
			} else if (_btnSelected == mImgBtnFindPath) { // 길찾기
				if (mFragmentFindPath == null) {
					mFragmentFindPath = new FindPathFragment();
				}

				replaceFragment(mFragmentFindPath, R.id.mainFrameLayout, Global.FRAG_FINDPATH_TAG, true);
			} else if (_btnSelected == mImgBtnCallStats) { // 콜 많은 지역
				if (mFragmentCallStats == null) {
					mFragmentCallStats = new CallStatsFragment();
				}

				replaceFragment(mFragmentCallStats, R.id.mainFrameLayout, Global.FRAG_CALLSTATS_TAG, true);
			} else if(_btnSelected == mImgBtnQuestion) {	// 문의사항
				if(mFragmentQuestion == null) {
					mFragmentQuestion = new QuestionFragment();
				}

				replaceFragment(mFragmentQuestion, R.id.mainFrameLayout, Global.FRAG_QUESTION_TAG, true);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onBackPressed() {
		try {
			long pressTime = System.currentTimeMillis();

			if(pressTime - mLastPressTime <= Global.DOUBLE_PRESS_INTERVAL) {
				new AlertDialog.Builder(mContext)
						.setIcon(android.R.drawable.ic_dialog_alert)
						.setCancelable(false)
						.setTitle("앱 종료")
						.setMessage("앱을 종료하시겠습니까?")
						.setPositiveButton("종료", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								sendLogOut();
								stopWokitokiService();
								SVUtil.finishActivity((Activity) mContext);
							}
						})
						.setNegativeButton("취소", null)
						.show();
			} else if (getFragmentManager().getBackStackEntryCount() > 1) {
				getFragmentManager().popBackStack();
				mLastPressTime = pressTime;

//				SVUtil.log("error", TAG, "back stack entry count : " + getFragmentManager().getBackStackEntryCount());
			} else if(getFragmentManager().getBackStackEntryCount() == 1) {
				new AlertDialog.Builder(mContext)
						.setIcon(android.R.drawable.ic_dialog_alert)
						.setCancelable(false)
						.setTitle("앱 종료")
						.setMessage("앱을 종료하시겠습니까?")
						.setPositiveButton("종료", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								stopWokitokiService();
								sendLogOut();
								SVUtil.finishActivity((Activity) mContext);
							}
						})
						.setNegativeButton("취소", null)
						.show();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void sendLogOut() {
		try {
//			SVUtil.log("error", TAG, "sendLogOut()");
			RequestParams params = new RequestParams();
			params.add("driverId", mDriverInfo.getmDriverId());
			NetClient.send(mContext, Global.URL_DRIVER_LOGOUT, "POST", params, new NoResponseDataAck(), new NetResponseCallback(new NetResponse() {
				@Override
				public void onResponse(NetAPI _netAPI) {
				}
			}));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void stopWokitokiService() {
		try {
			ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
			List<ActivityManager.RunningServiceInfo> info;

			info = am.getRunningServices(Integer.MAX_VALUE);
//			Log.i("aa", "Service 갯수 : " + info.size());
			for (Iterator it = info.iterator(); it.hasNext(); ) {
				ActivityManager.RunningServiceInfo runningServiceInfo = (ActivityManager.RunningServiceInfo) it.next();
				//Log.i("aa", runningServiceInfo.service.getClassName() + "");
				if (runningServiceInfo.service.getClassName().contains("kr.moonlightdriver.driver.activity.WokitokiService")) {
					Intent i = new Intent();
					i.setComponent(runningServiceInfo.service);
					stopService(i);
					Log.i("aa", "Service 종료 시킴");

					NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

					NotificationCompat.Builder builder = new NotificationCompat.Builder(MainActivity.this);
					builder.setSmallIcon(R.drawable.ic_launcher)
							.setContentTitle("달빛기사")
							.setContentText("접속이 종료되었습니다. 다시 눌러 시작할 수 있습니다."); // 알림바에서 자동 삭제;
					builder.setAutoCancel(true);
					builder.setOngoing(false);
					Intent intent = new Intent(getApplicationContext(), ServiceExtenstionDialog.class);
					PendingIntent pIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
					builder.setContentIntent(pIntent);
					manager.notify(1, builder.build());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

//	public void clearBackStackFragment() {
//		try {
//			int count = getFragmentManager().getBackStackEntryCount();
//
//			for (int i = 0; i < count; i++) {
//				getFragmentManager().popBackStackImmediate();
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	public void removeGoogleMapFragment(String _tag, int _mapResourceId) {
		try {
			Fragment f = getFragmentManager().findFragmentByTag(_tag);

			if (f != null) {
				SupportMapFragment mapFragment = (SupportMapFragment) MainActivity.mSupportFragmentManager.findFragmentById(_mapResourceId);
				if(mapFragment != null) {
					MainActivity.mSupportFragmentManager.beginTransaction().remove(mapFragment).commit();
				}

				getFragmentManager().beginTransaction().remove(f).commit();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void replaceFragment(Fragment _frag, int _view, String _tag, boolean _addToBackStack) {
		try {
			FragmentTransaction transaction = getFragmentManager().beginTransaction();
			transaction.replace(_view, _frag, _tag);

			if(_addToBackStack) {
				transaction.addToBackStack(_tag);
			}

			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void makePhoneCall() {
		try {
			if(checkPhoneCallPermission()) {
				String uri = "tel:" + Global.HEADQUARTER_NUMBER;

				Intent intent = new Intent(Intent.ACTION_CALL);
				intent.setData(Uri.parse(uri));
				startActivity(intent);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private boolean checkPhoneCallPermission() {
		boolean isPermissionGranted = true;

//		try {
//			if(Build.VERSION.SDK_INT >= 23) {
//				if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//					ActivityCompat.requestPermissions((Activity) mContext, new String[] { Manifest.permission.CALL_PHONE }, Global.CALL_PHONE_REQ_CODE);
//					isPermissionGranted = false;
//				}
//			}
//		} catch(Exception e) {
//			e.printStackTrace();
//			isPermissionGranted = false;
//		}

		return isPermissionGranted;
	}

	public void checkDriverConfirmData(final String _shuttleId, final String _fragmentFrom) {
		try {
			RequestParams params = new RequestParams();
			params.add("driverId", mDriverInfo.getmDriverId());

			NetClient.send(mContext, Global.URL_DRIVER_DATA_CHECK, "POST", params, new CheckDriverConfirmDataAck(), new NetResponseCallback(new NetResponse() {
				@Override
				public void onResponse(NetAPI _netAPI) {
					try {
						if (_netAPI != null) {
							CheckDriverConfirmDataAck retNetApi = (CheckDriverConfirmDataAck) _netAPI;
							ResultData result = retNetApi.getmResultData();
							mInsuranceCompanyDataList = retNetApi.getmInsuranceCompanyDataList();

							if (result.getmCode() == 100) {
								mFragmentShuttleChatting = new ShuttleChattingFragment();

								Bundle args = new Bundle();
								args.putString("shuttleId", _shuttleId);
								args.putString("fragmentFrom", _fragmentFrom);

								mFragmentShuttleChatting.setArguments(args);

								replaceFragment(mFragmentShuttleChatting, R.id.mainFrameLayout, Global.FRAG_SHUTTLE_CHATTING_TAG, true);
							} else if (result.getmCode() == 229) {
								SVUtil.showDialogWithListener(mContext, "알림", "대리운전 기사로 등록 하시면 확인 가능합니다.\n등록 하시겠습니까?", "확인", new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										int listSize = mInsuranceCompanyDataList.size();
										String[] insuranceCompany = new String[listSize + 1];
										insuranceCompany[0] = "선택";

										for (int i = 1; i < listSize + 1; i++) {
											insuranceCompany[i] = mInsuranceCompanyDataList.get(i - 1).getmCompanyName();
										}

										dialog.dismiss();

										showDriverConfirmDataPopup(insuranceCompany);
									}
								}, "취소", new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										dialog.dismiss();
									}
								});
							} else {
								SVUtil.showSimpleDialog(mContext, result.getmDetail());
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void showDriverConfirmDataPopup(String[] _insuranceCompany) {
		try {
			LayoutInflater factory = LayoutInflater.from(this);
			View layoutView = factory.inflate(R.layout.popup_driver_confirm_data, null);

			final EditText driverNameEditText = (EditText) layoutView.findViewById(R.id.popup_driver_confirm_data_name);
			final EditText driverLicenseEditText = (EditText) layoutView.findViewById(R.id.popup_driver_confirm_data_driver_license);
			final EditText insuranceNumberEditText = (EditText) layoutView.findViewById(R.id.popup_driver_confirm_data_insurance_number);
			Spinner insuranceCompanySpinner = (Spinner) layoutView.findViewById(R.id.popup_driver_confirm_data_insurance_company);
			ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, _insuranceCompany);
			insuranceCompanySpinner.setAdapter(adapter);

			insuranceCompanySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
				@Override
				public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
					mSelectedInsuranceCompany = position;
				}

				@Override
				public void onNothingSelected(AdapterView<?> parent) {
				}
			});

			final AlertDialog dialog = SVUtil.showDialogWithListener(mContext, "대리운전 기사 정보", layoutView, "등록", null, "취소", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});

			dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						boolean isValid = true;
						String message = "";
						String driverName = driverNameEditText.getText().toString();
						String driverLicense = driverLicenseEditText.getText().toString();
						String insuranceNumber = insuranceNumberEditText.getText().toString();

						if (driverName.isEmpty()) {
							isValid = false;
							message = "이름을 입력해주세요.";
						}

						if (driverLicense.isEmpty()) {
							isValid = false;
							message = "운전면허번호를 입력해주세요.";
						}

						if (mSelectedInsuranceCompany == 0) {
							isValid = false;
							message = "가입된 대리운전 보험회사를 선택해주세요.";
						}

						if (insuranceNumber.isEmpty()) {
							isValid = false;
							message = "보험번호를 입력해주세요.";
						}

						if (isValid) {
							RequestParams params = new RequestParams();
							params.add("driverId", mDriverInfo.getmDriverId());
							params.add("name", driverName);
							params.add("driverLicense", driverLicense);
							params.add("insuranceCompanyId", mInsuranceCompanyDataList.get(mSelectedInsuranceCompany - 1).getmInsuranceCompanyId());
							params.add("insuranceNumber", insuranceNumber);

							NetClient.send(mContext, Global.URL_UPDATE_DRIVER_CONFIRM_DATA, "POST", params, new DriverConfirmDataAck(), new NetResponseCallback(new NetResponse() {
								@Override
								public void onResponse(NetAPI _netAPI) {
									try {
										if (_netAPI != null) {
											DriverConfirmDataAck retNetApi = (DriverConfirmDataAck) _netAPI;
											ResultData result = retNetApi.getmResultData();

											if (result.getmCode() != 100) {
												SVUtil.showSimpleDialog(mContext, result.getmDetail());
											} else {
												dialog.dismiss();

												mDriverInfo = retNetApi.getmDriverData();
												SVUtil.showSimpleDialog(mContext, "등록되었습니다.");
											}
										}
									} catch (Exception e) {
										e.printStackTrace();
									}
								}
							}));
						} else {
							Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	Handler ChangeCarpoolFragmentHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			try {
				String _actionId = msg.obj.toString();

				Bundle carpoolArgs = new Bundle();
				carpoolArgs.putString("taxiId", _actionId);

				CarpoolDetailFragment frag_carpool_detail = new CarpoolDetailFragment();
				frag_carpool_detail.setArguments(carpoolArgs);
				replaceFragment(frag_carpool_detail, R.id.mainFrameLayout, Global.FRAG_CARPOOL_DETAIL_TAG, false);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};

	public Handler ChangeOutbackFragmentHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			try {
				replaceFragment(mFragmentOutback, R.id.mainFrameLayout, Global.FRAG_OUTBACK_TAG, true);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};

	private void moveToFragmentByNotification(String _actionFragment, String _actionId) {
		try {
			switch (_actionFragment) {
				case "CarpoolDetail":
					removeGoogleMapFragment(Global.FRAG_CARPOOL_DETAIL_TAG, R.id.carpool_detail_map);

					ChangeCarpoolFragmentHandler.sendMessageDelayed(ChangeCarpoolFragmentHandler.obtainMessage(0, _actionId), 100);
					break;
				case "QuestionDetail":
					Bundle questionArgs = new Bundle();
					questionArgs.putString("questionId", _actionId);

					QuestionDetailFragment frag_question_detail = new QuestionDetailFragment();
					frag_question_detail.setArguments(questionArgs);

					replaceFragment(frag_question_detail, R.id.mainFrameLayout, Global.FRAG_QUESTION_DETAIL_TAG, false);
					break;
				case "TwoInOneDetail" :
					Bundle twoInOneArgs = new Bundle();
					twoInOneArgs.putString("boardId", _actionId);

					TwoInOneDetailFragment frag_twoinone_detail = new TwoInOneDetailFragment();
					frag_twoinone_detail.setArguments(twoInOneArgs);

					replaceFragment(frag_twoinone_detail, R.id.mainFrameLayout, Global.FRAG_TWOINONE_DETAIL_TAG, false);
					break;
				//todo 새롭게 추가됨
				case "ShuttlePointListDetail":
					Bundle shuttleArgs = new Bundle();
					shuttleArgs.putString("shuttleId", _actionId);
					shuttleArgs.putBoolean("isNotifi", true);
					ShuttleChattingFragment shuttleChattingFragment;
					ShuttleChattingFragment fragment = (ShuttleChattingFragment)getFragmentManager().findFragmentByTag(Global.FRAG_SHUTTLE_CHATTING_TAG);
					if(fragment != null && fragment.isVisible()) {
						Log.e("MainActivity", "no ShuttleChattingFragment");
						shuttleChattingFragment = mFragmentShuttleChatting;
					}else {
						Log.e("MainActivity", "new ShuttleChattingFragment and replace");
						shuttleChattingFragment = new ShuttleChattingFragment();
						shuttleChattingFragment.setArguments(shuttleArgs);
						replaceFragment(shuttleChattingFragment, R.id.mainFrameLayout, Global.FRAG_SHUTTLE_CHATTING_TAG, true);


					}
					break;

			}

			if(!mIsSplashRemoved) {
				mIsSplashRemoved = true;
				removeSplashFragment();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private boolean checkLocationPermission() {
		boolean isPermissionGranted = true;

//		try {
//			if(Build.VERSION.SDK_INT >= 23) {
//				if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
//						|| ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//					ActivityCompat.requestPermissions((Activity) mContext, new String[] { Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION }, Global.ACCESS_LOCATION_REQ_CODE);
//					isPermissionGranted = false;
//				}
//			}
//		} catch(Exception e) {
//			e.printStackTrace();
//			isPermissionGranted = false;
//		}

		return isPermissionGranted;
	}

	private class ChangeLocationReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			try {
				String action = intent.getAction();
				switch (action) {
					case SendLocationService.ACTION :
						double lat = intent.getDoubleExtra("lat", 0);
						double lng = intent.getDoubleExtra("lng", 0);
						double alt = intent.getDoubleExtra("alt", 0);

						if(lat == 0 || lng == 0) {
							return;
						}

						mCurrentLocation = new LatLng(lat, lng);

						addCurrentLocationToPastPath(mCurrentLocation);

						GeomagneticField field = new GeomagneticField((float) lat, (float) lng, (float) alt, System.currentTimeMillis());

						if(mFragmentOutback != null && mIsUpdateCurrentLocation && mFragmentOutback.mIsFragmentRunning) {
							mFragmentOutback.changeMyLocation(field.getDeclination());
						}

						if(mFragmentShuttleChatting != null && !ShuttleChattingFragment.mIsFinishFragment) {
							mFragmentShuttleChatting.changeMyLocation();
						}

						if(mFragmentCarpool != null && !CarpoolFragment.mIsFinishFragment) {
							mFragmentCarpool.changeMyLocation();
						}
						break;
					case SendLocationService.CHECK_PERMISSION_REQ :
						String permission_type = intent.getStringExtra("permissionType");
						SVUtil.log("error", TAG, "onReceive() permission_type : " + permission_type);
						if(permission_type != null) {
							if(permission_type.equals("location_permission")) {
								checkLocationPermission();
							} else if(permission_type.equals("phone_number_permission")) {
								checkPhoneNumberPermission();
							}
						}
						break;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	protected void onStart() {
		try {
			mChangeLocationReceiver = new ChangeLocationReceiver();
			IntentFilter intentFilter = new IntentFilter();
			intentFilter.addAction(SendLocationService.ACTION);
			intentFilter.addAction(SendLocationService.CHECK_PERMISSION_REQ);

			registerReceiver(mChangeLocationReceiver, intentFilter);
		} catch (Exception e) {
			e.printStackTrace();
		}

		super.onStart();
	}

	@Override
	protected void onResume() {
		super.onResume();
		Log.e("MainActivity", "onResume");
		try {
			sendBroadcast(new Intent("com.skt.intent.action.GPS_TURN_ON"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onStop() {
		Log.e("MainActivity", "onStop");
		try {
			sendBroadcast(new Intent("com.skt.intent.action.GPS_TURN_OFF"));

			unregisterReceiver(mChangeLocationReceiver);
		} catch (Exception e) {
			e.printStackTrace();
		}

		super.onStop();
	}

	@Override
	protected void onDestroy() {
		SVUtil.log("error", TAG, "onDestroy()");
		Log.e("MainActivity", "onDestory");
		super.onDestroy();

		try {
			sendLogOut();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

//	@Override
//	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//		try {
//			SVUtil.log("error", TAG, "requestCode : " + requestCode);
//			if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
//				switch (requestCode) {
//					case Global.CALL_PHONE_REQ_CODE:
//						makePhoneCall();
//						break;
//					case Global.PHONE_NUMBER_REQ_CODE :
//						SVUtil.log("error", TAG, "getUserPhoneNumber : " + Global.PHONE_NUMBER_REQ_CODE);
//						getUserPhoneNumber();
//						break;
//					case Global.ACCESS_LOCATION_REQ_CODE :
//						startLocationService();
//						break;
//				}
//			} else {
//				Toast.makeText(mContext, "권한 승인이 거부 되었습니다.", Toast.LENGTH_SHORT).show();
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
}