package kr.moonlightdriver.driver.Network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

import kr.moonlightdriver.driver.Network.NetData.ResultData;
import kr.moonlightdriver.driver.Network.NetData.ShuttleTransferData;

/**
 * Created by youngmin on 2015-10-13.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShuttleTransferResultAck extends NetAPI {
	@JsonProperty("result")
	private ResultData mResultData;

	@JsonProperty("transferData")
	private ArrayList<ShuttleTransferData> mShuttleTransferDataList;

	public ShuttleTransferResultAck() {
	}

	public ShuttleTransferResultAck(ResultData mResultData, ArrayList<ShuttleTransferData> mShuttleTransferDataList) {
		this.mResultData = mResultData;
		this.mShuttleTransferDataList = mShuttleTransferDataList;
	}

	public ResultData getmResultData() {
		return mResultData;
	}

	public void setmResultData(ResultData mResultData) {
		this.mResultData = mResultData;
	}

	public ArrayList<ShuttleTransferData> getmShuttleTransferDataList() {
		return mShuttleTransferDataList;
	}

	public void setmShuttleTransferDataList(ArrayList<ShuttleTransferData> mShuttleTransferDataList) {
		this.mShuttleTransferDataList = mShuttleTransferDataList;
	}
}