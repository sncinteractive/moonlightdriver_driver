package kr.moonlightdriver.driver.Network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

import kr.moonlightdriver.driver.Network.NetData.ResultData;
import kr.moonlightdriver.driver.Network.NetData.WokitokiData;

/**
 * Created by lk on 2016. 10. 2..
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class WokitokiListAck extends NetAPI{
    @JsonProperty("result")
    private ResultData mResultData;
    @JsonProperty("wokitokis")
    private ArrayList<WokitokiData> mWokitokilist;

    public WokitokiListAck(){}

    public WokitokiListAck(ResultData mResultData, ArrayList<WokitokiData> mWokitokilist) {
        this.mResultData = mResultData;
        this.mWokitokilist = mWokitokilist;
    }

    public ResultData getmResultData() {
        return mResultData;
    }

    public ArrayList<WokitokiData> getmWokitokilist() {
        return mWokitokilist;
    }
}
