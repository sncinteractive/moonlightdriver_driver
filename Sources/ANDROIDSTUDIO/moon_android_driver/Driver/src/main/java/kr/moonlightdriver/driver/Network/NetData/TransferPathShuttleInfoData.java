package kr.moonlightdriver.driver.Network.NetData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 * Created by youngmin on 2015-12-31.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TransferPathShuttleInfoData {
	@JsonProperty("busId")
	private String mBusId;

	@JsonProperty("shuttleName")
	private String mShuttleName;

	@JsonProperty("routeType")
	private String mRouteType;

	@JsonProperty("phone")
	private String mPhone;

	@JsonProperty("firstTime")
	private String mFirstTime;

	@JsonProperty("lastTime")
	private String mLastTime;

	@JsonProperty("saturdayFirstTime")
	private String mSaturdayFirstTime;

	@JsonProperty("saturdayLastTime")
	private String mSaturdayLastTime;

	@JsonProperty("sundayFirstTime")
	private String mSundayFirstTime;

	@JsonProperty("sundayLastTime")
	private String mSundayLastTime;

	@JsonProperty("term")
	private int mTerm;

	@JsonProperty("totalTime")
	private int mTotalTime;

	@JsonProperty("path")
	private ArrayList<ShuttleTransferLocationData> mPathList;

	public TransferPathShuttleInfoData() {}

	public TransferPathShuttleInfoData(String mBusId, String mShuttleName, String mRouteType, String mPhone, String mFirstTime, String mLastTime, String mSaturdayFirstTime, String mSaturdayLastTime, String mSundayFirstTime, String mSundayLastTime, int mTerm, int mTotalTime, ArrayList<ShuttleTransferLocationData> mPathList) {
		this.mBusId = mBusId;
		this.mShuttleName = mShuttleName;
		this.mRouteType = mRouteType;
		this.mPhone = mPhone;
		this.mFirstTime = mFirstTime;
		this.mLastTime = mLastTime;
		this.mSaturdayFirstTime = mSaturdayFirstTime;
		this.mSaturdayLastTime = mSaturdayLastTime;
		this.mSundayFirstTime = mSundayFirstTime;
		this.mSundayLastTime = mSundayLastTime;
		this.mTerm = mTerm;
		this.mTotalTime = mTotalTime;
		this.mPathList = mPathList;
	}

	public String getmBusId() {
		return mBusId;
	}

	public void setmBusId(String mBusId) {
		this.mBusId = mBusId;
	}

	public int getmTotalTime() {
		return mTotalTime;
	}

	public void setmTotalTime(int mTotalTime) {
		this.mTotalTime = mTotalTime;
	}

	public String getmShuttleName() {
		return mShuttleName;
	}

	public void setmShuttleName(String mShuttleName) {
		this.mShuttleName = mShuttleName;
	}

	public String getmRouteType() {
		return mRouteType;
	}

	public void setmRouteType(String mRouteType) {
		this.mRouteType = mRouteType;
	}

	public String getmPhone() {
		return mPhone;
	}

	public void setmPhone(String mPhone) {
		this.mPhone = mPhone;
	}

	public String getmFirstTime() {
		return mFirstTime;
	}

	public void setmFirstTime(String mFirstTime) {
		this.mFirstTime = mFirstTime;
	}

	public String getmLastTime() {
		return mLastTime;
	}

	public void setmLastTime(String mLastTime) {
		this.mLastTime = mLastTime;
	}

	public String getmSaturdayFirstTime() {
		return mSaturdayFirstTime;
	}

	public void setmSaturdayFirstTime(String mSaturdayFirstTime) {
		this.mSaturdayFirstTime = mSaturdayFirstTime;
	}

	public String getmSaturdayLastTime() {
		return mSaturdayLastTime;
	}

	public void setmSaturdayLastTime(String mSaturdayLastTime) {
		this.mSaturdayLastTime = mSaturdayLastTime;
	}

	public String getmSundayFirstTime() {
		return mSundayFirstTime;
	}

	public void setmSundayFirstTime(String mSundayFirstTime) {
		this.mSundayFirstTime = mSundayFirstTime;
	}

	public String getmSundayLastTime() {
		return mSundayLastTime;
	}

	public void setmSundayLastTime(String mSundayLastTime) {
		this.mSundayLastTime = mSundayLastTime;
	}

	public int getmTerm() {
		return mTerm;
	}

	public void setmTerm(int mTerm) {
		this.mTerm = mTerm;
	}

	public ArrayList<ShuttleTransferLocationData> getmPathList() {
		return mPathList;
	}

	public void setmPathList(ArrayList<ShuttleTransferLocationData> mPathList) {
		this.mPathList = mPathList;
	}
}
