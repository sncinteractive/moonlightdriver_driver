package kr.moonlightdriver.driver.Network.NetData;

import com.google.android.gms.maps.model.LatLng;
import com.vividsolutions.jts.geom.Coordinate;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by youngmin on 2015-12-31.
 */
public class CallStatsCoordinatesData {
	private int mSeoulAreaIndex;
	private String mSiName;
	private String mGuName;
	private String mDongName;
	private int mIsGu;
	private int mIsDong;
	private String mCoordinates;

	public CallStatsCoordinatesData() {}

	public CallStatsCoordinatesData(int mSeoulAreaIndex, String mSiName, String mGuName, String mDongName, int mIsGu, int mIsDong, String mCoordinates) {
		this.mCoordinates = mCoordinates;
		this.mDongName = mDongName;
		this.mGuName = mGuName;
		this.mIsDong = mIsDong;
		this.mIsGu = mIsGu;
		this.mSeoulAreaIndex = mSeoulAreaIndex;
		this.mSiName = mSiName;
	}

	public ArrayList<LatLng> getmCoordinatesGoogle() {
		ArrayList<LatLng> retCoordinate = new ArrayList<>();

		try {
			JSONArray coordinates = new JSONArray(this.mCoordinates);

			for (int k = 0; k < coordinates.length(); k++) {
				double lat = coordinates.getJSONArray(k).getDouble(1);
				double lng = coordinates.getJSONArray(k).getDouble(0);

				retCoordinate.add(new LatLng(lat, lng));
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return retCoordinate;
	}

	public Coordinate[] getmCoordinatesJTS() {
		Coordinate[] jtsCoordinates = null;

		try {
			JSONArray coordinates = new JSONArray(this.mCoordinates);
			jtsCoordinates = new Coordinate[coordinates.length()];

			for (int k = 0; k < coordinates.length(); k++) {
				double lat = coordinates.getJSONArray(k).getDouble(1);
				double lng = coordinates.getJSONArray(k).getDouble(0);

				jtsCoordinates[k] = new Coordinate(lng, lat);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return jtsCoordinates;
	}

	public String getName() {
		if(this.getmIsGu() == 1) {
			return this.mGuName;
		} else {
			return this.mGuName + "_" + this.mDongName;
		}
	}

	public int getGeoType() {
		if(this.getmIsGu() == 1) {
			return 0;
		} else {
			return 1;
		}
	}

	public String getmCoordinates() {
		return mCoordinates;
	}

	public void setmCoordinates(String mCoordinates) {
		this.mCoordinates = mCoordinates;
	}

	public String getmDongName() {
		return mDongName;
	}

	public void setmDongName(String mDongName) {
		this.mDongName = mDongName;
	}

	public String getmGuName() {
		return mGuName;
	}

	public void setmGuName(String mGuName) {
		this.mGuName = mGuName;
	}

	public int getmIsDong() {
		return mIsDong;
	}

	public void setmIsDong(int mIsDong) {
		this.mIsDong = mIsDong;
	}

	public int getmIsGu() {
		return mIsGu;
	}

	public void setmIsGu(int mIsGu) {
		this.mIsGu = mIsGu;
	}

	public int getmSeoulAreaIndex() {
		return mSeoulAreaIndex;
	}

	public void setmSeoulAreaIndex(int mSeoulAreaIndex) {
		this.mSeoulAreaIndex = mSeoulAreaIndex;
	}

	public String getmSiName() {
		return mSiName;
	}

	public void setmSiName(String mSiName) {
		this.mSiName = mSiName;
	}
}
