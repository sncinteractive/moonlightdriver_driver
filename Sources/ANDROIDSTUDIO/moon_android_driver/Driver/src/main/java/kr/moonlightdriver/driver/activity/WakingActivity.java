package kr.moonlightdriver.driver.activity;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;

import kr.moonlightdriver.driver.Network.NetApi.NetAPI;
import kr.moonlightdriver.driver.Network.NetApi.NetResponse;
import kr.moonlightdriver.driver.Network.NetApi.NetResponseCallback;
import kr.moonlightdriver.driver.Network.NetApi.NoResponseDataAck;
import kr.moonlightdriver.driver.Network.NetClient.NetClient;
import kr.moonlightdriver.driver.Network.NetData.ResultData;
import kr.moonlightdriver.driver.R;
import kr.moonlightdriver.driver.utils.Global;
import kr.moonlightdriver.driver.utils.SVUtil;

public class WakingActivity extends Activity implements OnClickListener {
	private static final String TAG = "WakingActivity";
	private Button mBtn1;
	private Button mBtn2;
	private EditText mEditTextMessage;
	private Context mContext;

	private String mTaxiId;
	private String mQuestionId;
	private String mBoardId;
	private String mType;
	private String mApplicantPhoneNumber;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
//		SVUtil.log("error", TAG, "onCreate()");

		super.onCreate(savedInstanceState);

		try {
			this.requestWindowFeature(Window.FEATURE_NO_TITLE);
			setContentView(R.layout.activity_waking);

			mContext = this;

			TextView textViewTitle = (TextView) findViewById(R.id.waking_titleTV);
			TextView btnDivider = (TextView) findViewById(R.id.waking_btn_dividerTV);
			TextView textViewContents = (TextView) findViewById(R.id.waking_TV);
			mEditTextMessage = (EditText) findViewById(R.id.waking_ET);
			mBtn1 = (Button) findViewById(R.id.waking_btn1);
			mBtn2 = (Button) findViewById(R.id.waking_btn2);

			textViewContents.setText(getIntent().getStringExtra("message"));
			mTaxiId = getIntent().getStringExtra("taxiId");
			mApplicantPhoneNumber = getIntent().getStringExtra("phone_apply");
			mType = getIntent().getStringExtra("type");

			switch (mType) {
				case "request_taxi" :
					mBtn1.setOnClickListener(this);
					mBtn2.setOnClickListener(this);
					break;
				case "decline_taxi" :
					mBtn1.setVisibility(View.GONE);
					btnDivider.setVisibility(View.GONE);
					mEditTextMessage.setVisibility(View.GONE);

					textViewTitle.setText("거절 메시지 도착");
					mBtn2.setText("확인");

					mBtn2.setOnClickListener(this);
					break;
				case "success_taxi" :
					mBtn1.setVisibility(View.GONE);
					btnDivider.setVisibility(View.GONE);
					mEditTextMessage.setVisibility(View.GONE);

					textViewTitle.setText("수락 메시지 도착");
					mBtn2.setText("확인");

					mBtn2.setOnClickListener(this);
					break;
				case "create_taxi" :
					mBtn1.setVisibility(View.GONE);
					btnDivider.setVisibility(View.GONE);
					mEditTextMessage.setVisibility(View.GONE);

					textViewTitle.setText("택시 카풀 등록");
					mBtn2.setText("카풀상세정보");

					mBtn2.setOnClickListener(this);
					break;
				case "question_answer" :
					mQuestionId = getIntent().getStringExtra("questionId");
					mBtn1.setVisibility(View.GONE);
					btnDivider.setVisibility(View.GONE);
					mEditTextMessage.setVisibility(View.GONE);

					textViewTitle.setText("문의사항 답변 등록");
					mBtn2.setText("확인");

					mBtn2.setOnClickListener(this);
					break;
				case "question_register" :
					mQuestionId = getIntent().getStringExtra("questionId");
					mBtn1.setVisibility(View.GONE);
					btnDivider.setVisibility(View.GONE);
					mEditTextMessage.setVisibility(View.GONE);

					textViewTitle.setText("신규 문의사항 등록");
					mBtn2.setText("확인");

					mBtn2.setOnClickListener(this);
					break;
				case "twoinone_answer" :
					mBoardId = getIntent().getStringExtra("boardId");
					mBtn1.setVisibility(View.GONE);
					btnDivider.setVisibility(View.GONE);
					mEditTextMessage.setVisibility(View.GONE);

					textViewTitle.setText("2인1조 답변 등록");
					mBtn2.setText("확인");

					mBtn2.setOnClickListener(this);
					break;
			}

			Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
			long[] pattern = {50, 500, 50, 500};
			v.vibrate(pattern, -1);

			Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			Ringtone r = RingtoneManager.getRingtone(this, notification);
			r.play();

			getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onClick(View v) {
		try {
			if (v == mBtn1) {
				String url = Global.URL_CARPOOL_DECLINE.replaceAll(":taxiId", mTaxiId);
				String message = mEditTextMessage.getText().toString();

				sendCarpoolAcceptDeclineMessage(url, mTaxiId, mApplicantPhoneNumber, message);
			} else if (v == mBtn2) {
				if (mType.equals("request_taxi")) {
					String url = Global.URL_CARPOOL_ACCEPT.replaceAll(":taxiId", mTaxiId);
					String message = mEditTextMessage.getText().toString();

					sendCarpoolAcceptDeclineMessage(url, mTaxiId, mApplicantPhoneNumber, message);
				} else if(mType.equals("create_taxi")) {
					changeActivity(mType);
				} else if(mType.equals("success_taxi")) {
					changeActivity(mType);//				finish();
				} else if(mType.equals("decline_taxi")) {
					changeActivity(mType);
				} else if(mType.equals("question_answer")) {
					changeActivity(mType);
				} else if(mType.equals("twoinone_answer")) {
					changeActivity(mType);
				} else if(mType.equals("question_register")) {
					changeActivity(mType);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void sendCarpoolAcceptDeclineMessage(String _url, String _taxiId, String _phoneNumber, String _message) {
		SVUtil.showProgressDialog(mContext, "전송중..");

		RequestParams params = new RequestParams();
		params.add("taxiId", _taxiId);
		params.add("phone", _phoneNumber);
		params.add("message", _message);

		NetClient.send(mContext, _url, "POST", params, new NoResponseDataAck(), new NetResponseCallback(new NetResponse() {
			@Override
			public void onResponse(NetAPI _netAPI) {
				try {
					SVUtil.hideProgressDialog();

					if (_netAPI != null) {
						NoResponseDataAck retNetApi = (NoResponseDataAck) _netAPI;
						ResultData result = retNetApi.getmResultData();

						if (result.getmCode() != 100) {
							SVUtil.showDialogWithListener(mContext, result.getmDetail(), "확인", new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									finish();
								}
							});
						} else {
							finish();
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}));
	}

	private void changeActivity(String _type) {
		Intent intent = new Intent(this, MainActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

		try {
			if(!_type.isEmpty()) {
				switch (_type) {
					case "create_taxi":
					case "success_taxi":
					case "decline_taxi":
						intent.putExtra("actionFragment", "CarpoolDetail");
						intent.putExtra("actionId", mTaxiId);
						break;
					case "question_answer":
					case "question_register" :
						intent.putExtra("actionFragment", "QuestionDetail");
						intent.putExtra("actionId", mQuestionId);
						break;
					case "twoinone_answer" :
						intent.putExtra("actionFragment", "TwoInOneDetail");
						intent.putExtra("actionId", mBoardId);
						break;
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}

		startActivity(intent);
		finish();
	}
}
