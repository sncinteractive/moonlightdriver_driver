package kr.moonlightdriver.driver.Network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

import kr.moonlightdriver.driver.Network.NetData.ChatJoinedDriverData;
import kr.moonlightdriver.driver.Network.NetData.ResultData;
import kr.moonlightdriver.driver.Network.NetData.ShuttleChatData;

/**
 * Created by youngmin on 2015-10-13.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class StartFinishChatAck extends NetAPI {
	@JsonProperty("result")
	private ResultData mResultData;
	@JsonProperty("comments")
	private ArrayList<ShuttleChatData> mShuttleChatDataList;
	@JsonProperty("joinedDrivers")
	private ArrayList<ChatJoinedDriverData> mJoinedDriverDataList;
	@JsonProperty("nickName")
	private String mNickName;

	public StartFinishChatAck() {}

	public StartFinishChatAck(ArrayList<ChatJoinedDriverData> mJoinedDriverDataList, String mNickName, ResultData mResultData, ArrayList<ShuttleChatData> mShuttleChatDataList) {
		this.mJoinedDriverDataList = mJoinedDriverDataList;
		this.mNickName = mNickName;
		this.mResultData = mResultData;
		this.mShuttleChatDataList = mShuttleChatDataList;
	}

	public ArrayList<ChatJoinedDriverData> getmJoinedDriverDataList() {
		return mJoinedDriverDataList;
	}

	public void setmJoinedDriverDataList(ArrayList<ChatJoinedDriverData> mJoinedDriverDataList) {
		this.mJoinedDriverDataList = mJoinedDriverDataList;
	}

	public String getmNickName() {
		return mNickName;
	}

	public void setmNickName(String mNickName) {
		this.mNickName = mNickName;
	}

	public ResultData getmResultData() {
		return mResultData;
	}

	public void setmResultData(ResultData mResultData) {
		this.mResultData = mResultData;
	}

	public ArrayList<ShuttleChatData> getmShuttleChatDataList() {
		return mShuttleChatDataList;
	}

	public void setmShuttleChatDataList(ArrayList<ShuttleChatData> mShuttleChatDataList) {
		this.mShuttleChatDataList = mShuttleChatDataList;
	}
}
