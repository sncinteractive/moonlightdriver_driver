package kr.moonlightdriver.driver.Network.NetData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by youngmin on 2015-10-07.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResultData {
	@JsonProperty("code")
	private int mCode;
	@JsonProperty("define")
	private String mDefine;
	@JsonProperty("detail")
	private String mDetail;

	public ResultData() {}

	public ResultData(int mCode, String mDefine, String mDetail) {
		this.mCode = mCode;
		this.mDefine = mDefine;
		this.mDetail = mDetail;
	}

	public int getmCode() {
		return mCode;
	}

	public void setmCode(int mCode) {
		this.mCode = mCode;
	}

	public String getmDefine() {
		return mDefine;
	}

	public void setmDefine(String mDefine) {
		this.mDefine = mDefine;
	}

	public String getmDetail() {
		return mDetail;
	}

	public void setmDetail(String mDetail) {
		this.mDetail = mDetail;
	}
}
