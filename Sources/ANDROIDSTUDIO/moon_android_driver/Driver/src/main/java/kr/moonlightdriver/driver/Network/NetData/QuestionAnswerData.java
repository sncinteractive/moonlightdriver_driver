package kr.moonlightdriver.driver.Network.NetData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by youngmin on 2015-10-28.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class QuestionAnswerData {
	@JsonProperty("answerId")
	private String mAnswerId;

	@JsonProperty("questionId")
	private String mQuestionId;

	@JsonProperty("answer")
	private String mAnswer;

	@JsonProperty("createdDate")
	private Long mCreatedDate;

	public QuestionAnswerData() {}

	public QuestionAnswerData(String mAnswer, String mAnswerId, Long mCreatedDate, String mQuestionId) {
		this.mAnswer = mAnswer;
		this.mAnswerId = mAnswerId;
		this.mCreatedDate = mCreatedDate;
		this.mQuestionId = mQuestionId;
	}

	public String getmAnswer() {
		return mAnswer;
	}

	public void setmAnswer(String mAnswer) {
		this.mAnswer = mAnswer;
	}

	public String getmAnswerId() {
		return mAnswerId;
	}

	public void setmAnswerId(String mAnswerId) {
		this.mAnswerId = mAnswerId;
	}

	public Long getmCreatedDate() {
		return mCreatedDate;
	}

	public void setmCreatedDate(Long mCreatedDate) {
		this.mCreatedDate = mCreatedDate;
	}

	public String getmQuestionId() {
		return mQuestionId;
	}

	public void setmQuestionId(String mQuestionId) {
		this.mQuestionId = mQuestionId;
	}
}
