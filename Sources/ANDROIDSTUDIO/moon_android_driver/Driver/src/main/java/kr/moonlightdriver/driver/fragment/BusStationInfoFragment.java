package kr.moonlightdriver.driver.fragment;


import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.kevinsawicki.http.HttpRequest;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import cz.msebera.android.httpclient.Header;
import kr.moonlightdriver.driver.Network.NetApi.BusRouteListByStationIdAck;
import kr.moonlightdriver.driver.Network.NetClient.NetClient;
import kr.moonlightdriver.driver.Network.NetData.BusRouteListData;
import kr.moonlightdriver.driver.Network.NetData.BusRouteListDataByStationId;
import kr.moonlightdriver.driver.Network.NetData.BusStationListData;
import kr.moonlightdriver.driver.Network.NetData.ResultData;
import kr.moonlightdriver.driver.R;
import kr.moonlightdriver.driver.activity.MainActivity;
import kr.moonlightdriver.driver.utils.Global;
import kr.moonlightdriver.driver.utils.SVUtil;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * Created by redjjol on 30/10/14.
 */
public class BusStationInfoFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener{
	private static final String TAG = "BusStationInfoFragment";

	private ImageButton mBtnReload;
	private ImageButton mBtnToMap;
	private ArrayList<BusRouteListData> mBusRouteList;
	private BusRouteListAdapter mBusRouteListAdapter;

	private String mStationId;
	private String mArsId;
	private String mLocalStationId;
	private String mBusRouteArea;
	private String mStationName;
	private String mOutbackFrom;
	private String mCurrentPositionName;
	private double mCenterLat;
	private double mCenterLng;
	private double mPositionLat;
	private double mPositionLng;

	private MainActivity mMainActivity;

	private View mView;

	private boolean mIsFinishFragment;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		try {
			mView = inflater.inflate(R.layout.frag_bus_station_info, container, false);

			mMainActivity = (MainActivity) getActivity();

			mIsFinishFragment = false;

			TextView titleTextView = (TextView) mView.findViewById(R.id.station_info_act_titleTV);
			TextView arsIdTextView = (TextView) mView.findViewById(R.id.station_info_frag_arsidTV);
			mBtnReload = (ImageButton) mView.findViewById(R.id.station_info_act_reloadBtn);
			mBtnReload.setOnClickListener(this);

			mBtnToMap = (ImageButton) mView.findViewById(R.id.station_info_frag_btn2);
			mBtnToMap.setOnClickListener(this);

			StickyListHeadersListView busRouteListView = (StickyListHeadersListView) mView.findViewById(R.id.station_info_act_listView);
			busRouteListView.setOnItemClickListener(this);

			mBusRouteList = new ArrayList<>();
			mBusRouteListAdapter = new BusRouteListAdapter(mMainActivity, R.layout.row_station_info, mBusRouteList);
			busRouteListView.setAdapter(mBusRouteListAdapter);

			Bundle bundle = BusStationInfoFragment.this.getArguments();
			mStationId = bundle.getString("stationId", "");
			mArsId = bundle.getString("arsId", "");
			mStationName = bundle.getString("stationName", "");
			mBusRouteArea = bundle.getString("busRouteArea", "");
			mOutbackFrom = bundle.getString("from", "");
			mCurrentPositionName = bundle.getString("positionName", "");
			mCenterLat = bundle.getDouble("centerLat");
			mCenterLng = bundle.getDouble("centerLng");
			mPositionLat = bundle.getDouble("positionLat");
			mPositionLng = bundle.getDouble("positionLng");

			String arsIdText = "[" + mArsId + "]";
			titleTextView.setText(mStationName);
			arsIdTextView.setText(arsIdText);

			getBusStationInfoData();
		} catch (InflateException e) {
			e.printStackTrace();
		}

		return mView;
	}

	@Override
	public void onClick(View view) {
		try {
			if (view == mBtnReload) {
				getBusStationInfoData();
			} else if (view == mBtnToMap) {
				returnToOutbackFragment();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void getBusStationInfoData() {
		try {
			new GetRouteByStationsFromLocalDB().execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void returnToOutbackFragment() {
		try {
			Bundle args = new Bundle();
			args.putString("from", mOutbackFrom);
			args.putString("positionName", mCurrentPositionName);
			args.putDouble("lat", mPositionLat);
			args.putDouble("lng", mPositionLng);
			args.putDouble("centerLat", mCenterLat);
			args.putDouble("centerLng", mCenterLng);

			mMainActivity.mFragmentOutback = new OutbackFragment();
			mMainActivity.mFragmentOutback.setArguments(args);

			mMainActivity.removeGoogleMapFragment(Global.FRAG_OUTBACK_TAG, R.id.outback_map);

			mMainActivity.ChangeOutbackFragmentHandler.sendEmptyMessageDelayed(0, 100);
//		    mMainActivity.replaceFragment(mMainActivity.mFragmentOutback, R.id.mainFrameLayout, Global.FRAG_OUTBACK_TAG);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
		try {
			Bundle args = new Bundle();
			args.putString("busRouteId", mBusRouteList.get(pos).getmBusRouteId());
			args.putString("busRouteName", mBusRouteList.get(pos).getmRouteName());
			args.putString("stationId", mStationId);
			args.putString("arsId", mArsId);
			args.putString("busRouteArea", mBusRouteArea);
			args.putString("stationName", mStationName);
			args.putString("from", mOutbackFrom);
			args.putString("positionName", mCurrentPositionName);
			args.putDouble("centerLat", mCenterLat);
			args.putDouble("centerLng", mCenterLng);
			args.putDouble("positionLat", mPositionLat);
			args.putDouble("positionLng", mPositionLng);

			BusInfoFragment frag = new BusInfoFragment();
			frag.setArguments(args);

			mMainActivity.replaceFragment(frag, R.id.mainFrameLayout, Global.FRAG_BUS_INFO_TAG, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private class GetRouteByStationsFromLocalDB extends AsyncTask<Void, Void, Boolean> {
		ArrayList<BusRouteListData> mRetBusRouteListData;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			SVUtil.showProgressDialog(mMainActivity, "로딩중...");
			this.mRetBusRouteListData = new ArrayList<>();
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			try {
				BusRouteListByStationIdAck retBusRouteListNetApi = getBusRouteListByStationId(mStationId);
				if(retBusRouteListNetApi == null) {
					return false;
				}

				ResultData result = retBusRouteListNetApi.getmResultData();
				if(result.getmCode() != 100) {
					return false;
				}

				ArrayList<BusRouteListDataByStationId> retBusRouteData = retBusRouteListNetApi.getmBusRouteDataListByStationId();
				BusStationListData retBusStationDetailData = retBusRouteListNetApi.getmBusStationDetailData();
				for(BusRouteListDataByStationId busRouteItem : retBusRouteData) {
					if(mIsFinishFragment) {
						return true;
					}

					mLocalStationId = retBusStationDetailData.getmLocalStationId();

					String msg1 = "";
					if(!mLocalStationId.equals("0")) {
						msg1 = "로딩중...";
					}

					this.mRetBusRouteListData.add(new BusRouteListData(busRouteItem.getmBusRouteType(), busRouteItem.getmBusRouteTypeName(), busRouteItem.getmBusRouteName(), busRouteItem.getmBusRouteId(), busRouteItem.getmDirection(),
							busRouteItem.getmNextStation(), msg1, "", busRouteItem.getmLocalBusRouteId(), busRouteItem.getmBusRouteArea()));
				}
			} catch(Exception e) {
				e.printStackTrace();
				return false;
			}

			return true;
		}

		@Override
		protected void onPostExecute(Boolean _retStatus) {
			SVUtil.hideProgressDialog();

			if(mIsFinishFragment) {
				return;
			}

			try {
				if(_retStatus) {
					mBusRouteList.clear();

					for(int i = 0; i < this.mRetBusRouteListData.size(); i++) {
						mBusRouteList.add(this.mRetBusRouteListData.get(i));
					}

					Collections.sort(mBusRouteList, busRouteTypeSort);

					mBusRouteListAdapter.notifyDataSetChanged();

					if(!mLocalStationId.equals("0")) {
						String[] localStationIdArray = mLocalStationId.split("/");

						if(localStationIdArray.length > 0 && !localStationIdArray[0].equals("0")) {
							getSeoulBusData(localStationIdArray[0]);
						}

						if(localStationIdArray.length > 1 && !localStationIdArray[1].equals("0")) {
							new GetGbusData(localStationIdArray[1]).execute();
						}

						if(localStationIdArray.length > 2 && !localStationIdArray[2].equals("0")) {
							localStationIdArray[2] = localStationIdArray[2].replace("ICB", "");
							new GetGbusData(localStationIdArray[2]).execute();
						}
					}
				} else {
					SVUtil.showDialogWithListener(mMainActivity, "정류장 정보 조회 중 오류가 발생하였습니다.\n잠시 후 다시 시도해주세요.", "확인", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							returnToOutbackFragment();
						}
					});
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}

	private BusRouteListByStationIdAck getBusRouteListByStationId(String stationId) {
		BusRouteListByStationIdAck retBusRouteListNetApi = null;
		Reader reader = null;
		JsonParser jp = null;

		try {
			String url = Global.URL_BUS_ROUTE_LIST_BY_STATION_ID + "?stationId=" + stationId;
//			SVUtil.log("error", TAG, "bus route list by station id url : " + url);
			HttpRequest request = HttpRequest.get(url);
			request.acceptGzipEncoding().uncompress(true);
			reader = request.reader();
			ObjectMapper mapper = new ObjectMapper();

			jp = mapper.getFactory().createParser(reader);
			retBusRouteListNetApi = mapper.readValue(jp, BusRouteListByStationIdAck.class);

			reader.close();
			reader = null;
			jp.close();
			jp = null;
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				if(reader != null) {
					reader.close();
				}

				if(jp != null && !jp.isClosed()) {
					jp.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return retBusRouteListNetApi;
	}

	private class ViewHolder {
		public TextView mRouteNameTextView;
		public TextView mDirectionTextView;
		public TextView mNextStationTextView;
		public TextView mMsg1TextView;
		public TextView mMsg2TextView;
	}

	private class ViewHolder_header {
		TextView mHeaderTextView;
	}

	private class BusRouteListAdapter extends ArrayAdapter<BusRouteListData> implements StickyListHeadersAdapter {
		ArrayList<BusRouteListData> mBusInfoBoxList;

		private int cellID;
		ViewHolder holder;

		public BusRouteListAdapter(Context cxt, int cellID, ArrayList<BusRouteListData> _busInfoBoxList) {
			super(cxt, cellID, _busInfoBoxList);
			this.mBusInfoBoxList = _busInfoBoxList;
			this.cellID = cellID;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;

			try {
				if (v == null) {
					LayoutInflater li = (LayoutInflater) mMainActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					v = li.inflate(cellID, null);
					holder = new ViewHolder();
					holder.mRouteNameTextView = (TextView) v.findViewById(R.id.row_station_info_rtNmTV);
					holder.mDirectionTextView = (TextView) v.findViewById(R.id.row_station_info_adirectionTV);
					holder.mNextStationTextView = (TextView) v.findViewById(R.id.row_station_info_nxtStnTV);
					holder.mMsg1TextView = (TextView) v.findViewById(R.id.row_station_info_arrmsg1TV);
					holder.mMsg2TextView = (TextView) v.findViewById(R.id.row_station_info_arrmsg2TV);

					v.setTag(holder);

				} else {
					holder = (ViewHolder) v.getTag();
				}

				BusRouteListData box = mBusInfoBoxList.get(position);

				String direction = (box.getmDirection() != null && !box.getmDirection().isEmpty()) ? (box.getmDirection() + " 방면") : "";
				String nextStation = (box.getmNextStation() != null && !box.getmNextStation().isEmpty()) ? ("다음: " + box.getmNextStation()) : "";
				String msg1 = (box.getmMsg1() != null && !box.getmMsg1().isEmpty()) ? box.getmMsg1() : "도착예정정보없음";

				holder.mRouteNameTextView.setText(box.getmRouteName());
				holder.mDirectionTextView.setText(direction);
				holder.mNextStationTextView.setText(nextStation);
				holder.mMsg1TextView.setText(msg1);
				holder.mMsg2TextView.setText(box.getmMsg2());
			} catch (Exception e) {
				e.printStackTrace();
			}

			return v;
		}

		@Override
		public View getHeaderView(int position, View view, ViewGroup viewGroup) {
			try {
				ViewHolder_header holder;
				if (view == null) {
					holder = new ViewHolder_header();
					view = mMainActivity.getLayoutInflater().inflate(R.layout.row_header, viewGroup, false);
					holder.mHeaderTextView = (TextView) view.findViewById(R.id.row_header_titleTV);
					view.setTag(holder);
				} else {
					holder = (ViewHolder_header) view.getTag();
				}

				BusRouteListData box = mBusInfoBoxList.get(position);

				if(box.getmRouteTypeName() == null || box.getmRouteTypeName().isEmpty()) {
					holder.mHeaderTextView.setText("기타");
				} else {
					holder.mHeaderTextView.setText(box.getmRouteTypeName());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return view;
		}

		@Override
		public long getHeaderId(int position) {
			return mBusInfoBoxList.get(position).getmRouteType();
		}
	}

	private void getSeoulBusData(String _localStationId) {
		AsyncHttpClient asyncHttpClient = NetClient.getAsyncHttpClient();
		final String url = Global.BUS_STATION_INFO_BY_ARSID + _localStationId;

		asyncHttpClient.get(url, new TextHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, String responseBody) {
				try {
					if(mIsFinishFragment) {
						return;
					}

//					SVUtil.log("error", TAG, "getSeoulBusData() url : " + url);
//					SVUtil.log("error", TAG, "getSeoulBusData() responseBody : " + responseBody);

					if (!responseBody.isEmpty()) {
						JSONObject resultHeader = XML.toJSONObject(responseBody).getJSONObject("ServiceResult").getJSONObject("msgHeader");
						if (resultHeader.getInt("headerCd") == 0) {
							JSONObject jsonObj = XML.toJSONObject(responseBody).getJSONObject("ServiceResult").getJSONObject("msgBody");
							JSONArray arrayObj;
							if (jsonObj.toString().contains("[{")) {
								arrayObj = jsonObj.getJSONArray("itemList");
							} else {
								arrayObj = new JSONArray();
								arrayObj.put(jsonObj.getJSONObject("itemList"));
							}

							for (int i = 0; i < arrayObj.length(); i++) {
								if(mIsFinishFragment) {
									return;
								}

								updateSeoulBusInfo(arrayObj.getJSONObject(i));
							}
						}
					}
				} catch (Exception _e) {
					_e.printStackTrace();
				} finally {
					mBusRouteListAdapter.notifyDataSetChanged();
				}
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, String responseBody, Throwable error) {
				SVUtil.log("error", TAG, "ERROR URL : " + url);

				SVUtil.hideProgressDialog();
				error.printStackTrace();

				if (error instanceof java.net.SocketTimeoutException) {
					SVUtil.showTimeoutDialog(mMainActivity);
				}
			}
		});
	}

	private class GetGbusData extends AsyncTask<Void, Void, String> {
		private String mLocalStationId;
		public GetGbusData(String _localStationId) {
			this.mLocalStationId = _localStationId;
		}

		@Override
		protected String doInBackground(Void... voids) {
			try {
				String url = "http://openapi.gbis.go.kr/ws/rest/busarrivalservice/station?serviceKey=" + Global.TRANSPORT_SERVICE_KEY + "&stationId=" + this.mLocalStationId;
				String result = HttpRequest.get(url).body();

//				SVUtil.log("error", TAG, "GetGbusData() url : " + url);
//				SVUtil.log("error", TAG, "GetGbusData() responseBody : " + result);

				if(result != null && !result.isEmpty()) {
					JSONObject resultHeader = XML.toJSONObject(result).getJSONObject("response").getJSONObject("msgHeader");
					if(resultHeader.getInt("resultCode") == 0){
						JSONObject jsonObj = XML.toJSONObject(result).getJSONObject("response").getJSONObject("msgBody");
						JSONArray arrayObj = new JSONArray();

						if (jsonObj.toString().contains("[{")) {
							arrayObj = jsonObj.getJSONArray("busArrivalList");
						} else {
							arrayObj.put(jsonObj.getJSONObject("busArrivalList"));
						}

						for (int i = 0; i < arrayObj.length(); i++) {
							if(mIsFinishFragment) {
								return null;
							}

							updateGBusInfo(arrayObj.getJSONObject(i));
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(String _result) {
			if(mIsFinishFragment) {
				return;
			}

			mBusRouteListAdapter.notifyDataSetChanged();
		}
	}

	private void updateSeoulBusInfo(JSONObject _newData) {
		try {
			for(int i = 0; i < mBusRouteList.size(); i++) {
				BusRouteListData box = mBusRouteList.get(i);
				if(!box.getmBusRoutArea().startsWith("서울")) {
					continue;
				}

				if(box.getmLocalBusRouteId().equals(_newData.optString("busRouteId", ""))) {
					String direction = _newData.optString("adirection", "").trim();
					String nextStation = _newData.optString("nxtStn", "").trim();
					String msg1 = _newData.optString("arrmsg1", "").trim();
					String msg2 = _newData.optString("arrmsg2", "").trim();

					box.setmDirection(direction.isEmpty() ? box.getmDirection() : direction);
					box.setmNextStation(nextStation.isEmpty() ? box.getmNextStation() : nextStation);
					box.setmMsg1(msg1.isEmpty() ? box.getmMsg1() : msg1);
					box.setmMsg2(msg2.isEmpty() ? box.getmMsg2() : msg2);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void updateGBusInfo(JSONObject _newData) {
		try {
			for(int i = 0; i < mBusRouteList.size(); i++) {
				BusRouteListData box = mBusRouteList.get(i);
				if(!(box.getmBusRoutArea().startsWith("경기") || box.getmBusRoutArea().startsWith("인천"))) {
					continue;
				}

				if(box.getmLocalBusRouteId().equals(_newData.optString("routeId", ""))) {
					String locationNo1 = _newData.optString("locationNo1", "").trim();
					String locationNo2 = _newData.optString("locationNo2", "").trim();
					String predictTime1 = _newData.optString("predictTime1", "").trim();
					String predictTime2 = _newData.optString("predictTime2", "").trim();

					box.setmMsg1(locationNo1.isEmpty() ? box.getmMsg1() : (predictTime1 + "분후 [" + locationNo1 + "번째 전]"));
					box.setmMsg2(locationNo2.isEmpty() ? box.getmMsg2() : (predictTime2 + "분후 [" + locationNo2 + "번째 전]"));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onStart() {
		super.onStart();

		mIsFinishFragment = false;
	}

	@Override
	public void onResume() {
		super.onResume();

		try {
			mMainActivity.changeButtonImage(mMainActivity.mImgBtnOutback);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onPause() {
		super.onPause();

		mIsFinishFragment = true;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();

		try {
			if(mView != null){
				ViewGroup parent = (ViewGroup) mView.getParent();
				if(parent!=null){
					parent.removeView(mView);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		setTargetFragment(null, -1);
	}

	private final Comparator<BusRouteListData> busRouteTypeSort = new Comparator<BusRouteListData>() {
		@Override
		public int compare(BusRouteListData box, BusRouteListData box2) {
			return box.getmRouteTypeName().compareTo(box2.getmRouteTypeName());
		}
	};
}
