package kr.moonlightdriver.driver.Network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import kr.moonlightdriver.driver.Network.NetData.CarpoolData;
import kr.moonlightdriver.driver.Network.NetData.ResultData;

/**
 * Created by youngmin on 2015-10-13.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RegisterTaxiCarpoolAck extends NetAPI {
	@JsonProperty("result")
	private ResultData mResultData;
	@JsonProperty("taxi")
	private CarpoolData mTaxiCarpoolData;

	public RegisterTaxiCarpoolAck() {}

	public RegisterTaxiCarpoolAck(ResultData mResultData, CarpoolData mTaxiCarpoolData) {
		this.mResultData = mResultData;
		this.mTaxiCarpoolData = mTaxiCarpoolData;
	}

	public ResultData getmResultData() {
		return mResultData;
	}

	public void setmResultData(ResultData mResultData) {
		this.mResultData = mResultData;
	}

	public CarpoolData getmTaxiCarpoolData() {
		return mTaxiCarpoolData;
	}

	public void setmTaxiCarpoolData(CarpoolData mTaxiCarpoolData) {
		this.mTaxiCarpoolData = mTaxiCarpoolData;
	}
}
