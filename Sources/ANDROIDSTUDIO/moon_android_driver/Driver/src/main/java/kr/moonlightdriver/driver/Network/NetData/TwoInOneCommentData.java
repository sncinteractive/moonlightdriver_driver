package kr.moonlightdriver.driver.Network.NetData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Created by youngmin on 2015-10-28.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TwoInOneCommentData {
	@JsonProperty("driverId")
	private String mDriverId;

	@JsonProperty("nickName")
	private String mNickName;

	@JsonProperty("comment")
	private String mComment;

	@JsonProperty("createdAt")
	private Long mCreatedAt;

	public TwoInOneCommentData() {}

	public TwoInOneCommentData(String mComment, Long mCreatedAt, String mDriverId, String mNickName) {
		this.mComment = mComment;
		this.mCreatedAt = mCreatedAt;
		this.mDriverId = mDriverId;
		this.mNickName = mNickName;
	}

	public String getmComment() {
		return mComment;
	}

	public void setmComment(String mComment) {
		this.mComment = mComment;
	}

	public Long getmCreatedAt() {
		return mCreatedAt;
	}

	public void setmCreatedAt(Long mCreatedAt) {
		this.mCreatedAt = mCreatedAt;
	}

	public String getmDriverId() {
		return mDriverId;
	}

	public void setmDriverId(String mDriverId) {
		this.mDriverId = mDriverId;
	}

	public String getmNickName() {
		return mNickName;
	}

	public void setmNickName(String mNickName) {
		this.mNickName = mNickName;
	}
}
