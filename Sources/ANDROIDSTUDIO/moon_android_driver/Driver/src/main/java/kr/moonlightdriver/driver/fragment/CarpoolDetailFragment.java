package kr.moonlightdriver.driver.fragment;

import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.ui.IconGenerator;
import com.loopj.android.http.RequestParams;
import com.skt.Tmap.TMapData;
import com.skt.Tmap.TMapPoint;
import com.skt.Tmap.TMapPolyLine;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import kr.moonlightdriver.driver.Network.NetApi.CarpoolInfoAck;
import kr.moonlightdriver.driver.Network.NetApi.CarpoolParticipateAck;
import kr.moonlightdriver.driver.Network.NetApi.NetAPI;
import kr.moonlightdriver.driver.Network.NetApi.NetResponse;
import kr.moonlightdriver.driver.Network.NetApi.NetResponseCallback;
import kr.moonlightdriver.driver.Network.NetApi.NoResponseDataAck;
import kr.moonlightdriver.driver.Network.NetClient.NetClient;
import kr.moonlightdriver.driver.Network.NetData.CarpoolData;
import kr.moonlightdriver.driver.Network.NetData.DriverPointData;
import kr.moonlightdriver.driver.Network.NetData.ResultData;
import kr.moonlightdriver.driver.R;
import kr.moonlightdriver.driver.activity.MainActivity;
import kr.moonlightdriver.driver.utils.BalloonAdapter;
import kr.moonlightdriver.driver.utils.Global;
import kr.moonlightdriver.driver.utils.SVUtil;

/**
 * Created by redjjol on 10/11/14.
 */
public class CarpoolDetailFragment extends Fragment implements View.OnClickListener {
	private static final String TAG = "CarpoolDetailFragment";
	private ImageButton mBtnBack;
	private ImageButton mBtnZoomIn;
	private ImageButton mBtnZoomOut;
	private Button mBtnCall;
	private Button mBtnApply;
	private Button mBtnEdit;
	private Button mBtnDelete;
	private Button mBtnMapSize;
	private TextView mStartLocationTextView;
	private TextView mEndLocationTextView;
	private TextView mFareTextView;
	private TextView mParticipatedTextView;
	private TextView mTimeTextView;
	private TextView mRegistrantTextView;
	private LinearLayout mCarpoolDetailLayout;

	private String mRegistrantPhoneNumber;
	private Dialog mDialog;

	private String mTaxiId;
	private MainActivity mMainActivity;
	private GoogleMap mGoogleMap;
	private Marker mShowInfoWindowMarker;
	private View mView;

	private ArrayList<LatLng> mDriversPoint;
	private boolean mIsMapMaxSize;

	private boolean mIsFinishFragment;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		try {
			mView = inflater.inflate(R.layout.frag_carpool_detail, container, false);

			mMainActivity = (MainActivity) getActivity();

			mIsFinishFragment = false;
			mShowInfoWindowMarker = null;

			mDriversPoint = new ArrayList<>();

			Bundle bundle = CarpoolDetailFragment.this.getArguments();
			mTaxiId = bundle.getString("taxiId");
			mRegistrantPhoneNumber = bundle.getString("registrant_phone");

			mCarpoolDetailLayout = (LinearLayout) mView.findViewById(R.id.carpool_detail_layout);
			mRegistrantTextView = (TextView) mView.findViewById(R.id.carpool_detail_registrantTV);
			mStartLocationTextView = (TextView) mView.findViewById(R.id.carpool_detail_startLocTV);
			mEndLocationTextView = (TextView) mView.findViewById(R.id.carpool_detail_endLocTV);
			mFareTextView = (TextView) mView.findViewById(R.id.carpool_detail_fareTV);
			mParticipatedTextView = (TextView) mView.findViewById(R.id.carpool_detail_participatedTV);
			mTimeTextView = (TextView) mView.findViewById(R.id.carpool_detail_timeTV);

			mBtnBack = (ImageButton) mView.findViewById(R.id.carpool_detail_backBtn);
			mBtnCall = (Button) mView.findViewById(R.id.carpool_detail_callBtn);
			mBtnApply = (Button) mView.findViewById(R.id.carpool_detail_requestBtn);
			mBtnEdit = (Button) mView.findViewById(R.id.carpool_detail_editBtn);
			mBtnDelete = (Button) mView.findViewById(R.id.carpool_detail_deleteBtn);
			mBtnMapSize = (Button) mView.findViewById(R.id.carpool_detail_map_size_btn);
			mBtnZoomIn = (ImageButton) mView.findViewById(R.id.carpool_detail_zoomin_btn);
			mBtnZoomOut = (ImageButton) mView.findViewById(R.id.carpool_detail_zoomout_btn);

			mBtnBack.setOnClickListener(this);
			mBtnCall.setOnClickListener(this);
			mBtnApply.setOnClickListener(this);
			mBtnEdit.setOnClickListener(this);
			mBtnDelete.setOnClickListener(this);
			mBtnMapSize.setOnClickListener(this);
			mBtnZoomIn.setOnClickListener(this);
			mBtnZoomOut.setOnClickListener(this);

			mIsMapMaxSize = false;

			setUpMapIfNeeded();

			getCarpoolInfo(mTaxiId);
		} catch(InflateException e) {
			e.printStackTrace();
		}

		return mView;
	}

	private void checkRegistrant() {
		try {
			if(mMainActivity.mDriverInfo != null && mMainActivity.mDriverInfo.getmPhone().equals(mRegistrantPhoneNumber)) {
				mBtnCall.setVisibility(View.GONE);
				mBtnApply.setVisibility(View.GONE);
				mBtnEdit.setVisibility(View.VISIBLE);
				mBtnDelete.setVisibility(View.VISIBLE);
			} else {
				mBtnCall.setVisibility(View.VISIBLE);
				mBtnApply.setVisibility(View.VISIBLE);
				mBtnEdit.setVisibility(View.GONE);
				mBtnDelete.setVisibility(View.GONE);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setUpMapIfNeeded() {
		try {
			if (mGoogleMap == null) {
				mGoogleMap = ((SupportMapFragment) MainActivity.mSupportFragmentManager.findFragmentById(R.id.carpool_detail_map)).getMap();
				if (mGoogleMap != null) {
					setUpMap();
				}
			}

//			if (mGoogleMap == null) {
//				SupportMapFragment mapFragment = (SupportMapFragment) MainActivity.mSupportFragmentManager.findFragmentById(R.id.carpool_detail_map);
//				mapFragment.getMapAsync(this);
//			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

//	@Override
//	public void onMapReady(GoogleMap googleMap) {
//		try {
//			mGoogleMap = googleMap;
//			setUpMap();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	private void setUpMap() {
		try {
			mGoogleMap.getUiSettings().setRotateGesturesEnabled(false);
			mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
			mGoogleMap.getUiSettings().setCompassEnabled(false);

			mGoogleMap.setInfoWindowAdapter(new BalloonAdapter(mMainActivity.getLayoutInflater()));

			mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mMainActivity.mCurrentLocation, 15));

			mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
				@Override
				public boolean onMarkerClick(Marker marker) {
					try {
						String marker_snippet = marker.getSnippet();

						if (marker_snippet == null || marker_snippet.isEmpty()) {
							return true;
						}

//				SVUtil.log("error", TAG, "marker_snippet : " + marker_snippet);
						if (marker_snippet.equals("registrant") || marker_snippet.equals("passenger")) {
							marker.showInfoWindow();
							mShowInfoWindowMarker = marker;
						}
					} catch (Exception e) {
						e.printStackTrace();
					}

					return false;
				}
			});

			mGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
				@Override
				public void onMapClick(LatLng latLng) {
					try {
						if(mShowInfoWindowMarker != null) {
							mShowInfoWindowMarker.hideInfoWindow();
							mShowInfoWindowMarker = null;
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void addRegistrantMarker(String _text, LatLng _position) {
		try {
			mGoogleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_taxi_registrant_40)).position(_position).snippet("registrant").title(_text).anchor(0.5f, 1.0f));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void addPassengerMarker(String _text, LatLng _position) {
		try {
			mGoogleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_taxi_passenger_40)).position(_position).snippet("passenger").title(_text).anchor(0.5f, 1.0f));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void moveToBounds(Polyline p) {
		try {
			LatLngBounds.Builder builder = new LatLngBounds.Builder();
			List<LatLng> arr = p.getPoints();

			for(int i = 0; i < arr.size();i++){
				builder.include(arr.get(i));
			}

			int listSize = mDriversPoint.size();
			for(int i = 0; i < listSize; i++) {
				builder.include(mDriversPoint.get(i));
			}

			LatLngBounds bounds = builder.build();
			int padding = 40; // offset from edges of the map in pixels

			mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, padding));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void getCarpoolInfo(String _taxiId) {
		SVUtil.showProgressDialog(mMainActivity, "로딩중...");

		RequestParams params = new RequestParams();
		params.add("taxiId", _taxiId);

		String url = Global.URL_CARPOOL_INFO.replaceAll(":taxiId", _taxiId);

		NetClient.send(mMainActivity, url, "POST", params, new CarpoolInfoAck(), new NetResponseCallback(new NetResponse() {
			@Override
			public void onResponse(NetAPI _netAPI) {
				SVUtil.hideProgressDialog();

				try {
					if (_netAPI != null && !mIsFinishFragment) {
						CarpoolInfoAck retNetApi = (CarpoolInfoAck) _netAPI;
						ResultData result = retNetApi.getmResultData();
						if (result.getmCode() != 100) {
							SVUtil.showDialogWithListener(mMainActivity, result.getmDetail(), "확인", new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									returnToCarpoolFragment();
								}
							});

							return;
						}

						CarpoolData retData = retNetApi.getmCarpoolInfo();
						ArrayList<DriverPointData> driverPointData = retNetApi.getmDriverPointData();

						String registrant = retData.getmRegistrant() + "기사님";
						String price = retData.getmPrice() + "";
						String participated = driverPointData.size() + "/" + retData.getmDriverListData().getmMax();

						mRegistrantTextView.setText(registrant);
						mStartLocationTextView.setText(retData.getmStartLocation().getmLocationName());
						mEndLocationTextView.setText(retData.getmEndLocation().getmLocationName());
						mFareTextView.setText(price);
						mParticipatedTextView.setText(participated);
						mTimeTextView.setText(new SimpleDateFormat("HH:mm").format(new Date(retData.getmDepartureTime())));
						mRegistrantPhoneNumber = retData.getmRegistrantPhone();

						checkRegistrant();

						LatLng startLocation = new LatLng(retData.getmStartLocation().getmLocations().getmCoordinates().get(1), retData.getmStartLocation().getmLocations().getmCoordinates().get(0));
						LatLng endLocation = new LatLng(retData.getmEndLocation().getmLocations().getmCoordinates().get(1), retData.getmEndLocation().getmLocations().getmCoordinates().get(0));

						mGoogleMap.addMarker(new MarkerOptions().position(startLocation).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_start_findpath)).anchor(0.5f, 1.0f));
						mGoogleMap.addMarker(new MarkerOptions().position(endLocation).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_finish_findpath)).anchor(0.5f, 1.0f));

						drawCarPath(startLocation.latitude, startLocation.longitude, endLocation.latitude, endLocation.longitude);

						int listCount = driverPointData.size();
						LatLng driversPoint;
						for (int i = 0; i < listCount; i++) {
							if (mGoogleMap == null) {
								return;
							}

							DriverPointData item = driverPointData.get(i);
							driversPoint = new LatLng(item.getmLocation().getmCoordinates().get(1), item.getmLocation().getmCoordinates().get(0));
							if (item.getmPhoneNumber().equals(mRegistrantPhoneNumber)) {
								addRegistrantMarker("등록자 " + retData.getmRegistrant(), driversPoint);
							} else {
								addPassengerMarker("동승자 " + item.getmPhoneNumber().substring(item.getmPhoneNumber().length() - 4, item.getmPhoneNumber().length()), driversPoint);
							}

							mDriversPoint.add(driversPoint);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}));
	}

	private void returnToCarpoolFragment() {
		try {
			mMainActivity.mFragmentCarpool = new CarpoolFragment();
			mMainActivity.replaceFragment(mMainActivity.mFragmentCarpool, R.id.mainFrameLayout, Global.FRAG_CARPOOL_TAG, false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void moveToCarpoolMakeFragment() {
		try {
			CarpoolMakeFragment frag = new CarpoolMakeFragment();

			Bundle args = new Bundle();
			args.putString("taxiId", mTaxiId);

			frag.setArguments(args);

			FragmentTransaction transaction = getFragmentManager().beginTransaction();
			transaction.replace(R.id.mainFrameLayout, frag, Global.FRAG_CARPOOL_MAKE_TAG);
			transaction.addToBackStack(null);
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void drawCarPath(double _start_lat, double _start_lng, double _end_lat, double _end_lng) {
		try {
			TMapPoint startPoint = new TMapPoint(_start_lat, _start_lng);
			TMapPoint endPoint = new TMapPoint(_end_lat, _end_lng);

			new CarPathSearchTask().execute(startPoint, endPoint);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	class CarPathSearchTask extends AsyncTask<TMapPoint, Integer, TMapPolyLine> {
		private int mDistance;
		private int mTime;
		private LatLng mEndPosition;

		@Override
		protected void onPreExecute() {
			this.mDistance = 0;
			this.mTime = 0;
		}

		@Override
		protected TMapPolyLine doInBackground(TMapPoint... params) {
			TMapData data = new TMapData();
			TMapPoint start = params[0];
			TMapPoint end = params[1];

			try {
				mEndPosition = new LatLng(end.getLatitude(), end.getLongitude());
				Document doc = data.findPathDataAllType(TMapData.TMapPathType.CAR_PATH, start, end);
				NodeList childNodes = doc.getElementsByTagName("Document").item(0).getChildNodes();
				int childSize = childNodes.getLength();

				for(int i = 0; i < childSize; i++) {
					Node childItem = childNodes.item(i);
					if(childItem.getNodeName().equals("tmap:totalDistance")) {
						mDistance = Integer.parseInt(childItem.getFirstChild().getNodeValue());
					} else if (childItem.getNodeName().equals("tmap:totalTime")) {
						mTime = Integer.parseInt(childItem.getFirstChild().getNodeValue());
					}
				}

				return data.findPathDataWithType(TMapData.TMapPathType.CAR_PATH, start, end);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(TMapPolyLine result) {
			try {
				if(mIsFinishFragment) {
					return;
				}

				if (result != null && mGoogleMap != null && mView != null) {
					ArrayList<TMapPoint> tmapPolylinePath = result.getLinePoint();
					ArrayList<LatLng> googlePolylinePath = new ArrayList<>();

					int pathSize = tmapPolylinePath.size();
					for (int i = 0; i < pathSize; i++) {
						googlePolylinePath.add(new LatLng(tmapPolylinePath.get(i).getLatitude(), tmapPolylinePath.get(i).getLongitude()));
					}

					PolylineOptions opt = new PolylineOptions();
					opt.color(Color.BLUE);
					opt.width(10);
					opt.addAll(googlePolylinePath);

					Polyline pathPolyline = mGoogleMap.addPolyline(opt);

					String totalTime = SVUtil.getTotalTimeString(mTime);
					String totalDist;
					if(mDistance >= 1000) {
						double distances = mDistance / 1000.0;
						totalDist = String.format("%.2f", distances) + "km";
					} else {
						totalDist = mDistance + "m";
					}

					makeDistanceMarker(mEndPosition, "거리 : " + totalDist + "\n" + "소요시간 : " + totalTime, 180, -180);

					moveToBounds(pathPolyline);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void makeDistanceMarker(LatLng _position, String _text, int _rotate, int _contentRotate) {
		try {
			IconGenerator iconFactory = new IconGenerator(mMainActivity);
			iconFactory.setRotation(_rotate);
			iconFactory.setContentRotation(_contentRotate);
			MarkerOptions markerOptions = new MarkerOptions().
					icon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon(_text))).
					position(_position).
					anchor(iconFactory.getAnchorU(), iconFactory.getAnchorV());

			mGoogleMap.addMarker(markerOptions);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void destroyFragment() {
		try {
			if(mGoogleMap != null) {
				mGoogleMap.clear();
				mGoogleMap = null;
			}

			SupportMapFragment f = (SupportMapFragment) MainActivity.mSupportFragmentManager.findFragmentById(R.id.carpool_detail_map);
			if (f != null) {
				MainActivity.mSupportFragmentManager.beginTransaction().remove(f).commitAllowingStateLoss();
			}

			if (mView != null) {
				ViewGroup parent = (ViewGroup) mView.getParent();
				if (parent != null)
					parent.removeView(mView);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onClick(View view) {
		try {
			if (view == mBtnBack) {
				returnToCarpoolFragment();
			} else if (view == mBtnCall) {
				String uri = "tel:" + mRegistrantPhoneNumber;

				Intent intent = new Intent(Intent.ACTION_CALL);
				intent.setData(Uri.parse(uri));

				startActivity(intent);
			} else if (view == mBtnApply) {
				showParticipateDialog();
			} else if(view == mBtnEdit) {
				moveToCarpoolMakeFragment();
			} else if(view == mBtnDelete) {
				SVUtil.showDialogWithListener(mMainActivity, "알림", "택시 카풀을 삭제 하시겠습니까?", "삭제", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						deleteTaxiCarpool();
					}
				}, "취소", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
			} else if(view == mBtnMapSize) {
				mIsMapMaxSize = !mIsMapMaxSize;

				if(mIsMapMaxSize) {
					mBtnMapSize.setText("작게▼");
					mCarpoolDetailLayout.setVisibility(View.GONE);
				} else {
					mBtnMapSize.setText("크게▲");
					mCarpoolDetailLayout.setVisibility(View.VISIBLE);
				}
			} else if(view == mBtnZoomIn) {
				mGoogleMap.animateCamera(CameraUpdateFactory.zoomIn());
			} else if(view == mBtnZoomOut) {
				mGoogleMap.animateCamera(CameraUpdateFactory.zoomOut());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void deleteTaxiCarpool() {
		RequestParams params = new RequestParams();
		params.add("taxiId", mTaxiId);
		params.add("driverId", mMainActivity.mDriverInfo.getmDriverId());

		NetClient.send(mMainActivity, Global.URL_DELETE_CARPOOL, "POST", params, new NoResponseDataAck(), new NetResponseCallback(new NetResponse() {
			@Override
			public void onResponse(NetAPI _netAPI) {
				try {
					if (_netAPI != null && !mIsFinishFragment) {
						NoResponseDataAck retNetApi = (NoResponseDataAck) _netAPI;
						ResultData result = retNetApi.getmResultData();
						String message = "카풀 삭제를 완료 했습니다.";
						if (result.getmCode() != 100) {
							message = result.getmDetail();
						}

						SVUtil.showDialogWithListener(mMainActivity, message, "확인", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								returnToCarpoolFragment();
							}
						});
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}));
	}

	private void showParticipateDialog() {
		try {
			View customView = View.inflate(mMainActivity, R.layout.dialog_apply_message, null);
			mDialog = new Dialog(mMainActivity);
			mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

			final EditText ET = (EditText) customView.findViewById(R.id.dialog_apply_ET);
			Button cancelBtn = (Button) customView.findViewById(R.id.dialog_apply_Btn1);
			Button applyBtn = (Button) customView.findViewById(R.id.dialog_apply_Btn2);

			cancelBtn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					mDialog.dismiss();

				}
			});

			applyBtn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					sendParticipateMessage(mMainActivity.mDriverInfo.getmPhone(), mTaxiId, ET.getText().toString());
					mDialog.dismiss();
				}
			});
			mDialog.setContentView(customView);
			mDialog.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void sendParticipateMessage(String _phone, String _taxiId, String _message) {
		SVUtil.showProgressDialog(mMainActivity, "동승 신청중...");

		RequestParams params = new RequestParams();
		params.add("taxiId", _taxiId);
		params.add("phone", _phone);
		params.add("message", _message);

		String url = Global.URL_CARPOOL_PARTICIPATE_REQ.replaceAll(":taxiId", _taxiId);

		NetClient.send(mMainActivity, url, "POST", params, new CarpoolParticipateAck(), new NetResponseCallback(new NetResponse() {
			@Override
			public void onResponse(NetAPI _netAPI) {
				SVUtil.hideProgressDialog();

				try {
					if (_netAPI != null && !mIsFinishFragment) {
						CarpoolParticipateAck retNetApi = (CarpoolParticipateAck) _netAPI;
						ResultData result = retNetApi.getmResultData();
						if (result.getmCode() != 100) {
							SVUtil.showSimpleDialog(mMainActivity, result.getmDetail());
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}));
	}

	@Override
	public void onStart() {
		super.onStart();

		mIsFinishFragment = false;
	}

	@Override
	public void onResume() {
		super.onResume();

		try {
			mMainActivity.changeButtonImage(mMainActivity.mImgBtnCarpool);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onPause() {
		super.onPause();

		mIsFinishFragment = true;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		SVUtil.hideProgressDialog();

		destroyFragment();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		setTargetFragment(null, -1);
	}
}
