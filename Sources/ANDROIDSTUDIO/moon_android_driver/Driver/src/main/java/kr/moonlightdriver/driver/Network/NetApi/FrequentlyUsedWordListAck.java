package kr.moonlightdriver.driver.Network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

import kr.moonlightdriver.driver.Network.NetData.FrequentlyUsedWordData;
import kr.moonlightdriver.driver.Network.NetData.ResultData;

/**
 * Created by youngmin on 2015-10-13.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class FrequentlyUsedWordListAck extends NetAPI {
	@JsonProperty("result")
	private ResultData mResultData;
	@JsonProperty("lists")
	private ArrayList<FrequentlyUsedWordData> mFrequentlyUsedWordDataList;

	public FrequentlyUsedWordListAck() {}

	public FrequentlyUsedWordListAck(ArrayList<FrequentlyUsedWordData> mFrequentlyUsedWordDataList, ResultData mResultData) {
		this.mFrequentlyUsedWordDataList = mFrequentlyUsedWordDataList;
		this.mResultData = mResultData;
	}

	public ArrayList<FrequentlyUsedWordData> getmFrequentlyUsedWordDataList() {
		return mFrequentlyUsedWordDataList;
	}

	public void setmFrequentlyUsedWordDataList(ArrayList<FrequentlyUsedWordData> mFrequentlyUsedWordDataList) {
		this.mFrequentlyUsedWordDataList = mFrequentlyUsedWordDataList;
	}

	public ResultData getmResultData() {
		return mResultData;
	}

	public void setmResultData(ResultData mResultData) {
		this.mResultData = mResultData;
	}
}
