package kr.moonlightdriver.driver.Network.NetApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import kr.moonlightdriver.driver.Network.NetData.BeaconData;
import kr.moonlightdriver.driver.Network.NetData.ResultData;

/**
 * Created by lk on 2016. 10. 2..
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class BeaconDataAck extends NetAPI {
    @JsonProperty("result")
    private ResultData mResultData;
    @JsonProperty("beacon")
    private BeaconData mBeaconData;

    public BeaconDataAck(){}

    public BeaconDataAck(ResultData mResultData, BeaconData mBeaconData) {
        this.mResultData = mResultData;
        this.mBeaconData = mBeaconData;
    }

    public ResultData getmResultData() {
        return mResultData;
    }

    public BeaconData getmBeaconData() {
        return mBeaconData;
    }
}
