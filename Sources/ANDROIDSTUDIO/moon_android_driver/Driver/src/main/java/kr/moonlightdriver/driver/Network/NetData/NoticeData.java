package kr.moonlightdriver.driver.Network.NetData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by youngmin on 2015-10-28.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class NoticeData {
	@JsonProperty("noticeId")
	private String mNoticeId;

	@JsonProperty("seq")
	private int mSeq;

	@JsonProperty("title")
	private String mTitle;

	@JsonProperty("contents")
	private String mContents;

	@JsonProperty("image")
	private String mImage;

	@JsonProperty("link")
	private String mLink;

	@JsonProperty("startDate")
	private Long mStartDate;

	@JsonProperty("endDate")
	private Long mEndDate;

	@JsonProperty("createdDate")
	private Long mCreatedDate;

	@JsonProperty("updatedDate")
	private Long mUpdatedDate;

	public NoticeData() {}

	public NoticeData(String mContents, Long mCreatedDate, Long mEndDate, String mImage, String mLink, String mNoticeId, int mSeq, Long mStartDate, String mTitle, Long mUpdatedDate) {
		this.mContents = mContents;
		this.mCreatedDate = mCreatedDate;
		this.mEndDate = mEndDate;
		this.mImage = mImage;
		this.mLink = mLink;
		this.mNoticeId = mNoticeId;
		this.mSeq = mSeq;
		this.mStartDate = mStartDate;
		this.mTitle = mTitle;
		this.mUpdatedDate = mUpdatedDate;
	}

	public String getmContents() {
		return mContents;
	}

	public void setmContents(String mContents) {
		this.mContents = mContents;
	}

	public Long getmCreatedDate() {
		return mCreatedDate;
	}

	public void setmCreatedDate(Long mCreatedDate) {
		this.mCreatedDate = mCreatedDate;
	}

	public Long getmEndDate() {
		return mEndDate;
	}

	public void setmEndDate(Long mEndDate) {
		this.mEndDate = mEndDate;
	}

	public String getmImage() {
		return mImage;
	}

	public void setmImage(String mImage) {
		this.mImage = mImage;
	}

	public String getmLink() {
		return mLink;
	}

	public void setmLink(String mLink) {
		this.mLink = mLink;
	}

	public String getmNoticeId() {
		return mNoticeId;
	}

	public void setmNoticeId(String mNoticeId) {
		this.mNoticeId = mNoticeId;
	}

	public int getmSeq() {
		return mSeq;
	}

	public void setmSeq(int mSeq) {
		this.mSeq = mSeq;
	}

	public Long getmStartDate() {
		return mStartDate;
	}

	public void setmStartDate(Long mStartDate) {
		this.mStartDate = mStartDate;
	}

	public String getmTitle() {
		return mTitle;
	}

	public void setmTitle(String mTitle) {
		this.mTitle = mTitle;
	}

	public Long getmUpdatedDate() {
		return mUpdatedDate;
	}

	public void setmUpdatedDate(Long mUpdatedDate) {
		this.mUpdatedDate = mUpdatedDate;
	}
}
