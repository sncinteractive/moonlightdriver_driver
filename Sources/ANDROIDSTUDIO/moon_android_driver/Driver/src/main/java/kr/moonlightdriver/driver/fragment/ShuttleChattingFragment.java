package kr.moonlightdriver.driver.fragment;

import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AlphaAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.ui.IconGenerator;
import com.loopj.android.http.RequestParams;

import org.java_websocket.exceptions.WebsocketNotConnectedException;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import kr.moonlightdriver.driver.Network.NetApi.BeaconListAck;
import kr.moonlightdriver.driver.Network.NetApi.FrequentlyUsedWordListAck;
import kr.moonlightdriver.driver.Network.NetApi.NetAPI;
import kr.moonlightdriver.driver.Network.NetApi.NetResponse;
import kr.moonlightdriver.driver.Network.NetApi.NetResponseCallback;
import kr.moonlightdriver.driver.Network.NetApi.NoResponseDataAck;
import kr.moonlightdriver.driver.Network.NetApi.ShuttlePathAck;
import kr.moonlightdriver.driver.Network.NetApi.ShuttleRealTimeDataAck;
import kr.moonlightdriver.driver.Network.NetApi.StartFinishChatAck;
import kr.moonlightdriver.driver.Network.NetApi.WokitokiListAck;
import kr.moonlightdriver.driver.Network.NetClient.NetClient;
import kr.moonlightdriver.driver.Network.NetData.BeaconData;
import kr.moonlightdriver.driver.Network.NetData.ChatJoinedDriverData;
import kr.moonlightdriver.driver.Network.NetData.FrequentlyUsedWordData;
import kr.moonlightdriver.driver.Network.NetData.ResultData;
import kr.moonlightdriver.driver.Network.NetData.ShuttleChatData;
import kr.moonlightdriver.driver.Network.NetData.ShuttlePathData;
import kr.moonlightdriver.driver.Network.NetData.ShuttlePathPointData;
import kr.moonlightdriver.driver.Network.NetData.ShuttleRealTimeData;
import kr.moonlightdriver.driver.Network.NetData.WokitokiData;
import kr.moonlightdriver.driver.R;
import kr.moonlightdriver.driver.activity.MainActivity;
import kr.moonlightdriver.driver.activity.WokitokiEntryListActivity;
import kr.moonlightdriver.driver.activity.WokitokiService;
import kr.moonlightdriver.driver.utils.Global;
import kr.moonlightdriver.driver.utils.SVUtil;
import kr.moonlightdriver.driver.utils.SockJSImpl;
import kr.moonlightdriver.driver.utils.WebSocketUtil;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by redjjol on 3/08/15.
 */
public class ShuttleChattingFragment extends Fragment {
	private static final String TAG = "ShuttleChattingFragment";

	public static final String PUSH_MSG_1 = "PUSH_MSG_1";
	public static final String PUSH_MSG_2 = "PUSH_MSG_2";
	public static final String PUSH_MSG_3 = "PUSH_MSG_3";
	public static final String PUSH_MSG_4 = "PUSH_MSG_4";
	public static final String PUSH_MSG_5 = "PUSH_MSG_5";
	public static final String PUSH_MSG_6 = "PUSH_MSG_6";

	private static final int MSG_1 = 1;
	private static final int MSG_2 = 2;

	private MainActivity mMainActivity;
	private ListView mListViewChatMessage;
	private ArrayList<ShuttleChatData> mShuttleChatDataList;
	private ArrayList<ChatJoinedDriverData> mChatJoinedDriverDataList;
	private ChatListAdapter mChatListAdapter;
	private TextView mTextViewShuttleName;
	private String mShuttleId;
	private String mFragmentFrom;
	private ShuttlePathData mShuttlePathData;
	public boolean mIsFragmentRunning;

	private Button mBtnSendMyPosition;
    private Button mBtnChatList;
	private EditText mEditTextChatMessage;
	private Button mBtnChatListSize;
	private Button mBtnCallShuttleDriver;
	private LinearLayout mShuttleStationListLayout;
	private TextView mTextViewShuttleStationListTitle;
	private ImageButton mImgBtnBack;
	private ImageButton mImgBtnCall;
	private ImageButton mImgBtnShuttleInfo;
	private ImageButton mImgBtnShuttleInfo2;
	private ImageButton mBtnSendMessage;
	private ImageButton mImgBtnCurrentPostion;
	private Button mBtnFrequentlyUsedWord;
	private ImageButton mImgBtnZoomIn;
	private ImageButton mImgBtnZoomOut;
	private Button mBtnShuttleStationList;
	private ListView mListViewShuttleStationList;
	private RelativeLayout mMapLayout;
	private PushAlertReceiver mPushAlertReceiver;

    /**
     * UI Component
     */
    private LinearLayout mChattingLayout;
    private LinearLayout mWokitokiLayout;
    private ImageButton mToggleButton;
    private RelativeLayout mAboveLayout;
    private ImageButton mMicButton;
    private Button mSoundButton;
    private Button mMuteButton;
    private Button mQuitServiceButton;
    private Button mAgreeAddressButton;
    private ImageButton mPrivateWokitokiButton;
    private ImageButton mEveryWokitokiButton;
    private TextView mMicTextView;
    private TextView mLastWokitokiPopupText;
    private Dialog inviteDialog;

    /**
     * WebSocket
     */
    private SockJSImpl sockJS;
    private Timer timer;

    /**
     * Wokitoki (Audio, Recoder)
     */

    private boolean wListenStarted = false;
    private boolean wMicStarted = false;
    private int wWokitokiStateCheck = 0;
    private final int WOKITOKI_STABLE = 0;
    private final int WOKITOKI_PLAY = 1;
    private final int WOKITOKI_RECODE = 2;
    private final int WOKITOKI_CHECK = 9;

    private int BufferElements2Rec = 1024;
    private int BytesPerElement = 2;

    private int minBufferSize = AudioTrack.getMinBufferSize(RECORDER_SAMPLERATE, RECORDER_CHANNELS_OUT, AudioFormat.ENCODING_PCM_16BIT);
    private AudioTrack player = new AudioTrack(AudioManager.STREAM_VOICE_CALL, 8000, RECORDER_CHANNELS_OUT, AudioFormat.ENCODING_PCM_16BIT, minBufferSize * 4, AudioTrack.MODE_STREAM);
    int overallBytes;

    private byte[] buffer;
    short sData[] = new short[BufferElements2Rec];
    byte[] bytes = new byte[BufferElements2Rec * 2];

    private AudioRecord recorder = null;
    private Thread recordingThread = null;
    private boolean isRecording = false;

    private boolean isSoundOn = true;
    private boolean isMute = false;
    private boolean isPrivate = false;
    private boolean isAgreeGPS = true;
    private boolean isQuitService = true;
    private String otherDriverId;
    private String mNickName;

    private HashMap<String, Marker> personPositionHashMap;
	private HashMap<String, Marker> shuttlePositionHashMap;

    private static final int RECORDER_SAMPLERATE = 8000;
    private static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_MONO;
    private static final int RECORDER_CHANNELS_OUT = AudioFormat.CHANNEL_OUT_MONO;
    private static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;

	private GoogleMap mGoogleMap;
	private Polyline mShuttlePolyLine;
	private Marker mCurrentPositionMarker;

	private View mView;

	private ArrayList<Marker> mShuttleStationMarkers;
	private ArrayList<ShuttlePathPointData> mShuttlePathPointData;
	private ArrayList<FrequentlyUsedWordData> mFrequentlyUsedWordList;
	private ArrayList<ShuttlePathPointData> mShowListShuttlePathPointData;
	private ArrayList<Marker> mShuttleRealTimeMarkers;
	private ArrayList<Marker> mBeaconeRealTimeMarkers;

	private SimpleDateFormat mDateFormat;
	private ShuttleStationAdapter mShuttleStationListAdapter;

	private ArrayList<Marker> mArrowMarkers;

	public static boolean mIsFinishFragment;

	private boolean mIsChatListMaxSize;
    private LatLngBounds bounds;

    boolean isRealEnd = true;

	private String mVirtualNumber = "";
	private boolean mIsSecretCalling;
	boolean isNotifi;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		Log.e("ShuttleChattingFragment", "onCreateView");
		mView = null;

		try {
			mView = inflater.inflate(R.layout.frag_shuttle_chatting, container, false);

			mMainActivity = (MainActivity) getActivity();

			mIsChatListMaxSize = false;
			mIsFinishFragment = false;
			mIsFragmentRunning = false;
			mIsSecretCalling = false;

			Bundle bundle = ShuttleChattingFragment.this.getArguments();
			mShuttleId = bundle.getString("shuttleId");
			mFragmentFrom = bundle.getString("fragmentFrom");
			isNotifi = bundle.getBoolean("isNotifi", false);


			mShuttlePathPointData = new ArrayList<>();
			mShuttleChatDataList = new ArrayList<>();
			mChatJoinedDriverDataList = new ArrayList<>();
			mShuttleStationMarkers = new ArrayList<>();
			mShuttleRealTimeMarkers = new ArrayList<>();
			mBeaconeRealTimeMarkers = new ArrayList<>();
			mFrequentlyUsedWordList = new ArrayList<>();
			mShowListShuttlePathPointData = new ArrayList<>();
			mArrowMarkers = new ArrayList<>();

			mDateFormat = new SimpleDateFormat("yyyy년 MM월 dd일 HH시 mm분");

			mShuttleStationListLayout = (LinearLayout) mView.findViewById(R.id.frag_shuttle_chat_station_list_layout);
			mShuttleStationListLayout.setVisibility(View.GONE);

			mBtnCallShuttleDriver = (Button) mView.findViewById(R.id.fragment_return_chat_callBtn);
			mBtnCallShuttleDriver.setVisibility(View.GONE);
			mImgBtnCurrentPostion = (ImageButton) mView.findViewById(R.id.frag_shuttle_chat_btn_current_postion);
			mMapLayout = (RelativeLayout) mView.findViewById(R.id.frag_shuttle_chat_map_layout);
			mImgBtnBack = (ImageButton) mView.findViewById(R.id.frag_shuttle_chat_btn_back);
			mImgBtnCall = (ImageButton) mView.findViewById(R.id.frg_shuttle_chat_btn_call);
			mTextViewShuttleStationListTitle = (TextView) mView.findViewById(R.id.frag_shuttle_chat_text_view_title);
			mImgBtnShuttleInfo = (ImageButton) mView.findViewById(R.id.frag_shuttle_chat_btn_info);
			mImgBtnShuttleInfo2 = (ImageButton) mView.findViewById(R.id.frag_shuttle_chat_btn_info2);
			mShuttleStationListAdapter = new ShuttleStationAdapter(mMainActivity, R.layout.row_shuttle_station_info, mShowListShuttlePathPointData);
			mListViewShuttleStationList = (ListView) mView.findViewById(R.id.frag_shuttle_chat_station_list_view);
			mListViewShuttleStationList.setAdapter(mShuttleStationListAdapter);
			mTextViewShuttleName = (TextView) mView.findViewById(R.id.fragment_return_shuttle_nameTV);
			getFrequentlyUsedWord();	// 자주 쓰는 문구 조회

			mListViewChatMessage = (ListView) mView.findViewById(R.id.frag_return_chatListView);
			mEditTextChatMessage = (EditText) mView.findViewById(R.id.frag_return_chatET);

			mBtnSendMessage = (ImageButton) mView.findViewById(R.id.frag_return_sendBtn);
			mBtnFrequentlyUsedWord = (Button) mView.findViewById(R.id.frag_return_fuwBtn);
			mImgBtnZoomIn = (ImageButton) mView.findViewById(R.id.frg_shuttle_chat_btn_zoom_in);
			mImgBtnZoomOut =  (ImageButton) mView.findViewById(R.id.frag_sb_shuttle_zoom_out);
			mBtnSendMyPosition = (Button) mView.findViewById(R.id.fragment_return_send_my_poistionBtn);
            mBtnChatList = (Button) mView.findViewById(R.id.fragment_return_chat_listBtn);    // todo insert
			mBtnChatListSize = (Button) mView.findViewById(R.id.frag_return_chat_list_size_btn);
			mChatListAdapter = new ChatListAdapter(mMainActivity, R.layout.row_chat, mShuttleChatDataList);
			mListViewChatMessage.setAdapter(mChatListAdapter);
			mBtnShuttleStationList = (Button) mView.findViewById(R.id.frag_sb_shuttle_listBtn);

            //todo 추가된 부분
            mChattingLayout = (LinearLayout) mView.findViewById(R.id.frag_return_chatdetailLL);
            mWokitokiLayout = (LinearLayout) mView.findViewById(R.id.frag_return_wokitokiLL);
            mToggleButton = (ImageButton) mView.findViewById(R.id.fragment_return_ToggleBtn);
            mAboveLayout = (RelativeLayout) mView.findViewById(R.id.frag_shuttle_above_layout);
            mMicButton = (ImageButton) mView.findViewById(R.id.frag_return_mic_btn);

            mSoundButton = (Button) mView.findViewById(R.id.frag_return_sound_btn);
            mMuteButton = (Button) mView.findViewById(R.id.frag_return_mute_btn);
            mAgreeAddressButton = (Button) mView.findViewById(R.id.frag_return_agreeAdress_btn);
            mQuitServiceButton = (Button) mView.findViewById(R.id.frag_return_push_btn);

            mPrivateWokitokiButton = (ImageButton) mView.findViewById(R.id.frag_return_1_1_wokitoki_btn);
            mEveryWokitokiButton = (ImageButton) mView.findViewById(R.id.frag_return_1_n_wokitoki_btn);

            mMicTextView = (TextView) mView.findViewById(R.id.frag_return_mic_tv);
            mLastWokitokiPopupText = (TextView) mView.findViewById(R.id.frag_return_popup);

            personPositionHashMap = new HashMap<>();
			shuttlePositionHashMap = new HashMap<>();

            AudioManager am = (AudioManager) mMainActivity.getSystemService(MainActivity.AUDIO_SERVICE);
            am.setSpeakerphoneOn(true);

			mPushAlertReceiver = new PushAlertReceiver();
			IntentFilter intentFilter = new IntentFilter();
			intentFilter.addAction(ShuttleChattingFragment.PUSH_MSG_1);
			intentFilter.addAction(ShuttleChattingFragment.PUSH_MSG_2);
			intentFilter.addAction(ShuttleChattingFragment.PUSH_MSG_3);
			intentFilter.addAction(ShuttleChattingFragment.PUSH_MSG_4);
			intentFilter.addAction(ShuttleChattingFragment.PUSH_MSG_5);
			intentFilter.addAction(ShuttleChattingFragment.PUSH_MSG_6);

			getActivity().registerReceiver(mPushAlertReceiver, intentFilter);

			setUpMapIfNeeded();


		} catch (Exception e) {
			e.printStackTrace();
		}

		//todo 추가된 부분
		if (sockJS == null) {
			connectSockJS();
		}

		showLastPersonPopUp();
        try {
			mMainActivity.stopWokitokiService();
        }catch (Exception e){
            e.printStackTrace();
        }
        return mView;
    }

    private void showLastPersonPopUp() {
        try {
            ViewTreeObserver viewTreeObserver = mView.getViewTreeObserver();
            /**
             * Fragment onWindowFocusChanged 이벤트가 발생되지 않아 mOnGlobalLayoutListener 대체
             * 공중에 팝업을 동적 위치에서 띄우기 위해 어쩔 수 없이 선택함
             */
            ViewTreeObserver.OnGlobalLayoutListener mOnGlobalLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {

                @Override
                public void onGlobalLayout() {
                    FrameLayout.LayoutParams p = (FrameLayout.LayoutParams) mMicTextView.getLayoutParams();
                    p.setMargins(0, (int) (mMicButton.getHeight() * 0.58), 0, 0);
                    mMicTextView.setLayoutParams(p);
                    mMicTextView.getViewTreeObserver()
                            .removeGlobalOnLayoutListener(this);
                }
            };

            viewTreeObserver.addOnGlobalLayoutListener(mOnGlobalLayoutListener);
		} catch(Exception e) {
			e.printStackTrace();
		}
    }
//
//    private void stopService() {
//        try {
//            ActivityManager am = (ActivityManager) mMainActivity.getSystemService(Context.ACTIVITY_SERVICE);
//            List<ActivityManager.RunningServiceInfo> info;
//
//            info = am.getRunningServices(Integer.MAX_VALUE);
//            Log.i("aa", "Service 갯수 : " + info.size());
//            for (Iterator it = info.iterator(); it.hasNext(); ) {
//                ActivityManager.RunningServiceInfo runningServiceInfo = (ActivityManager.RunningServiceInfo) it.next();
//                if (runningServiceInfo.service.getClassName().contains("kr.moonlightdriver.driver")) {
//                    Intent i = new Intent();
//                    i.setComponent(runningServiceInfo.service);
//                    mMainActivity.stopService(i);
//                    Log.i("aa", "Service 종료 시킴");
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//	}

	private void showShuttleInfoDialog() {
		try {
			LayoutInflater factory = LayoutInflater.from(mMainActivity);
			View layoutView = factory.inflate(R.layout.popup_bus_route_info, null);
			TextView startStationTV = (TextView) layoutView.findViewById(R.id.popup_bus_route_info_start_station);
			TextView endStationTV = (TextView) layoutView.findViewById(R.id.popup_bus_route_info_end_station);
			TextView termTV = (TextView) layoutView.findViewById(R.id.popup_bus_route_info_term);
			TextView firstTimeTV = (TextView) layoutView.findViewById(R.id.popup_bus_route_info_first_time);
			TextView lastTimeTV = (TextView) layoutView.findViewById(R.id.popup_bus_route_info_last_time);

			startStationTV.setText(mShuttlePathData.getmStartStation());
			endStationTV.setText(mShuttlePathData.getmEndStation());

			String term = mShuttlePathData.getmTerm() != 0 ? (mShuttlePathData.getmTerm() + "분") : "-";
			String[] shuttleRunningTime = getShuttleRunningTime(mShuttlePathData);
			String firstTime = shuttleRunningTime[0].equals("") ? "-" : shuttleRunningTime[0];
			String lastTime = shuttleRunningTime[1].equals("") ? "-" : shuttleRunningTime[1];

			termTV.setText(term);
			firstTimeTV.setText(firstTime);
			lastTimeTV.setText(lastTime);

			SVUtil.showDialogWithListener(mMainActivity, "[" + mShuttlePathData.getmShuttleName() + "] 노선 정보", layoutView, "확인", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String[] getShuttleRunningTime(ShuttlePathData _shuttleData) {
		String[] retTime = new String[] { "", ""};

		try {
			Calendar cal = Calendar.getInstance();
			int weekday = cal.get(Calendar.DAY_OF_WEEK);

			retTime[0] = _shuttleData.getmFirstTime();
			retTime[1] = _shuttleData.getmLastTime();
			boolean isHoliday = SVUtil.checkHoliday(_shuttleData.getmHolidayDate());
			if(isHoliday) {
				weekday = 1;
			}

			if(weekday == 1) {
				retTime[0] = _shuttleData.getmSundayFirstTime();
				retTime[1] = _shuttleData.getmSundayLastTime();
			} else if(weekday == 7) {
				retTime[0] = _shuttleData.getmSaturdayFirstTime();
				retTime[1] = _shuttleData.getmSaturdayLastTime();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return retTime;
	}

	private void showShuttleStationList() {
		try {
			String shuttleName = mShuttlePathData.getmShuttleName() + " 셔틀 노선";
			mTextViewShuttleStationListTitle.setText(shuttleName);
			mShuttleStationListAdapter.notifyDataSetChanged();
			mShuttleStationListLayout.setVisibility(View.VISIBLE);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void getShuttlePath(final String _shuttleId) {
		SVUtil.showProgressDialog(mMainActivity, "로딩중...");

		String url = Global.URL_SHUTTLE_PATH.replace(":busId", _shuttleId);
		NetClient.send(mMainActivity, url, "GET", null, new ShuttlePathAck(), new NetResponseCallback(new NetResponse() {
			@Override
			public void onResponse(NetAPI _netAPI) {
				try {
					SVUtil.hideProgressDialog();

					mShuttlePathPointData.clear();

					if (_netAPI != null && !mIsFinishFragment) {
						ShuttlePathAck retNetApi = (ShuttlePathAck) _netAPI;
						ResultData result = retNetApi.getmResultData();
						if (result.getmCode() != 100) {
							SVUtil.showDialogWithListener(mMainActivity, result.getmDetail(), "확인", new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									returnToShuttleFragment();
								}
							});

							return;
						}

						mShuttlePathData = retNetApi.getmShuttlePathData();

						if(mShuttlePathData.getmPhone() != null && !mShuttlePathData.getmPhone().isEmpty()) {
							mBtnCallShuttleDriver.setVisibility(View.VISIBLE);
						}

						mShuttlePathData.setmFirstTime(SVUtil.convertTime(mShuttlePathData.getmFirstTime()));
						mShuttlePathData.setmLastTime(SVUtil.convertTime(mShuttlePathData.getmLastTime()));

						ArrayList<ShuttlePathPointData> shuttlePathPointData = mShuttlePathData.getmShuttlePathPointData();
						int listSize = shuttlePathPointData.size();

						for (int i = 0; i < listSize; i++) {
							shuttlePathPointData.get(i).setmFirstTime(SVUtil.convertTime(shuttlePathPointData.get(i).getmFirstTime()));
							shuttlePathPointData.get(i).setmLastTime(SVUtil.convertTime(shuttlePathPointData.get(i).getmLastTime()));

							mShuttlePathPointData.add(shuttlePathPointData.get(i));
						}

						mShuttlePathData.setmStartStation(SVUtil.getShuttleStartStation(mShuttlePathPointData));
						mShuttlePathData.setmEndStation(SVUtil.getShuttleEndStation(mShuttlePathPointData));

						updateShuttleNameText();
//						String shuttleName = mShuttlePathData.getmShuttleName() + "(" + mChatJoinedDriverDataList.size() + "명)";
//						mTextViewShuttleName.setText(shuttleName);

//						startChatting(mShuttleId);

                        initShuttlePathView();

                        drawShuttlePath(mShuttlePathPointData, true);

						getRealTimeShuttlePosition();
						getRealTimeBeaconPosition(_shuttleId);

						//todo insert
						if(isNotifi)
							showShuttleStationList();


						try {
							if (mFragmentFrom.equals("main_station_list")) {
								showShuttleStationList();
							}
						}catch (Exception e){
							e.printStackTrace();
						}

					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}));
	}

	private void getRealTimeShuttlePosition() {
		try {
			if(mShuttlePathData.getmRealTimeCategoryName() == null || mShuttlePathData.getmRealTimeCategoryName().isEmpty()) {
				return;
			}

			removeRealTimeMarker();

			NetClient.send(mMainActivity, Global.URL_SHUTTLE_REAL_TIME, "GET", null, new ShuttleRealTimeDataAck(), new NetResponseCallback(new NetResponse() {
				@Override
				public void onResponse(NetAPI _netAPI) {
					try {
						SVUtil.hideProgressDialog();

						if (_netAPI != null && !mIsFinishFragment) {
							ShuttleRealTimeDataAck retNetApi = (ShuttleRealTimeDataAck) _netAPI;
							ResultData result = retNetApi.getmResultData();
							if (result.getmCode() != 100) {
								SVUtil.showDialogWithListener(mMainActivity, result.getmDetail(), "확인", new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										returnToShuttleFragment();
									}
								});

								return;
							}

							ArrayList<ShuttleRealTimeData> retData = retNetApi.getmShuttleRealTimeData();
//							SVUtil.log("error", TAG, "retData size : " + retData.size());

							IconGenerator iconFactory = new IconGenerator(mMainActivity);
							LatLng shuttle_position;
							for(ShuttleRealTimeData item : retData) {
								if(item.getmRealTimeCategoryName().startsWith(mShuttlePathData.getmRealTimeCategoryName())) {
									shuttle_position = new LatLng(item.getmShuttleLocationData().getmCoordinates().get(1), item.getmShuttleLocationData().getmCoordinates().get(0));
									addShuttleMarker(iconFactory, item.getmRealTimeCategoryName(), shuttle_position, "shuttle_position");
								}
							}

							handler.sendEmptyMessageDelayed(MSG_1, Global.SHUTTLE_POSITION_REFRESH_TIME);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void getRealTimeBeaconPosition(final String _shuttleId){
		try{
			String url = Global.URL_BEACON_CHECK_REAL_TIME.replace(":busId", _shuttleId);
			Log.e("beacon url", "getRealTimeBeacinPosition : " + url); // // TODO: remove;
			NetClient.send(mMainActivity, url, "GET", null, new BeaconListAck(), new NetResponseCallback(new NetResponse() {
				@Override
				public void onResponse(NetAPI _netAPI) {
					if(_netAPI != null && !mIsFinishFragment) {
						BeaconListAck retNetApi = (BeaconListAck) _netAPI;
						ResultData result = retNetApi.getmResultData();
						if(result.getmCode() != 100){
							return;
						}

						removeRealTimeBeaconMarker();

						IconGenerator iconFactory = new IconGenerator(mMainActivity);
						LatLng shuttle_position;
						ArrayList<BeaconData> retData = retNetApi.getmBeaconListData();

						for(int i=0; i<retData.size(); i++){
							shuttle_position = new LatLng(retData.get(i).getLocation().getmCoordinates().get(1), retData.get(i).getLocation().getmCoordinates().get(0));
							addBeaconMarker(iconFactory, retData.get(i).getmText() + " ", shuttle_position, "beacon_position");
						}

						Message msg = handler.obtainMessage();
						msg.obj = _shuttleId;
						msg.what = MSG_2;
						handler.sendMessageDelayed(msg, Global.SHUTTLE_POSITION_REFRESH_TIME);
					}
				}
			}));
		}catch (Exception e){
			e.printStackTrace();
		}
	}

	private void addShuttleMarker(IconGenerator _iconFactory, String _title, LatLng _position, String _snippet) {
		try {
			MarkerOptions markerOptions = new MarkerOptions().icon(BitmapDescriptorFactory.fromBitmap(_iconFactory.makeIcon(_title))).position(_position).anchor(_iconFactory.getAnchorU(), _iconFactory.getAnchorV()).snippet("");

			mShuttleRealTimeMarkers.add(mGoogleMap.addMarker(new MarkerOptions().position(_position).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_realtime_shuttle)).snippet(_snippet).anchor(0.5f, 0.5f)));
			mShuttleRealTimeMarkers.add(mGoogleMap.addMarker(markerOptions));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void addBeaconMarker(IconGenerator _iconFactory, String _title, LatLng _position, String _snippet) {
		try {
			MarkerOptions markerOptions = new MarkerOptions().icon(BitmapDescriptorFactory.fromBitmap(_iconFactory.makeIcon(_title))).position(_position).anchor(_iconFactory.getAnchorU(), _iconFactory.getAnchorV()).snippet("");

			mBeaconeRealTimeMarkers.add(mGoogleMap.addMarker(new MarkerOptions().position(_position).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_realtime_shuttle)).snippet(_snippet).anchor(0.5f, 0.5f)));
			mBeaconeRealTimeMarkers.add(mGoogleMap.addMarker(markerOptions));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void removeRealTimeBeaconMarker() {
		try {
			int realTimeMarkerSize = mBeaconeRealTimeMarkers.size();
			for(int i = 0; i < realTimeMarkerSize; i++) {
				mBeaconeRealTimeMarkers.get(i).remove();
			}

			mBeaconeRealTimeMarkers.clear();
			Log.e("ShuttleRealTime", "Clear Marker");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void updateShuttleNameText() {
		try {
			if(mShuttlePathData == null) {
				return;
			}

			String ShuttleName = mShuttlePathData.getmShuttleName() + "(" + mChatJoinedDriverDataList.size() + "명)";
			mTextViewShuttleName.setText(ShuttleName);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void startChatting(String _shuttleId) {
		String url = Global.URL_START_CHAT.replaceAll(":busId", _shuttleId).replaceAll(":driverId", mMainActivity.mDriverInfo.getmDriverId());
		NetClient.send(mMainActivity, url, "POST", null, new StartFinishChatAck(), new NetResponseCallback(new NetResponse() {
			@Override
			public void onResponse(NetAPI _netAPI) {
				try {
					mChatJoinedDriverDataList.clear();
					mShuttleChatDataList.clear();

					if (_netAPI != null && !mIsFinishFragment) {
						StartFinishChatAck retNetApi = (StartFinishChatAck) _netAPI;
						ResultData result = retNetApi.getmResultData();
						if (result.getmCode() != 100) {
							SVUtil.showDialogWithListener(mMainActivity, result.getmDetail(), "확인", new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									returnToShuttleFragment();
								}
							});

							return;
						}

						ArrayList<ShuttleChatData> shuttleChatDataList = retNetApi.getmShuttleChatDataList();
						int listSize = shuttleChatDataList.size();
						for (int i = 0; i < listSize; i++) {
							mShuttleChatDataList.add(shuttleChatDataList.get(i));
						}

						ArrayList<ChatJoinedDriverData> chatJoinedDriverDataList = retNetApi.getmJoinedDriverDataList();
						listSize = chatJoinedDriverDataList.size();
						for (int i = 0; i < listSize; i++) {
							mChatJoinedDriverDataList.add(chatJoinedDriverDataList.get(i));
						}

                        mNickName = retNetApi.getmNickName();
					}

					if (mShuttleChatDataList.size() == 0) {
						mShuttleChatDataList.add(new ShuttleChatData(null, "", "", 0, 0, "채팅 내용이 없습니다.", "normal", "SYSTEM"));
					}

					mChatListAdapter.notifyDataSetChanged();

					updateShuttleNameText();
//					String driverCount = mShuttlePathData.getmShuttleName() + "(" + mChatJoinedDriverDataList.size() + "명)";
//					mTextViewShuttleName.setText(driverCount);
				} catch (Exception e) {
					e.printStackTrace();
                }
            }
        }));

        String url2 = Global.SERVER_HOST + "/busnew/" + _shuttleId + "/wokitokilist?pageNo=1";
        NetClient.send(mMainActivity, url2, "GET", null, new WokitokiListAck(), new NetResponseCallback(new NetResponse() {
            @Override
            public void onResponse(NetAPI _netAPI) {
	            try {
		            if (_netAPI != null) {
			            WokitokiListAck retNetApi = (WokitokiListAck) _netAPI;
			            ResultData result = retNetApi.getmResultData();
			            if (result.getmCode() != 100) {
				            SVUtil.showDialogWithListener(mMainActivity, result.getmDetail(), "확인", new DialogInterface.OnClickListener() {
					            @Override
					            public void onClick(DialogInterface dialog, int which) {
						            returnToShuttleFragment();
					            }
				            });

				            return;
			            }

			            final ArrayList<WokitokiData> list = retNetApi.getmWokitokilist();
			            mMainActivity.runOnUiThread(new Runnable() {
				            @Override
				            public void run() {
					            try {
						            Handler h = new Handler();
						            h.postDelayed(new Runnable() {
							            @Override
							            public void run() {
								            try {
									            AlphaAnimation animation = new AlphaAnimation(1.0f, 0.0f);
									            animation.setDuration(1000);
									            mLastWokitokiPopupText.setAnimation(animation);
									            mLastWokitokiPopupText.setVisibility(View.GONE);
								            } catch (Exception e) {
									            e.printStackTrace();
								            }
							            }
						            }, 3000);

						            if (list.size() == 0) {
							            mLastWokitokiPopupText.setVisibility(View.GONE);
						            } else {
							            String name = "마지막 발언\n" + list.get(0).getmNickName() + "님";
							            mLastWokitokiPopupText.setText(name);
							            mLastWokitokiPopupText.setVisibility(View.VISIBLE);
						            }
					            } catch (Exception e) {
						            e.printStackTrace();
					            }
				            }
			            });
		            }
	            } catch (Exception e) {
		            e.printStackTrace();
	            }
			}
		}));
	}

	private void finishChatting(String _shuttleId) {
		String url = Global.URL_END_CHAT.replaceAll(":busId", _shuttleId).replaceAll(":driverId", mMainActivity.mDriverInfo.getmDriverId());
		NetClient.send(mMainActivity, url, "POST", null, new StartFinishChatAck(), new NetResponseCallback(new NetResponse() {
			@Override
			public void onResponse(NetAPI _netAPI) {
				try {
					if (_netAPI != null && !mIsFinishFragment) {
						StartFinishChatAck retNetApi = (StartFinishChatAck) _netAPI;
						ResultData result = retNetApi.getmResultData();
						if (result.getmCode() != 100) {
							SVUtil.showDialogWithListener(mMainActivity, result.getmDetail(), "확인", new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									returnToShuttleFragment();
								}
							});
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}));
	}

	private void returnToShuttleFragment() {
		try {
//			finishChatting(mShuttleId);
			mMainActivity.mFragmentShuttle = new ShuttleFragment();
			mMainActivity.replaceFragment(mMainActivity.mFragmentShuttle, R.id.mainFrameLayout, Global.FRAG_SHUTTLE_TAG, false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void sendMessage(String _messageType, String _message, String _wordId) {
		try {
			addShuttleComment(_message, _messageType, _wordId);

			mIsChatListMaxSize = true;
			mBtnChatListSize.setText("창작게");
            //mMapLayout.setVisibility(View.GONE);
			mEditTextChatMessage.setText("");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void addShuttleComment(String _message, String _messageType, String _wordId) {
		try {
			double lat = 0;
			double lng = 0;

			if(_messageType.equals("fuw_location") || _messageType.equals("my_location")) {
				lat = mMainActivity.mCurrentLocation.latitude;
				lng = mMainActivity.mCurrentLocation.longitude;
			}

			RequestParams params = new RequestParams();
			params.add("driverId", mMainActivity.mDriverInfo.getmDriverId());
			params.add("nickName", "");
			params.add("messageType", _messageType);
			params.add("message", _message);
			params.add("wordId", _wordId);
			params.add("lat", lat + "");
			params.add("lng", lng + "");

			String url = Global.URL_ADD_SHUTTLE_COMMENT.replaceAll(":busId", mShuttleId);
			NetClient.send(mMainActivity, url, "POST", params, new NoResponseDataAck(), new NetResponseCallback(new NetResponse() {
				@Override
				public void onResponse(NetAPI _netAPI) {
					try {
						if (_netAPI != null && !mIsFinishFragment) {
							NoResponseDataAck retNetApi = (NoResponseDataAck) _netAPI;
							ResultData result = retNetApi.getmResultData();
							if (result.getmCode() != 100) {
								SVUtil.showDialogWithListener(mMainActivity, result.getmDetail(), "확인", new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										returnToShuttleFragment();
									}
								});
							}

							SVUtil.hideSoftInput(mMainActivity);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setUpMapIfNeeded() {
		try {
			if (mGoogleMap == null) {
				mGoogleMap = ((SupportMapFragment) MainActivity.mSupportFragmentManager.findFragmentById(R.id.frag_sb_shuttle_FL)).getMap();
				if (mGoogleMap != null) {
					setUpMap();
				}
			}

//			if (mGoogleMap == null) {
//				SupportMapFragment mapFragment = (SupportMapFragment) MainActivity.mSupportFragmentManager.findFragmentById(R.id.frag_sb_shuttle_FL);
//				mapFragment.getMapAsync(this);
//			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

//	@Override
//	public void onMapReady(GoogleMap googleMap) {
//		try {
//			mGoogleMap = googleMap;
//			setUpMap();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	private void setUpMap() {
		try {
			mGoogleMap.getUiSettings().setRotateGesturesEnabled(false);
			mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
			mGoogleMap.getUiSettings().setCompassEnabled(false);

			mCurrentPositionMarker = mGoogleMap.addMarker(new MarkerOptions().position(mMainActivity.mCurrentLocation).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_carpool_current_position)).anchor(0.5f, 1.0f));
			mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mMainActivity.mCurrentLocation, 15));

			getShuttlePath(mShuttleId);

			setEventListener();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void changeMyLocation() {
		try {
			SVUtil.log("error", TAG, "===== changeMyLocation() =====");

			if(mIsFinishFragment) {
				return;
			}

			if (mGoogleMap != null && mFragmentFrom != null && !mFragmentFrom.equals("findpath")) {
				mCurrentPositionMarker.setPosition(mMainActivity.mCurrentLocation);
				mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(mMainActivity.mCurrentLocation));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private final Comparator<FrequentlyUsedWordData> frequentlyUsedWordCountSort = new Comparator<FrequentlyUsedWordData>() {
		@Override
		public int compare(FrequentlyUsedWordData box, FrequentlyUsedWordData box2) {
			return box2.getmUsedCount() - box.getmUsedCount();
		}
	};

	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			try {
				switch (msg.what) {
					case MSG_1 :
						getRealTimeShuttlePosition();
						break;
					case MSG_2 :
						getRealTimeBeaconPosition((String) msg.obj);
						break;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};

	private void registerVirtualNumber() {
		try {
			RequestParams params = new RequestParams();
			params.add("type", "shuttle");
			params.add("recvId", mShuttleId);
			params.add("sendId", mMainActivity.mDriverInfo.getmDriverId());

			SVUtil.showProgressDialog(mMainActivity, "전화 연결중...");

			NetClient.send(mMainActivity, Global.URL_REGISTER_VIRTUAL_NUMBER, "POST", params, new NoResponseDataAck(), new NetResponseCallback(new NetResponse() {
				@Override
				public void onResponse(NetAPI _netAPI) {
					if (_netAPI != null && !mIsFinishFragment) {
						NoResponseDataAck retNetApi = (NoResponseDataAck) _netAPI;
						ResultData result = retNetApi.getmResultData();
						if (result.getmCode() != 100) {
							SVUtil.hideProgressDialog();
							SVUtil.showSimpleDialog(mMainActivity, result.getmDetail());
						}
					}
				}
			}));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void unregisterVirtualNumber() {
		try {
			RequestParams params = new RequestParams();
			params.add("type", "shuttle");
			params.add("virtual_number", mVirtualNumber);
			params.add("sendId", mMainActivity.mDriverInfo.getmDriverId());

			NetClient.send(mMainActivity, Global.URL_UNREGISTER_VIRTUAL_NUMBER, "POST", params, new NoResponseDataAck(), new NetResponseCallback(new NetResponse() {
				@Override
				public void onResponse(NetAPI _netAPI) {
					if (_netAPI != null && !mIsFinishFragment) {
						mIsSecretCalling = false;
						mVirtualNumber = "";

						NoResponseDataAck retNetApi = (NoResponseDataAck) _netAPI;
						ResultData result = retNetApi.getmResultData();
						if (result.getmCode() != 100) {
							SVUtil.showSimpleDialog(mMainActivity, result.getmDetail());
						}
					}
				}
			}));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setEventListener() {
		try {
			mBtnCallShuttleDriver.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					registerVirtualNumber();
				}
			});

			mImgBtnCurrentPostion.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(mMainActivity.mCurrentLocation));
				}
			});

			mTextViewShuttleName.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(mMainActivity, R.layout.row_joined_drivers);

						for (ChatJoinedDriverData driverData : mChatJoinedDriverDataList) {
							arrayAdapter.add(driverData.getmNickName());
						}

						arrayAdapter.notifyDataSetChanged();

						SVUtil.showAdapterDialog(mMainActivity, "채팅방에 접속중인 기사들", arrayAdapter, null, "확인", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();
							}
						});
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			mImgBtnBack.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					mShuttleStationListLayout.setVisibility(View.GONE);
				}
			});

			mImgBtnCall.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					SVUtil.showDialogWithListener(mMainActivity, "알림", "상황실 전화 연결은 유료로 추가 요금이 발생할수 있습니다.\n연결 하시겠습니까?", "연결", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							mMainActivity.makePhoneCall();
						}
					}, "취소", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});
				}
			});

			mImgBtnShuttleInfo.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					showShuttleInfoDialog();
				}
			});

			mImgBtnShuttleInfo2.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					showShuttleInfoDialog();
				}
			});

			mListViewShuttleStationList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					try {
						mShuttleStationListLayout.setVisibility(View.GONE);

						ShuttlePathPointData shuttlePathPointData = mShowListShuttlePathPointData.get(position);
						mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(shuttlePathPointData.getmShuttleLocationData().getmCoordinates().get(1), shuttlePathPointData.getmShuttleLocationData().getmCoordinates().get(0)), 18));
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			mImgBtnZoomIn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					mGoogleMap.animateCamera(CameraUpdateFactory.zoomIn());
				}
			});

			mImgBtnZoomOut.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					mGoogleMap.animateCamera(CameraUpdateFactory.zoomOut());
				}
			});

			mBtnSendMyPosition.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					sendMessage("my_location", "님의 현위치", "");
				}
			});

			mBtnSendMessage.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						String message = mEditTextChatMessage.getText().toString();
						if(message.isEmpty()) {
							return;
						}

						sendMessage("normal", message, "");
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			mBtnFrequentlyUsedWord.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					FrequentlyUsedWordBoxAdapter fuwAdapter = new FrequentlyUsedWordBoxAdapter(mMainActivity, R.layout.row_frequently_used_word, mFrequentlyUsedWordList);

					SVUtil.showAdapterDialog(mMainActivity, "상용 문구", fuwAdapter, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							try {
								if (which < 0 || which >= mFrequentlyUsedWordList.size()) {
									return;
								}

								FrequentlyUsedWordData wordData = mFrequentlyUsedWordList.get(which);

								String wordId = wordData.getmWordId();
								String message = wordData.getmWord();
								String messageType = "fuw";
								if (wordData.getmPriority() == 3 || wordData.getmPriority() == 9) {
									messageType = "fuw_location";
								}

								wordData.setmUsedCount(wordData.getmUsedCount() + 1);

								mFrequentlyUsedWordList.set(which, wordData);
								Collections.sort(mFrequentlyUsedWordList, frequentlyUsedWordCountSort);

								sendMessage(messageType, message, wordId);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}, "취소", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});
				}
			});

			mBtnChatListSize.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						// keyboard down
						SVUtil.hideSoftInput(mMainActivity);

						mIsChatListMaxSize = !mIsChatListMaxSize;

						if(mIsChatListMaxSize) {
							mBtnChatListSize.setText("창작게");
							mMapLayout.setVisibility(View.GONE);
                            mAboveLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 100));
						} else {
							mBtnChatListSize.setText("창크게");
							mMapLayout.setVisibility(View.VISIBLE);
                            mAboveLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 4));
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			mBtnShuttleStationList.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					showShuttleStationList();
				}
			});

			mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
				@Override
				public boolean onMarkerClick(Marker marker) {
					try {
						if (marker.getSnippet() == null) {
							return true;
						}

						if (marker.getSnippet().equals("shuttle_station")) {
							mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 18));
						} else if(marker.getSnippet().equals("shuttle_position")) {
							marker.showInfoWindow();
                        } else if (marker.getSnippet().equals("기사님")) {
                            marker.showInfoWindow();
						}
					} catch (Exception e) {
						e.printStackTrace();
					}

					return true;
				}
			});

			mGoogleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
				@Override
				public void onCameraChange(CameraPosition cameraPosition) {
					try {
						float current_zoom = cameraPosition.zoom;

//						SVUtil.log("error", TAG, "current_zoom : " + current_zoom);

						drawShuttlePath(mShuttlePathPointData, false);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

            //todo 추가된 부분
            mToggleButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
	                try {
		                if (mChattingLayout.getVisibility() == View.GONE) {
			                mWokitokiLayout.setVisibility(View.GONE);
			                mChattingLayout.setVisibility(View.VISIBLE);
			                mToggleButton.setBackgroundResource(R.drawable.btn_wokitoki);
			                mBtnChatListSize.setVisibility(View.VISIBLE);
                            mBtnFrequentlyUsedWord.setVisibility(View.VISIBLE); // todo insert
                            mBtnSendMyPosition.setVisibility(View.VISIBLE);
                            mBtnChatList.setVisibility(View.GONE);
		                } else {
			                mChattingLayout.setVisibility(View.GONE);
			                mWokitokiLayout.setVisibility(View.VISIBLE);
			                mToggleButton.setBackgroundResource(R.drawable.btn_chat);
			                mBtnChatListSize.setVisibility(View.GONE);
                            mBtnFrequentlyUsedWord.setVisibility(View.GONE);    // todo insert
                            mBtnSendMyPosition.setVisibility(View.GONE);
                            mBtnChatList.setVisibility(View.VISIBLE);
		                }
	                } catch (Exception e) {
		                e.printStackTrace();
	                }
                }
            });

            mMicButton.setOnTouchListener(//		                SVUtil.log("error", TAG, event.getAction() + "");
                    new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
	                try {
//		                SVUtil.log("error", TAG, event.getAction() + "");

		                switch (event.getAction()) {
			                case MotionEvent.ACTION_DOWN:    // 마이크를 누르고 있을 떄
				                wWokitokiStateCheck = WOKITOKI_RECODE;
				                if (!wMicStarted) {
					                try {
						                wMicStarted = true;
						                startRecording();
						                sockJS.send(sendMsg("start"));
						                mMicButton.setImageDrawable(getResources().getDrawable(R.drawable.btn_mic_send_me));
					                } catch (WebsocketNotConnectedException e) {
						                e.printStackTrace();
						                //Toast.makeText(mMainActivity, "서버와 연결 상태가 좋지 않습니다.", Toast.LENGTH_SHORT).show();
						                wMicStarted = false;
						                stopRecording();
						                mMicButton.setImageDrawable(getResources().getDrawable(R.drawable.btn_mic));
						                SVUtil.showDialogWithListener(mMainActivity, "서버와 연결 상태가 좋지 않습니다..", "확인", new DialogInterface.OnClickListener() {
							                @Override
							                public void onClick(DialogInterface dialog, int which) {
								                returnToShuttleFragment();
							                }
						                });
					                }
				                }
				                break;
			                case MotionEvent.ACTION_OUTSIDE:
			                case MotionEvent.ACTION_UP: // 마이크를 땠을 때
				                wMicStarted = false;
				                wWokitokiStateCheck = WOKITOKI_STABLE;
				                stopRecording();
				                try {
					                sockJS.send(sendMsg("stop"));
				                } catch (WebsocketNotConnectedException e) {
					                e.printStackTrace();
				                }
				                mMicButton.setImageDrawable(getResources().getDrawable(R.drawable.btn_mic));
				                break;
		                }
	                } catch (Exception e) {
		                e.printStackTrace();
	                }
                    return false;
                }
            });


            mSoundButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
	                try {
		                if (isSoundOn) { // 스피커폰 온 상태라면
			                AudioManager am = (AudioManager) mMainActivity.getSystemService(MainActivity.AUDIO_SERVICE);
			                am.setSpeakerphoneOn(false); // true : on / false : off
			                mSoundButton.setBackgroundResource(R.drawable.btn_speaker_off);
			                isSoundOn = false;
		                } else {
			                AudioManager am = (AudioManager) mMainActivity.getSystemService(MainActivity.AUDIO_SERVICE);
			                am.setSpeakerphoneOn(true); // true : on / false : off
			                mSoundButton.setBackgroundResource(R.drawable.btn_speaker_on);
			                isSoundOn = true;
		                }
	                } catch (Exception e) {
		                e.printStackTrace();
	                }
                }
            });

            mMuteButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						isMute = !isMute;
						if (isMute) {
							mMuteButton.setBackgroundResource(R.drawable.btn_mute_on);
						} else {
							mMuteButton.setBackgroundResource(R.drawable.btn_mute_off);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

            mBtnChatList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
	                try {
		                Intent intent = new Intent();    // todo 인덴트기 아니라 다이어그램으로 바꿔줄 것
		                intent.setClass(mMainActivity, WokitokiEntryListActivity.class);
		                intent.putExtra("cnt", mChatJoinedDriverDataList.size());
		                for (int i = 0; i < mChatJoinedDriverDataList.size(); i++) {
			                intent.putExtra("driverId_" + i, mChatJoinedDriverDataList.get(i).getmDriverId());
			                intent.putExtra("joinedDate" + i, mChatJoinedDriverDataList.get(i).getmJoinDate());
			                intent.putExtra("nickName" + i, mChatJoinedDriverDataList.get(i).getmNickName());
		                }
		                intent.putExtra("myDriverId", mMainActivity.mDriverInfo.getmDriverId());
                        isRealEnd = false;
		                startActivityForResult(intent, 100);
	                } catch (Exception e) {
		                e.printStackTrace();
	                }
                }
            });

            mQuitServiceButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
	                try {
                        if (isQuitService) {
                            mQuitServiceButton.setBackgroundResource(R.drawable.btn_quit_off);
                            isQuitService = false;
		                } else {
                            mQuitServiceButton.setBackgroundResource(R.drawable.btn_quit_on);
                            isQuitService = true;
		                }
	                } catch (Exception e) {
		                e.printStackTrace();
	                }
                }
            });

            mEveryWokitokiButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
	                try {
		                isPrivate = false;
		                mPrivateWokitokiButton.setImageDrawable(getResources().getDrawable(R.drawable.btn_wokitoki_1_1_off));
		                mEveryWokitokiButton.setImageDrawable(getResources().getDrawable(R.drawable.btn_wokitoki_1_n_on));
                        sockJS.send(sendInvite("상대방이 대화방을 나가셨습니다.", true, otherDriverId));
                        otherDriverId = "";
	                } catch (Exception e) {
		                e.printStackTrace();
	                }
                }
            });

            mPrivateWokitokiButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
	                try {
                        if (otherDriverId.equals("")) {
			                Intent intent = new Intent();
			                intent.setClass(mMainActivity, WokitokiEntryListActivity.class);
			                intent.putExtra("cnt", mChatJoinedDriverDataList.size());
			                for (int i = 0; i < mChatJoinedDriverDataList.size(); i++) {
				                intent.putExtra("driverId_" + i, mChatJoinedDriverDataList.get(i).getmDriverId());
				                intent.putExtra("joinedDate" + i, mChatJoinedDriverDataList.get(i).getmJoinDate());
				                intent.putExtra("nickName" + i, mChatJoinedDriverDataList.get(i).getmNickName());
			                }
			                intent.putExtra("myDriverId", mMainActivity.mDriverInfo.getmDriverId());
                            isRealEnd = false;
			                startActivityForResult(intent, 100);
		                } else {
			                isPrivate = true;
			                mPrivateWokitokiButton.setImageDrawable(getResources().getDrawable(R.drawable.btn_wokitoki_1_1_on));
			                mEveryWokitokiButton.setImageDrawable(getResources().getDrawable(R.drawable.btn_wokitoki_1_n_off));
		                }
	                } catch (Exception e) {
		                e.printStackTrace();
	                }
                }
            });

            mAgreeAddressButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
	                try {
		                if (isAgreeGPS) {
			                mAgreeAddressButton.setBackgroundResource(R.drawable.btn_agreeaddress_off);
			                isAgreeGPS = false;
		                } else {
			                mAgreeAddressButton.setBackgroundResource(R.drawable.btn_agreeaddress_on);
			                isAgreeGPS = true;
		                }
	                } catch (Exception e) {
		                e.printStackTrace();
	                }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        isRealEnd = true;
	    try {
		    super.onActivityResult(requestCode, resultCode, data);
		    if (resultCode == 100) {
			    otherDriverId = data.getStringExtra("driverId");
			    isPrivate = true;
			    mPrivateWokitokiButton.setImageDrawable(getResources().getDrawable(R.drawable.btn_wokitoki_1_1_on));
			    mEveryWokitokiButton.setImageDrawable(getResources().getDrawable(R.drawable.btn_wokitoki_1_n_off));
			    if (sockJS == null)
				    connectSockJS();
                sockJS.send(sendInvite("invite", false, otherDriverId));
		    }
	    } catch (Exception e) {
		    e.printStackTrace();
	    }
	}

	private class ShuttleStationViewHolder {
		public TextView mTextViewStationName;
		public TextView mTextViewDirection;
		public TextView mTextViewTime;
	}

	private class ShuttleStationAdapter extends ArrayAdapter<ShuttlePathPointData> {
		ArrayList<ShuttlePathPointData> mShuttleStationList;
		private int mRowId;
		private ShuttleStationViewHolder mHolder;

		public ShuttleStationAdapter(Context _context, int _rowId, ArrayList<ShuttlePathPointData> _shuttleStationList) {
			super(_context, _rowId, _shuttleStationList);
			this.mShuttleStationList = _shuttleStationList;
			this.mRowId = _rowId;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;

			try {
				if (v == null) {
					LayoutInflater li = (LayoutInflater) mMainActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					v = li.inflate(mRowId, null);

					mHolder = new ShuttleStationViewHolder();
					mHolder.mTextViewStationName = (TextView) v.findViewById(R.id.row_shuttle_station_name);
					mHolder.mTextViewDirection = (TextView) v.findViewById(R.id.row_shuttle_direction);
					mHolder.mTextViewTime = (TextView) v.findViewById(R.id.row_shuttle_station_time);

					v.setTag(mHolder);
				} else {
					mHolder = (ShuttleStationViewHolder) v.getTag();
				}

				ShuttlePathPointData shuttleStationData = mShuttleStationList.get(position);

				String shuttleDirection = mMainActivity.mShuttleCategoryList[mShuttlePathData.getmCategory()] + " 방면";
				String timeStr = "운행시간 - " + shuttleStationData.getmFirstTime() + " ~ " + shuttleStationData.getmLastTime();

				mHolder.mTextViewStationName.setText(shuttleStationData.getmShuttlePointName());
				mHolder.mTextViewDirection.setText(shuttleDirection);
				mHolder.mTextViewTime.setText(timeStr);
			} catch (Exception e) {
				e.printStackTrace();
			}

			return v;
		}
	}

	private class FUWViewHolder {
		public TextView mTextViewWordId;
		public TextView mTextViewWord;
	}

	private class FrequentlyUsedWordBoxAdapter extends ArrayAdapter<FrequentlyUsedWordData> {
		ArrayList<FrequentlyUsedWordData> mWordList;
		private int mRowId;
		private FUWViewHolder mHolder;

		public FrequentlyUsedWordBoxAdapter(Context _context, int _rowId, ArrayList<FrequentlyUsedWordData> _wordList) {
			super(_context, _rowId, _wordList);
			this.mWordList = _wordList;
			this.mRowId = _rowId;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;

			try {
				if (v == null) {
					LayoutInflater li = (LayoutInflater) mMainActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					v = li.inflate(mRowId, null);
					mHolder = new FUWViewHolder();
					mHolder.mTextViewWordId = (TextView) v.findViewById(R.id.tv_fuw_id);
					mHolder.mTextViewWord = (TextView) v.findViewById(R.id.tv_fuw_word);

					v.setTag(mHolder);
				} else {
					mHolder = (FUWViewHolder) v.getTag();
				}

				FrequentlyUsedWordData wordData = mWordList.get(position);
				mHolder.mTextViewWordId.setText(wordData.getmWordId());
				mHolder.mTextViewWord.setText(wordData.getmWord());
			} catch (Exception e) {
				e.printStackTrace();
			}

			return v;
		}
	}

	private class ChatListAdapter extends ArrayAdapter<ShuttleChatData> {
		ArrayList<ShuttleChatData> mShuttleChatData;

		private int mRowChatLayoutId;
		ViewHolder_chat mChatViewHolder;

		public ChatListAdapter(Context _context, int _rowChatLayoutId, ArrayList<ShuttleChatData> _shuttleChatData) {
			super(_context, _rowChatLayoutId, _shuttleChatData);
			this.mShuttleChatData = _shuttleChatData;
			this.mRowChatLayoutId = _rowChatLayoutId;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;

			try {
				if (v == null) {
					LayoutInflater li = (LayoutInflater) mMainActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					v = li.inflate(mRowChatLayoutId, null);
					mChatViewHolder = new ViewHolder_chat();
					mChatViewHolder.mTextViewSendDate = (TextView) v.findViewById(R.id.row_chat_dateTV);
					mChatViewHolder.mTextViewNickName = (TextView) v.findViewById(R.id.row_chat_nameTV);
					mChatViewHolder.mTextViewComment = (TextView) v.findViewById(R.id.row_chat_commentTV);
					mChatViewHolder.mBtnReqLocation = (ImageView) v.findViewById(R.id.btn_req_location);
					mChatViewHolder.mBtnShowMyLocation = (ImageView) v.findViewById(R.id.btn_show_my_location);

					v.setTag(mChatViewHolder);
				} else {
					mChatViewHolder = (ViewHolder_chat) v.getTag();
				}

				ShuttleChatData shuttleChatItem = mShuttleChatData.get(position);

				mChatViewHolder.mBtnReqLocation.setTag(position);
				mChatViewHolder.mBtnShowMyLocation.setTag(position);

				if(shuttleChatItem.getmCreateTime() != null) {
					mChatViewHolder.mTextViewSendDate.setText(mDateFormat.format(new Date(shuttleChatItem.getmCreateTime())));
				} else {
					mChatViewHolder.mTextViewSendDate.setText("");
				}

				if(shuttleChatItem.getmMessageType().equals("fuw_location")) {
					String nickName = shuttleChatItem.getmNickName() + " : ";
					String message = shuttleChatItem.getmMessage() + "(지도에서 확인)";

					mChatViewHolder.mTextViewNickName.setText(nickName);
					mChatViewHolder.mTextViewComment.setText(message);
					mChatViewHolder.mBtnReqLocation.setVisibility(View.GONE);
					mChatViewHolder.mBtnShowMyLocation.setVisibility(View.VISIBLE);
				} else if(shuttleChatItem.getmMessageType().equals("my_location")) {
					String nickName = shuttleChatItem.getmNickName() + " : ";
					String message = shuttleChatItem.getmNickName() + "님의 현위치(지도에서 확인)";

					mChatViewHolder.mTextViewNickName.setText(nickName);
					mChatViewHolder.mTextViewComment.setText(message);
					mChatViewHolder.mBtnReqLocation.setVisibility(View.GONE);
					mChatViewHolder.mBtnShowMyLocation.setVisibility(View.VISIBLE);
				} else if(shuttleChatItem.getmDriverId().isEmpty() || mMainActivity.mDriverInfo.getmDriverId().equals(shuttleChatItem.getmDriverId())) {
					String nickName = shuttleChatItem.getmNickName() + " : ";

					mChatViewHolder.mTextViewNickName.setText(nickName);
					mChatViewHolder.mTextViewComment.setText(shuttleChatItem.getmMessage());
					mChatViewHolder.mBtnReqLocation.setVisibility(View.GONE);
					mChatViewHolder.mBtnShowMyLocation.setVisibility(View.GONE);
				} else {
					String message = " : " + shuttleChatItem.getmMessage();

					mChatViewHolder.mTextViewNickName.setText(shuttleChatItem.getmNickName());
					mChatViewHolder.mTextViewComment.setText(message);
					mChatViewHolder.mBtnReqLocation.setVisibility(View.VISIBLE);
					mChatViewHolder.mBtnShowMyLocation.setVisibility(View.GONE);
				}

				mChatViewHolder.mBtnShowMyLocation.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						try {
							int position = (Integer) v.getTag();
							ShuttleChatData chatItem = mShuttleChatDataList.get(position);

							moveToChatLocationFragment(chatItem.getmNickName(), chatItem.getmLat(), chatItem.getmLng());
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});

				mChatViewHolder.mBtnReqLocation.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						try {
							int position = (Integer) v.getTag();
							final ShuttleChatData chatItem = mShuttleChatDataList.get(position);

							LayoutInflater factory = LayoutInflater.from(mMainActivity);
							View layoutView = factory.inflate(R.layout.popup_req_location, null);
							TextView textview = (TextView) layoutView.findViewById(R.id.textview_req_location);
							final EditText editText = (EditText) layoutView.findViewById(R.id.req_location_send_msg);

							String confirmMessage = "[" + chatItem.getmNickName() + "] 님의 위치 확인 메시지를 전송하시겠습니까?";
							textview.setText(confirmMessage);

							SVUtil.showDialogWithListener(mMainActivity, "위치 확인 요청", layoutView, "확인", new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									String sendMsg = editText.getText().toString();
									String url = Global.URL_REQ_LOCATION.replaceAll(":busId", mShuttleId);

									RequestParams params = new RequestParams();
									params.add("sendDriverId", mMainActivity.mDriverInfo.getmDriverId());
									params.add("recvDriverId", chatItem.getmDriverId());
									params.add("message", sendMsg);

									NetClient.send(mMainActivity, url, "POST", params, new NoResponseDataAck(), new NetResponseCallback(new NetResponse() {
										@Override
										public void onResponse(NetAPI _netAPI) {
											if (_netAPI != null && !mIsFinishFragment) {
												NoResponseDataAck retNetApi = (NoResponseDataAck) _netAPI;
												ResultData result = retNetApi.getmResultData();
												if (result.getmCode() != 100) {
													SVUtil.showSimpleDialog(mMainActivity, result.getmDetail());
												}

												SVUtil.hideSoftInput(mMainActivity);
											}
										}
									}));
								}
							}, "취소", new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									dialog.dismiss();
								}
							});
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			} catch (Exception e) {
				e.printStackTrace();
			}

			return v;
		}
	}

	private class ViewHolder_chat {
		TextView mTextViewSendDate;
		TextView mTextViewNickName;
		TextView mTextViewComment;
		ImageView mBtnReqLocation;
		ImageView mBtnShowMyLocation;
	}

	private void drawShuttlePath (ArrayList<ShuttlePathPointData> _shuttlePathPointData, boolean _isMoveToBounds) {
		try {
			if(mGoogleMap == null) {
				return;
			}

			initShuttlePathView();

			PolylineOptions opt = new PolylineOptions();
			opt.color(Color.parseColor("#ed482b"));
			opt.width(SVUtil.pxFromDp(mMainActivity, 9));

			int skipCount;
			BitmapDescriptor shuttleStationMarker;
			BitmapDescriptor directionArrow = BitmapDescriptorFactory.fromResource(R.drawable.ic_shuttle_direction_22);
			float current_zoom = mGoogleMap.getCameraPosition().zoom;

			if(current_zoom >= 18) {
				skipCount = 1;
				shuttleStationMarker = BitmapDescriptorFactory.fromResource(R.drawable.ic_shuttle_station_40);
			} else if(current_zoom >= 15) {
				skipCount = 5;
				shuttleStationMarker = BitmapDescriptorFactory.fromResource(R.drawable.ic_shuttle_station_40);
			} else if(current_zoom >= 13) {
				skipCount = 10;
				shuttleStationMarker = BitmapDescriptorFactory.fromResource(R.drawable.ic_shuttle_station_40);
			} else if(current_zoom >= 12) {
				skipCount = 20;
				shuttleStationMarker = BitmapDescriptorFactory.fromResource(R.drawable.ic_shuttle_station_20);
			} else if(current_zoom >= 11) {
				skipCount = 30;
				shuttleStationMarker = BitmapDescriptorFactory.fromResource(R.drawable.ic_shuttle_station_20);
			} else if(current_zoom >= 10) {
				skipCount = 45;
				shuttleStationMarker = BitmapDescriptorFactory.fromResource(R.drawable.ic_shuttle_station_15);
			} else {
				skipCount = 60;
				directionArrow = null;
				shuttleStationMarker = BitmapDescriptorFactory.fromResource(R.drawable.ic_shuttle_station_15);
			}

			mShowListShuttlePathPointData.clear();

			int listSize = _shuttlePathPointData.size();
			LatLng pathPoint;
			ShuttlePathPointData prevStationName = null;
			int pointCount = 0;

			for(int i = 0; i < listSize; i++) {
				if(mGoogleMap == null) {
					return;
				}

				ShuttlePathPointData shuttlePathPointItem = _shuttlePathPointData.get(i);
				pathPoint = new LatLng(shuttlePathPointItem.getmShuttleLocationData().getmCoordinates().get(1), shuttlePathPointItem.getmShuttleLocationData().getmCoordinates().get(0));

				opt.add(pathPoint);

				if (shuttlePathPointItem.getmShuttlePointName().equals("__point")) {
//					pointCount++;
				} else { // 셔틀 정류장
					mShowListShuttlePathPointData.add(shuttlePathPointItem);
					mShuttleStationMarkers.add(mGoogleMap.addMarker(new MarkerOptions().position(pathPoint).icon(shuttleStationMarker).snippet("shuttle_station").anchor(0.5f, 1.0f)));
				}

				if(prevStationName != null && pointCount % skipCount == 0) {
					LatLng prevPoint = new LatLng(prevStationName.getmShuttleLocationData().getmCoordinates().get(1), prevStationName.getmShuttleLocationData().getmCoordinates().get(0));
					if(directionArrow != null) {
						mArrowMarkers.add(addShuttleLineArrowMarker(pathPoint, SVUtil.getRotationDegrees(prevPoint, pathPoint), directionArrow));
					}
				}

				pointCount++;
				prevStationName = shuttlePathPointItem;
			}

			mShuttlePolyLine = mGoogleMap.addPolyline(opt);

			if(_isMoveToBounds) {
				moveToBounds(mShuttlePolyLine);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private Marker addShuttleLineArrowMarker(LatLng _position, float _rotateDegree, BitmapDescriptor _directionArrow) {
		try {
			return mGoogleMap.addMarker(new MarkerOptions().position(_position).icon(_directionArrow).anchor(0.5f, 0.5f).rotation(_rotateDegree).snippet("shuttle_direction"));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private void initShuttlePathView() {
		try {
			if (mShuttlePolyLine != null) {
				mShuttlePolyLine.remove();
			}

			int listSize = mShuttleStationMarkers.size();
			for (int i = 0; i < listSize; i++) {
				mShuttleStationMarkers.get(i).remove();
			}

			mShuttleStationMarkers.clear();

			int markerSize = mArrowMarkers.size();
			for(int i = 0; i < markerSize; i++) {
				mArrowMarkers.get(i).remove();
			}

			mArrowMarkers.clear();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void removeRealTimeMarker() {
		try {
			int realTimeMarkerSize = mShuttleRealTimeMarkers.size();
			for(int i = 0; i < realTimeMarkerSize; i++) {
				mShuttleRealTimeMarkers.get(i).remove();
			}

			mShuttleRealTimeMarkers.clear();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void moveToBounds(Polyline p) {
		try {
			LatLngBounds.Builder builder = new LatLngBounds.Builder();
			List<LatLng> arr = p.getPoints();

			for(int i = 0; i < arr.size();i++){
				builder.include(arr.get(i));
			}

            bounds = builder.build();
			int padding = 40; // offset from edges of the map in pixels
			mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, padding));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void someoneHasJoined(long sendDate, String message, String _drivers) {
		try {
			if(mIsFinishFragment) {
				return;
			}

			mShuttleChatDataList.add(new ShuttleChatData(sendDate, "", "", 0, 0, message, "normal", "SYSTEM"));

			mChatListAdapter.notifyDataSetChanged();
			mListViewChatMessage.smoothScrollToPosition(mShuttleChatDataList.size() - 1);

			JSONArray ja_drivers = new JSONArray(_drivers);
			mChatJoinedDriverDataList = new ArrayList<>();

            // todo joinDate 추가됨
			for (int i=0; i < ja_drivers.length(); i++) {
				JSONObject jo_driver = ja_drivers.getJSONObject(i);
//				SVUtil.log("error", TAG, "nickName : " + jo_driver.getString("nickName"));
                mChatJoinedDriverDataList.add(new ChatJoinedDriverData(jo_driver.getString("driverId"), jo_driver.getString("nickName"), jo_driver.getString("joinDate")));
			}

			updateShuttleNameText();
//			String driverCount = mShuttlePathData.getmShuttleName() + "(" + mChatJoinedDriverDataList.size() + "명)";
//			mTextViewShuttleName.setText(driverCount);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void someoneSendLocation(long _sendDate, String _driverId, String _nickName, String _message, String _messageType, double _lat, double _lng) {
		try {
			if(mIsFinishFragment) {
				return;
			}

			mShuttleChatDataList.add(new ShuttleChatData(_sendDate, _driverId, "", _lat, _lng, _message, _messageType, _nickName));

			mChatListAdapter.notifyDataSetChanged();
			mListViewChatMessage.smoothScrollToPosition(mShuttleChatDataList.size() - 1);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	private void someoneSendReqLocation(final String _sendDriverId, String _sendNickName, String _message) {
		try {
			LayoutInflater factory = LayoutInflater.from(mMainActivity);
			View layoutView = factory.inflate(R.layout.popup_res_location, null);
			TextView textview = (TextView) layoutView.findViewById(R.id.textview_res_location);
			TextView textview_msg = (TextView) layoutView.findViewById(R.id.textview_res_location_msg);
			String confirmMessage = "[" + _sendNickName + "]님이 위치 확인 요청을 신청하였습니다.\n현재 위치를 전송하시겠습니까?";
			textview.setText(confirmMessage);
			textview_msg.setText(_message);

			SVUtil.showDialogWithListener(mMainActivity, "위치 확인 요청", layoutView, "확인", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					try {
						RequestParams params = new RequestParams();
						params.add("sendDriverId", mMainActivity.mDriverInfo.getmDriverId());
						params.add("recvDriverId", _sendDriverId);
						params.add("result", "OK");
						params.add("lat", mMainActivity.mCurrentLocation.latitude + "");
						params.add("lng", mMainActivity.mCurrentLocation.longitude + "");

						String url = Global.URL_RES_LOCATION.replaceAll(":busId", mShuttleId);
						NetClient.send(mMainActivity, url, "POST", params, new NoResponseDataAck(), new NetResponseCallback(new NetResponse() {
							@Override
							public void onResponse(NetAPI _netAPI) {
								try {
									if (_netAPI != null && !mIsFinishFragment) {
										NoResponseDataAck retNetApi = (NoResponseDataAck) _netAPI;
										ResultData result = retNetApi.getmResultData();
										if (result.getmCode() != 100) {
											SVUtil.showSimpleDialog(mMainActivity, result.getmDetail());
										}
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						}));
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}, "취소", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					try {
						RequestParams params = new RequestParams();
						params.add("sendDriverId", mMainActivity.mDriverInfo.getmDriverId());
						params.add("recvDriverId", _sendDriverId);
						params.add("result", "REJECT");

						String url = Global.URL_RES_LOCATION.replaceAll(":busId", mShuttleId);
						NetClient.send(mMainActivity, url, "POST", params, new NoResponseDataAck(), new NetResponseCallback(new NetResponse() {
							@Override
							public void onResponse(NetAPI _netAPI) {
								try {
									if (_netAPI != null && !mIsFinishFragment) {
										NoResponseDataAck retNetApi = (NoResponseDataAck) _netAPI;
										ResultData result = retNetApi.getmResultData();
										if (result.getmCode() != 100) {
											SVUtil.showSimpleDialog(mMainActivity, result.getmDetail());
										}
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						}));
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	private void someoneSendResLocation(final String _sendNickName, String _result, final double _lat, final double _lng) {
		try {
			if(_result.equals("OK")) {
				SVUtil.showDialogWithListener(mMainActivity, "위치 확인 결과", "[" + _sendNickName + "]님의 위치를 지금 확인 하시겠습니까?", "확인", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						moveToChatLocationFragment(_sendNickName, _lat, _lng);
					}
				}, "취소", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
			} else {
				SVUtil.showSimpleDialog(mMainActivity, "[" + _sendNickName + "]님이 위치 확인 요청을 거절하셨습니다.");
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	private void moveToChatLocationFragment(String _nickName, double _lat, double _lng) {
		try {
			ChatLocationFragment frag = new ChatLocationFragment();

			Bundle args = new Bundle();
			args.putString("nickName", _nickName);
			args.putDouble("lat", _lat);
			args.putDouble("lng", _lng);
			args.putString("shuttleId", mShuttleId);

			frag.setArguments(args);

			FragmentTransaction transaction = mMainActivity.getFragmentManager().beginTransaction();
			transaction.add(R.id.mainFrameLayout, frag, Global.FRAG_CHAT_LOCATION_TAG);
			transaction.addToBackStack(null);
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void getFrequentlyUsedWord() {
		NetClient.send(mMainActivity, Global.URL_FREQUENTLY_USED_WORD_LIST, "GET", null, new FrequentlyUsedWordListAck(), new NetResponseCallback(new NetResponse() {
			@Override
			public void onResponse(NetAPI _netAPI) {
				try {
					if (_netAPI != null && !mIsFinishFragment) {
						FrequentlyUsedWordListAck retNetApi = (FrequentlyUsedWordListAck) _netAPI;
						ResultData result = retNetApi.getmResultData();
						if (result.getmCode() != 100) {
							SVUtil.showSimpleDialog(mMainActivity, result.getmDetail());
						} else {
							mFrequentlyUsedWordList.clear();

							ArrayList<FrequentlyUsedWordData> frequentlyUsedWordDataList = retNetApi.getmFrequentlyUsedWordDataList();
							int listSize = frequentlyUsedWordDataList.size();
							for (int i = 0; i < listSize; i++) {
								mFrequentlyUsedWordList.add(frequentlyUsedWordDataList.get(i));
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}));
	}

	@Override
	public void onStart() {
		super.onStart();
//		SVUtil.log("error", TAG, "onStart()");

        try {
//			if(mGoogleMap != null) {
//				getShuttlePath(mShuttleId);
//			}

	        startChatting(mShuttleId);
        } catch (Exception e) {
            e.printStackTrace();
        }
	}

	@Override
	public void onResume() {
		super.onResume();

        try {
            SVUtil.log("error", TAG, "onResume()");

            mIsFinishFragment = false;

			try {
				mMainActivity.changeButtonImage(mMainActivity.mImgBtnShuttle);
			}catch (Exception e){
				e.printStackTrace();
			}
            mMainActivity.stopWokitokiService();
            // todo 추가
            if (sockJS == null) {
                connectSockJS();
            }

            resetWokitoki();
        } catch (Exception e) {
            e.printStackTrace();
        }
	}

	@Override
	public void onPause() {
		super.onPause();

		try {
			SVUtil.hideProgressDialog();
		} catch(Exception e) {
			e.printStackTrace();
		}

		resetWokitoki();
	}

	@Override
	public void onStop() {
		super.onStop();
        SVUtil.log("error", TAG, "onStop()");

		try {
			finishChatting(mShuttleId);

			resetWokitoki();

            if (isRealEnd) {
                SharedPreferences pref = mMainActivity.getSharedPreferences("pref", MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("mShuttleId", mShuttleId);
                editor.putString("mDriverId", mMainActivity.mDriverInfo.getmDriverId() + "");
                editor.putString("mShuttleName", mShuttlePathData.getmShuttleName() + "");
                editor.putBoolean("isPrivate", isPrivate);
                editor.putString("otherDriverId", otherDriverId);
                editor.commit();
                Log.e("onStop", "Service Start");
                Intent intent = new Intent(mMainActivity, WokitokiService.class);
                intent.setPackage("kr.moonlightdriver.driver");

                if(isQuitService) {
	                mMainActivity.startService(intent);
                }

                sockJS.isLive = false;
				timer.cancel();
				sockJS = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        Log.e("ShuttleChattingFragment", "onDestoryView");

        try {
//	        finishChatting(mShuttleId);

	        removeRealTimeMarker();

	        mIsFinishFragment = true;

			if(mGoogleMap != null) {
				mGoogleMap.clear();
				mGoogleMap = null;
			}

			SupportMapFragment f = (SupportMapFragment) MainActivity.mSupportFragmentManager.findFragmentById(R.id.frag_sb_shuttle_FL);
			if(f != null) {
				MainActivity.mSupportFragmentManager.beginTransaction().remove(f).commitAllowingStateLoss();
			}

			if(mView != null){
				ViewGroup parent = (ViewGroup) mView.getParent();
				if(parent!=null) {
					parent.removeView(mView);
				}
			}

			mMainActivity.mFragmentShuttleChatting = null;

	        getActivity().unregisterReceiver(mPushAlertReceiver);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		setTargetFragment(null, -1);
    }

    private void connectSockJS() {
        try {
            sockJS = new SockJSImpl(Global.WOKITIKI_SERVER_HOST + "/eventbus", mShuttleId, mMainActivity.mDriverInfo.getmDriverId(), true) {
                @Override
                public void scheduleHeartbeat() {
                    timer = new Timer();
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            String ping = "[\"{\\\"type\\\":\\\"ping\\\"}\"]";
                            try {
                                send(ping);

	                            if (isAgreeGPS) {
                                    send(sendGPS());
                                }

                                scheduleHeartbeat();
                            } catch (WebsocketNotConnectedException e) {
                                e.printStackTrace();
//                                SVUtil.showSimpleDialog(mMainActivity, "인터넷 연결이 불안정합니다.");
//                                mMainActivity.replaceFragment(new ShuttleFragment(), R.id.mainFrameLayout, Global.FRAG_SHUTTLE_TAG, true);
                                sockJS.isLive = false;
                                sockJS = null;
                                timer.cancel();
                                connectSockJS();
                            }

                            if (wWokitokiStateCheck == WOKITOKI_PLAY) {
	                            wWokitokiStateCheck = WOKITOKI_CHECK;
                            } else if (wWokitokiStateCheck == WOKITOKI_RECODE) {
	                            wWokitokiStateCheck = WOKITOKI_CHECK;
                            } else if (wWokitokiStateCheck == WOKITOKI_CHECK) {
	                            resetWokitoki();
                            }
                        }
                    }, 5000);
                }

                @Override
                public void parseSockJS(String s) {
                    try {
                        s = s.replace("\\\"", "\"");
                        s = s.replace("\\\\", "\\");
//                        s = s.replace("\\\\\"", "\"");
                        s = s.substring(3, s.length() - 2); // a[" ~ "] 없애기
//	                    SVUtil.log("error", TAG, "Reci : " + s);

                        JSONObject json = new JSONObject(s);
	                    String msgType = json.getString("type");
	                    if(!msgType.equals("err")) {
		                    JSONObject body = new JSONObject(json.getString("body"));
		                    String bodyType = body.getString("type");
		                    String msg = body.getString("msg");
		                    String senderId = body.getString("senderId");
		                    String nickName = body.getString("senderNick");
		                    boolean isPrivateMsg = body.getBoolean("isPrivateMsg");

//		                    SVUtil.log("error", TAG, "isPlayCondition : " + isPlayCondition(isPrivateMsg, senderId));

		                    if (isPlayCondition(isPrivateMsg, senderId)) {
//			                    SVUtil.log("error", TAG, "aa : " + isPush + " / " + (!personPositionHashMap.containsKey(nickName)));
			                    switch (bodyType) {
				                    case "gps" :
					                    updatePersonLocate(msg, nickName);
					                    break;
				                    case "invite" :
                                        showInviteDialog(msg, senderId, body.getString("otherDriverId"), nickName);
					                    break;
				                    case "normal" :
					                    playWokitoki(msg, senderId, nickName);
					                    break;
				                    case "notifi" :
                                        if (!personPositionHashMap.containsKey(nickName)) {
						                    playNotificationSound();
					                    }
					                    break;
			                    }
		                    }
	                    }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                private void playWokitoki(String msg, String senderId, String nickName) {
	                try {
//		                SVUtil.log("error", TAG, "playWokitoki() my nick : " + mMainActivity.mDriverInfo.getmDriverId() + " / " + senderId);

		                msg = WebSocketUtil.toSt(msg);

		                if (msg.equals("start")) {
			                initWokitoki(nickName);
		                }

		                buffer = Base64.decode(msg, Base64.URL_SAFE);

		                overallBytes += player.write(buffer, 0, buffer.length);

		                wWokitokiStateCheck = WOKITOKI_PLAY;

//		                SVUtil.log("error", TAG, "started " + wListenStarted + "  /  overallBytes : " + overallBytes);
		                if (!wListenStarted && overallBytes > minBufferSize) {
			                playSound();
			                Log.d(TAG, "재생 시작");
			                updateMicUISpeakOther(nickName);
		                }

		                if (msg.equals("stop")) {
			                resetWokitoki();
		                }
	                } catch (Exception e) {
		                e.printStackTrace();
	                }
                }

                private void initWokitoki(final String nickName) {
	                try {
//		                SVUtil.log("error", TAG, "WokiToki Start");

		                resetWokitoki();
		                //player = null;
		                //player = new AudioTrack(AudioManager.STREAM_VOICE_CALL, 4000, RECORDER_CHANNELS_OUT, AudioFormat.ENCODING_PCM_16BIT, minBufferSize * 4, AudioTrack.MODE_STREAM);
		                //minBufferSize = AudioTrack.getMinBufferSize(RECORDER_SAMPLERATE, RECORDER_CHANNELS_OUT, AudioFormat.ENCODING_PCM_16BIT);
		                overallBytes = 0;
		                mMainActivity.runOnUiThread(new Runnable() {
			                @Override
			                public void run() {
				                try {
					                mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(personPositionHashMap.get(nickName).getPosition(), 16));
//					                SVUtil.log("error", TAG, "aa : " + nickName);
				                } catch (Exception e) {
					                e.printStackTrace();
				                }
			                }
		                });
	                } catch (Exception e) {
		                e.printStackTrace();
	                }
                }

                private void updateMicUISpeakOther(final String nickName) {
                    try {
                        mMainActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mMicButton.setImageDrawable(getResources().getDrawable(R.drawable.btn_mic_send_other));
                                mMicButton.setClickable(false);
                                mMicButton.setEnabled(false);
                                mMicTextView.setVisibility(View.VISIBLE);
                                mMicTextView.setText(nickName);
                            }
                        });
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    }
                }

                private void playSound() {
	                try {
		                player.play();
		                player.setPlaybackPositionUpdateListener(new AudioTrack.OnPlaybackPositionUpdateListener() {
			                @Override
			                public void onMarkerReached(AudioTrack track) {

			                }

			                @Override
			                public void onPeriodicNotification(AudioTrack track) {
				                resetWokitoki();
			                }
		                });

		                wListenStarted = true;
	                } catch (Exception e) {
		                e.printStackTrace();
	                }
                }

                private void showInviteDialog(String msg, final String senderId, final String otherId, final String nickName) {
	                try {
                        if (otherId.equals(mMainActivity.mDriverInfo.getmDriverId() + "")) {
			                if (msg.equals("invite")) {
				                mMainActivity.runOnUiThread(new Runnable() {
					                @Override
					                public void run() {
						                inviteDialog = new InviteAgreeDialog(mMainActivity, nickName, new View.OnClickListener() {
							                @Override
							                public void onClick(View v) { // Agree
								                try {
									                otherDriverId = senderId;
									                isPrivate = true;
									                mPrivateWokitokiButton.setImageDrawable(getResources().getDrawable(R.drawable.btn_wokitoki_1_1_on));
									                mEveryWokitokiButton.setImageDrawable(getResources().getDrawable(R.drawable.btn_wokitoki_1_n_off));
                                                    send(sendInvite("초대를 수락하셨습니다.", true, otherDriverId));
									                inviteDialog.dismiss();
								                } catch (Exception e) {
									                e.printStackTrace();
								                }
							                }
						                }, new View.OnClickListener() {
							                @Override
							                public void onClick(View v) { // DisAgree
								                try {
                                                    send(sendInvite("초대를 수락하지 않으셨습니다.", true, otherDriverId));
									                inviteDialog.dismiss();
								                } catch (Exception e) {
									                e.printStackTrace();
								                }
							                }
						                });

						                inviteDialog.show();
					                }
				                });
			                } else {
				                final String finalMsg = msg;
				                mMainActivity.runOnUiThread(new Runnable() {
					                @Override
					                public void run() {
						                Toast.makeText(mMainActivity, finalMsg, Toast.LENGTH_SHORT).show();
					                }
				                });
			                }
		                }
	                } catch (Exception e) {
		                e.printStackTrace();
	                }
                }

                private void updatePersonLocate(String msg, final String nickName) {
	                try {
		                final String position[] = msg.split("&");
		                final LatLng l = new LatLng(Double.parseDouble(position[0].replace("lat=", "")), Double.parseDouble(position[1].replace("lng=", "")));

		                mMainActivity.runOnUiThread(new Runnable() {
			                @Override
			                public void run() {
				                try {
					                Marker m;
					                if (!personPositionHashMap.containsKey(nickName)) {
						                m = mGoogleMap.addMarker(new MarkerOptions().position(l).icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_driver_40)).snippet("기사님").title(nickName));
						                m.showInfoWindow();
						                personPositionHashMap.put(nickName, m);
					                }

					                personPositionHashMap.get(nickName).setPosition(l);
				                } catch (Exception e) {
					                e.printStackTrace();
				                }
			                }
		                });
	                } catch (Exception e) {
		                e.printStackTrace();
	                }
                }
            };
            sockJS.connectBlocking();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (Exception e) {
	        e.printStackTrace();
        }
    }

    private boolean isPlayCondition(boolean isPrivateMsg, String senderId) {
	    try {
//		    SVUtil.log("error", TAG, "isPlayCondition() senderId = " + senderId + " / myId  = " + mMainActivity.mDriverInfo.getmDriverId() + "isMute = " +isMute + " / isPrivate = " + isPrivate + " / isPrivateMSG = " + isPrivateMsg);

		    if (senderId.equals(mMainActivity.mDriverInfo.getmDriverId() + "")) {
			    return false;
		    }

		    if (isMute) {
			    return false;
		    }

			if (isPrivate) {       // 내가 비밀 모드일 떄
				return isPrivateMsg && otherDriverId.equals(senderId);
			} else { // 아닐 때 전체모드 일떄
				return !isPrivateMsg;      // todo 반전시켜 주어야 함
			}
	    } catch (Exception e) {
		    e.printStackTrace();
	    }

	    return false;
    }

    /**
     * sendMsg
     * @param msg
     * 음성 데이터를 Base64로 인코딩된 String
     * @return JSON msg
     * WebSocket 에서 처리할 데이터로 가공 된 JSON Object
     */
    private JSONObject sendMsg(String msg) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("type", "publish");
            obj.put("address", "to.server.channel");
            JSONObject body = new JSONObject();
            body.put("type", "normal");
            body.put("channel_id", mShuttleId);
            body.put("senderId", mMainActivity.mDriverInfo.getmDriverId() + "");
            body.put("msg", msg);
            body.put("senderNick", mNickName);
            body.put("isPrivateMsg", isPrivate);
            body.put("msg", msg);
            body.put("gps", "lat=" + mMainActivity.mCurrentLocation.latitude + "&lng=" + mMainActivity.mCurrentLocation.longitude);
            body.put("sendPN", mMainActivity.mUserPhoneNumber + "");
            obj.put("body", body);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return obj;
    }

    /**
     * sendGPS
     * GPS 정보를 실어서 보냄
     * @return
     * WebSocket 에서 처리할 데이터로 가공 된 JSON Object
     */
    public JSONObject sendGPS() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("type", "publish");
            obj.put("address", "to.server.channel");
            JSONObject body = new JSONObject();
            body.put("type", "gps");
            body.put("channel_id", mShuttleId);
            body.put("senderId", mMainActivity.mDriverInfo.getmDriverId() + "");
            body.put("senderNick", mNickName + "");
            body.put("isPrivateMsg", false);
            body.put("msg", "lat=" + mMainActivity.mCurrentLocation.latitude + "&lng=" + mMainActivity.mCurrentLocation.longitude);
            obj.put("body", body);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return obj;
    }

    /**
     * sendInvite
     * @param msg
     * string Data
     * @param isPrivate
     * 비밀 메시지 여부
     * @param otherDriverId
     * 수신자
     * @return
     * WebSocket 에서 처리할 데이터로 가공 된 JSON Object
     */
    public JSONObject sendInvite(String msg, boolean isPrivate, String otherDriverId) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("type", "publish");
            obj.put("address", "to.server.channel");
            JSONObject body = new JSONObject();
            body.put("type", "invite");
            body.put("isPrivateMsg", isPrivate);
            body.put("channel_id", mShuttleId);
            body.put("otherDriverId", otherDriverId + "");
            body.put("senderId", mMainActivity.mDriverInfo.getmDriverId() + "");
            body.put("senderNick", mNickName + "");
            body.put("msg", msg);
            obj.put("body", body);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return obj;
    }

    private void startRecording() {

	    try {
		    int bufferSize = AudioRecord.getMinBufferSize(RECORDER_SAMPLERATE, RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING);
		    recorder = new AudioRecord(MediaRecorder.AudioSource.MIC, RECORDER_SAMPLERATE, RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING, BufferElements2Rec * BytesPerElement);

            if (recorder.getState() != AudioRecord.STATE_INITIALIZED) {
	            throw new Exception("AudioRecord init failed");
            }

		    recorder.startRecording();
		    isRecording = true;
		    recordingThread = new Thread(new Runnable() {
			    public void run() {
				    sendAudioDataToServer();
			    }
		    }, "AudioRecorder Thread");

		    recordingThread.start();
	    } catch (Exception e) {
		    e.printStackTrace();
	    }
    }

    private void sendAudioDataToServer() {
	    try {
		    while (isRecording) {
			    Arrays.fill(sData, (short) 0);
			    wWokitokiStateCheck = WOKITOKI_RECODE;

			    recorder.read(sData, 0, BufferElements2Rec);

			    byte bData[] = short2byte(sData);

			    try {
				    sockJS.send(sendMsg(WebSocketUtil.toJS(Base64.encodeToString(bData, 0, BufferElements2Rec * BytesPerElement, Base64.URL_SAFE))));
			    } catch (WebsocketNotConnectedException e) {
				    e.printStackTrace();
			    }
		    }
	    } catch (Exception e) {
		    e.printStackTrace();
	    }
    }

    private byte[] short2byte(short[] sData) {
	    try {
		    Arrays.fill(bytes, (byte) 0);
		    int shortArrsize = sData.length;
		    for (int i = 0; i < shortArrsize; i++) {
			    bytes[i * 2] = (byte) (sData[i] & 0x00FF);
			    bytes[(i * 2) + 1] = (byte) (sData[i] >> 8);
			    sData[i] = 0;
		    }
	    } catch (Exception e) {
		    e.printStackTrace();
	    }

        return bytes;
    }

    private void stopRecording() {
	    try {
		    // stops the recording activity
		    if (null != recorder) {
			    isRecording = false;
			    recorder.stop();
			    recorder.release();
			    recorder = null;
			    recordingThread = null;
		    }
	    } catch (Exception e) {
		    e.printStackTrace();
	    }
    }

    private void resetWokitoki() {
	    try {
		    resetPlayer();
		    stopRecording();
		    resetWokitokiUI();
		    resetCheckCodes();
	    } catch (Exception e) {
		    e.printStackTrace();
	    }
    }

    private void resetCheckCodes() {
	    try {
		    wWokitokiStateCheck = WOKITOKI_STABLE;
		    wListenStarted = false;

		    if (wMicStarted) {
			    stopRecording();
			    try {
				    sockJS.send(sendMsg("stop"));
			    } catch (WebsocketNotConnectedException e) {
				    e.printStackTrace();
			    }
		    }

		    wMicStarted = false;
	    } catch (Exception e) {
		    e.printStackTrace();
	    }
    }

    /**
     * resetWokitikiUI
     * 워키토키 관련 UI를 초기화함
     */
    private void resetWokitokiUI() {
        try {
            mMainActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
	                    if(!mIsFinishFragment) {
		                    mMicButton.setImageDrawable(getResources().getDrawable(R.drawable.btn_mic));
		                    mMicButton.setClickable(true);
		                    mMicButton.setEnabled(true);
		                    mMicTextView.setVisibility(View.GONE);

		                    if(mGoogleMap != null && bounds != null) {
			                    mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 40));
		                    }
	                    }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * resetPlayer
     * AudioTrack 관련설정을 초기화함.
     */
    private void resetPlayer() {
	    try {
		    if (player != null) {
			    player.flush();
			    player.stop();
			    player.release();
		    }

		    player = new AudioTrack(AudioManager.STREAM_VOICE_CALL, 8000, RECORDER_CHANNELS_OUT, AudioFormat.ENCODING_PCM_16BIT, minBufferSize * 4, AudioTrack.MODE_STREAM);
	    } catch (Exception e) {
		    e.printStackTrace();
	    }
    }

    /**
     * playNotificationSound
     * 사람이 들어오면 음성파일을 재생함. 파일이 없을 경우 copyfile을 호출해 asset에서 가져온다.
     */
    public void playNotificationSound() {
	    try {
		    int sampleFreq = 44100;

		    File file = new File("/data/data/" + mMainActivity.getPackageName() + "/sound");
		    if (!file.exists()) {
//			    Log.e("aa", file.exists() + "");
			    file.mkdir();
			    copyfile();
		    }
		    file = new File("/data/data/" + mMainActivity.getPackageName() + "/sound/notifi.wav");

		    int shortSizeInBytes = Short.SIZE / Byte.SIZE;

		    int minBufferSize = AudioTrack.getMinBufferSize(sampleFreq, AudioFormat.CHANNEL_OUT_MONO,
				    AudioFormat.ENCODING_PCM_16BIT);

		    int bufferSizeInBytes = (int) (file.length() / shortSizeInBytes);

		    final AudioTrack at = new AudioTrack(AudioManager.STREAM_MUSIC, sampleFreq,
				    AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT, minBufferSize,
				    AudioTrack.MODE_STREAM);

		    int i = 0;
		    byte[] s = new byte[bufferSizeInBytes];

		    final FileInputStream fin = new FileInputStream("/data/data/" + mMainActivity.getPackageName() + "/sound/notifi.wav");
		    final DataInputStream dis = new DataInputStream(fin);

		    at.play();
		    while ((i = dis.read(s)) != -1) {
			    at.write(s, 0, i);
		    }
	    } catch (Exception e) {
		    e.printStackTrace();
	    }
    }

    /**
     *  copyfile
     *  notify를 위한 음성파일을 asset 에서 data 로 복사한다.
     */
    private void copyfile() {
	    InputStream is = null;
	    FileOutputStream fos = null;

	    try {
		    File outDir = new File("/data/data/" + mMainActivity.getPackageName() + "/sound");
		    outDir.mkdirs();

		    is = mMainActivity.getAssets().open("notifi.wav");
            int size = is.available();
            byte[] buffer = new byte[size];
            File outfile = new File(outDir + "/notifi.wav");
            fos = new FileOutputStream(outfile);

		    for (int c = is.read(buffer); c != -1; c = is.read(buffer)) {
                fos.write(buffer, 0, c);
            }

            is.close();
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
		    try {
			    if(is != null) {
				    is.close();
			    }

			    if(fos != null) {
				    fos.close();
			    }
		    } catch (Exception e) {
			    e.printStackTrace();
		    }
	    }
	}

	public void makeSecretPhoneCall(String _phone_number) {
		try {
			String uri = "tel:" + _phone_number;

			mIsSecretCalling = true;

			Intent intent = new Intent(Intent.ACTION_CALL);
			intent.setData(Uri.parse(uri));

			startActivity(intent);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private class PushAlertReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			try {
				String action = intent.getAction();
				long sendDate;
				String message;
				String senderNickName;
				double lat;
				double lng;

				switch (action) {
					case ShuttleChattingFragment.PUSH_MSG_1:
						sendDate = intent.getLongExtra("sendDate", 0);
						message = intent.getStringExtra("message");
						String strDrivers = intent.getStringExtra("strDrivers");

						someoneHasJoined(sendDate, message, strDrivers);
						break;
					case ShuttleChattingFragment.PUSH_MSG_2:
						sendDate = intent.getLongExtra("sendDate", 0);
						String driverId = intent.getStringExtra("driverId");
						String nickName = intent.getStringExtra("nickName");
						message = intent.getStringExtra("message");
						String messageType = intent.getStringExtra("messageType");
						lat = intent.getDoubleExtra("lat", 0);
						lng = intent.getDoubleExtra("lng", 0);

						someoneSendLocation(sendDate, driverId, nickName, message, messageType, lat, lng);
						break;
					case ShuttleChattingFragment.PUSH_MSG_3:
						String senderDriverId = intent.getStringExtra("senderDriverId");
						senderNickName = intent.getStringExtra("senderNickName");
						message = intent.getStringExtra("message");

						someoneSendReqLocation(senderDriverId, senderNickName, message);
						break;
					case ShuttleChattingFragment.PUSH_MSG_4:
						senderNickName = intent.getStringExtra("senderNickName");
						String result = intent.getStringExtra("result");
						lat = intent.getDoubleExtra("lat", 0);
						lng = intent.getDoubleExtra("lng", 0);

						someoneSendResLocation(senderNickName, result, lat, lng);
						break;
					case ShuttleChattingFragment.PUSH_MSG_5:
						SVUtil.hideProgressDialog();

						mVirtualNumber = intent.getStringExtra("virtualNumber");
						result = intent.getStringExtra("result");

						if(result != null && result.equals("00")) {
							makeSecretPhoneCall(mVirtualNumber);
						} else {
							SVUtil.showSimpleDialog(mMainActivity, "전화 연결에 실패했습니다.");
						}

						break;
					case ShuttleChattingFragment.PUSH_MSG_6:
						String call_state = intent.getStringExtra("call_state");

						SVUtil.log("error", TAG, "call_state : " + call_state);

						if(mIsSecretCalling && call_state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
							unregisterVirtualNumber();
						}
						break;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
