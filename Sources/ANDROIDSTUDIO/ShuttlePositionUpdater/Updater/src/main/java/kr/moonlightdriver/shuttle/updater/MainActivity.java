package kr.moonlightdriver.shuttle.updater;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
	private static final String TAG = "MainActivity";

	private static final String PROPERTY_SERVICE_STATUS = "SERVICE_STATUS";

	private Button mBtnStartStop;
	private ListView mUpdateResultListView;

	private String mServiceStatus;

	private ArrayList<String> mUpdateResultList;
	private ArrayAdapter<String> mUpdateResultAdapter;
	private ShuttlePositionReceiver mShuttlePositionReceiver;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		try {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.activity_main);

			mBtnStartStop = (Button) findViewById(R.id.btn_start_stop);
			mUpdateResultListView = (ListView) findViewById(R.id.update_result_list);

			mUpdateResultList = new ArrayList<>();
			mUpdateResultAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, mUpdateResultList);
			mUpdateResultListView.setAdapter(mUpdateResultAdapter);

			mBtnStartStop.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					if(mServiceStatus.equals("start")) {
						mServiceStatus = "stop";
						stopShuttlePositionService();
					} else {
						mServiceStatus = "start";
						startShuttlePositionService();
					}

					updateButtonText();
					setServiceStatus(mServiceStatus);
				}
			});

			mServiceStatus = getServiceStatus();

			updateButtonText();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void startShuttlePositionService() {
		try {
			stopShuttlePositionService();

			Intent serviceIntent = new Intent();
			serviceIntent.setClass(this, ShuttlePositionService.class);
			startService(serviceIntent);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void stopShuttlePositionService() {
		try {
			Intent serviceIntent = new Intent();
			serviceIntent.setClass(this, ShuttlePositionService.class);
			stopService(serviceIntent);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setServiceStatus(String _serviceStatus) {
		try {
			SharedPreferences prefs = getSharedPreferences(MainActivity.class.getSimpleName(), Context.MODE_PRIVATE);
			SharedPreferences.Editor editor = prefs.edit();
			editor.putString(PROPERTY_SERVICE_STATUS, _serviceStatus);
			editor.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String getServiceStatus() {
		String serviceStatus = "stop";

		try {
			SharedPreferences prefs = getSharedPreferences(MainActivity.class.getSimpleName(), Context.MODE_PRIVATE);
			serviceStatus = prefs.getString(PROPERTY_SERVICE_STATUS, "stop");
		} catch (Exception e) {
			e.printStackTrace();
		}

		return serviceStatus;
	}

	private void updateButtonText() {
		try {
			if(mServiceStatus.equals("start")) {
				mBtnStartStop.setText("중지");
			} else {
				mBtnStartStop.setText("시작");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private class ShuttlePositionReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			try {
				if(intent.getAction().equals(ShuttlePositionService.ACTION)) {
					String retStatus = intent.getStringExtra("status");
					int resultCount = intent.getIntExtra("resultCount", 0);

					if(retStatus.equals("Success")) {
						mUpdateResultList.add(resultCount + "건 성공(" + getUpdateDate() + ")");
					} else if(retStatus.equals("Failure")) {
						mUpdateResultList.add("실패(" + getUpdateDate() + ")");
					} else if(retStatus.equals("InvalidTIme")) {
						mUpdateResultList.add("운행 시간 아님(" + getUpdateDate() + ")");
					}

					if(mUpdateResultList.size() > 200) {
						mUpdateResultList.remove(0);
					}

					mUpdateResultAdapter.notifyDataSetChanged();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private String getUpdateDate() {
		String retDate = "";
		try {
			Calendar cal = Calendar.getInstance();

			String year = cal.get(Calendar.YEAR) + "-";
			String month = (cal.get(Calendar.MONTH) + 1 < 10 ? "0" + (cal.get(Calendar.MONTH) + 1) : "" + (cal.get(Calendar.MONTH) + 1)) + "-";
			String day = (cal.get(Calendar.DAY_OF_MONTH) < 10 ? "0" + cal.get(Calendar.DAY_OF_MONTH) : "" + cal.get(Calendar.DAY_OF_MONTH)) + " ";
			String hour = (cal.get(Calendar.HOUR_OF_DAY) < 10 ? "0" + cal.get(Calendar.HOUR_OF_DAY) : "" + cal.get(Calendar.HOUR_OF_DAY)) + ":";
			String minutes = (cal.get(Calendar.MINUTE) < 10 ? "0" + cal.get(Calendar.MINUTE) : "" + cal.get(Calendar.MINUTE)) + ":";
			String second = cal.get(Calendar.SECOND) < 10 ? "0" + cal.get(Calendar.SECOND) : "" + cal.get(Calendar.SECOND);

			retDate = year + month + day + hour + minutes + second;
		} catch(Exception e) {
			e.printStackTrace();
		}

		return retDate;
	}

	@Override
	protected void onStart() {
		try {
			super.onStart();

			mShuttlePositionReceiver = new ShuttlePositionReceiver();
			IntentFilter intentFilter = new IntentFilter();
			intentFilter.addAction(ShuttlePositionService.ACTION);

			registerReceiver(mShuttlePositionReceiver, intentFilter);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onStop() {
		try {
			super.onStop();

			unregisterReceiver(mShuttlePositionReceiver);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
